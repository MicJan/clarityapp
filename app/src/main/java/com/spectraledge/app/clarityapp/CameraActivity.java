package com.spectraledge.app.clarityapp;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class CameraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        Log.d(MainActivity.TAG, "CameraActivity onCreate");

        super.onCreate(savedInstanceState);

        // the fragment gets first created at this point (or restarted)
        setContentView(R.layout.activity_camera);

        final Uri outDirUri = getIntent().getParcelableExtra(MainActivity.EXTRA_OUTDIR_URI);

        if (outDirUri != null) {
            final CameraActivityFragment fragment = (CameraActivityFragment) getFragmentManager().findFragmentById(R.id.camera_fragment);
            if (fragment != null) {
                fragment.setDestDir(outDirUri);
            }
        }

    }

    @Override
    public void onResume() {
        Log.d(MainActivity.TAG, "CameraActivity onResume");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(MainActivity.TAG, "CameraActivity onPause");
        final CameraActivityFragment fragment = (CameraActivityFragment) getFragmentManager().findFragmentById(R.id.camera_fragment);
        if (fragment != null) {
            fragment.pauseProcessorThread();
        }
        Log.d(MainActivity.TAG, "CameraActivity done pausing");
        super.onPause();
    }

}
