package com.spectraledge.app.clarityapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class ImageActivity extends AppCompatActivity {

    static {
        System.loadLibrary("native-lib");
    }

    Uri __imageUri = null;
    Uri __outDirUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get the parameters passed in from the main activity
        final Intent intent = getIntent();
        __imageUri = intent.getParcelableExtra(MainActivity.EXTRA_IMAGE_URI);
        __outDirUri = intent.getParcelableExtra(MainActivity.EXTRA_OUTDIR_URI);

        // the fragment gets first created at this point
        setContentView(R.layout.activity_image);
    }

    public Uri getImageUri() {
        return __imageUri;
    }

    public Uri getOutDirUri() {
        return __outDirUri;
    }

    @Override
    public void onBackPressed() {
        final ImageActivityFragment fragment = (ImageActivityFragment) getFragmentManager().findFragmentById(R.id.image_fragment);
        boolean workingInBackground = false;
        if (fragment != null) {
            workingInBackground = fragment.doingWorkInBackground();
        }
        if (workingInBackground) {
            // still doing work in the background, ask for a confirmation
            new ConfirmationDialog().show(getFragmentManager(), "dialog");
        } else {
            // not doing any work anymore, finish as usual
            finish();
        }
    }

    public static class ConfirmationDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Activity activity = getActivity();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.finish_processing)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // user has decided to force stop
                            final ImageActivityFragment fragment = (ImageActivityFragment) getFragmentManager().findFragmentById(R.id.image_fragment);
                            if (fragment != null) {
                                fragment.stopOffscreenThread();
                            }
                            activity.finish();
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // user has decided to come back another time
                                }
                            })
                    .create();
        }
    }

}
