package com.spectraledge.app.clarityapp;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

public class MainActivityFragment extends Fragment {

    HashMap<Uri, Bitmap> __items = null;
    Uri[] __locations = null;
    ImageAdapter __imageAdapter = null;
    Uri __destDir = null;

    public static final int VIEW_EDGE_SIZE = 285;
    public static final int CROP_EDGE_SIZE = 8;

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        __items = new HashMap<>();
        __imageAdapter = new ImageAdapter(getActivity());

        // retain if orientation changed
        setRetainInstance(true);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    void setDestDir(Uri location) {
        __destDir = location;
    }

    Uri getDestDir() {
        return __destDir;
    }

    void addUri(Uri uri) {

        if (!__items.keySet().contains(uri)) {
            ParcelFileDescriptor fileDescriptor = null;
            try {
                // see if this is an asset or not
                if (uri.getScheme().equals("android.resource")) {
                    // asset path
                    AssetFileDescriptor assetFileDescriptor = getActivity().getContentResolver().openAssetFileDescriptor(uri, "r");
                    if (assetFileDescriptor != null) {
                        fileDescriptor = assetFileDescriptor.getParcelFileDescriptor();
                        if (fileDescriptor != null) {
                            Bitmap bitmap = BitmapUtil.decodeWithinBounds(fileDescriptor,
                                    VIEW_EDGE_SIZE, VIEW_EDGE_SIZE, assetFileDescriptor.getStartOffset());
                            if (bitmap != null) {
                                __items.put(uri, bitmap);
                                fileDescriptor.close();
                            }
                        }
                        assetFileDescriptor.close();
                    }
                } else {
                    // non-asset path
                    fileDescriptor = getActivity().getContentResolver().openFileDescriptor(uri, "r");
                    if (fileDescriptor != null) {
                        Bitmap bitmap = BitmapUtil.decodeWithinBounds(fileDescriptor, VIEW_EDGE_SIZE, VIEW_EDGE_SIZE);
                        if (bitmap != null) {
                            __items.put(uri, bitmap);
                            fileDescriptor.close();
                        }
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    void removeUri(Uri uri) {
        if (__items.keySet().contains(uri)) {
            __items.keySet().remove(uri);
        }
    }

    void clearUris() {
        __items.clear();
    }

    void updateUi() {
        Runnable addBitmapRunnable = new Runnable() {
            @Override
            public void run() {
                final GridView gridview = (GridView) getActivity().findViewById(R.id.gridview);
                gridview.invalidateViews();
            }
        };
        getActivity().runOnUiThread(addBitmapRunnable);
    }

    void updateItems() {
        if (__items != null) {
            __locations = new Uri[__items.keySet().size()];
            __items.keySet().toArray(__locations);
            // sort alphabetically
            Arrays.sort(__locations, new Comparator<Uri>() {
                @Override
                public int compare(Uri o1, Uri o2) {
                    String s1 = o1.getLastPathSegment();
                    String s2 = o2.getLastPathSegment();
                    return s1.compareTo(s2);
                }
            });
        }
    }

    void update() {
        updateItems();
        updateUi();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        GridView gridview = (GridView) view.findViewById(R.id.gridview);
        gridview.setAdapter(__imageAdapter);
        // Adjust to the device orientation
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        if ((display.getRotation() == Surface.ROTATION_0) || (display.getRotation() == Surface.ROTATION_180)) {
            // portrait
            gridview.setNumColumns(1);
        } else {
            // landscape
            gridview.setNumColumns(2);
        }
        __imageAdapter.setDisplayOrientation(display.getRotation());

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            // Callback method to be invoked when an item in this AdapterView has been clicked.
            // Implementers can call getItemAtPosition(position) if they need to access the data associated with the selected item.
            /*parent      AdapterView: The AdapterView where the click happened.
            view        View: The view within the AdapterView that was clicked
                        (this will be a view provided by the adapter)
            position    int: The position of the view in the adapter.
            id          long: The row id of the item that was clicked.*/
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                if (__destDir != null) {

                    if (position < __locations.length) {
                        Uri location = __locations[position];
                        // don't update the resource uris, they are not part of the most recent few scheme
                        if (!location.getScheme().equals("android.resource")) {
                            MainActivity activity = (MainActivity) getActivity();
                            if (activity != null) {
                                activity.updateKeyForFilepath(location.toString());
                            }
                        }
                        final Intent i = new Intent(getActivity(), ImageActivity.class);
                        i.putExtra(MainActivity.EXTRA_IMAGE_URI, location);
                        i.putExtra(MainActivity.EXTRA_OUTDIR_URI, __destDir);
                        startActivity(i);
                    }

                } else {
                    Toast.makeText(getActivity(), "Please set output directory first!",
                            Toast.LENGTH_SHORT).show();
                    MainActivity activity = (MainActivity) getActivity();
                    if (activity != null) {
                        activity.pickOutputDirectory();
                    }
                }

            }
        });
        return view;
    }

    class ImageAdapter extends BaseAdapter {
        private Context __context;
        private int __displayOrientation;

        public ImageAdapter(Context c) {
            __context = c;
            __displayOrientation = Surface.ROTATION_0;
        }

        @Override
        public int getCount() {
            // How many items are in the data set represented by this Adapter.
            return __items.keySet().size();
        }

        @Override
        public Object getItem(int position) {
            // Get the data item associated with the specified position in the data set.
            return null;
        }

        @Override
        public long getItemId(int position) {
            // Get the row id associated with the specified position in the list.
            // Does this need to be in sync with the setNumColumns call on the gridView?
            if ((__displayOrientation == Surface.ROTATION_0) || (__displayOrientation == Surface.ROTATION_180)) {
                // portrait, one column
                return position;
            } else {
                // landscape, two columns
                return position / 2;
            }
        }

        @Override
        public int getViewTypeCount() {
            // Returns the number of types of Views that will be created by getView(int, View, ViewGroup).
            // Each type represents a set of views that can be converted in getView(int, View, ViewGroup).
            // If the adapter always returns the same type of View for all items, this method should return 1.
            return 1;
        }

        @Override
        public int getItemViewType(int position) {
            // Get the type of View that will be created by getView(int, View, ViewGroup) for the specified item.
            return 1;
        }

        @Override
        public boolean hasStableIds() {
            // Indicates whether the item ids are stable across changes to the underlying data.
            return true;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            // Get a View that displays the data at the specified position in the data set.
            // You can either create a View manually or inflate it from an XML layout file.
            /*position        int: The position of the item within the adapter's
                            data set of the item whose view we want.
            convertView     View: The old view to reuse, if possible. Note: You should check that
                            this view is non-null and of an appropriate type before using.
                            If it is not possible to convert this view to display the correct data,
                            this method can create a new view. Heterogeneous lists can specify their number of view types,
                            so that this View is always of the right type (see getViewTypeCount() and getItemViewType(int)).
            parent          ViewGroup: The parent that this view will eventually be attached to*/

            if (position < __locations.length) {

                Uri location = __locations[position];
                Bitmap bitmap = __items.get(location);

                if (convertView == null) {

                    ImageView imageView = null;
                    imageView = new ImageView(__context);
                    LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(VIEW_EDGE_SIZE, VIEW_EDGE_SIZE);
                    params1.gravity = Gravity.LEFT;
                    imageView.setLayoutParams(params1);
                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    imageView.setPadding(CROP_EDGE_SIZE, CROP_EDGE_SIZE, CROP_EDGE_SIZE, CROP_EDGE_SIZE);
                    imageView.setImageBitmap(bitmap);

                    TextView textView = null;
                    textView = new TextView(__context);
                    LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, VIEW_EDGE_SIZE);
                    params2.gravity = Gravity.RIGHT;
                    textView.setLayoutParams(params2);
                    textView.setPadding(CROP_EDGE_SIZE, 0, CROP_EDGE_SIZE, 0);
                    textView.setText(location.getLastPathSegment());

                    LinearLayout layout = new LinearLayout(__context);
                    layout.addView(imageView);
                    layout.addView(textView);

                    return layout;

                } else {

                    LinearLayout layout = (LinearLayout) convertView;

                    ImageView imageView = (ImageView) layout.getChildAt(0);
                    imageView.setImageBitmap(bitmap);

                    TextView textView = (TextView) layout.getChildAt(1);
                    textView.setText(location.getLastPathSegment());

                    return layout;
                }

            } else {
                return null;
            }

        }

        public void setDisplayOrientation(int orientation) {
            __displayOrientation = orientation;
        }

    }

}
