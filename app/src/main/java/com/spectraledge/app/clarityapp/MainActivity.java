package com.spectraledge.app.clarityapp;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.UriPermission;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.provider.DocumentFile;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    // permissions result codes
    public static final int CODE_PERMISSIONS = 1;
    // intent result codes
    public static final int CODE_PICK_IMAGES = 1;
    public static final int CODE_PICK_OUT_DIR = 2;

    public static final String TAG = "ClarityApp";

    public static final String EXTRA_IMAGE_URI = "extra_image_uri";
    public static final String EXTRA_OUTDIR_URI = "extra_outdir_uri";

    public static final String[] PERMISSION_NAMES = {Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};

    public static final String MESSAGE_SET_OUTDIR = "Please select an output directory...";
    public static final String MESSAGE_LOAD_PIC = "Please pick an image (or images) to load...";

    public static final int MAX_RECENT_COUNT = 3;

    Context __appContext = null;
    ContentResolver __appContentResolver = null;
    SharedPreferences __sharedPrefs = null;
    // loaded from shared preferences
    String __outDirFromPrefs = null;
    Uri __outDirUriFromPrefs = null;
    DocumentFile __outDirDocFromPrefs = null;
    Set<String> __recentFilesFromPrefs = null;
    HashMap<String, Uri> __recentFilesUriFromPrefs = null;
    HashMap<String, DocumentFile> __recentFilesDocFromPrefs = null;
    HashMap<String, String> __recentFilesSeqNumFromPrefs = null;
    // loaded from content resolver
    List<UriPermission> __persistedUriPerms = null;
    ArrayList<Uri> __persistedUris = null;

    MainActivityFragment __fragment = null;

    static {
        System.loadLibrary("native-lib");
    }

    private Runnable __takePicRunnable = new Runnable() {
        @Override
        public void run() {
            final Activity activity = MainActivity.this;
            Uri destDir = getOutDirFromFragment();
            if (destDir != null) {
                final Intent i = new Intent(activity, CameraActivity.class);
                i.putExtra(EXTRA_OUTDIR_URI, destDir);
                startActivity(i);
            }
        }
    };

    private Runnable __loadPicRunnable = new Runnable() {
        @Override
        public void run() {
            Uri destDir = getOutDirFromFragment();
            if (destDir != null) {
                showToast(MESSAGE_LOAD_PIC);
                Intent intent = new Intent();
                if (intent != null) {
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                    intent.putExtra("android.content.extra.SHOW_ADVANCED", true);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    if (intent.resolveActivityInfo(getPackageManager(), 0) != null) {
                        startActivityForResult(intent, CODE_PICK_IMAGES);
                    }
                }
            }
        }
    };

    private Runnable __loadOutDirFromPrefs = new Runnable() {
        @Override
        public void run() {
            loadRecentsFromSharedPrefs();
            validateRecents();
            passRecentsToFragment();
        }
    };

    private Runnable __followUpRunnable = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.nav_camera) {
                    __followUpRunnable = __takePicRunnable;
                    if (permissionsGranted()) {
                        if (getOutDirFromFragment() != null) {
                            runOnUiThread(__takePicRunnable);
                        } else {
                            showToast(MESSAGE_SET_OUTDIR);
                            pickOutputDirectory();
                        }
                    } else {
                        new ConfirmationDialog().show(getFragmentManager(), "dialog");
                    }
                } else if (id == R.id.nav_load_picture) {
                    __followUpRunnable = __loadPicRunnable;
                    if (permissionsGranted()) {
                        if (getOutDirFromFragment() != null) {
                            runOnUiThread(__loadPicRunnable);
                        } else {
                            showToast(MESSAGE_SET_OUTDIR);
                            pickOutputDirectory();
                        }
                    } else {
                        new ConfirmationDialog().show(getFragmentManager(), "dialog");
                    }
                }
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });

        __fragment = (MainActivityFragment) getFragmentManager().findFragmentById(R.id.main_fragment);

        if (!permissionsGranted()) {
            __followUpRunnable = __loadOutDirFromPrefs;
            ActivityCompat.requestPermissions(this, PERMISSION_NAMES, CODE_PERMISSIONS);
        } else {
            runOnUiThread(__loadOutDirFromPrefs);
        }
    }

    protected void loadRecentsFromSharedPrefs() {

        __appContext = getApplicationContext();
        __appContentResolver = getContentResolver();
        __sharedPrefs = getPreferences(Context.MODE_PRIVATE);
        __recentFilesFromPrefs = new HashSet<>();
        __recentFilesUriFromPrefs = new HashMap<>();
        __recentFilesDocFromPrefs = new HashMap<>();
        __recentFilesSeqNumFromPrefs = new HashMap<>();
        __persistedUris = new ArrayList<>();

        // first load the output directory
        if ((__appContext != null) && (__sharedPrefs != null) && (__recentFilesFromPrefs != null) &&
                (__recentFilesUriFromPrefs != null)) {

            __outDirFromPrefs = __sharedPrefs.getString(getString(R.string.output_directory), null);

            if (__outDirFromPrefs != null) {
                __outDirUriFromPrefs = Uri.parse(__outDirFromPrefs);
                // need to use fromTreeUri because __outDirUriFromPrefs is a directory
                __outDirDocFromPrefs = DocumentFile.fromTreeUri(__appContext, __outDirUriFromPrefs);
            }

        }

        // second load the recent file list
        if ((__appContext != null) && (__sharedPrefs != null) && (__recentFilesFromPrefs != null) &&
                (__recentFilesUriFromPrefs != null) && (__recentFilesDocFromPrefs != null) &&
                (__recentFilesSeqNumFromPrefs != null)) {

            Set<String> recentFilesFromPrefs = __sharedPrefs.getStringSet(getString(R.string.recent_files), null);

            if (recentFilesFromPrefs != null) {

                for (String key : recentFilesFromPrefs) {

                    String filePath = null;
                    Uri fileUri = null;
                    DocumentFile fileDoc = null;

                    filePath = filePathFromKey(key);
                    if (filePath != null) {
                        fileUri = Uri.parse(filePath);
                    }
                    if (fileUri != null) {
                        fileDoc = DocumentFile.fromSingleUri(__appContext, fileUri);
                    }

                    if ((filePath != null) && (fileUri != null) && (fileDoc != null)) {
                        __recentFilesFromPrefs.add(filePath);
                        __recentFilesUriFromPrefs.put(filePath, fileUri);
                        __recentFilesDocFromPrefs.put(filePath, fileDoc);
                        __recentFilesSeqNumFromPrefs.put(filePath, key);
                    }

                }

            }

        }

        if ((__appContentResolver != null) && (__persistedUris != null)) {

            __persistedUriPerms = __appContentResolver.getPersistedUriPermissions();

            if (__persistedUriPerms != null) {
                for (UriPermission permission : __persistedUriPerms) {
                    __persistedUris.add(permission.getUri());
                }
            }

        }

    }

    protected void validateRecents() {

        // validate the recent file list first

        if ((__persistedUris != null) && (__recentFilesFromPrefs != null) && (__recentFilesUriFromPrefs != null)
                && (__recentFilesDocFromPrefs != null) && (__appContentResolver != null)) {

            Set<String> notValidOnes = new HashSet<>();

            for (String filePath : __recentFilesFromPrefs) {

                boolean recentFileValid = true;

                Uri recentUri = __recentFilesUriFromPrefs.get(filePath);
                DocumentFile recentDoc = __recentFilesDocFromPrefs.get(filePath);
                boolean isCameraFile = false;

                // the out dir may be invalid but shouldn't be null
                // (can't load files unless there is an output directory)
                if ((__outDirDocFromPrefs != null) && (__outDirFromPrefs != null)) {
                    // if the out dir is not null at this point, it's valid
                    isCameraFile = filePath.startsWith(__outDirFromPrefs);
                }

                if (isCameraFile) {
                    recentFileValid = (recentDoc.exists() && recentDoc.isFile());
                } else {
                    recentFileValid = (__persistedUris.contains(recentUri) && recentDoc.exists()
                            && recentDoc.isFile());
                }

                if (recentFileValid) {
                    if (!isCameraFile) {
                        // the developer docs recommend to refresh the permission
                        takeUriPermission(__appContentResolver, recentUri);
                    }
                } else {
                    // no longer valid - remove
                    notValidOnes.add(filePath);
                }

            }

            for (String filePath : notValidOnes) {

                Uri purgedUri = __recentFilesUriFromPrefs.get(filePath);
                __fragment.removeUri(purgedUri);

                boolean isCameraFile = false;

                // the out dir may be invalid but shouldn't be null
                // (can't load files unless there is an output directory)
                if ((__outDirDocFromPrefs != null) && (__outDirFromPrefs != null)) {
                    // if the out dir is not null at this point, it's valid
                    isCameraFile = filePath.startsWith(__outDirFromPrefs);
                }

                // if it's not a camera file, release the URI permissions
                if (!isCameraFile) {
                    releaseUriPermission(__appContentResolver, purgedUri);
                }

                __recentFilesFromPrefs.remove(filePath);
                __recentFilesUriFromPrefs.remove(filePath);
                __recentFilesDocFromPrefs.remove(filePath);
                __recentFilesSeqNumFromPrefs.remove(filePath);

            }

            if (notValidOnes.size() > 0) {
                __fragment.update();
            }

        }

        // validate the output directory second
        boolean outDirValid = true;

        if ((__persistedUris != null) && (__outDirFromPrefs != null) && (__outDirDocFromPrefs != null)
                && (__outDirUriFromPrefs != null) && (__appContentResolver != null)) {

            if (__persistedUris.contains(__outDirUriFromPrefs)) {
                if (!__outDirDocFromPrefs.isDirectory() || !__outDirDocFromPrefs.exists()) {
                    // no longer valid
                    outDirValid = false;
                }
            } else {
                // it's no longer on the persisted uris list
                outDirValid = false;
            }

            if (outDirValid) {
                // the developer docs recommend to refresh the permission
                takeUriPermission(__appContentResolver, __outDirUriFromPrefs);
            } else {
                // no longer valid - reset
                __outDirFromPrefs = null;
                __outDirDocFromPrefs = null;
                __outDirUriFromPrefs = null;
            }

        }

    }

    protected Set<String> getObsoleteFilePaths() {

        HashSet<String> purgedFilePaths = new HashSet<>();

        if ((__recentFilesFromPrefs != null) && (__recentFilesDocFromPrefs != null)
                && (__recentFilesUriFromPrefs != null)) {

            String[] recentFilePaths = new String[__recentFilesFromPrefs.size()];
            __recentFilesFromPrefs.toArray(recentFilePaths);

            // sort the recent files array by last access time
            Arrays.sort(recentFilePaths, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return (int) (seqNumFromKey(__recentFilesSeqNumFromPrefs.get(o2)) -
                            seqNumFromKey(__recentFilesSeqNumFromPrefs.get(o1)));
                }
            });

            int startPos = Math.min(recentFilePaths.length, MAX_RECENT_COUNT);

            for (int i = startPos; i < recentFilePaths.length; i++) {
                purgedFilePaths.add(recentFilePaths[i]);
            }

        }

        return purgedFilePaths;

    }

    protected void saveRecentsToSharedPrefs() {

        if ((__sharedPrefs != null) && (__appContentResolver != null)) {

            // get an editor handle
            SharedPreferences.Editor editor = __sharedPrefs.edit();

            if (__outDirFromPrefs != null) {
                editor.putString(getString(R.string.output_directory), __outDirFromPrefs);
            }

            if (__recentFilesSeqNumFromPrefs != null) {
                // due to a bug in the Android source code
                // https://issuetracker.google.com/issues/36943216
                // a copy of the set needs to be made - otherwise modifications won't register
                HashSet<String> recentFileKeys = new HashSet<>(__recentFilesSeqNumFromPrefs.values());
                if (recentFileKeys != null) {
                    editor.putStringSet(getString(R.string.recent_files), recentFileKeys);
                }
            }

            // finally, save all changes
            editor.commit();

        }

    }

    protected void passRecentsToFragment() {

        if (__fragment != null) {

            if (__outDirUriFromPrefs != null) {
                __fragment.setDestDir(__outDirUriFromPrefs);
            }

            if (__recentFilesUriFromPrefs != null) {
                for (Uri uri : __recentFilesUriFromPrefs.values()) {
                    __fragment.addUri(uri);
                }
            }

            Uri sample1Uri = Uri.parse("android.resource://com.spectraledge.app.clarityapp/raw/sample1");
            Uri sample2Uri = Uri.parse("android.resource://com.spectraledge.app.clarityapp/raw/sample2");

            if (sample1Uri != null) {
                __fragment.addUri(sample1Uri);
            }

            if (sample2Uri != null) {
                __fragment.addUri(sample2Uri);
            }

            __fragment.update();

        }

    }

    protected Uri getOutDirFromFragment() {

        Uri destDir = null;

        if (__fragment != null) {
            destDir = __fragment.getDestDir();
        }

        return destDir;

    }

    protected void processNewFileUri(Uri fileUri, Intent data) {

        if ((__appContext != null) && (__outDirDocFromPrefs != null)
                && (__appContentResolver != null) && (fileUri != null)
                && (__outDirFromPrefs != null)) {

            String filePath = fileUri.toString();
            DocumentFile fileDoc = DocumentFile.fromSingleUri(__appContext, fileUri);

            if ((filePath != null) && (fileDoc != null)) {

                // if the out dir is not null at this point, it's valid
                boolean isCameraFile = filePath.startsWith(__outDirFromPrefs);

                if (!isCameraFile) {
                    // not expected to have an intent granting the permission
                    takeUriPermission(__appContentResolver, fileUri);
                } else if (data != null) {
                    // file is expected not to have been snapped with a camera
                    takeUriPermission(__appContentResolver, fileUri, data);
                }

                // it's OK if the filePath is already in the data structures,
                // this will update its key with a new timestamp
                String key = keyForFilePath(filePath, nextSeqNum());

                if (key != null) {

                    // add the new file to the data structures
                    __recentFilesFromPrefs.add(filePath);
                    __recentFilesUriFromPrefs.put(filePath, fileUri);
                    __recentFilesDocFromPrefs.put(filePath, fileDoc);
                    __recentFilesSeqNumFromPrefs.put(filePath, key);

                    // pass it onto the fragment
                    if (__fragment != null) {
                        __fragment.addUri(fileUri);
                    }
                    
                }

                Set<String> purgedFilePaths = getObsoleteFilePaths();

                // update the data structures
                for (String purgedPath : purgedFilePaths) {

                    Uri purgedUri = __recentFilesUriFromPrefs.get(purgedPath);
                    __fragment.removeUri(purgedUri);

                    // if it's not a camera file, release the URI permissions
                    if (!purgedPath.startsWith(__outDirFromPrefs)) {
                        releaseUriPermission(__appContentResolver, purgedUri);
                    }

                    __recentFilesFromPrefs.remove(purgedPath);
                    __recentFilesUriFromPrefs.remove(purgedPath);
                    __recentFilesDocFromPrefs.remove(purgedPath);
                    __recentFilesSeqNumFromPrefs.remove(purgedPath);

                }

                __fragment.update();

            }

        }

    }

    protected void processNewOutDirUri(Uri outDirUri, Intent data) {

        if ((outDirUri != null) && (data != null) && (__appContext != null)) {

            takeUriPermission(__appContentResolver, outDirUri, data);

            // update the data structures
            __outDirUriFromPrefs = outDirUri;
            __outDirFromPrefs = outDirUri.toString();
            // need to use fromTreeUri because __outDirUriFromPrefs is a directory
            __outDirDocFromPrefs = DocumentFile.fromTreeUri(__appContext, __outDirUriFromPrefs);

            // finally, pass it onto the fragment
            if (__fragment != null) {
                __fragment.setDestDir(__outDirUriFromPrefs);
            }

        }

    }

    protected void releaseUriPermission(ContentResolver resolver, Uri uri) {
        int flags;
        flags = Intent.FLAG_GRANT_READ_URI_PERMISSION;
        flags = flags | Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
        resolver.releasePersistableUriPermission(uri, flags);
    }

    protected void takeUriPermission(ContentResolver resolver, Uri uri) {
        int flags;
        flags = Intent.FLAG_GRANT_READ_URI_PERMISSION;
        flags = flags | Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
        resolver.takePersistableUriPermission(uri, flags);
    }

    protected void takeUriPermission(ContentResolver resolver, Uri uri, Intent intent) {
        int flags = intent.getFlags();
        if ((flags & Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION) > 0) {
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION;
            flags = flags | Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
            resolver.takePersistableUriPermission(uri, flags);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Uri imageUri = intent.getParcelableExtra(EXTRA_IMAGE_URI);
        if (imageUri != null) {
            Log.d(TAG, "Got an extra URI: " + imageUri);
            processNewFileUri(imageUri, null);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onDestroy() {
        saveRecentsToSharedPrefs();
        super.onDestroy();
    }

    public void pickOutputDirectory() {
        if (permissionsGranted()) {
            Intent intent = new Intent();
            if (intent != null) {
                intent.setAction(Intent.ACTION_OPEN_DOCUMENT_TREE);
                intent.putExtra("android.content.extra.SHOW_ADVANCED", true);
                if (intent.resolveActivityInfo(getPackageManager(), 0) != null) {
                    startActivityForResult(intent, CODE_PICK_OUT_DIR);
                }
            }
        } else {
            new ConfirmationDialog().show(getFragmentManager(), "dialog");
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        boolean permsGranted = true;

        if (requestCode == CODE_PERMISSIONS) {

            for (int i = 0; i < grantResults.length; i++) {
                permsGranted = permsGranted && (grantResults[i] == PackageManager.PERMISSION_GRANTED);
            }

            if (permsGranted) {
                showToast("Permissions OK");
                if (__followUpRunnable != null) {
                    runOnUiThread(__followUpRunnable);
                }
            } else {
                showToast("Permissions not granted!");
            }

        }

    }

    protected void showToast(CharSequence text) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(this, text, duration);
        toast.setGravity(Gravity.BOTTOM, toast.getXOffset(), toast.getYOffset());
        toast.show();
    }

    protected boolean permissionsGranted() {
        boolean permsGranted = true;
        for (String name : PERMISSION_NAMES) {
            permsGranted = permsGranted && (ContextCompat.checkSelfPermission(this,
                    name) == PackageManager.PERMISSION_GRANTED);
        }
        return permsGranted;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == CODE_PICK_IMAGES) {
                ClipData clipData = data.getClipData();
                if (clipData != null) {
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        Uri location = item.getUri();
                        if (location != null) {
                            processNewFileUri(location, data);
                        }
                    }
                } else {
                    Uri location = data.getData();
                    if (location != null) {
                        processNewFileUri(location, data);
                    }
                }
            } else if (requestCode == CODE_PICK_OUT_DIR) {
                Uri location = data.getData();
                if (location != null) {
                    processNewOutDirUri(location, data);
                    if (__followUpRunnable != null) {
                        runOnUiThread(__followUpRunnable);
                    }
                }
            }
        }

    }

    protected long nextSeqNum() {
        return System.currentTimeMillis();
    }

    protected String keyForFilePath(String filePath, long seqNum) {
        return String.format("%s;%d", filePath, seqNum);
    }

    protected String filePathFromKey(String key) {
        String filePath = null;
        int splitPos = key.indexOf(";");
        if (splitPos > -1) {
            filePath = key.substring(0, splitPos);
        }
        return filePath;
    }

    protected long seqNumFromKey(String key) {
        long seqNum = 0;
        int splitPos = key.indexOf(";");
        if (splitPos > -1) {
            seqNum = Long.parseLong(key.substring(splitPos + 1));
        }
        return seqNum;
    }

    void updateKeyForFilepath(String filePath) {

        if (__recentFilesSeqNumFromPrefs != null) {
            if (__recentFilesSeqNumFromPrefs.containsKey(filePath)) {
                String key = keyForFilePath(filePath, nextSeqNum());
                __recentFilesSeqNumFromPrefs.put(filePath, key);
            }
        }

    }

    public static class ConfirmationDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Activity activity = getActivity();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.request_permission)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (activity != null) {
                                ActivityCompat.requestPermissions(activity,
                                        PERMISSION_NAMES,
                                        CODE_PERMISSIONS);
                            }
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (activity != null) {
                                        activity.finish();
                                    }
                                }
                            })
                    .create();
        }
    }

}
