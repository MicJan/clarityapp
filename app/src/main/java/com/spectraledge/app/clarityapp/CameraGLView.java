package com.spectraledge.app.clarityapp;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class CameraGLView extends GLSurfaceView {

    CameraGLRenderer __renderer;
    ViewListener __listener;

    public CameraGLView(Context context) {
        this(context, null);
    }

    public CameraGLView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
        // setPaused(Long.MAX_VALUE, true);
    }

    private void init() {
        Log.d(MainActivity.TAG, "CameraGLView init");
        setEGLContextClientVersion(2);
        // the fragment will be preserved on screen orientation change
        // but the view will be recreated
        // setPreserveEGLContextOnPause(true);
        __renderer = new CameraGLRenderer();
        setRenderer(__renderer);
    }

    public SurfaceTexture getSurfaceTexture() {
        return __renderer.getSurfaceTexture();
    }

    public void setViewListener(ViewListener viewListener) {
        __listener = viewListener;
        __renderer.setViewListener(__listener);
    }

    public void setGeometry(int rotation, int bufferWidth, int bufferHeight,
                            int viewportWidth, int viewportHeight,
                            int displayWidth, int displayHeight, boolean mirror) {
        __renderer.setGeometry(rotation, bufferWidth, bufferHeight, viewportWidth, viewportHeight,
                displayWidth, displayHeight, mirror);
    }

    public void setPaused(long timestamp, boolean paused) {
        if (__renderer != null) {
            __renderer.setPaused(timestamp, paused);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        Log.d(MainActivity.TAG, "onDetachedFromWindow");
        super.onDetachedFromWindow();
    }

    @Override
    protected void onAttachedToWindow() {
        Log.d(MainActivity.TAG, "onAttachedToWindow");
        super.onAttachedToWindow();
    }

    @Override
    public void onWindowVisibilityChanged(int visibility) {
        Log.d(MainActivity.TAG, "onWindowVisibilityChanged");
        if (visibility == View.VISIBLE) {
            if (__renderer != null) {
                __renderer.resetSignalledReady();
            }
        }
        super.onWindowVisibilityChanged(visibility);
    }

    @Override
    public void onPause() {
        Log.d(MainActivity.TAG, "CameraGLView onPause");
        super.onPause();
    }

    @Override
    public void onResume() {
        Log.d(MainActivity.TAG, "CameraGLView onResume");
        super.onResume();
        if (__renderer != null) {
            __renderer.resetSignalledReady();
        }
    }

    interface ViewListener {
        void onRendererCreated();
    }

}

class CameraGLRenderer implements GLSurfaceView.Renderer {

    // counterclockwise winding
    private final float[] VERTEX_COORDS = {
            -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f
    };
    // flipped vertically for external textures
    private final float[] TEX_COORDS_0 = {
            0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
            1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f
    };
    private final float[] TEX_COORDS_90 = {
            1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
            1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f
    };
    private final float[] TEX_COORDS_180 = {
            1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
            0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f
    };
    private final float[] TEX_COORDS_270 = {
            0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f
    };
    private final float[] TEX_COORDS_0_MIRROR = {
            1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f
    };
    private final float[] TEX_COORDS_90_MIRROR = {
            1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f,
            1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f
    };
    private final float[] TEX_COORDS_180_MIRROR = {
            0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
            1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f
    };
    private final float[] TEX_COORDS_270_MIRROR = {
            0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
            0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f
    };
    private static final int FLOAT_SIZE_BYTES = 4;
    private static final int GL_TEXTURE_EXTERNAL_OES = 0x8D65;

    private FloatBuffer __vertBuffer;
    private FloatBuffer __coordBuffer;

    private int __program;
    private int __texture;
    private int __positionAttr;
    private int __coordsAttr;
    private SurfaceTexture __surfaceTexture;

    private int __viewportX;
    private int __viewportY;
    private int __viewportWidth;
    private int __viewportHeight;
    private boolean __signalledReady;
    private long __timestamp = 0;
    private DrawState __state = DrawState.PASSTHROUGH;
    private Object __sync = new Object();

    enum DrawState {
        PASSTHROUGH,
        FILTER,
        PAUSE
    }

    CameraGLView.ViewListener __listener;

    private final String VERTEX_SHADER =
            "attribute vec4 aPosition;\n" +
                    "attribute vec4 aTextureCoord;\n" +
                    "varying vec2 vTextureCoord;\n" +
                    "void main() {\n" +
                    "  gl_Position = aPosition;\n" +
                    "  vTextureCoord = aTextureCoord.xy;\n" +
                    "}\n";

    private final String FRAGMENT_SHADER =
            "#extension GL_OES_EGL_image_external : require\n" +
                    "precision highp float;\n" +
                    "varying vec2 vTextureCoord;\n" +
                    /*"uniform sampler2D sTexture;\n" +*/
                    "uniform samplerExternalOES sTexture;\n" +
                    "void main() {\n" +
                    "  gl_FragColor = texture2D(sTexture, vTextureCoord);\n" +
                    "}\n";

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

        Log.d(MainActivity.TAG, "surface created");

        __vertBuffer = ByteBuffer.allocateDirect(VERTEX_COORDS.length
                * FLOAT_SIZE_BYTES).order(ByteOrder.nativeOrder()).asFloatBuffer();
        __vertBuffer.put(VERTEX_COORDS).position(0);
        __coordBuffer = ByteBuffer.allocateDirect(TEX_COORDS_0.length
                * FLOAT_SIZE_BYTES).order(ByteOrder.nativeOrder()).asFloatBuffer();
        __coordBuffer.put(TEX_COORDS_0).position(0);

        __program = createProgram(VERTEX_SHADER, FRAGMENT_SHADER);
        if (__program == 0) {
            return;
        }
        __positionAttr = GLES20.glGetAttribLocation(__program, "aPosition");
        checkGlError("glGetAttribLocation aPosition");
        if (__positionAttr == -1) {
            throw new RuntimeException("Could not get attrib location for aPosition");
        }
        __coordsAttr = GLES20.glGetAttribLocation(__program, "aTextureCoord");
        checkGlError("glGetAttribLocation aTextureCoord");
        if (__coordsAttr == -1) {
            throw new RuntimeException("Could not get attrib location for aTextureCoord");
        }

        int[] textures = new int[1];
        GLES20.glGenTextures(1, textures, 0);

        __texture = textures[0];
        GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, __texture);
        checkGlError("glBindTexture");

        // Can't do mipmapping with camera source
        GLES20.glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MIN_FILTER,
                GLES20.GL_NEAREST);
        GLES20.glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MAG_FILTER,
                GLES20.GL_LINEAR);
        // Clamp to edge is the only option
        GLES20.glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_S,
                GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_T,
                GLES20.GL_CLAMP_TO_EDGE);
        checkGlError("glTexParameteri");

        GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, 0);

        __surfaceTexture = new SurfaceTexture(__texture);

        __signalledReady = false;

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        Log.d(MainActivity.TAG, "Surface changed: " + width + "x" + height);
        // moved here because GL view may get detached while the renderer is being constructed
        if (!__signalledReady) {
            if (__listener != null) {
                __listener.onRendererCreated();
                __signalledReady = true;
            }
        }
    }

    @Override
    public void onDrawFrame(GL10 gl) {

        // how is this paused if onPause is not called?

        boolean shouldDraw = false;
        long timestamp = 0;
        DrawState state = DrawState.PASSTHROUGH;

        synchronized (this) {
            state = __state;
            timestamp = __timestamp;
        }

        if (state != DrawState.PASSTHROUGH) {
            synchronized (__sync) {
                __sync.notify();
            }
        }

        __surfaceTexture.updateTexImage();

        if (state == DrawState.PASSTHROUGH) {
            shouldDraw = true;
        } else if (state == DrawState.FILTER) {
            Log.d(MainActivity.TAG, "Filter state, surface timestamp: " + __surfaceTexture.getTimestamp() +
                    " timestamp: " + timestamp);
            // wait until you get the first buffer from the preview
            // NOTE: each camera may have its own timestamp source,
            // and one source may produce timestamps always greater than those
            // from another source!
            // At the same time, we may get a timestamp from the 'right' source
            // but a frame or two after the first preview frame!
            // assume the slowest camera preview is 5fps
            shouldDraw = (__surfaceTexture.getTimestamp() >= timestamp) &&
                    !((__surfaceTexture.getTimestamp() - timestamp) > 0.2 * 1e9);
            if (shouldDraw) {
                synchronized (this) {
                    __state = DrawState.PASSTHROUGH;
                }
            } /*else {
                Log.d(MainActivity.TAG, "Rejecting " + __surfaceTexture.getTimestamp() + " against " + timestamp);
            }*/
        } else {
            // Log.d(MainActivity.TAG, "Not drawing");
            shouldDraw = false;
        }

        if (shouldDraw) {
            GLES20.glViewport(__viewportX, __viewportY, __viewportWidth, __viewportHeight);

            GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
            GLES20.glUseProgram(__program);
            checkGlError("glUseProgram");

            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, __texture);

            __vertBuffer.position(0);
            GLES20.glVertexAttribPointer(__positionAttr, 2, GLES20.GL_FLOAT, false,
                    0, __vertBuffer);
            checkGlError("glVertexAttribPointer");
            GLES20.glEnableVertexAttribArray(__positionAttr);
            checkGlError("glEnableVertexAttribArray");

            __coordBuffer.position(0);
            GLES20.glVertexAttribPointer(__coordsAttr, 2, GLES20.GL_FLOAT, false,
                    0, __coordBuffer);
            checkGlError("glVertexAttribPointer");
            GLES20.glEnableVertexAttribArray(__coordsAttr);
            checkGlError("glEnableVertexAttribArray");

            GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 6);
            checkGlError("glDrawArrays");

        } else {
            GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
        }

    }

    private int loadShader(int shaderType, String source) {
        int shader = GLES20.glCreateShader(shaderType);
        if (shader != 0) {
            GLES20.glShaderSource(shader, source);
            GLES20.glCompileShader(shader);
            int[] compiled = new int[1];
            GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
            if (compiled[0] == 0) {
                Log.e(MainActivity.TAG, "Could not compile shader " + shaderType + ":");
                Log.e(MainActivity.TAG, GLES20.glGetShaderInfoLog(shader));
                GLES20.glDeleteShader(shader);
                shader = 0;
            }
        }
        return shader;
    }

    private int createProgram(String vertexSource, String fragmentSource) {
        int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexSource);
        if (vertexShader == 0) {
            return 0;
        }
        int pixelShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentSource);
        if (pixelShader == 0) {
            return 0;
        }

        int program = GLES20.glCreateProgram();
        if (program != 0) {
            GLES20.glAttachShader(program, vertexShader);
            checkGlError("glAttachShader");
            GLES20.glAttachShader(program, pixelShader);
            checkGlError("glAttachShader");
            GLES20.glLinkProgram(program);
            int[] linkStatus = new int[1];
            GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkStatus, 0);
            if (linkStatus[0] != GLES20.GL_TRUE) {
                Log.e(MainActivity.TAG, "Could not link program: ");
                Log.e(MainActivity.TAG, GLES20.glGetProgramInfoLog(program));
                GLES20.glDeleteProgram(program);
                program = 0;
            }
        }
        return program;
    }

    private void checkGlError(String op) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(MainActivity.TAG, op + ": glError " + error);
            // throw new RuntimeException(op + ": glError " + error);
        }
    }

    protected float[] scaleToFit(int imgWidth,
                                 int imgHeight, int screenWidth, int screenHeight) {

        float corrX = 1.0f;
        float corrY = 1.0f;
        float x = ((float) imgWidth) / ((float) screenWidth);
        float y = ((float) imgHeight) / ((float) screenHeight);
        float overX = x > 1.0f ? x - 1.0f : 0;
        float overY = y > 1.0f ? y - 1.0f : 0;
        float underX = x < 1.0f ? 1.0f - x : 0;
        float underY = y < 1.0f ? 1.0f - y : 0;

        if ((overX > 0) && (overY > 0)) {
            if (overX > overY) {
                corrX = 1.0f / x;
                corrY = corrX;
            } else {
                corrY = 1.0f / y;
                corrX = corrY;
            }
        } else if (overX > 0) {
            corrX = 1.0f / x;
            corrY = corrX;
        } else if (overY > 0) {
            corrY = 1.0f / y;
            corrX = corrY;
        } else if ((underX > 0) && (underY > 0)) {
            if (underX > underY) {
                corrY = 1.0f / y;
                corrX = corrY;
            } else {
                corrX = 1.0f / x;
                corrY = corrX;
            }
        } else if (underX > 0) {
            corrY = 1.0f / y;
            corrX = corrY;
        } else if (underY > 0) {
            corrX = 1.0f / x;
            corrY = corrX;
        }

        return new float[]{corrX, corrY};

    }

    public SurfaceTexture getSurfaceTexture() {
        return __surfaceTexture;
    }

    public void setViewListener(CameraGLView.ViewListener viewListener) {
        __listener = viewListener;
    }

    public void setGeometry(int rotation, int bufferWidth, int bufferHeight,
                            int viewportWidth, int viewportHeight,
                            int displayWidth, int displayHeight,
                            boolean mirror) {

        Log.d(MainActivity.TAG, "CameraGLRenderer setGeometry rotation: " + rotation +
                " bufferWidth: " + bufferWidth + " bufferHeight: " + bufferHeight +
                " viewportWidth: " + viewportWidth + " viewportHeight: " + viewportHeight +
                " displayWidth: " + displayWidth + " displayHeight: " + displayHeight +
                " mirror: " + mirror);

        Log.d(MainActivity.TAG, "viewport aspect ratio: " +
                ((float) (viewportWidth)) / ((float) (viewportHeight)));

        Log.d(MainActivity.TAG, "display aspect ratio: " +
                ((float) (displayWidth)) / ((float) (displayHeight)));

        if (!mirror) {
            if (rotation == 0) {
                __coordBuffer.put(TEX_COORDS_0).position(0);
            } else if (rotation == 90) {
                __coordBuffer.put(TEX_COORDS_90).position(0);
            } else if (rotation == 270) {
                __coordBuffer.put(TEX_COORDS_270).position(0);
            } else {
                __coordBuffer.put(TEX_COORDS_180).position(0);
            }
        } else {
            if (rotation == 0) {
                __coordBuffer.put(TEX_COORDS_0_MIRROR).position(0);
            } else if (rotation == 90) {
                __coordBuffer.put(TEX_COORDS_90_MIRROR).position(0);
            } else if (rotation == 270) {
                __coordBuffer.put(TEX_COORDS_270_MIRROR).position(0);
            } else {
                __coordBuffer.put(TEX_COORDS_180_MIRROR).position(0);
            }
        }

        float[] corrFactors = scaleToFit(viewportWidth, viewportHeight, displayWidth, displayHeight);

        // expand to fit the screen size as much as possible
        __viewportWidth = (int) (viewportWidth * corrFactors[0]);
        __viewportHeight = (int) (viewportHeight * corrFactors[1]);
        __viewportX = Math.max(displayWidth - __viewportWidth, 0) / 2;
        __viewportY = Math.max(displayHeight - __viewportHeight, 0) / 2;

        Log.d(MainActivity.TAG, "Corr factors: " + corrFactors[0] + " " + corrFactors[1]);

        Log.d(MainActivity.TAG, "Viewport width: " + __viewportWidth + " height: " +
                __viewportHeight + " x: " + __viewportX + " y: " + __viewportY);

        if (__surfaceTexture != null) {
            __surfaceTexture.setDefaultBufferSize(bufferWidth, bufferHeight);
        }

    }

    public void setPaused(long timestamp, boolean paused) {
        Log.d(MainActivity.TAG, "Timestamp " + timestamp);
        synchronized (this) {
            __timestamp = timestamp;
            if (paused) {
                __state = DrawState.PAUSE;
            } else {
                __state = DrawState.FILTER;
            }
        }
        // wait for the request to be ack'ed by the renderer thread
        synchronized (__sync) {
            try {
                __sync.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void resetSignalledReady() {
        __signalledReady = false;
    }

}
