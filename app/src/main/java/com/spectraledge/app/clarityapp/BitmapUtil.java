package com.spectraledge.app.clarityapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class BitmapUtil {

    protected static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    protected static int readExifRotation(FileDescriptor fileDescriptor) {
        int rotation = -1;
        File cacheDir = Environment.getExternalStorageDirectory();
        FileInputStream inStream = new FileInputStream(fileDescriptor);
        File tempFile = new File(cacheDir, "temp");
        try {
            FileOutputStream outStream = new FileOutputStream(tempFile);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inStream.read(buf)) > 0) {
                outStream.write(buf, 0, len);
            }
            inStream.close();
            outStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            ExifInterface exif = new ExifInterface(tempFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                rotation = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                rotation = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                rotation = 270;
            }
            tempFile.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotation;
    }

    public static Bitmap decodeWithinBounds(ParcelFileDescriptor fileDescriptor, int width, int height) {
        FileDescriptor descriptor = fileDescriptor.getFileDescriptor();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFileDescriptor(descriptor, null, options);
        options.inSampleSize = calculateInSampleSize(options, width, height);
        options.inJustDecodeBounds = false;
        // just cache the downsized thumbnail
        Bitmap bitmap = BitmapFactory.decodeFileDescriptor(descriptor, null, options);
        int rotation = readExifRotation(descriptor);
        Log.d(MainActivity.TAG, "rotation tag: " + rotation);
        Matrix matrix = new Matrix();
        if (rotation > -1) {
            matrix.postRotate(rotation);
        }
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
        return bitmap;
    }

    // this is intended to decode file descriptors opened by the asset manager
    public static Bitmap decodeWithinBounds(ParcelFileDescriptor fileDescriptor, int width, int height, long offset) {
        FileDescriptor descriptor = fileDescriptor.getFileDescriptor();
        FileInputStream inStream = new FileInputStream(descriptor);
        Bitmap bitmap = null;
        // skip what the asset file descriptor said was the offset
        // also, ignore the exif header - make sure the files included in the resources
        // have 0 orientation
        try {
            FileChannel channel = inStream.getChannel();
            channel.position(offset);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(inStream, null, options);
            options.inSampleSize = calculateInSampleSize(options, width, height);
            options.inJustDecodeBounds = false;
            // just cache the downsized thumbnail
            channel.position(offset);
            bitmap = BitmapFactory.decodeStream(inStream, null, options);
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static Bitmap decode(ParcelFileDescriptor fileDescriptor) {
        FileDescriptor descriptor = fileDescriptor.getFileDescriptor();
        BitmapFactory.Options options = new BitmapFactory.Options();
        // full (original) size
        Bitmap bitmap = BitmapFactory.decodeFileDescriptor(descriptor, null, options);
        int rotation = readExifRotation(descriptor);
        Log.d(MainActivity.TAG, "rotation tag: " + rotation);
        Matrix matrix = new Matrix();
        if (rotation > -1) {
            matrix.postRotate(rotation);
        }
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
        return bitmap;
    }

    // this is intended to decode file descriptors opened by the asset manager
    public static Bitmap decode(ParcelFileDescriptor fileDescriptor, long offset) {
        FileDescriptor descriptor = fileDescriptor.getFileDescriptor();
        FileInputStream inStream = new FileInputStream(descriptor);
        Bitmap bitmap = null;
        // skip what the asset file descriptor said was the offset
        // also, ignore the exif header - make sure the files included in the resources
        // have 0 orientation
        try {
            inStream.skip(offset);
            // full (original) size
            bitmap = BitmapFactory.decodeStream(inStream);
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

}
