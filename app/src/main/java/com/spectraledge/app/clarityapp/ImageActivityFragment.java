package com.spectraledge.app.clarityapp;

import android.app.Activity;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.v4.provider.DocumentFile;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageActivityFragment extends Fragment {

    // parameters valid for the duration of the lifetime of the fragment
    Uri __imageUri = null;
    Uri __outDirUri = null;

    // the activity and the view will be recreated on orientation change
    Object __uiSync = new Object();
    ImageActivity __activity = null;
    ImageView __imageView = null;
    TextView __textView = null;
    TextView __scrollTextView = null;
    boolean __interruptRequested = false;

    Bitmap __inBitmap = null;
    Bitmap __outBitmap = null;

    HandlerThread __offscreenThread = null;
    Handler __offscreenHandler = null;
    Runnable __offscreenRunnable = null;
    Runnable __saveRunnable = null;
    Thread __processingThread = null;

    SurfaceTexture __windowSurface = null;
    FileOutputStream __outputStream = null;
    int __backgroundCount = 0;
    CharSequence __marqueeText = null;

    final String TEXT_ORIGINAL = "Viewing: original image";
    final String TEXT_PROCESSED = "Viewing: processed image";
    final String FILE_SUFFIX = "_CLARITY";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        // the parameters are valid for the duration of the lifetime of the fragment
        final ImageActivity activity = (ImageActivity) getActivity();
        __imageUri = activity.getImageUri();
        __outDirUri = activity.getOutDirUri();
        // the offscreen thread will only be destroyed when the fragment is destroyed
        startOffscreenThread();
        // to be run on an off-screen thread
        __offscreenRunnable = new Runnable() {
            @Override
            public void run() {
                Log.d(MainActivity.TAG, "Offscreen runnable starting");
                boolean done = false;
                while (!done) {
                    boolean interruptRequested = false;
                    Activity activity = null;
                    ImageView imageView = null;
                    synchronized (__uiSync) {
                        interruptRequested = __interruptRequested;
                        activity = __activity;
                        imageView = __imageView;
                    }
                    final ImageView view = imageView;
                    if (!interruptRequested) {
                        // do the next bit of work
                        if (__outBitmap != null) {
                            if (__processingThread != null) {
                                if (!__processingThread.isAlive()) {
                                    // processing has been done, ready to display the output
                                    // update the UI to display the output bitmap
                                    updateImageView(__outBitmap, TEXT_PROCESSED);
                                    done = true;
                                }
                            } else {
                                // showToast("Starting processing");
                                synchronized (__uiSync) {
                                    __marqueeText = "Started processing";
                                }
                                updateScrollTextView(__marqueeText);
                                runProcessing();
                                if (__processingThread != null) {
                                    try {
                                        Log.d(MainActivity.TAG, "Offscreen thread waiting for the processing thread to finish");
                                        __processingThread.join();
                                        // showToast("Done processing");
                                        updateScrollTextView("Done processing");
                                        // update the UI to display the output bitmap
                                        updateImageView(__outBitmap, TEXT_PROCESSED);
                                        if (__saveRunnable != null) {
                                            synchronized (__uiSync) {
                                                __backgroundCount++;
                                            }
                                            __offscreenHandler.post(__saveRunnable);
                                        }
                                        done = true;
                                    } catch (InterruptedException topE) {
                                        // we'll get interrupted if destroy is requested
                                        Log.d(MainActivity.TAG, "Waiting for the processing thread interrupted");
                                        if ((__processingThread != null) && (__processingThread.isAlive())) {
                                            signalProcessingInterrupt();
                                            try {
                                                Log.d(MainActivity.TAG, "Offscreen thread waiting for the processing thread to finish");
                                                __processingThread.join();
                                            } catch (InterruptedException bottomE) {
                                                Log.d(MainActivity.TAG, "Waiting for the processing thread interrupted");
                                            }
                                        }
                                        // showToast("Processing interrupted");
                                        synchronized (__uiSync) {
                                            __marqueeText = "Processing interrupted";
                                        }
                                        updateScrollTextView(__marqueeText);
                                        Log.d(MainActivity.TAG, "Interrupt request processed");
                                        __processingThread = null;
                                        done = true;
                                    }
                                }
                            }
                        } else if (__windowSurface != null) {
                            // prepare output bitmap
                            __outBitmap = Bitmap.createBitmap(__inBitmap.getWidth(),
                                    __inBitmap.getHeight(), __inBitmap.getConfig());
                        } else if (__inBitmap != null) {
                            // update the UI to display the input bitmap
                            updateImageView(__inBitmap, TEXT_ORIGINAL);
                            // prepare GLES
                            loadAssets(activity.getAssets());
                            __windowSurface = new SurfaceTexture(0);
                            if (__windowSurface != null) {
                                Surface surface = new Surface(__windowSurface);
                                if (surface != null) {
                                    initialiseGLES(surface);
                                    surface.release();
                                }
                            }
                        } else {
                            // showToast("Loading input");
                            synchronized (__uiSync) {
                                __marqueeText = "Loading input";
                            }
                            updateScrollTextView(__marqueeText);
                            // nothing has been done yet, decode the Uri
                            loadImageUri();
                        }
                    } else {
                        // showToast("Interrupted");
                        done = true;
                    }
                }
                Log.d(MainActivity.TAG, "Offscreen runnable done");
                synchronized (__uiSync) {
                    __backgroundCount--;
                }
            }
        };
        __saveRunnable = new Runnable() {
            @Override
            public void run() {
                final Activity activity = getActivity();
                if ((__outBitmap != null) && (__outDirUri != null) && (__imageUri != null)) {
                    ContentResolver contentResolver = activity.getContentResolver();
                    Uri parentDir = DocumentsContract.buildDocumentUriUsingTree(__outDirUri,
                            DocumentsContract.getTreeDocumentId(__outDirUri));
                    Cursor cursor = null;
                    try {
                        cursor = contentResolver.query(__imageUri, null, null, null, null, null);
                    } catch (UnsupportedOperationException e) {
                        e.printStackTrace();
                    }
                    if (cursor != null && cursor.moveToFirst()) {
                        String inFullFileName = cursor.getString(
                                cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        String inFileName = inFullFileName.substring(0, inFullFileName.lastIndexOf('.'));
                        String outFileName = null;
                        Bitmap.CompressFormat format = null;
                        // save as JPG - it's quicker
                        outFileName = inFileName + FILE_SUFFIX;
                        format = Bitmap.CompressFormat.JPEG;
                        if (outFileName != null) {
                            final Uri destUri = DocumentsContract
                                    .createDocument(contentResolver, parentDir, "image/jpeg", outFileName);
                            if (destUri != null) {
                                try {
                                    ParcelFileDescriptor parcelDescriptor = contentResolver.openFileDescriptor(destUri, "w");
                                    FileDescriptor fileDescriptor = parcelDescriptor.getFileDescriptor();
                                    if (fileDescriptor != null) {
                                        final FileOutputStream stream = new FileOutputStream(fileDescriptor);
                                        synchronized (__uiSync) {
                                            __outputStream = stream;
                                        }
                                        Log.d(MainActivity.TAG, stream.toString());
                                        try {
                                            Log.d(MainActivity.TAG, "Compressing the bitmap");
                                            boolean compressed = __outBitmap.compress(format, 100, stream);
                                            Log.d(MainActivity.TAG, "Done compressing: " + compressed);
                                        } catch (Exception e) {
                                            Log.d(MainActivity.TAG, "Compress interrupted");
                                        }
                                        // showToast("File saved " + outFileName);
                                        DocumentFile file = DocumentFile.fromSingleUri(activity, destUri);
                                        StringBuilder builder = new StringBuilder();
                                        builder.append("Finished processing ");
                                        builder.append(inFullFileName);
                                        builder.append(" (");
                                        builder.append(__outBitmap.getWidth());
                                        builder.append("x");
                                        builder.append(__outBitmap.getHeight());
                                        builder.append(")\n");
                                        builder.append("Processing time: ");
                                        builder.append(getProcessingTime());
                                        builder.append("ms\n");
                                        builder.append("Saved output file: ");
                                        builder.append(file.getName());
                                        synchronized (__uiSync) {
                                            __marqueeText = builder.toString();
                                        }
                                        showToast(__marqueeText.toString());
                                        updateScrollTextView(__marqueeText.toString().replace("\n", " ") + "\n");
                                        if (stream != null) {
                                            try {
                                                stream.close();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        synchronized (__uiSync) {
                                            __outputStream = null;
                                        }
                                    }
                                    if (parcelDescriptor != null) {
                                        parcelDescriptor.close();
                                    }
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
                synchronized (__uiSync) {
                    __backgroundCount--;
                }
            }
        };
    }

    @Override
    public void onDestroy() {
        // only destroyed when the activity is not coming back
        attachToEGLContext();
        if (__windowSurface != null) {
            __windowSurface.detachFromGLContext();
        }
        terminateGLES();
        detachFromEGLContext();
        super.onDestroy();
    }

    // not currently used - only useful if writing
    // the output file out takes unacceptably long
    // to wait for it
    public void interruptOutputWriteOut() {
        // cancel processing / writing out to the output stream
        FileOutputStream stream = null;
        synchronized (__uiSync) {
            stream = __outputStream;
            __outputStream = null;
        }
        if (stream != null) {
            try {
                Log.d(MainActivity.TAG, "Closing the stream from the UI thread");
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Log.d(MainActivity.TAG, "Stream null?");
        }
    }

    public boolean doingWorkInBackground() {
        int backgroundCount = 0;
        synchronized (__uiSync) {
            backgroundCount = __backgroundCount;
        }
        Log.d(MainActivity.TAG, "Doing work in background: " + (backgroundCount > 0));
        return (backgroundCount > 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        // read only, no need to sync
        if (__inBitmap != null) {
            updateImageView(__inBitmap, TEXT_ORIGINAL);
        }
        if (__marqueeText != null) {
            updateScrollTextView(__marqueeText.toString().replace("\n", " ") + "\n");
        }
        if (__offscreenRunnable != null) {
            // the runnable will be added to the queue
            synchronized (__uiSync) {
                __backgroundCount++;
            }
            __offscreenHandler.post(__offscreenRunnable);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void stopOffscreenThread() {
        synchronized (__uiSync) {
            __interruptRequested = true;
        }
        if (__offscreenThread != null) {
            try {
                Log.d(MainActivity.TAG, "Fragment waiting for the offscreen thread to finish");
                // if the thread is sleeping (waiting for the processing to finish), interrupt it
                __offscreenThread.interrupt();
                // quit the thread Runnable loop
                __offscreenThread.quit();
                // finish off the last Runnable queued
                __offscreenThread.join();
                Log.d(MainActivity.TAG, "Fragment done waiting for the offscreen thread");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            __offscreenThread = null;
        }
        __offscreenHandler = null;
        if (BuildConfig.DEBUG && (__backgroundCount > 0)) {
            throw new AssertionError();
        }
    }

    protected void startOffscreenThread() {
        // set up an off-screen thread to do non-UI work
        synchronized (__uiSync) {
            __interruptRequested = false;
        }
        __offscreenThread = new HandlerThread("OffscreenThread");
        if (__offscreenThread != null) {
            // start needs to be called first
            __offscreenThread.start();
            __offscreenHandler = new Handler(__offscreenThread.getLooper());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // the activity and the view will be recreated on orientation change
        synchronized (__uiSync) {
            __activity = (ImageActivity) getActivity();
            __imageView = (ImageView) view.findViewById(R.id.image_view);
            __textView = (TextView) view.findViewById(R.id.text_view);
            __scrollTextView = (TextView) view.findViewById(R.id.scroll_text_view);
        }
        View.OnTouchListener listener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                int action = MotionEventCompat.getActionMasked(event);
                switch (action) {
                    case (MotionEvent.ACTION_DOWN):
                        if (__inBitmap != null) {
                            updateImageView(__inBitmap, TEXT_ORIGINAL);
                        }
                        return true;
                    case (MotionEvent.ACTION_MOVE):
                        return true;
                    case (MotionEvent.ACTION_UP):
                        if ((__outBitmap != null) && ((__processingThread != null) && !__processingThread.isAlive())) {
                            updateImageView(__outBitmap, TEXT_PROCESSED);
                        }
                        return true;
                    case (MotionEvent.ACTION_CANCEL):
                        return true;
                    case (MotionEvent.ACTION_OUTSIDE):
                        return true;
                    default:
                        return false;
                }
            }
        };
        __imageView.setOnTouchListener(listener);
    }

    void runProcessing() {
        if ((__inBitmap != null) && (__outBitmap != null)) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    Log.d(MainActivity.TAG, "Processing thread started");
                    attachToEGLContext();
                    processBitmapCPU(__inBitmap, __outBitmap);
                    detachFromEGLContext();
                    Log.d(MainActivity.TAG, "Processing thread finished");
                }
            };
            __processingThread = new Thread(runnable);
            __processingThread.start();
        }
    }

    void showToast(String message) {
        Activity activity = null;
        synchronized (__uiSync) {
            activity = __activity;
        }
        if (activity != null) {
            final String text = message;
            final Activity currentActivity = activity;
            Runnable updateImageViewRunnable = new Runnable() {
                @Override
                public void run() {
                    if (!currentActivity.isFinishing()) {
                        int duration = Toast.LENGTH_LONG;
                        Toast toast = Toast.makeText(currentActivity, text, duration);
                        toast.show();
                    }
                }
            };
            activity.runOnUiThread(updateImageViewRunnable);
        }
    }

    void loadImageUri() {
        final Uri imageUri = __imageUri;
        ParcelFileDescriptor fileDescriptor = null;
        Activity activity = null;
        synchronized (__uiSync) {
            activity = __activity;
        }
        if (activity != null) {
            try {
                // see if this is an asset or not
                if (imageUri.getScheme().equals("android.resource")) {
                    // asset path
                    AssetFileDescriptor assetFileDescriptor = getActivity().getContentResolver().openAssetFileDescriptor(imageUri, "r");
                    if (assetFileDescriptor != null) {
                        fileDescriptor = assetFileDescriptor.getParcelFileDescriptor();
                        if (fileDescriptor != null) {
                            __inBitmap = BitmapUtil.decode(fileDescriptor, assetFileDescriptor.getStartOffset());
                            if (fileDescriptor != null) {
                                fileDescriptor.close();
                            }
                        }
                        assetFileDescriptor.close();
                    }
                } else {
                    // non-asset path
                    fileDescriptor = activity.getContentResolver().openFileDescriptor(imageUri, "r");
                    if (fileDescriptor != null) {
                        __inBitmap = BitmapUtil.decode(fileDescriptor);
                        if (fileDescriptor != null) {
                            fileDescriptor.close();
                        }
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    void updateScrollTextView(CharSequence text) {
        Activity activity = null;
        TextView scrollTextView = null;
        synchronized (__uiSync) {
            activity = __activity;
            scrollTextView = __scrollTextView;
        }
        final TextView tView = scrollTextView;
        final CharSequence t = text;
        if (activity != null) {
            Runnable updateImageViewRunnable = new Runnable() {
                @Override
                public void run() {
                    if (tView != null) {
                        tView.setText(t);
                        tView.setSelected(true);
                    }
                }
            };
            activity.runOnUiThread(updateImageViewRunnable);
        }
    }

    void updateImageView(Bitmap bitmap, CharSequence text) {
        final Bitmap sourceBitmap = bitmap;
        Activity activity = null;
        ImageView imageView = null;
        TextView textView = null;
        synchronized (__uiSync) {
            activity = __activity;
            imageView = __imageView;
            textView = __textView;
        }
        final ImageView iView = imageView;
        final TextView tView = textView;
        final CharSequence t = text;
        if (activity != null) {
            Runnable updateImageViewRunnable = new Runnable() {
                @Override
                public void run() {
                    if (iView != null) {
                        if (sourceBitmap != null) {
                            iView.setImageBitmap(sourceBitmap);
                            iView.invalidate();
                        }
                    }
                    if (tView != null) {
                        tView.setText(t);
                    }
                }
            };
            activity.runOnUiThread(updateImageViewRunnable);
        }
    }

    native void loadAssets(AssetManager assetManager);

    native void initialiseGLES(Surface windowSurface);

    native void terminateGLES();

    native void processBitmap(Bitmap bitmapIn, Bitmap bitmapOut);

    native void processBitmapGLES(Bitmap bitmapIn, Bitmap bitmapOut);

    native void processBitmapCPU(Bitmap bitmapIn, Bitmap bitmapOut);

    native long getProcessingTime();

    native void signalProcessingInterrupt();

    native void attachToEGLContext();

    native void detachFromEGLContext();

}
