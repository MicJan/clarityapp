package com.spectraledge.app.clarityapp;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Size;
import android.util.SizeF;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class CameraActivityFragment extends Fragment {

    CameraDevice __cameraDevice = null;
    CameraCaptureSession __captureSession = null;
    CameraGLView __glView = null;
    Button __snapButton = null;
    ImageButton __switchButton = null;
    CaptureRequest.Builder __requestBuilder = null;
    ImageReader __imageReader = null;
    int __displayOrientation = -1;
    int __cameraOrientation = -1;
    Uri __destDir = null;
    Thread __saveFileThread = null;
    final String FILE_PREFIX = "CAMERA_";
    int __whichCamera = CameraCharacteristics.LENS_FACING_BACK;
    long __firstTimestamp = 0;
    boolean __firstPreviewSignalled = false;

    ImageReader.OnImageAvailableListener __imageReaderListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
            Log.d(MainActivity.TAG, "onImageAvailable");
            // Callback that is called when a new image is available from ImageReader.
            final Image image = reader.acquireNextImage();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {

                    if (image != null) {

                        final Activity activity = getActivity();
                        Image.Plane[] planes = image.getPlanes();
                        Image.Plane plane = null;

                        if (planes.length > 0) {
                            plane = planes[0];
                        }

                        if ((plane != null) && (activity != null) && (__destDir != null)) {

                            String fileName = null;
                            long currentTimeMillis = Calendar.getInstance().getTimeInMillis();
                            // camera still capture is JPG
                            fileName = FILE_PREFIX + currentTimeMillis + ".jpg";

                            ContentResolver contentResolver = activity.getContentResolver();
                            Uri parentDir = DocumentsContract.buildDocumentUriUsingTree(__destDir,
                                    DocumentsContract.getTreeDocumentId(__destDir));
                            final Uri destUri = DocumentsContract
                                    .createDocument(contentResolver, parentDir, "image*//*", fileName);

                            if (destUri != null) {
                                try {
                                    ParcelFileDescriptor parcelDescriptor = contentResolver.openFileDescriptor(destUri, "w");
                                    FileDescriptor fileDescriptor = parcelDescriptor.getFileDescriptor();
                                    FileOutputStream stream = null;
                                    ByteBuffer buffer = null;
                                    byte[] array = null;
                                    if (fileDescriptor != null) {
                                        try {
                                            stream = new FileOutputStream(fileDescriptor);
                                            buffer = plane.getBuffer();
                                            if ((stream != null) && (buffer != null)) {
                                                array = new byte[buffer.remaining()];
                                                if (array != null) {
                                                    buffer.get(array);
                                                    stream.write(array);
                                                    Log.d(MainActivity.TAG, "File saved " + fileName);
                                                    final String outFileName = fileName;
                                                    Runnable infoRunnable = new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            if (!activity.isFinishing()) {
                                                                Toast.makeText(activity, "File saved " + outFileName,
                                                                        Toast.LENGTH_SHORT).show();
                                                                // Go back to the main screen and pass the newly captured picture to it
                                                                Intent intent = new Intent(activity, MainActivity.class);
                                                                intent.putExtra(MainActivity.EXTRA_IMAGE_URI, destUri);
                                                                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                                startActivity(intent);
                                                            }
                                                        }
                                                    };

                                                    activity.runOnUiThread(infoRunnable);

                                                }
                                            }
                                        } catch (FileNotFoundException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        } finally {
                                            if (stream != null) {
                                                try {
                                                    stream.close();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    }
                                    if (parcelDescriptor != null) {
                                        parcelDescriptor.close();
                                    }
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                        image.close();
                    }

                }
            };
            __saveFileThread = new Thread(runnable);
            __saveFileThread.start();
        }
    };

    class EventThread extends Thread {

        Object __lock = new Object();
        LinkedList<ProcessorEvent> __queue = new LinkedList<>();
        boolean __viewReady = false;
        boolean __rendererReady = false;
        boolean __cameraReady = false;
        boolean __switchRequested = false;
        ProcessorState __state = ProcessorState.STATE_PAUSED;

        @Override
        public void run() {

            setName("EventThread");

            Log.d(MainActivity.TAG, "EventThread starting");

            while (true) {

                ProcessorEvent event = ProcessorEvent.UNDEFINED;

                synchronized (__lock) {

                    // Retrieves and removes the head of this queue, or returns null if this queue is empty
                    event = __queue.poll();

                    if (event == null) {

                        try {
                            __lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        // Retrieves and removes the head of this queue. This method differs
                        // from poll only in that it throws an exception if this queue is empty.
                        // If we've been signalled, we expect to have something in the queue
                        try {
                            event = __queue.remove();
                        } catch (NoSuchElementException e) {
                            e.printStackTrace();
                            break;
                        }
                    }

                    switch (event) {
                        case VIEW_OFFSCREEN:
                            __viewReady = false;
                            break;
                        case VIEW_ONSCREEN:
                            __viewReady = true;
                            break;
                        case RENDERER_AVAILABLE:
                            __rendererReady = true;
                            break;
                        case RENDERER_UNAVAILABLE:
                            __rendererReady = false;
                            break;
                        case CAMERA_CLOSED:
                            __cameraReady = false;
                            break;
                        case FIRST_PREVIEW_AVAILABLE:
                            __cameraReady = true;
                            break;
                        case CAMERA_SWITCH_REQUESTED:
                            __switchRequested = true;
                            break;
                    }
                }

                Log.d(MainActivity.TAG, "ProcessorEvent " + event + " state " + __state);
                if (event == ProcessorEvent.INTERRUPT) {
                    break;
                }

                if (__state == ProcessorState.STATE_PAUSED) {
                    // changing state?
                    if ((__viewReady) && (__rendererReady)) {
                        if (!__switchRequested) {
                            synchronized (__lock) {
                                __state = ProcessorState.STATE_PENDING_RUNNING;
                            }
                            Log.d(MainActivity.TAG, "Starting camera 1");
                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    initCamera();
                                }
                            };
                            __offscreenHandler.post(runnable);
                        }
                    }
                } else if (__state == ProcessorState.STATE_PENDING_RUNNING) {
                    // changing state?
                    if (__cameraReady) {
                        if (__switchRequested) {
                            Log.d(MainActivity.TAG, "Unpausing camera render " + __firstTimestamp);
                            __glView.setPaused(__firstTimestamp, false);
                            synchronized (__lock) {
                                __state = ProcessorState.STATE_RUNNING;
                                __switchRequested = false;
                            }
                        } else {
                            synchronized (__lock) {
                                __state = ProcessorState.STATE_RUNNING;
                            }
                        }
                    } else if (!__viewReady || !__rendererReady) {
                        synchronized (__lock) {
                            __state = ProcessorState.STATE_RUNNING_CANCELLED;
                        }
                    }
                } else if (__state == ProcessorState.STATE_RUNNING) {
                    // changing state?
                    if (!__cameraReady) {
                        // why has the camera been closed?
                        if (__switchRequested) {
                            synchronized (__lock) {
                                __state = ProcessorState.STATE_PENDING_RUNNING;
                            }
                            // because a camera switch has been requested
                            Log.d(MainActivity.TAG, "Pausing camera renderer " + __firstTimestamp);
                            __glView.setPaused(Long.MAX_VALUE, true);
                            Log.d(MainActivity.TAG, "Starting camera 2");
                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    initCamera();
                                }
                            };
                            __offscreenHandler.post(runnable);
                        } else {
                            // because the view has been destroyed (screen orientation changed)
                            synchronized (__lock) {
                                __state = ProcessorState.STATE_PAUSED;
                            }
                        }
                    } else if (!__viewReady || !__rendererReady) {
                        synchronized (__lock) {
                            __state = ProcessorState.STATE_RUNNING_CANCELLED;
                        }
                    }
                } else if (__state == ProcessorState.STATE_RUNNING_CANCELLED) {
                    if (__cameraReady) {
                        synchronized (__lock) {
                            __state = ProcessorState.STATE_PENDING_PAUSED;
                        }
                        Log.d(MainActivity.TAG, "Tearing down camera");
                        Runnable runnable = new Runnable() {
                            @Override
                            public void run() {
                                teardownCamera();
                            }
                        };
                        __offscreenHandler.post(runnable);
                    }
                } else if (__state == ProcessorState.STATE_PENDING_PAUSED) {
                    if (!__cameraReady) {
                        synchronized (__lock) {
                            __state = ProcessorState.STATE_PAUSED;
                        }
                    }
                }

            }

            Log.d(MainActivity.TAG, "EventThread exiting");

        }

        public boolean post(ProcessorEvent event) {
            boolean accepted = false;
            synchronized (__lock) {
                if (event == ProcessorEvent.CAMERA_SWITCH_REQUESTED) {
                    if (!__switchRequested && (__state == ProcessorState.STATE_RUNNING)
                            && !__queue.contains(ProcessorEvent.CAMERA_SWITCH_REQUESTED)) {
                        __queue.add(event);
                        __lock.notify();
                        accepted = true;
                    }
                } else {
                    __queue.add(event);
                    __lock.notify();
                    accepted = true;
                }
                if (accepted) {
                    Log.d(MainActivity.TAG, "Processor thread queued: " + event);
                }
            }
            return accepted;
        }

        public boolean isSwitching() {
            boolean switching = false;
            synchronized (__lock) {
                switching = __switchRequested || __queue.contains(ProcessorEvent.CAMERA_SWITCH_REQUESTED);
            }
            return switching;
        }

        public void waitUntilInState(ProcessorState expState) {
            ProcessorState state = ProcessorState.STATE_RUNNING;
            Log.d(MainActivity.TAG, "Wait until in state " + expState);
            do {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
                synchronized (__lock) {
                    state = __state;
                }
            } while (state != expState);
        }

        public void reset() {
            __viewReady = false;
            __rendererReady = false;
            __cameraReady = false;
            __switchRequested = false;
            __state = ProcessorState.STATE_PAUSED;
        }
    }

    // will control the renderer
    EventThread __processorThread = null;

    enum ProcessorEvent {
        VIEW_OFFSCREEN,
        VIEW_ONSCREEN,
        RENDERER_AVAILABLE,
        RENDERER_UNAVAILABLE,
        CAMERA_CLOSED,
        FIRST_PREVIEW_AVAILABLE,
        CAMERA_SWITCH_REQUESTED,
        INTERRUPT,
        UNDEFINED
    }

    enum ProcessorState {
        STATE_PENDING_RUNNING,
        STATE_RUNNING,
        STATE_RUNNING_CANCELLED,
        STATE_PENDING_PAUSED,
        STATE_PAUSED,
    }

    HandlerThread __offscreenThread = null;
    Handler __offscreenHandler = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        __processorThread = new EventThread();
        __processorThread.start();
        __offscreenThread = new HandlerThread("OffscreenThread");
        if (__offscreenThread != null) {
            // start needs to be called first
            __offscreenThread.start();
            __offscreenHandler = new Handler(__offscreenThread.getLooper());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        __glView = (CameraGLView) view.findViewById(R.id.glView);
        __glView.setViewListener(new CameraGLView.ViewListener() {
            @Override
            public void onRendererCreated() {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        __processorThread.post(ProcessorEvent.RENDERER_AVAILABLE);
                    }
                };
                __offscreenHandler.post(runnable);
            }
        });
        __snapButton = (Button) view.findViewById(R.id.pictureBtn);
        __snapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(MainActivity.TAG, "Button clicked");
                if (!__processorThread.isSwitching()) {
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            lockFocus();
                        }
                    };
                    __offscreenHandler.post(runnable);
                }
            }
        });
        __switchButton = (ImageButton) view.findViewById(R.id.switchBtn);
        __switchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(MainActivity.TAG, "Button clicked");
                if (__processorThread.post(ProcessorEvent.CAMERA_SWITCH_REQUESTED)) {
                    if (__whichCamera == CameraCharacteristics.LENS_FACING_BACK) {
                        __whichCamera = CameraCharacteristics.LENS_FACING_FRONT;
                    } else {
                        __whichCamera = CameraCharacteristics.LENS_FACING_BACK;
                    }
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            teardownCamera();
                        }
                    };
                    __offscreenHandler.post(runnable);
                }
            }
        });
    }

    protected void enterPreviewMode() {
        Log.d(MainActivity.TAG, "Start enterPreviewMode");
        final CaptureRequest.Builder builder = __requestBuilder;
        if (builder != null) {
            try {
                // CONTROL_AF_MODE  Whether auto-focus (AF) is currently enabled, and what mode it is set to.
                // CONTROL_AF_MODE_CONTINUOUS_PICTURE   In this mode, the AF algorithm
                // modifies the lens position continually to attempt
                // to provide a constantly-in-focus image stream.
                builder.set(CaptureRequest.CONTROL_AF_MODE,
                        CameraMetadata.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

                // Build a request using the current target Surfaces and settings.
                CaptureRequest request = builder.build();

                // Request endlessly repeating capture of images by this capture session.
                // With this method, the camera device will continually capture images
                // using the settings in the provided CaptureRequest, at the maximum rate possible.
                /*request	    CaptureRequest: the request to repeat indefinitely
                listener	CameraCaptureSession.CaptureCallback: The callback object
                            to notify every time the request finishes processing.
                            If null, no metadata will be produced for this stream of requests,
                            although image data will still be produced.
                handler     Handler: the handler on which the listener should be invoked,
                            or null to use the current thread's looper.*/
                __captureSession.setRepeatingRequest(request, new CameraCaptureSession.CaptureCallback() {
                    @Override
                    public void onCaptureStarted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, long timestamp, long frameNumber) {
                        super.onCaptureStarted(session, request, timestamp, frameNumber);
                        // Log.d(MainActivity.TAG, "" + timestamp);
                        if (__firstTimestamp == 0) {
                            __firstTimestamp = timestamp;
                        }
                    }

                    @Override
                    public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                                   @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                        if (!__firstPreviewSignalled) {
                            Log.d(MainActivity.TAG, "Posting first preview available " + __firstTimestamp +
                                    " frame number " + result.getFrameNumber());
                            __processorThread.post(ProcessorEvent.FIRST_PREVIEW_AVAILABLE);
                            __firstPreviewSignalled = true;
                        }
                    }

                    @Override
                    public void onCaptureSequenceCompleted(@NonNull CameraCaptureSession session,
                                                           int sequenceId, long frameNumber) {

                    }
                }, __offscreenHandler);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
    }

    protected void createCameraPreviewSession() {
        try {
            final Activity activity = getActivity();
            if (activity != null) {

                final CameraGLView glView = __glView;
                if ((glView != null) && (__imageReader != null)) {

                    // this is in reaction to callback from CameraGLView, so is expected not to return null
                    SurfaceTexture texture = glView.getSurfaceTexture();
                    Surface glSurface = new Surface(texture);
                    Surface imageSurface = __imageReader.getSurface();

                    if ((texture == null) || (imageSurface == null)) {
                        Log.d(MainActivity.TAG, "NULL SURFACE");
                    }

                    if (__cameraDevice != null) {

                        // Create a CaptureRequest.Builder for new capture requests, initialized with template for a target use case.
                        // templateType     int: An enumeration selecting the use case for this request;
                        //                  one of the CameraDevice.TEMPLATE_ values.
                        __requestBuilder = __cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

                        final CaptureRequest.Builder builder = __requestBuilder;

                        if (builder != null) {

                            // Add a surface to the list of targets for this request
                            // The Surface added must be one of the surfaces included in the most recent
                            // call to createCaptureSession(List, CameraCaptureSession.StateCallback, Handler),
                            // when the request is given to the camera device.
                            builder.addTarget(glSurface);

                            // Create a new camera capture session by providing the target output set of Surfaces to the camera device.
                            // The active capture session determines the set of potential output Surfaces for the camera device
                            // for each capture request. A given request may use all or only some of the outputs.
                            // Once the CameraCaptureSession is created, requests can be submitted with capture,
                            // captureBurst, setRepeatingRequest, or setRepeatingBurst.
                            /*outputs     List: The new set of Surfaces that should be made available
                                        as targets for captured image data.
                            callback	CameraCaptureSession.StateCallback: The callback to notify
                                        about the status of the new capture session.
                            handler     Handler: The handler on which the callback should be invoked,
                                        or null to use the current thread's looper.*/
                            __cameraDevice.createCaptureSession(Arrays.asList(new Surface[]{glSurface, imageSurface}),
                                    new CameraCaptureSession.StateCallback() {

                                        @Override
                                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                                            // This method is called when the camera device has finished
                                            // configuring itself, and the session can start processing capture requests.
                                            Log.d(MainActivity.TAG, "Capture session configured");
                                            if (__cameraDevice != null) {
                                                __captureSession = cameraCaptureSession;
                                                enterPreviewMode();
                                            }

                                        }

                                        @Override
                                        public void onConfigureFailed(
                                                @NonNull CameraCaptureSession cameraCaptureSession) {
                                            // This method is called if the session cannot be configured as requested.
                                            Log.d(MainActivity.TAG, "Capture session configure failed");
                                        }

                                        @Override
                                        public void onClosed(@NonNull CameraCaptureSession session) {
                                            Log.d(MainActivity.TAG, "Capture session closed");
                                        }
                                    }, __offscreenHandler
                            );
                        }
                    }
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    protected boolean isCameraOrientationSwapped(int displayOrientation, int cameraOrientation) {

        boolean swappedDimensions = false;

        if ((displayOrientation == 0) || (displayOrientation == 180)) {
            if ((cameraOrientation == 90) || (cameraOrientation == 270)) {
                swappedDimensions = true;
            }
        } else if ((displayOrientation == 90) || (displayOrientation == 270)) {
            if ((cameraOrientation == 0) || (cameraOrientation == 180)) {
                swappedDimensions = true;
            }
        }

        return swappedDimensions;

    }

    protected int getDisplayOrientation(Display display) {
        int rotation = display.getRotation();
        int rotDegrees = -1;
        if (rotation == Surface.ROTATION_0) {
            rotDegrees = 0;
        } else if (rotation == Surface.ROTATION_90) {
            rotDegrees = 90;
        } else if (rotation == Surface.ROTATION_180) {
            rotDegrees = 180;
        } else if (rotation == Surface.ROTATION_270) {
            rotDegrees = 270;
        }
        return rotDegrees;
    }

    protected int getDeviceDefaultOrientation(Display display, Configuration config) {

        int rotation = display.getRotation();

        if (((rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) &&
                config.orientation == Configuration.ORIENTATION_LANDSCAPE)
                || ((rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) &&
                config.orientation == Configuration.ORIENTATION_PORTRAIT)) {
            return Configuration.ORIENTATION_LANDSCAPE;
        } else {
            return Configuration.ORIENTATION_PORTRAIT;
        }
    }

    protected Size findPreviewSize(Size[] sizes, int width, int height) {

        Log.d(MainActivity.TAG, "Looking for preferred preview size, max width " + width + " max height " + height);

        Size prefSize = null;
        // for preview, find the smallest one closest to the display dimensions
        int smallestDiff = 4096 * 4096;

        for (Size size : sizes) {
            if ((size.getWidth() <= width) && (size.getHeight() <= height)) {
                int diffArea = Math.abs(size.getWidth() - width) * Math.abs(size.getHeight() - height);
                if (diffArea < smallestDiff) {
                    smallestDiff = diffArea;
                    prefSize = size;
                }
            }
        }

        return prefSize;

    }

    protected Size findPictureSize(Size[] sizes) {

        Log.d(MainActivity.TAG, "Looking for preferred picture size");

        Size prefSize = null;
        // use the largest available size for still pictures
        int largestArea = -1;

        for (Size size : sizes) {
            int area = size.getWidth() * size.getHeight();
            if (area > largestArea) {
                largestArea = area;
                prefSize = size;
            }
        }

        return prefSize;

    }

    protected int getCorrectRotation(int displayOrientation, int cameraOrientation) {
        // compensate for how the camera orientation relates to the display orientation
        int rotation = 0;
        if (__whichCamera == CameraCharacteristics.LENS_FACING_BACK) {
            if (cameraOrientation == 90) {
                if (displayOrientation == 0) {
                    rotation = 90;
                } else if (displayOrientation == 90) {
                    rotation = 0;
                } else if (displayOrientation == 270) {
                    rotation = 180;
                } else {
                    rotation = 270;
                }
            } else if (cameraOrientation == 270) {
                if (displayOrientation == 0) {
                    rotation = 270;
                } else if (displayOrientation == 90) {
                    rotation = 180;
                } else if (displayOrientation == 270) {
                    rotation = 0;
                } else {
                    rotation = 90;
                }
            } else if (cameraOrientation == 0) {
                if (displayOrientation == 0) {
                    rotation = 0;
                } else if (displayOrientation == 90) {
                    rotation = 270;
                } else if (displayOrientation == 270) {
                    rotation = 90;
                } else {
                    rotation = 180;
                }
            } else {
                if (displayOrientation == 0) {
                    rotation = 180;
                } else if (displayOrientation == 90) {
                    rotation = 90;
                } else if (displayOrientation == 270) {
                    rotation = 270;
                } else {
                    rotation = 0;
                }
            }
        } else {
            if (cameraOrientation == 90) {
                if (displayOrientation == 0) {
                    rotation = 90;
                } else if (displayOrientation == 90) {
                    rotation = 180;
                } else if (displayOrientation == 270) {
                    rotation = 0;
                } else {
                    rotation = 270;
                }
            } else if (cameraOrientation == 270) {
                if (displayOrientation == 0) {
                    rotation = 270;
                } else if (displayOrientation == 90) {
                    rotation = 0;
                } else if (displayOrientation == 270) {
                    rotation = 180;
                } else {
                    rotation = 90;
                }
            } else if (cameraOrientation == 0) {
                if (displayOrientation == 0) {
                    rotation = 0;
                } else if (displayOrientation == 90) {
                    rotation = 90;
                } else if (displayOrientation == 270) {
                    rotation = 270;
                } else {
                    rotation = 180;
                }
            } else {
                if (displayOrientation == 0) {
                    rotation = 180;
                } else if (displayOrientation == 90) {
                    rotation = 270;
                } else if (displayOrientation == 270) {
                    rotation = 90;
                } else {
                    rotation = 0;
                }
            }
        }
        /* (cameraOrientation - displayOrientation + 360) % 360 */
        Log.d(MainActivity.TAG, "getCorrectRotation returning " + rotation);
        return rotation;
    }

    protected boolean prepare(CameraCharacteristics characteristics, Display display, Configuration config) {

        boolean prepared = false;

        // find out about camera orientation relative to display orientation
        __cameraOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
        Log.d(MainActivity.TAG, "Camera orientation: " + __cameraOrientation);

        int defaultOrientation = getDeviceDefaultOrientation(display, config);
        Log.d(MainActivity.TAG, "Device default orientation: " +
                ((defaultOrientation == Configuration.ORIENTATION_LANDSCAPE) ? "landscape" : "portrait"));

        Size sensorSize = characteristics.get(CameraCharacteristics.SENSOR_INFO_PIXEL_ARRAY_SIZE);
        Log.d(MainActivity.TAG, "sensorSize width: " + sensorSize.getWidth() + " height: " + sensorSize.getHeight());

        float cameraRatio = ((float) (sensorSize.getWidth())) / ((float) (sensorSize.getHeight()));
        Log.d(MainActivity.TAG, "NATURAL CAMERA ASPECT RATIO: " + cameraRatio);

        SizeF physicalSize = characteristics.get(CameraCharacteristics.SENSOR_INFO_PHYSICAL_SIZE);
        Log.d(MainActivity.TAG, "physicalSize width: " + physicalSize.getWidth() + " height: " + physicalSize.getHeight());
        cameraRatio = ((float) (physicalSize.getWidth())) / ((float) (physicalSize.getHeight()));
        Log.d(MainActivity.TAG, "NATURAL CAMERA ASPECT RATIO: " + cameraRatio);

        Point displaySize = new Point();
        display.getSize(displaySize);
        Log.d(MainActivity.TAG, "Display size: " + displaySize.x + "x" + displaySize.y);

        __displayOrientation = getDisplayOrientation(display);
        Log.d(MainActivity.TAG, "Device orientation: " + __displayOrientation);

        boolean swappedDimensions = isCameraOrientationSwapped(__displayOrientation, __cameraOrientation);
        Log.d(MainActivity.TAG, "Swapped dimensions: " + swappedDimensions);

        // find out what preview configurations are available
        StreamConfigurationMap map = characteristics.get(
                CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

        if (map != null) {

            Log.d(MainActivity.TAG, "JPEG sizes");
            Size[] pictureSizes = map.getOutputSizes(ImageFormat.JPEG);
            for (Size size : pictureSizes) {
                // what sizes in JPEG format does the camera support?
                Log.d(MainActivity.TAG, size.getWidth() + "x" + size.getHeight());
            }

            Size pictureSize = findPictureSize(pictureSizes);

            if (pictureSize != null) {

                // Create a new reader for images of the desired size and format.
                /*width     int: The default width in pixels of the Images that this reader will produce.
                height      int: The default height in pixels of the Images that this reader will produce.
                format      int: The format of the Image that this reader will produce. This must be one of the ImageFormat
                            or PixelFormat constants. Note that not all formats are supported, like ImageFormat.NV21.
                maxImages   int: The maximum number of images the user will want to access simultaneously.
                            This should be as small as possible to limit memory use. Once maxImages Images are obtained by the user,
                            one of them has to be released before a new Image will become available for access
                            through acquireLatestImage() or acquireNextImage(). Must be greater than 0.*/
                __imageReader = ImageReader.newInstance(pictureSize.getWidth(), pictureSize.getHeight(),
                        ImageFormat.JPEG, 1);

                // Register a listener to be invoked when a new image becomes available from the ImageReader.
                /*listener	ImageReader.OnImageAvailableListener: The listener that will be run.
                handler     Handler: The handler on which the listener should be invoked, or null
                            if the listener should be invoked on the calling thread's looper.*/
                __imageReader.setOnImageAvailableListener(__imageReaderListener, __offscreenHandler);

            }

            Log.d(MainActivity.TAG, "Live preview sizes");
            Size[] previewSizes = map.getOutputSizes(SurfaceTexture.class);
            for (Size size : previewSizes) {
                // how about for live preview?
                Log.d(MainActivity.TAG, size.getWidth() + "x" + size.getHeight());
            }

            // landscape camera
            int maxWidth = displaySize.x;
            int maxHeight = displaySize.y;

            if (swappedDimensions) {
                // portrait camera
                maxWidth = displaySize.y;
                maxHeight = displaySize.x;
            }

            Size previewSize = findPreviewSize(previewSizes, maxWidth, maxHeight);

            int bufferWidth = previewSize.getWidth();
            int bufferHeight = previewSize.getHeight();

            int viewportWidth = previewSize.getWidth();
            int viewportHeight = previewSize.getHeight();

            if (swappedDimensions) {
                viewportWidth = previewSize.getHeight();
                viewportHeight = previewSize.getWidth();
            }

            if (previewSize != null) {

                // Preview size is relative to the camera coordinate system
                Log.d(MainActivity.TAG, "Picked preview size: " + previewSize.getWidth() + "x" + previewSize.getHeight());

                final CameraGLView glView = __glView;

                if (glView != null) {
                    int correctRotation = getCorrectRotation(__displayOrientation, __cameraOrientation);
                    glView.setGeometry(correctRotation, bufferWidth, bufferHeight,
                            viewportWidth, viewportHeight, displaySize.x, displaySize.y,
                            __whichCamera == CameraCharacteristics.LENS_FACING_FRONT);
                    /*ViewGroup.LayoutParams layoutParams = glView.getLayoutParams();
                    layoutParams.width = displaySize.x;
                    layoutParams.height = displaySize.y;
                    glView.setLayoutParams(layoutParams);
                    glView.invalidate();
                    glView.requestLayout();*/
                    prepared = true;
                }

            }
        }

        return prepared;
    }

    protected void initCamera() {

        final Activity activity = getActivity();

        if (activity != null) {

            if (ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                CameraManager cameraManager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);

                if (cameraManager != null) {

                    try {

                        // TODO: this should be done in onCreate - decide which ones
                        // there can be more than one camera of the same facing type!
                        String[] cameraIds = cameraManager.getCameraIdList();
                        ArrayList<String> matchedIds = new ArrayList<>();

                        for (String cameraId : cameraManager.getCameraIdList()) {
                            CameraCharacteristics characteristics
                                    = cameraManager.getCameraCharacteristics(cameraId);
                            // which way is the camera facing?
                            Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                            if (facing != null) {
                                if (facing == __whichCamera) {
                                    matchedIds.add(cameraId);
                                }
                            }
                        }

                        // TODO: disable the switch button if there's only one camera
                        String cameraId = matchedIds.get(0);

                        // for (String cameraId : cameraManager.getCameraIdList()) {

                        CameraCharacteristics characteristics
                                = cameraManager.getCameraCharacteristics(cameraId);
                        Display display = activity.getWindowManager().getDefaultDisplay();
                        Configuration config = getResources().getConfiguration();

                        // which way is the camera facing?
                        Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                        if (facing != null) {

                            if (facing == __whichCamera) {

                                Log.d(MainActivity.TAG, "facing " + facing + " whichCamera " + __whichCamera);

                                if (prepare(characteristics, display, config)) {

                                    if (facing == CameraCharacteristics.LENS_FACING_FRONT) {
                                        Log.d(MainActivity.TAG, "LENS_FACING_FRONT");
                                    } else if (facing == CameraCharacteristics.LENS_FACING_BACK) {
                                        Log.d(MainActivity.TAG, "LENS_FACING_BACK");
                                    }
                                    Log.d(MainActivity.TAG, "Opening camera " + cameraId);

                                    // Open a connection to a camera with the given ID.
                                        /*cameraId	String: The unique identifier of the camera device to open
                                        callback	CameraDevice.StateCallback: The callback which is invoked once the camera is opened
                                        handler     Handler: The handler on which the callback should be invoked,
                                                    or null to use the current thread's looper.*/
                                    cameraManager.openCamera(cameraId, new CameraDevice.StateCallback() {
                                        @Override
                                        public void onOpened(@NonNull CameraDevice camera) {
                                            // The method called when a camera device has finished opening.
                                            Log.d(MainActivity.TAG, "Camera opened");
                                            __cameraDevice = camera;
                                            createCameraPreviewSession();
                                        }

                                        @Override
                                        public void onDisconnected(@NonNull CameraDevice camera) {
                                            // The method called when a camera device is no longer available for use.
                                            camera.close();
                                        }

                                        @Override
                                        public void onError(@NonNull CameraDevice camera, int error) {
                                            // The method called when a camera device has encountered an error.
                                            camera.close();
                                        }

                                        @Override
                                        public void onClosed(@NonNull CameraDevice camera) {
                                            __firstTimestamp = 0;
                                            __firstPreviewSignalled = false;
                                            Log.d(MainActivity.TAG, "Posting camera closed");
                                            __processorThread.post(ProcessorEvent.CAMERA_CLOSED);
                                        }
                                    }, __offscreenHandler);

                                }

                            }
                        }

                        // }

                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

        }

    }

    protected void teardownCamera() {
        Log.d(MainActivity.TAG, "teardownCamera");
        if (__captureSession != null) {
            __captureSession.close();
        }
        if (__cameraDevice != null) {
            __cameraDevice.close();
        }
        if (__imageReader != null) {
            __imageReader.close();
        }
    }

    protected void lockFocus() {

        Log.d(MainActivity.TAG, "Start lockFocus");

        try {
            // CONTROL_AF_TRIGGER - Whether the camera device will trigger autofocus for this request
            // CONTROL_AF_TRIGGER_START - Autofocus will trigger now
            __requestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_START);
            /*
            Submit a request for an image to be captured by the camera device.
            request     CaptureRequest: the settings for this capture
            listener	CameraCaptureSession.CaptureCallback: The callback object to notify once
                        this request has been processed. If null, no metadata will be produced
                        for this capture, although image data will still be produced.
            handler     Handler: the handler on which the listener should be invoked,
                        or null to use the current thread's looper.
            */
            __captureSession.capture(__requestBuilder.build(), new CameraCaptureSession.CaptureCallback() {

                        @Override
                        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                                        @NonNull CaptureRequest request,
                                                        @NonNull CaptureResult partialResult) {
                            // This callback will always fire after the last onCaptureProgressed(CameraCaptureSession, CaptureRequest, CaptureResult);
                            // in other words, no more partial results will be delivered once the completed result is available.
                            super.onCaptureProgressed(session, request, partialResult);
                        }

                        @Override
                        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                                       @NonNull CaptureRequest request,
                                                       @NonNull TotalCaptureResult result) {
                            // This method is called when an image capture has fully completed and all the result metadata is available.
                            super.onCaptureCompleted(session, request, result);
                            Log.d(MainActivity.TAG, "lockFocus complete");
                            captureStillPicture();
                        }

                        @Override
                        public void onCaptureFailed(@NonNull CameraCaptureSession session,
                                                    @NonNull CaptureRequest request,
                                                    @NonNull CaptureFailure failure) {
                            // This method is called instead of onCaptureCompleted(CameraCaptureSession, CaptureRequest, TotalCaptureResult)
                            // when the camera device failed to produce a CaptureResult for the request.
                            super.onCaptureFailed(session, request, failure);
                        }

                    },
                    __offscreenHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    protected void captureStillPicture() {

        Log.d(MainActivity.TAG, "Start captureStillPicture");

        try {
            if ((__cameraDevice != null) && (__captureSession != null) && (__imageReader != null)) {

                // Create a CaptureRequest.Builder for new capture requests, initialized with template for a target use case.
                // templateType     int: An enumeration selecting the use case for this request;
                //                  one of the CameraDevice.TEMPLATE_ values.
                // TEMPLATE_STILL_CAPTURE   Create a request suitable for still image capture.
                final CaptureRequest.Builder captureBuilder =
                        __cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);

                if (captureBuilder != null) {

                    // Add a surface to the list of targets for this request
                    // The Surface added must be one of the surfaces included in the most recent
                    // call to createCaptureSession(List, CameraCaptureSession.StateCallback, Handler),
                    // when the request is given to the camera device.
                    // Get a Surface that can be used to produce Images for this ImageReader.
                    captureBuilder.addTarget(__imageReader.getSurface());

                    // CONTROL_AF_MODE_CONTINUOUS_PICTURE In this mode, the AF algorithm modifies the lens position
                    // continually to attempt to provide a constantly-in-focus image stream.
                    captureBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                            CameraMetadata.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

                    // JPEG_ORIENTATION The orientation for a JPEG image.
                    captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, getCorrectRotation(__displayOrientation, __cameraOrientation));

                    // Cancel any ongoing repeating capture set by either setRepeatingRequest
                    // or setRepeatingBurst(List, CameraCaptureSession.CaptureCallback, Handler).
                    // Has no effect on requests submitted through capture or captureBurst.
                    __captureSession.stopRepeating();

                    /*
                    Submit a request for an image to be captured by the camera device.
                    request     CaptureRequest: the settings for this capture
                    listener	CameraCaptureSession.CaptureCallback: The callback object to notify once
                                this request has been processed. If null, no metadata will be produced
                                for this capture, although image data will still be produced.
                    handler     Handler: the handler on which the listener should be invoked,
                                or null to use the current thread's looper.
                    */
                    __captureSession.capture(captureBuilder.build(), new CameraCaptureSession.CaptureCallback() {
                        @Override
                        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                                       @NonNull CaptureRequest request,
                                                       @NonNull TotalCaptureResult result) {
                            Log.d(MainActivity.TAG, "captureStillPicture complete");
                            unlockFocus();
                        }
                    }, __offscreenHandler);
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    protected void unlockFocus() {

        Log.d(MainActivity.TAG, "Start unlockFocus");

        try {
            // CONTROL_AF_TRIGGER - Whether the camera device will trigger autofocus for this request
            // CONTROL_AF_TRIGGER_CANCEL - Autofocus will return to its initial state, and cancel any currently active trigger.
            __requestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
            /*
            Submit a request for an image to be captured by the camera device.
            request     CaptureRequest: the settings for this capture
            listener	CameraCaptureSession.CaptureCallback: The callback object to notify once
                        this request has been processed. If null, no metadata will be produced
                        for this capture, although image data will still be produced.
            handler     Handler: the handler on which the listener should be invoked,
                        or null to use the current thread's looper.
            */
            __captureSession.capture(__requestBuilder.build(), new CameraCaptureSession.CaptureCallback() {

                        @Override
                        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                                        @NonNull CaptureRequest request,
                                                        @NonNull CaptureResult partialResult) {
                            // This callback will always fire after the last onCaptureProgressed(CameraCaptureSession, CaptureRequest, CaptureResult);
                            // in other words, no more partial results will be delivered once the completed result is available.
                            super.onCaptureProgressed(session, request, partialResult);
                        }

                        @Override
                        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                                       @NonNull CaptureRequest request,
                                                       @NonNull TotalCaptureResult result) {
                            // This method is called when an image capture has fully completed and all the result metadata is available.
                            super.onCaptureCompleted(session, request, result);
                            Log.d(MainActivity.TAG, "unlockFocus complete");
                        }

                        @Override
                        public void onCaptureFailed(@NonNull CameraCaptureSession session,
                                                    @NonNull CaptureRequest request,
                                                    @NonNull CaptureFailure failure) {
                            // This method is called instead of onCaptureCompleted(CameraCaptureSession, CaptureRequest, TotalCaptureResult)
                            // when the camera device failed to produce a CaptureResult for the request.
                            super.onCaptureFailed(session, request, failure);
                        }

                    },
                    __offscreenHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        Log.d(MainActivity.TAG, "Fragment onResume");
        super.onResume();
        __processorThread.post(ProcessorEvent.VIEW_ONSCREEN);
    }

    @Override
    public void onPause() {
        Log.d(MainActivity.TAG, "Fragment onPause activity finishing? " + getActivity().isFinishing());
        super.onPause();
    }

    public void pauseProcessorThread() {
        if (__processorThread.isSwitching()) {
            __processorThread.waitUntilInState(ProcessorState.STATE_RUNNING);
        }
        __processorThread.post(ProcessorEvent.VIEW_OFFSCREEN);
        __processorThread.post(ProcessorEvent.RENDERER_UNAVAILABLE);
        __processorThread.waitUntilInState(ProcessorState.STATE_PAUSED);
        __processorThread.reset();
    }

    @Override
    public void onDestroy() {
        if (__saveFileThread != null) {
            try {
                Log.d(MainActivity.TAG, "Waiting for the save file thread to finish");
                __saveFileThread.join();
                Log.d(MainActivity.TAG, "Done waiting for the save file thread to finish");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (__processorThread != null) {
            __processorThread.post(ProcessorEvent.INTERRUPT);
            try {
                Log.d(MainActivity.TAG, "Waiting for the processor thread to finish");
                __processorThread.join();
                Log.d(MainActivity.TAG, "Waiting for the processor thread to finish");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (__offscreenThread != null) {
            try {
                Log.d(MainActivity.TAG, "Fragment waiting for the offscreen thread to finish");
                // quit the thread Runnable loop
                __offscreenThread.quit();
                // finish off the last Runnable queued
                __offscreenThread.join();
                Log.d(MainActivity.TAG, "Fragment done waiting for the offscreen thread");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        super.onDestroy();
    }

    public void setDestDir(Uri destDir) {
        __destDir = destDir;
    }

}
