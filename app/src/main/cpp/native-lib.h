#ifndef CLARITYAPP_NATIVE_LIB_H
#define CLARITYAPP_NATIVE_LIB_H

#include <jni.h>

#include <android/log.h>
#include <android/bitmap.h>
#include <android/native_window.h>
#include <android/native_window_jni.h>

#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES3/gl3.h>
#include <GLES3/gl3ext.h>

#define __gl2_h_

#include <GLES2/gl2ext.h>

#include <cassert>
#include <cinttypes>
#include <cstdlib>

#include <dirent.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_processBitmap(JNIEnv *env,
                                                                         jobject instance,
                                                                         jobject bitmapIn,
                                                                         jobject bitmapOut);

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_initialiseGLES(JNIEnv *env,
                                                                          jobject instance,
                                                                          jobject windowSurface);

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_loadAssets(JNIEnv *env, jobject instance,
                                                                      jobject assetManager);

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_processBitmapGLES(JNIEnv *env,
                                                                             jobject instance,
                                                                             jobject bitmapIn,
                                                                             jobject bitmapOut);

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_attachToEGLContext(JNIEnv *env,
                                                                              jobject instance);

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_detachFromEGLContext(JNIEnv *env,
                                                                                jobject instance);

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_terminateGLES(JNIEnv *env,
                                                                         jobject instance);

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_signalProcessingInterrupt(JNIEnv *env,
                                                                                     jobject instance);

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_processBitmapCPU(JNIEnv *env,
                                                                            jobject instance,
                                                                            jobject bitmapIn,
                                                                            jobject bitmapOut);

JNIEXPORT jlong JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_getProcessingTime(JNIEnv *env,
                                                                             jobject instance);

#ifdef __cplusplus
}
#endif

#endif //CLARITYAPP_NATIVE_LIB_H
