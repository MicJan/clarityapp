#include <android/bitmap.h>
#include "native-lib.h"

// Clarity includes
#include "Bitmap.hpp"
#include "phusion.h"
#include "Conversion.hpp"
#include "Convolution.hpp"
#include "GradientCalcs.hpp"
#include "Utils.hpp"

#if 1

#define FLOG(...) \
    ((void)__android_log_print(ANDROID_LOG_DEBUG, "ClarityApp", __VA_ARGS__))

#else

#define FLOG(...)                                                        \
    do {                                                                 \
        FILE *f = fopen("/storage/emulated/0/log.txt", "a+");            \
        if (f != NULL) {                                                 \
            time_t rawtime;                                              \
            struct tm *timeinfo;                                         \
            char buf[255] = {0};                                         \
            time(&rawtime);                                              \
            timeinfo = localtime(&rawtime);                              \
            strftime(buf, 255, "%F %T.%S", timeinfo);                    \
            fprintf(f, "%s\t", buf);                                     \
            fprintf(f, __VA_ARGS__);                                     \
            fprintf(f, "\t%s\t%s:%d", __FUNCTION__, __FILE__, __LINE__); \
            fprintf(f, "\n");                                            \
            fflush(f);                                                   \
            fclose(f);                                                   \
        }                                                                \
    } while (0)

#endif

AAssetManager *gAssetManager = NULL;

ANativeWindow *gWindow = NULL;
// off-screen window surface
EGLDisplay gEglDisplay = EGL_NO_DISPLAY;
EGLSurface gEglSurface = EGL_NO_SURFACE;
EGLContext gEglContext = EGL_NO_CONTEXT;
EGLint gScreenWidth = 0;
EGLint gScreenHeight = 0;
// GLES programs
GLuint gProgram = 0;
GLuint gProgram2 = 0;
GLuint gProgram3 = 0;

GLuint gFBO = 0;
GLuint gFBOTex = 0;
GLuint gBitmapWidth = 0;
GLuint gBitmapHeight = 0;
GLuint gImgInTex = 0;

// UBO for cStretchLUT
#define LUT_BLOCK_NAME "LUTData"
GLuint gLUTUBO = 0;
GLint gLUTUBOSize = 0;
GLuint gLUTUBOIndex = 0;
#define NUM_UNIFORM_INDICES 1
GLuint gLUTUBOIndices[NUM_UNIFORM_INDICES] = {0};
GLint gLUTUBOSizes[NUM_UNIFORM_INDICES] = {0};
GLint gLUTUBOOffsets[NUM_UNIFORM_INDICES] = {0};
GLint gLUTUBOTypes[NUM_UNIFORM_INDICES] = {0};
#define LUT_BLOCK_ARR_NAME "cStretchLUT"
const char *gLUTUBONames[NUM_UNIFORM_INDICES] = {LUT_BLOCK_ARR_NAME};
#define LUT_BLOCK_BINDING 0

// UBO for alphaLUT
#define LUT_BLOCK_NAME2 "LUTData2"
GLuint gLUTUBO2 = 0;
GLint gLUTUBOSize2 = 0;
GLuint gLUTUBOIndex2 = 0;
#define NUM_UNIFORM_INDICES2 1
GLuint gLUTUBOIndices2[NUM_UNIFORM_INDICES2] = {0};
GLint gLUTUBOSizes2[NUM_UNIFORM_INDICES2] = {0};
GLint gLUTUBOOffsets2[NUM_UNIFORM_INDICES2] = {0};
GLint gLUTUBOTypes2[NUM_UNIFORM_INDICES2] = {0};
#define LUT_BLOCK_ARR_NAME2 "alphaLUT"
const char *gLUTUBONames2[NUM_UNIFORM_INDICES2] = {LUT_BLOCK_ARR_NAME2};
#define LUT_BLOCK_BINDING2 1

// the below are float LUTs
float gCStretchLUT[1024] = {0};
float gAlphaLUT[1024] = {0};

GLfloat gVertices[] = {-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f};
GLfloat gTexCoords[] = {0, 1.0f, 0, 0, 1.0f, 0.0f, 1.0f, 1.0f};

int64_t gLastProcessTimeUs = -1;
bool interruptFlag = false;

#define STR(s) #s
#define STRV(s) STR(s)

#define ATTRIBUTE_POSITION 0
#define ATTRIBUTE_TEXCOORD 1
#define UNIFORM_EXTSAMPLER 0

#define ATTRIBUTE_POSITION2 0
#define ATTRIBUTE_TEXCOORD2 1
#define UNIFORM_EXTSAMPLER2 0
#define UNIFORM_SAMPLER2_1 1
#define UNIFORM_SAMPLER2_2 2
#define UNIFORM_SAMPLER2_3 3
#define UNIFORM_SAMPLER2_4 4
#define UNIFORM_MIN2B 1

#define ATTRIBUTE_POSITION3 0
#define ATTRIBUTE_TEXCOORD3 1
#define UNIFORM_SAMPLER3 0

static const char VERTEX_SHADER[] =
        "#version 300 es\n"
                "layout(location = " STRV(ATTRIBUTE_POSITION) ") in vec2 pos;\n"
                "layout(location = " STRV(ATTRIBUTE_TEXCOORD) ") in vec2 texCoord;\n"
                "out vec2 vTexCoord;\n"
                "void main() {\n"
                "   gl_Position = vec4(pos, 0.0, 1.0);\n"
                "   vTexCoord = texCoord;\n"
                "}\n";

static const char FRAGMENT_SHADER[] =
        "#version 300 es\n"
                "#extension GL_OES_EGL_image_external : require\n"
                "#extension GL_OES_EGL_image_external_essl3 : enable\n"
                "precision highp float;\n"
                "precision highp samplerExternalOES;\n"
                "layout(location = " STRV(UNIFORM_EXTSAMPLER) ") uniform samplerExternalOES tex;\n"
                "in vec2 vTexCoord;\n"
                "out vec4 outColor;\n"
                "void main() {\n"
                "    outColor = texture(tex, vTexCoord);\n"
                "}\n";

static const char VERTEX_SHADER2[] =
        "#version 300 es\n"
                "layout(location = " STRV(ATTRIBUTE_POSITION2) ") in vec2 pos;\n"
                "layout(location = " STRV(ATTRIBUTE_TEXCOORD2) ") in vec2 texCoord;\n"
                "out vec2 vTexCoord;\n"
                "void main() {\n"
                "   gl_Position = vec4(pos, 0.0, 1.0);\n"
                // "   vTexCoord = texCoord;\n"
                // flip vertically for FBO
                "   vTexCoord = vec2(texCoord.x, 1.0 - texCoord.y);\n"
                "}\n";

static const char FRAGMENT_SHADER2[] =
        "#version 300 es\n"
                "#extension GL_OES_EGL_image_external : require\n"
                "#extension GL_OES_EGL_image_external_essl3 : enable\n"
                "precision highp float;\n"
                "precision highp samplerExternalOES;\n"
                "precision highp sampler2D;\n"
                // input image
                "layout(location = " STRV(UNIFORM_EXTSAMPLER2) ") uniform samplerExternalOES tex;\n"
                // the 'out' from the simpleIntegrationIV step
                "layout(location = " STRV(UNIFORM_SAMPLER2_1) ") uniform sampler2D tex1;\n"
                // muL
                "layout(location = " STRV(UNIFORM_SAMPLER2_2) ") uniform sampler2D tex2;\n"
                // maxL
                "layout(location = " STRV(UNIFORM_SAMPLER2_3) ") uniform sampler2D tex3;\n"
                // lum
                "layout(location = " STRV(UNIFORM_SAMPLER2_4) ") uniform sampler2D tex4;\n"
                "in vec2 vTexCoord;\n"
                "out vec4 outColor;\n"
                "layout(std140) uniform " LUT_BLOCK_NAME "\n"
                "{\n"
                "   float " LUT_BLOCK_ARR_NAME "[1024];\n"
                "};\n"
                "layout(std140) uniform " LUT_BLOCK_NAME2 "\n"
                "{\n"
                "   float " LUT_BLOCK_ARR_NAME2 "[1024];\n"
                "};\n"

                /*cStretchFact = min(strength*1.5,1);
                out = cstretch(clamp(out)) * cStretchFact + clamp(out) * (1-cStretchFact);

                alpha = alphacurve(clamp(out), 0.5);
                oppOut = alpha.*lum + (1-alpha).*clamp(out);

                mult=(-muL + sqrt(muL.^2+4.*(maxL.^2-maxL.*muL).*(oppOut)))./(2.*(maxL.^2-maxL.*muL));
                % imgOut = bsxfun(@times, imgIn, oppOut ./ max(lum,1e-5));

                imgOut = bsxfun(@times, imgIn, mult);*/

                "void main() {\n"
                "   vec4 imgIn = texture(tex, vTexCoord);\n"
                "   vec4 outCol = texture(tex1, vTexCoord);\n"
                "   vec4 muLCol = texture(tex2, vTexCoord);\n"
                "   vec4 maxLCol = texture(tex3, vTexCoord);\n"
                "   vec4 lumCol = texture(tex4, vTexCoord);\n"
                "   vec4 min = vec4(0.0, 0.0, 0.0, 1.0);\n"
                "   vec4 max = vec4(1.0, 1.0, 1.0, 1.0);\n"
                "   outCol = clamp(outCol, min, max);\n"
                "   const float cStretchFact = 0.45;\n"
                "   int indexOut = int(floor(outCol.r * 1023.0));\n"
                "   float stretched = " LUT_BLOCK_ARR_NAME "[indexOut] * cStretchFact + outCol.r * (1.0 - cStretchFact);\n"
                "   stretched = clamp(stretched, 0.0, 1.0);\n"
                "   int indexStretched = int(floor(stretched * 1023.0));\n"
                "   float alpha = " LUT_BLOCK_ARR_NAME2 "[indexStretched] * lumCol.r + (1.0 - " LUT_BLOCK_ARR_NAME2 "[indexStretched]) * stretched;\n"
                "   float sqrtVal = sqrt(muLCol.r * muLCol.r + 4.0 * (maxLCol.r * maxLCol.r - maxLCol.r * muLCol.r) * alpha);\n"
                "   float mult = (-muLCol.r + sqrtVal) / (2.0 * (maxLCol.r * maxLCol.r - maxLCol.r * muLCol.r));\n"
                "   vec4 imgOut = mult * imgIn;\n"
                "    outColor = vec4(imgOut.rgb, 1.0);\n"
                "}\n";

static const char FRAGMENT_SHADER2B[] =
        "#version 300 es\n"
                "#extension GL_OES_EGL_image_external : require\n"
                "#extension GL_OES_EGL_image_external_essl3 : enable\n"
                "precision highp float;\n"
                "precision highp sampler2D;\n"
                // input image
                "layout(location = " STRV(UNIFORM_EXTSAMPLER2) ") uniform sampler2D tex;\n"
                "layout(location = " STRV(UNIFORM_MIN2B) ") uniform float minMaxL;\n"
                "in vec2 vTexCoord;\n"
                "out vec4 outColor;\n"
                "layout(std140) uniform " LUT_BLOCK_NAME "\n"
                "{\n"
                "   float " LUT_BLOCK_ARR_NAME "[1024];\n"
                "};\n"
                "layout(std140) uniform " LUT_BLOCK_NAME2 "\n"
                "{\n"
                "   float " LUT_BLOCK_ARR_NAME2 "[1024];\n"
                "};\n"

                /*maxL = max(imgIn,[],3);
                muL  = mean(imgIn,3);
                lum = maxL.*maxL + (1-maxL).*muL;

                maxL = maxL + min(maxL(maxL(:)>0));*/

                // apply the below steps to imgIn, until the library is ready

                /*cStretchFact = min(strength*1.5,1);
                out = cstretch(clamp(out)) * cStretchFact + clamp(out) * (1-cStretchFact);

                alpha = alphacurve(clamp(out), 0.5);
                oppOut = alpha.*lum + (1-alpha).*clamp(out);

                mult=(-muL + sqrt(muL.^2+4.*(maxL.^2-maxL.*muL).*(oppOut)))./(2.*(maxL.^2-maxL.*muL));
                % imgOut = bsxfun(@times, imgIn, oppOut ./ max(lum,1e-5));

                imgOut = bsxfun(@times, imgIn, mult);*/

                "void main() {\n"
                "   vec4 imgIn = texture(tex, vTexCoord);\n"
                "   float maxL = max(max(imgIn.r, imgIn.g), imgIn.b);\n"
                "   maxL = maxL + minMaxL;\n"
                "   float muL = (imgIn.r + imgIn.g + imgIn.b) / 3.0;\n"
                "   float lum = maxL * maxL + (1.0 - maxL) * muL;\n"
                "   const float cStretchFact = 0.45;\n"
                "   int indexOut = int(floor(clamp(muL, 0.0, 1.0) * 1023.0));\n"
                "   float stretched = " LUT_BLOCK_ARR_NAME "[indexOut] * cStretchFact + lum * (1.0 - cStretchFact);\n"
                "   stretched = clamp(stretched, 0.0, 1.0);\n"
                "   int indexStretched = int(floor(stretched * 1023.0));\n"
                "   float alpha = " LUT_BLOCK_ARR_NAME2 "[indexStretched] * lum + (1.0 - " LUT_BLOCK_ARR_NAME2 "[indexStretched]) * stretched;\n"
                "   float sqrtVal = sqrt(muL * muL + 4.0 * (maxL * maxL - maxL * muL) * alpha);\n"
                "   float mult = (-muL + sqrtVal) / (2.0 * (maxL * maxL - maxL * muL));\n"
                "   vec4 imgOut = mult * imgIn;\n"
                "   outColor = vec4(imgOut.bgr, 1.0);\n"
                // "   outColor = vec4(imgOut.r, imgOut.g, imgOut.b, 1.0);\n"
                // "   outColor = vec4(maxL, maxL, maxL, 1.0);\n"
                // "   outColor = vec4(alpha, alpha, alpha, 1.0);\n"
                "}\n";

static const char VERTEX_SHADER3[] =
        "#version 300 es\n"
                "layout(location = " STRV(ATTRIBUTE_POSITION3) ") in vec2 pos;\n"
                "layout(location = " STRV(ATTRIBUTE_TEXCOORD3) ") in vec2 texCoord;\n"
                "out vec2 vTexCoord;\n"
                "void main() {\n"
                "   gl_Position = vec4(pos, 0.0, 1.0);\n"
                // flip vertically for FBO
                "   vTexCoord = vec2(texCoord.x, 1.0 - texCoord.y);\n"
                "}\n";

static const char FRAGMENT_SHADER3[] =
        "#version 300 es\n"
                "precision highp float;\n"
                "precision highp sampler2D;\n"
                "layout(location = " STRV(UNIFORM_SAMPLER3) ") uniform sampler2D tex;\n"
                "in vec2 vTexCoord;\n"
                "out vec4 outColor;\n"
                "void main() {\n"
                "    outColor = texture(tex, vTexCoord);\n"
                "}\n";


uint16_t outLUT[0x100] = {0};
double alphaLUT[0x400] = {0};

const char *eglStrToErr(EGLint err) {
    switch (err) {
        case EGL_SUCCESS:
            return "EGL_SUCCESS";
        case EGL_NOT_INITIALIZED:
            return "EGL_NOT_INITIALIZED";
        case EGL_BAD_ACCESS:
            return "EGL_BAD_ACCESS";
        case EGL_BAD_ALLOC:
            return "EGL_BAD_ALLOC";
        case EGL_BAD_ATTRIBUTE:
            return "EGL_BAD_ATTRIBUTE";
        case EGL_BAD_CONFIG:
            return "EGL_BAD_CONFIG";
        case EGL_BAD_CONTEXT:
            return "EGL_BAD_CONTEXT";
        case EGL_BAD_CURRENT_SURFACE:
            return "EGL_BAD_CURRENT_SURFACE";
        case EGL_BAD_DISPLAY:
            return "EGL_BAD_DISPLAY";
        case EGL_BAD_MATCH:
            return "EGL_BAD_MATCH";
        case EGL_BAD_NATIVE_PIXMAP:
            return "EGL_BAD_NATIVE_PIXMAP";
        case EGL_BAD_NATIVE_WINDOW:
            return "EGL_BAD_NATIVE_WINDOW";
        case EGL_BAD_PARAMETER:
            return "EGL_BAD_PARAMETER";
        case EGL_BAD_SURFACE:
            return "EGL_BAD_SURFACE";
        case EGL_CONTEXT_LOST:
            return "EGL_CONTEXT_LOST";
        default:
            return "UNKNOWN";
    }
}

void checkEGLError(const char *op, EGLBoolean returnVal = EGL_TRUE) {
    if (returnVal != EGL_TRUE) {
        FLOG("%s() returned %d", op, returnVal);
    }

    for (EGLint error = eglGetError(); error != EGL_SUCCESS;
         error = eglGetError()) {
        FLOG("after %s() eglError %s (0x%x)", op, eglStrToErr(error), error);
    }
}

void checkGLError(const char *op) {
    for (GLint error = glGetError(); error; error = glGetError()) {
        FLOG("after %s() glError (0x%x)", op, error);
    }
}

GLuint loadShader(GLenum shaderType, const char *pSource) {
    GLuint shader = glCreateShader(shaderType);
    if (shader) {
        glShaderSource(shader, 1, &pSource, NULL);
        glCompileShader(shader);
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint infoLen = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
            if (infoLen) {
                char *buf = (char *) malloc(infoLen);
                if (buf) {
                    glGetShaderInfoLog(shader, infoLen, NULL, buf);
                    FLOG("Could not compile shader %d:\n%s", shaderType, buf);
                    free(buf);
                }
                glDeleteShader(shader);
                shader = 0;
            }
        }
    }
    return shader;
}

GLuint createProgram(const char *pVertexSource, const char *pFragmentSource) {
    GLuint vertexShader = loadShader(GL_VERTEX_SHADER, pVertexSource);
    if (!vertexShader) {
        return 0;
    }

    GLuint pixelShader = loadShader(GL_FRAGMENT_SHADER, pFragmentSource);
    if (!pixelShader) {
        return 0;
    }

    GLuint program = glCreateProgram();
    if (program) {
        glAttachShader(program, vertexShader);
        checkGLError("glAttachShader");
        glAttachShader(program, pixelShader);
        checkGLError("glAttachShader");
        glLinkProgram(program);
        GLint linkStatus = GL_FALSE;
        glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
        if (linkStatus != GL_TRUE) {
            GLint bufLength = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &bufLength);
            if (bufLength) {
                char *buf = (char *) malloc(bufLength);
                if (buf) {
                    glGetProgramInfoLog(program, bufLength, NULL, buf);
                    FLOG("Could not link program:\n%s", buf);
                    free(buf);
                }
            }
            glDeleteProgram(program);
            program = 0;
        }
    }
    return program;
}

void loadLUTs() {
    if (gAssetManager != NULL) {

        AAsset *asset =
                AAssetManager_open(gAssetManager, "cstretch.bin", 0);

        if (asset != NULL) {

            unsigned int numDims = 0;
            unsigned int width = 0;
            unsigned int height = 0;

            AAsset_read(asset, &numDims, sizeof(numDims));
            FLOG("numDims: %d", numDims);

            AAsset_read(asset, &width, sizeof(width));
            FLOG("width: %d", width);

            AAsset_read(asset, &height, sizeof(height));
            FLOG("height: %d", height);

            AAsset_read(asset, gCStretchLUT, sizeof(float) * width * height);

            AAsset_close(asset);
        }

        asset = AAssetManager_open(gAssetManager, "alpha.bin", 0);

        if (asset != NULL) {

            unsigned int numDims = 0;
            unsigned int width = 0;
            unsigned int height = 0;

            AAsset_read(asset, &numDims, sizeof(numDims));
            FLOG("numDims: %d", numDims);

            AAsset_read(asset, &width, sizeof(width));
            FLOG("width: %d", width);

            AAsset_read(asset, &height, sizeof(height));
            FLOG("height: %d", height);

            AAsset_read(asset, gAlphaLUT, sizeof(float) * width * height);

            AAsset_close(asset);
        }

    }

}

void loadBitmap(unsigned char *dest, unsigned char *bitmapBits, AndroidBitmapInfo &bitmapInfo) {

    // In the case of AndroidBitmaps, stride is the number of bytes per row
    // contrast this with ANativeWindow_Buffer where stride is the number of pixels
    // assume data is ANDROID_BITMAP_FORMAT_RGBA_8888

    int width = bitmapInfo.width;
    int height = bitmapInfo.height;
    int bStride = bitmapInfo.stride / 4;
    int dataBPP = 4;

    // assume destination buffer is RGBA8888
    // or check if buf.format is WINDOW_FORMAT_RGBA_8888
    for (int i = 0; i < height; i++) {
        unsigned char *bufLine = (unsigned char *) dest + i * width * dataBPP;
        unsigned char *dataLine = (unsigned char *) bitmapBits + i * bStride * dataBPP;
        for (int j = 0; j < width; j++) {
            // RGBA 8888 both source and destination
            *bufLine++ = *dataLine++;
            *bufLine++ = *dataLine++;
            *bufLine++ = *dataLine++;
            *bufLine++ = *dataLine++;
        }
    }
}

void
loadAndroidBitmapIntoClarityBitmap(void *source, AndroidBitmapInfo &bitmapInfo, Bitmap &cBitmap) {

    // In the case of AndroidBitmaps, stride is the number of bytes per row

    int width = bitmapInfo.width;
    int height = bitmapInfo.height;
    int bStride = bitmapInfo.stride;

    for (int i = 0; i < height; i++) {
        unsigned char *srcLine = (unsigned char *) source + i * bStride;
        unsigned char *dstLine = (unsigned char *) cBitmap.GetRow(i);
        for (int j = 0; j < width; j++) {
            // RGBA 8888 source
            // RGB 888 destination
            *dstLine++ = *srcLine++;
            *dstLine++ = *srcLine++;
            *dstLine++ = *srcLine++;
            srcLine++; // skip alpha
        }
    }

}

void
loadClarityBitmapIntoAndroidBitmap(Bitmap &cBitmap, void *dest, AndroidBitmapInfo &bitmapInfo) {

    // In the case of AndroidBitmaps, stride is the number of bytes per row

    int width = bitmapInfo.width;
    int height = bitmapInfo.height;
    int bStride = bitmapInfo.stride;

    for (int i = 0; i < height; i++) {
        unsigned char *srcLine = (unsigned char *) cBitmap.GetRow(i);
        unsigned char *dstLine = (unsigned char *) dest + i * bStride;
        for (int j = 0; j < width; j++) {
            // RGBA 8888 destination
            // RGB 888 source
            *dstLine++ = *srcLine++;
            *dstLine++ = *srcLine++;
            *dstLine++ = *srcLine++;
            *dstLine++ = 0xFF; // no alpha in the clarity bitmap
        }
    }

}

void outputBitmap(unsigned char *src, unsigned char *bitmapBits, AndroidBitmapInfo &bitmapInfo) {

    // In the case of AndroidBitmaps, stride is the number of bytes per row
    // contrast this with ANativeWindow_Buffer where stride is the number of pixels
    // assume data is ANDROID_BITMAP_FORMAT_RGBA_8888

    int width = bitmapInfo.width;
    int height = bitmapInfo.height;
    int bStride = bitmapInfo.stride / 4;
    int dataBPP = 4;

    // assume destination buffer is RGBA8888
    // or check if buf.format is WINDOW_FORMAT_RGBA_8888
    for (int i = 0; i < height; i++) {
        unsigned char *bufLine = (unsigned char *) src + i * width * dataBPP;
        unsigned char *dataLine = (unsigned char *) bitmapBits + i * bStride * dataBPP;
        for (int j = 0; j < width; j++) {
            // RGBA 8888 both source and destination
            *dataLine++ = *bufLine++;
            *dataLine++ = *bufLine++;
            *dataLine++ = *bufLine++;
            *dataLine++ = *bufLine++;
        }
    }

}

void outputBitmapFromTexture(JNIEnv *env, jobject bitmapIn) {

    AndroidBitmapInfo infoIn = {0};
    void *pixelsIn = NULL;

    if ((AndroidBitmap_getInfo(env, bitmapIn, &infoIn) == ANDROID_BITMAP_RESULT_SUCCESS)) {

        FLOG("bitmapIn width: %d height: %d stride: %d format: 0x%x", infoIn.width,
             infoIn.height,
             infoIn.stride, infoIn.format);

        gBitmapWidth = infoIn.width;
        gBitmapHeight = infoIn.height;

        if ((AndroidBitmap_lockPixels(env, bitmapIn, &pixelsIn) ==
             ANDROID_BITMAP_RESULT_SUCCESS)
            && (infoIn.format == ANDROID_BITMAP_FORMAT_RGBA_8888)
                ) {

            // save the output
            unsigned char *output = new unsigned char[gBitmapWidth * gBitmapHeight * 4];

            if (output != NULL) {
                glReadPixels(0, 0, gBitmapWidth, gBitmapHeight, GL_RGBA, GL_UNSIGNED_BYTE,
                             output);

                // flip the image vertically!
                outputBitmap(output, (unsigned char *) pixelsIn, infoIn);

                delete[] output;
            }

            AndroidBitmap_unlockPixels(env, bitmapIn);
        }
    }

}

void loadBitmapIntoTexture(JNIEnv *env, jobject bitmapIn) {
    AndroidBitmapInfo infoIn = {0};
    void *pixelsIn = NULL;
    unsigned char *pixels = NULL;

    if ((AndroidBitmap_getInfo(env, bitmapIn, &infoIn) == ANDROID_BITMAP_RESULT_SUCCESS)) {

        FLOG("bitmapIn width: %d height: %d stride: %d format: 0x%x", infoIn.width,
             infoIn.height,
             infoIn.stride, infoIn.format);

        gBitmapWidth = infoIn.width;
        gBitmapHeight = infoIn.height;

        if ((AndroidBitmap_lockPixels(env, bitmapIn, &pixelsIn) ==
             ANDROID_BITMAP_RESULT_SUCCESS)
            && (infoIn.format == ANDROID_BITMAP_FORMAT_RGBA_8888)
                ) {

            pixels = new unsigned char[gBitmapWidth * gBitmapHeight * 4];

            if (pixels != NULL) {
                loadBitmap(pixels, (unsigned char *) pixelsIn, infoIn);
            }

            AndroidBitmap_unlockPixels(env, bitmapIn);
        }
    }

    if (pixels != NULL) {

        if (gImgInTex < 1) {

            glGenTextures(1, &gImgInTex);
            FLOG("gImgInTex: %d", gImgInTex);

            if (gImgInTex > 0) {

                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, gImgInTex);

                glTexImage2D(GL_TEXTURE_2D,
                             0,
                             GL_RGBA,
                             gBitmapWidth, gBitmapHeight,
                             0,
                             GL_RGBA,
                             GL_UNSIGNED_BYTE,
                             pixels);
                checkGLError("glTexImage2D");

                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                                GL_CLAMP_TO_EDGE);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                                GL_CLAMP_TO_EDGE);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                                GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                                GL_LINEAR);

                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, 0);

            }
        }

        delete[] pixels;
    }
}

int64_t systemTimeUs() {
    timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    return (now.tv_sec * 1000000000LL + now.tv_nsec) / 1000LL;
}

// Build a Phusion Plane description of the given Bitmap image
void DescribePlane(Phusion_Plane &plane, const Bitmap *pBMP) {
    plane.pData = pBMP->GetRow(0);
    plane.iStride = pBMP->GetStride();

    switch (pBMP->GetBPP()) {
        // 8-bit sample formats
        case 8:
            plane.flags = Phusion_PlaneFlagU8;
            plane.fmt = Phusion_PlaneRaster1;
            break;
        case 24:
            plane.flags = Phusion_PlaneFlagU8;
            plane.fmt = Phusion_PlaneRaster3;
            break;
        case 32:
            plane.flags = Phusion_PlaneFlagU8;
            plane.fmt = Phusion_PlaneRaster4;
            break;

            // 16-bit sample formats
        case 16:
            plane.flags = Phusion_PlaneFlagU16;
            plane.fmt = Phusion_PlaneRaster1;
            break;
        case 48:
            plane.flags = Phusion_PlaneFlagU16;
            plane.fmt = Phusion_PlaneRaster3;
            break;
        case 64:
            plane.flags = Phusion_PlaneFlagU16;
            plane.fmt = Phusion_PlaneRaster4;
            break;

        default:
            assert(!"Unsupported bit depth in DescribePlane()");
            break;
    }
}

void GenerateGaussian(phfloat_t linKern[], double std, unsigned kernWidth, double wgt = 1.0) {
    const double PI = 3.14179;
    double dstd2 = 2 * std * std;
    double denom = sqrt(PI * dstd2);
    double wtNorm = wgt / denom;

    // Construct 1D Gaussian filter kernel
    printf("Gaussian filter kernel sideband %u pixels\n", kernWidth);
    assert(kernWidth <= MAX_KERNEL_DIM);
    for (int dx = -(int) kernWidth; dx <= (int) kernWidth; dx++) {
        double wt = exp(-(dx * dx) / dstd2) * wtNorm;
        linKern[dx + (int) kernWidth] = wt;
    }
}

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_processBitmap(JNIEnv *env,
                                                                         jobject instance,
                                                                         jobject bitmapIn,
                                                                         jobject bitmapOut) {

    FLOG("processBitmap start");

    AndroidBitmapInfo infoIn = {0};
    AndroidBitmapInfo infoOut = {0};
    void *pixelsIn = NULL;
    void *pixelsOut = NULL;

    if ((AndroidBitmap_getInfo(env, bitmapIn, &infoIn) == ANDROID_BITMAP_RESULT_SUCCESS)
        && (AndroidBitmap_getInfo(env, bitmapOut, &infoOut) == ANDROID_BITMAP_RESULT_SUCCESS)) {

        FLOG("bitmapIn width: %d height: %d stride: %d format: 0x%x", infoIn.width, infoIn.height,
             infoIn.stride, infoIn.format);
        FLOG("bitmapOut width: %d height: %d stride: %d format: 0x%x", infoOut.width,
             infoOut.height, infoOut.stride, infoOut.format);

        if ((AndroidBitmap_lockPixels(env, bitmapIn, &pixelsIn) == ANDROID_BITMAP_RESULT_SUCCESS)
            &&
            (AndroidBitmap_lockPixels(env, bitmapOut, &pixelsOut) == ANDROID_BITMAP_RESULT_SUCCESS)
            && (infoIn.format == ANDROID_BITMAP_FORMAT_RGBA_8888)
            && (infoOut.format == ANDROID_BITMAP_FORMAT_RGBA_8888)) {

            // assume same geometry for in and out bitmaps
            const int inBPP = 4;

            for (unsigned y = 0; y < infoIn.height; y++) {

                // In the case of AndroidBitmaps, stride is the number of bytes per row
                // contrast this with ANativeWindow_Buffer where stride is the number of pixels
                unsigned char *line = ((unsigned char *) pixelsIn) + y * infoIn.stride;
                unsigned char *outLine = ((unsigned char *) pixelsOut) + y * infoIn.stride;

                for (unsigned x = 0; x < infoIn.width; x++) {

                    // red/blue swap
                    outLine[x * inBPP] = line[x * inBPP + 2];
                    outLine[x * inBPP + 2] = line[x * inBPP];
                    // g
                    outLine[x * inBPP + 1] = line[x * inBPP + 1];
                    // a
                    outLine[x * inBPP + 3] = line[x * inBPP + 3];

                }

            }

            usleep(5e6);

            AndroidBitmap_unlockPixels(env, bitmapOut);
            AndroidBitmap_unlockPixels(env, bitmapIn);

        }
    }

}

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_attachToEGLContext(JNIEnv *env,
                                                                              jobject instance) {

    FLOG("attachToEGLContext from thread 0x%x", gettid());
    eglMakeCurrent(gEglDisplay, gEglSurface, gEglSurface, gEglContext);
    checkEGLError("eglMakeCurrent");

}

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_detachFromEGLContext(JNIEnv *env,
                                                                                jobject instance) {

    FLOG("detachFromEGLContext from thread 0x%x", gettid());
    eglMakeCurrent(gEglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
    checkEGLError("eglMakeCurrent");

}

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_processBitmapGLES(JNIEnv *env,
                                                                             jobject instance,
                                                                             jobject bitmapIn,
                                                                             jobject bitmapOut) {

    FLOG("Starting processing");

    interruptFlag = false;

    loadBitmapIntoTexture(env, bitmapIn);

    if ((gEglDisplay != EGL_NO_DISPLAY) && (gEglContext != EGL_NO_CONTEXT) &&
        (gEglSurface != EGL_NO_SURFACE)) {

        if (gFBO < 1) {

            glGenFramebuffers(1, &gFBO);
            glGenTextures(1, &gFBOTex);
            // glGenRenderbuffers(1, &depth);

        }

        if ((gFBO > 0) && (gFBOTex > 0)) {
            // initialise the FBO
            glBindFramebuffer(GL_FRAMEBUFFER, gFBO);
            glBindTexture(GL_TEXTURE_2D, gFBOTex);

            glTexImage2D(GL_TEXTURE_2D,
                         0,
                         GL_RGBA,
                         gBitmapWidth, gBitmapHeight,
                         0,
                         GL_RGBA,
                         GL_UNSIGNED_BYTE,
                         NULL);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                                   gFBOTex,
                                   0);

            bool complete = false;

            {
                GLenum status;
                status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
                switch (status) {
                    case GL_FRAMEBUFFER_COMPLETE:
                        FLOG("Framebuffer complete");
                        complete = true;
                        break;

                    case GL_FRAMEBUFFER_UNSUPPORTED:
                    default:
                        FLOG("Framebuffer incomplete");
                }
            }

            glBindTexture(GL_TEXTURE_2D, 0);
            glBindFramebuffer(GL_FRAMEBUFFER, 0);

            if (complete) {

                glBindFramebuffer(GL_FRAMEBUFFER, gFBO);

                glViewport(0, 0, gBitmapWidth, gBitmapHeight);

                glClearColor(1.0, 1.0, 1.0, 1.0);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

                // render into an FBO
                if (gProgram2 > 0) {
                    glUseProgram(gProgram2);
                    checkGLError("glUseProgram");

                    glVertexAttribPointer(ATTRIBUTE_POSITION2, 2, GL_FLOAT, GL_FALSE, 0,
                                          gVertices);
                    glEnableVertexAttribArray(ATTRIBUTE_POSITION2);
                    glVertexAttribPointer(ATTRIBUTE_TEXCOORD2, 2, GL_FLOAT, GL_FALSE, 0,
                                          gTexCoords);
                    glEnableVertexAttribArray(ATTRIBUTE_TEXCOORD2);

                    // input image
                    glActiveTexture(GL_TEXTURE0);
                    glBindTexture(GL_TEXTURE_2D, gImgInTex);
                    glUniform1i(UNIFORM_EXTSAMPLER2, 0);

                    // 'minMaxL'
                    // TODO: actually calculate it
                    glUniform1f(UNIFORM_MIN2B, 1.0f / 256.0f);

                    glBindBufferBase(GL_UNIFORM_BUFFER, LUT_BLOCK_BINDING, gLUTUBO);
                    checkGLError("glBindBufferBase");

                    glBindBufferBase(GL_UNIFORM_BUFFER, LUT_BLOCK_BINDING2, gLUTUBO2);
                    checkGLError("glBindBufferBase");

                    FLOG("Starting processing");

                    int64_t timeStart = systemTimeUs();

                    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
                    checkGLError("glDrawArrays");

                    glFinish();

                    gLastProcessTimeUs = systemTimeUs() - timeStart;

                    for (int i = 0; (i < 100) && !interruptFlag; i++) {
                        usleep(5 * 1e4);
                    }

                    if (interruptFlag) {
                        FLOG("Processing interrupted");
                    }

                    FLOG("Finished processing in %"
                                 PRId64
                                 "ms", gLastProcessTimeUs / 1000UL);

                    if (!interruptFlag) {
                        outputBitmapFromTexture(env, bitmapOut);
                    }

                    glDisableVertexAttribArray(ATTRIBUTE_POSITION2);
                    glDisableVertexAttribArray(ATTRIBUTE_TEXCOORD2);
                    glActiveTexture(GL_TEXTURE0);
                    glBindTexture(GL_TEXTURE_2D, 0);

                    glUseProgram(0);
                    checkGLError("glUseProgram");
                }

                glBindFramebuffer(GL_FRAMEBUFFER, 0);
            }

        }
    }

}

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_signalProcessingInterrupt(JNIEnv *env,
                                                                                     jobject instance) {

    interruptFlag = true;

}

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_loadAssets(JNIEnv *env, jobject instance,
                                                                      jobject assetManager) {

    gAssetManager = AAssetManager_fromJava(env, assetManager);
    if (gAssetManager != NULL) {
        AAssetDir *topDir = AAssetManager_openDir(gAssetManager, "");
        const char *fileName = NULL;
        if (topDir != NULL) {
            while ((fileName = AAssetDir_getNextFileName(topDir)) != NULL) {
                FLOG("ASSET %s", fileName);
            }
            AAssetDir_close(topDir);
        }
        loadLUTs();
    }

}

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_initialiseGLES(JNIEnv *env,
                                                                          jobject instance,
                                                                          jobject windowSurface) {

    FLOG("Java_com_spectraledge_app_clarityapp_ImageActivityFragment_initialiseGLES");

    gWindow = ANativeWindow_fromSurface(env, windowSurface);

    if (gWindow != NULL) {
        EGLint context_attribs[] = {EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE};
        EGLint s_configAttribs[] = {EGL_SURFACE_TYPE,
                                    EGL_WINDOW_BIT,
                                    EGL_RENDERABLE_TYPE,
                                    EGL_OPENGL_ES2_BIT,
                                    EGL_RED_SIZE,
                                    8,
                                    EGL_GREEN_SIZE,
                                    8,
                                    EGL_BLUE_SIZE,
                                    8,
                                    EGL_NONE};

        EGLint format;
        EGLint numConfigs;
        EGLConfig config;

        gEglDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
        checkEGLError("eglGetDisplay");

        eglInitialize(gEglDisplay, 0, 0);
        checkEGLError("eglInitialize");

        eglChooseConfig(gEglDisplay, s_configAttribs, &config, 1, &numConfigs);
        checkEGLError("eglChooseConfig");

        eglGetConfigAttrib(gEglDisplay, config, EGL_NATIVE_VISUAL_ID, &format);
        checkEGLError("eglGetConfigAttrib");

        gEglSurface = eglCreateWindowSurface(gEglDisplay, config,
                                             gWindow, NULL);
        checkEGLError("eglCreateWindowSurface");

        gEglContext =
                eglCreateContext(gEglDisplay, config, NULL, context_attribs);
        checkEGLError("eglCreateContext");

        eglMakeCurrent(gEglDisplay, gEglSurface, gEglSurface, gEglContext);
        checkEGLError("eglMakeCurrent");

        eglQuerySurface(gEglDisplay, gEglSurface, EGL_WIDTH, &gScreenWidth);
        checkEGLError("eglQuerySurface");

        eglQuerySurface(gEglDisplay, gEglSurface, EGL_HEIGHT, &gScreenHeight);
        checkEGLError("eglQuerySurface");

        gProgram = createProgram(VERTEX_SHADER, FRAGMENT_SHADER);
        FLOG("gProgram: %d", gProgram);

        gProgram2 = createProgram(VERTEX_SHADER2, FRAGMENT_SHADER2B);
        FLOG("gProgram2: %d", gProgram2);

        gProgram3 = createProgram(VERTEX_SHADER3, FRAGMENT_SHADER3);
        FLOG("gProgram3: %d", gProgram3);

        glGenBuffers(1, &gLUTUBO);
        FLOG("gLUTUBO: %d", gLUTUBO);

        if ((gProgram2 > 0) && (gLUTUBO > 0)) {
            glUseProgram(gProgram2);

            gLUTUBOIndex = glGetUniformBlockIndex(gProgram2, LUT_BLOCK_NAME);
            FLOG("gLUTUBOIndex: %d", gLUTUBOIndex);

            glUniformBlockBinding(gProgram2, gLUTUBOIndex, LUT_BLOCK_BINDING);
            FLOG("Set the binding of gLUTUBOIndex to: %d", LUT_BLOCK_BINDING);

            glGetActiveUniformBlockiv(gProgram2, gLUTUBOIndex, GL_UNIFORM_BLOCK_DATA_SIZE,
                                      &gLUTUBOSize);
            FLOG("gLUTUBOSize: %d", gLUTUBOSize);

            glGetUniformIndices(gProgram2, NUM_UNIFORM_INDICES, gLUTUBONames, gLUTUBOIndices);
            FLOG("gLUTUBOIndices[0]: %d", gLUTUBOIndices[0]);

            glGetActiveUniformsiv(gProgram2, NUM_UNIFORM_INDICES, gLUTUBOIndices,
                                  GL_UNIFORM_OFFSET,
                                  gLUTUBOOffsets);
            FLOG("gLUTUBOOffsets[0]: %d", gLUTUBOOffsets[0]);

            glGetActiveUniformsiv(gProgram2, NUM_UNIFORM_INDICES, gLUTUBOIndices, GL_UNIFORM_SIZE,
                                  gLUTUBOSizes);
            FLOG("gLUTUBOSizes[0]: %d", gLUTUBOSizes[0]);

            glGetActiveUniformsiv(gProgram2, NUM_UNIFORM_INDICES, gLUTUBOIndices, GL_UNIFORM_TYPE,
                                  gLUTUBOTypes);
            FLOG("gLUTUBOTypes[0]: %d", gLUTUBOTypes[0]);

            // sanity check
            if ((gLUTUBOSizes[0] == 1024) &&
                (gLUTUBOTypes[0] == GL_FLOAT)) {

                // do it by the book, which is rather painful
                unsigned char *buffer = new unsigned char[gLUTUBOSize];

                if (buffer != NULL) {

                    memset(buffer, 0x0, gLUTUBOSize);

                    // std140 alignment rules mean the array stride is going to be vec4 of type float
                    for (int i = 0; i < 1024; i++) {
                        // copy element by element
                        memcpy(buffer + i * 4 * sizeof(GLfloat), &gCStretchLUT[i], sizeof(GLfloat));
                    }

                    glBindBuffer(GL_UNIFORM_BUFFER, gLUTUBO);
                    glBufferData(GL_UNIFORM_BUFFER, gLUTUBOSize, buffer,
                                 GL_STATIC_DRAW);
                    checkGLError("glBufferData");
                    glBindBuffer(GL_UNIFORM_BUFFER, 0);

                    delete[] buffer;
                }

            }

            glUseProgram(0);
        }

        glGenBuffers(1, &gLUTUBO2);
        FLOG("gLUTUBO: %d", gLUTUBO2);

        if ((gProgram2 > 0) && (gLUTUBO2 > 0)) {
            glUseProgram(gProgram2);

            gLUTUBOIndex2 = glGetUniformBlockIndex(gProgram2, LUT_BLOCK_NAME2);
            FLOG("gLUTUBOIndex2: %d", gLUTUBOIndex2);

            glUniformBlockBinding(gProgram2, gLUTUBOIndex2, LUT_BLOCK_BINDING2);
            FLOG("Set the binding of gLUTUBOIndex2 to: %d", LUT_BLOCK_BINDING2);

            glGetActiveUniformBlockiv(gProgram2, gLUTUBOIndex2, GL_UNIFORM_BLOCK_DATA_SIZE,
                                      &gLUTUBOSize2);
            FLOG("gLUTUBOSize2: %d", gLUTUBOSize2);

            glGetUniformIndices(gProgram2, NUM_UNIFORM_INDICES, gLUTUBONames2, gLUTUBOIndices2);
            FLOG("gLUTUBOIndices2[0]: %d", gLUTUBOIndices2[0]);

            glGetActiveUniformsiv(gProgram2, NUM_UNIFORM_INDICES, gLUTUBOIndices2,
                                  GL_UNIFORM_OFFSET,
                                  gLUTUBOOffsets2);
            FLOG("gLUTUBOOffsets2[0]: %d", gLUTUBOOffsets2[0]);

            glGetActiveUniformsiv(gProgram2, NUM_UNIFORM_INDICES, gLUTUBOIndices2, GL_UNIFORM_SIZE,
                                  gLUTUBOSizes2);
            FLOG("gLUTUBOSizes2[0]: %d", gLUTUBOSizes2[0]);

            glGetActiveUniformsiv(gProgram2, NUM_UNIFORM_INDICES, gLUTUBOIndices2, GL_UNIFORM_TYPE,
                                  gLUTUBOTypes2);
            FLOG("gLUTUBOTypes2[0]: %d", gLUTUBOTypes2[0]);

            // sanity check
            if ((gLUTUBOSizes2[0] == 1024) &&
                (gLUTUBOTypes2[0] == GL_FLOAT)) {

                // do it by the book, which is rather painful
                unsigned char *buffer = new unsigned char[gLUTUBOSize2];

                if (buffer != NULL) {

                    memset(buffer, 0x0, gLUTUBOSize2);

                    // std140 alignment rules mean the array stride is going to be vec4 of type float
                    for (int i = 0; i < 1024; i++) {
                        // copy element by element
                        memcpy(buffer + i * 4 * sizeof(GLfloat), &gAlphaLUT[i], sizeof(GLfloat));
                    }

                    glBindBuffer(GL_UNIFORM_BUFFER, gLUTUBO2);
                    glBufferData(GL_UNIFORM_BUFFER, gLUTUBOSize2, buffer,
                                 GL_STATIC_DRAW);
                    checkGLError("glBufferData");
                    glBindBuffer(GL_UNIFORM_BUFFER, 0);

                    delete[] buffer;
                }

            }

            glUseProgram(0);
        }

        eglMakeCurrent(gEglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        checkEGLError("eglMakeCurrent");
    }

}

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_terminateGLES(JNIEnv *env,
                                                                         jobject instance) {

    FLOG("Java_com_spectraledge_app_clarityapp_ImageActivityFragment_terminateGLES");

    if ((gEglDisplay != EGL_NO_DISPLAY) && (gEglContext != EGL_NO_CONTEXT) &&
        (gEglSurface != EGL_NO_SURFACE)) {

        GLuint textures[] = {gFBOTex, gImgInTex};
        glDeleteTextures(sizeof(textures) / sizeof(textures[0]), textures);

        GLuint fbos[] = {gFBO};
        glDeleteFramebuffers(sizeof(fbos) / sizeof(fbos[0]), fbos);

        GLuint buffers[] = {gLUTUBO, gLUTUBO2};
        glDeleteBuffers(sizeof(buffers) / sizeof(buffers[0]), buffers);

        glDeleteProgram(gProgram);
        glDeleteProgram(gProgram2);
        glDeleteProgram(gProgram3);

        eglDestroySurface(gEglDisplay, gEglSurface);
        checkEGLError("eglDestroySurface");

        eglDestroyContext(gEglDisplay, gEglContext);
        checkEGLError("eglDestroyContext");

        eglTerminate(gEglDisplay);
        checkEGLError("eglTerminate");

        if (gWindow != NULL) {
            ANativeWindow_release(gWindow);
        }

        gWindow = NULL;
        gEglDisplay = EGL_NO_DISPLAY;
        gEglSurface = EGL_NO_SURFACE;
        gEglContext = EGL_NO_CONTEXT;
        gScreenWidth = 0;
        gScreenHeight = 0;
        gProgram = 0;
        gProgram2 = 0;
        gProgram3 = 0;

        gFBO = 0;
        gFBOTex = 0;
        gBitmapWidth = 0;
        gBitmapHeight = 0;
        gImgInTex = 0;

        gLUTUBO = 0;
        gLUTUBOSize = 0;
        gLUTUBOIndex = 0;

        gLUTUBO2 = 0;
        gLUTUBOSize2 = 0;
        gLUTUBOIndex2 = 0;

    }

}

JNIEXPORT void JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_processBitmapCPU(JNIEnv *env,
                                                                            jobject instance,
                                                                            jobject bitmapIn,
                                                                            jobject bitmapOut) {

    FLOG("processBitmapCPU start");

    interruptFlag = false;

    // load the input bitmap into a clarity bitmap
    AndroidBitmapInfo infoIn = {0};
    void *pixelsIn = NULL;
    Bitmap cBitmapIn;

    if ((AndroidBitmap_getInfo(env, bitmapIn, &infoIn) == ANDROID_BITMAP_RESULT_SUCCESS)) {

        FLOG(">>> bitmapIn width: %d height: %d", infoIn.width, infoIn.height);

        if ((AndroidBitmap_lockPixels(env, bitmapIn, &pixelsIn) == ANDROID_BITMAP_RESULT_SUCCESS)
            && (infoIn.format == ANDROID_BITMAP_FORMAT_RGBA_8888)) {

            cBitmapIn.Create(infoIn.width, infoIn.height, 24);
            loadAndroidBitmapIntoClarityBitmap(pixelsIn, infoIn, cBitmapIn);
            AndroidBitmap_unlockPixels(env, bitmapIn);

        }
    }

    bool error = false;

    Phusion_ImageChannels inImgContrast;
    Phusion_ImageChannels rsImgContrast;
    Phusion_ImageChannels inImgGuide;
    Phusion_ImageChannels outImg;

    Phusion_Features phFeatures;  // Library feature set
    Phusion_Version phVersion;  // Structure describing the library version/revision
    Phusion_Handle phHandle;  // Handle to an instance of the Phusion library
    Phusion_Result phResult;  // Result code from library call
    Phusion_Config config;
    Phusion_Buffer outBuf;
    Phusion_Buffer wtBuf;
    Phusion_Buffer rsBuf;
    Phusion_Buffer inBuf;
    Bitmap lum;
    Bitmap outLum;
    Bitmap ftLum;
    Bitmap clImg;
    Bitmap rsLum;
    Bitmap rsRGB;
    Bitmap rsGuide;

    const unsigned ResamplingFactor = 8;
    Phusion_Polynomial polynomial[PHUSION_MAX_POLYNOMIALS];
    phfloat_t tmLinKern[2 * MAX_KERNEL_DIM + 1] = {0};
    phfloat_t umLinKern[2 * MAX_KERNEL_DIM + 1] = {0};


    // Check the version of the Phusion library being used
    phFeatures.featSize = sizeof(phFeatures);
    phResult = Phusion_Identify(&phVersion, &phFeatures);
    error = (phResult != Phusion_OK);

    if (!error && !interruptFlag) {
        config = Phusion_CfgUseCPU;      // CPU single-threaded
        phResult = Phusion_Initialise(config, &phHandle);
        error = (phResult != Phusion_OK);
    }

    uint32_t imHeight = cBitmapIn.GetHeight();
    uint32_t imWidth = cBitmapIn.GetWidth();
    uint8_t rgbBPP = cBitmapIn.GetBPP();
    uint32_t outWidth = imWidth;
    uint32_t outHeight = imHeight;
    uint8_t outBPP = rgbBPP;
    uint32_t lumWidth = imWidth;
    uint32_t lumHeight = imHeight;
    uint8_t lumBPP = 8;
    uint32_t rsHeight = (imHeight + ResamplingFactor - 1) / ResamplingFactor;
    uint32_t rsWidth = (imWidth + ResamplingFactor - 1) / ResamplingFactor;

    if (!error && !interruptFlag) {
        // Create luminance buffer
        error = !lum.Create(lumWidth, lumHeight, lumBPP);
    }

    uint16_t minL = 0;

    if (!error && !interruptFlag) {
        // Generate luminance image from input RGB image, returning the global non-zero minimum
        minL = RGB2Luminance(lum, cBitmapIn);
        // Create buffer for the output luminance image
        error = !outLum.Create(outWidth, outHeight, 8);
    }

    if (!error && !interruptFlag) {
        outBuf.nPlanes = 1;
        DescribePlane(outBuf.planes[0], &outLum);

        // Create thumbnail buffers in memory
        error = (!rsRGB.Create(rsWidth, rsHeight, rgbBPP) ||
                 !rsLum.Create(rsWidth, rsHeight, lumBPP) ||
                 !ftLum.Create(rsWidth, rsHeight, lumBPP) ||
                 !clImg.Create(rsWidth, rsHeight, lumBPP));
    }

    unsigned tmKernDim = (3 * min(rsWidth, rsHeight)) / 10;
    unsigned umKernDim = (3 * min(rsWidth, rsHeight)) / 20;
    const double strength = 0.3;  // TODO - obviously supposed to be configurable
    const double multip1 = 0.3;


    if (!error && !interruptFlag) {
        // Set up a description of the input image buffer
        // - 2 input planes:
        //      first plane - 3 interleaved samples, BGR (Windows DIB)
        //     second plane - 1 sample per pixel, NIR/Thermal
        inBuf.nPlanes = 2;
        DescribePlane(inBuf.planes[0], &cBitmapIn);
        DescribePlane(inBuf.planes[1], &lum);

        // Set up a description of the input image channels for each of the contrast and guide images,
        // - each image channel is mapped to a specified component of a given plane of the input buffer
        inImgContrast.nChannels = 3;
        inImgContrast.planeNumber[0] = 0;  // The first 3 components are the RGB data
        inImgContrast.planeNumber[1] = 0;
        inImgContrast.planeNumber[2] = 0;
        inImgContrast.compNumber[0] = 0;
        inImgContrast.compNumber[1] = 1;
        inImgContrast.compNumber[2] = 2;

        // - similarly for the guide image which has just 1 luminance channel
        inImgGuide.nChannels = 1;
        inImgGuide.planeNumber[0] = 0;
        inImgGuide.compNumber[0] = 0;

        rsImgContrast.nChannels = 3;
        rsImgContrast.planeNumber[0] = 1;
        rsImgContrast.planeNumber[1] = 1;
        rsImgContrast.planeNumber[2] = 1;
        rsImgContrast.compNumber[0] = 0;
        rsImgContrast.compNumber[1] = 1;
        rsImgContrast.compNumber[2] = 2;

        // Describe the buffer for resampled output
        rsBuf.nPlanes = 2;
        DescribePlane(rsBuf.planes[0], &rsRGB);
        DescribePlane(rsBuf.planes[1], &rsLum);

        // NOTE: Since the RGB Guide image will be created as a direct copy of the RGB input plane
        //       we don't need a separate description of the resampled guide channels

        // Compute filter kernels

        double tmStd = 0.10 * min(rsWidth, rsHeight);

        GenerateGaussian(tmLinKern, tmStd, tmKernDim);

        double umStd = 0.05 * min(rsWidth, rsHeight);

        GenerateGaussian(umLinKern, umStd, umKernDim, sqrt(multip1));

        const unsigned pivot = (unsigned) (0.5 * 0x400);

        for (unsigned in = 0; in < pivot; in++) {
            double ref = in / 1024.0;
            alphaLUT[in] = (4.0 * (ref - 1.0) * ref) + 1.0;
        }
        for (unsigned in = pivot; in < 0x400; in++) {
            // NOTE: LUT is currently indexed with [0:255]<<2, so reach unity at 0x3FC
            double out = (double) (in - pivot) / (0x3FC - pivot);
            alphaLUT[in] = (out > 1.0) ? 1.0 : out;
        }

        for (unsigned in = 0; in < 0x100; in++) {
            double cStretchFact = min(strength * 1.5, 1.0);

            double x = in / 255.0;
            double thrdegpoly = -0.64 * x * x * x + 0.96 * x * x + 0.68 * x;

            double contStretch = (thrdegpoly * cStretchFact + x * (1.0 - cStretchFact)) * 255.0;
            unsigned cst = (contStretch >= 255.0) ? 0xff : ((contStretch < 0.0) ? 0x00
                                                                                : (unsigned) contStretch);
            outLUT[in] = cst;
        }
        // Lastly, a description of the output image, which is the same as the guide image, so this is easy
        outImg = inImgGuide;
        error = !rsGuide.Create(rsWidth, rsHeight, 8);
    }

    int64_t timeStart = systemTimeUs();

    if (!error && !interruptFlag) {
        phResult = Phusion_ResampleImages(phHandle,
                                          &rsBuf, &inBuf,
                                          rsWidth, rsHeight,
                                          imWidth, imHeight,
                                          Phusion_ResBox, NULL);
        error = (phResult != Phusion_OK);
    }

    phpoly_t *pWws = NULL;
    phpoly_t *pWfn = NULL;
    const unsigned nWeights = 8;

    if (!error && !interruptFlag) {
        RGB2Gray(rsGuide, rsRGB);

        // Create weights from the downsampled RGB input image

        pWws = new phpoly_t[(nWeights / 2) * rsHeight *
                            rsWidth];  // Planar scratch workspace
        // TODO - !!!! Bodged height for now because we lazily run off the bottom!
        pWfn = new phpoly_t[(rsHeight + 10) * rsWidth *
                            nWeights];      // Interleaved weights

        error = ((pWws == NULL) || (pWfn == NULL));

        if (!error) {
            unsigned mwKernWidth = (min(rsWidth, rsHeight) * 4) / 5;
            mwKernWidth += !(mwKernWidth & 1);
            MakeWeights(pWfn, pWws, rsGuide, mwKernWidth, mwKernWidth, nWeights >> 1);
        }

        if (pWws != NULL) {
            delete[] pWws;
        }

    }

    // Tone mapping
    int32_t maxTM = 0;
    int32_t minTM = 0x100;

    if (!error && !interruptFlag) {

        // Describe the weights for use in pixel-processing
        wtBuf.nPlanes = nWeights;
        for (unsigned p = 0; p < nWeights; p++) {
            wtBuf.planes[p].pData = pWfn + (p * rsHeight * rsWidth);
// TODO - stride!!
            wtBuf.planes[p].iStride = rsWidth * sizeof(phpoly_t);
            wtBuf.planes[p].fmt = Phusion_PlaneRaster1;
            wtBuf.planes[p].flags = Phusion_PlaneFlagDF;
        }

        ConvolveImage(ftLum, rsLum, tmLinKern, tmKernDim, tmKernDim, maxTM, minTM, 0, false);

        error = ((maxTM < 0) || (minTM < 0));

    }

    int32_t maxUM;
    int32_t minUM;

    if (!error && !interruptFlag) {
        ToneMapping(ftLum, rsLum, (unsigned) maxTM, (unsigned) minTM, strength);
        // Unsharp masking
        ConvolveImage(clImg, ftLum, umLinKern, umKernDim, umKernDim, maxUM, minUM,
                      (int32_t) ((1.0 + multip1) * (1 << 28)), true);
        ftLum.Release();

        // Describe the buffer for input to standalone gradient calculations
        rsBuf.nPlanes = 2;
        DescribePlane(rsBuf.planes[0], &clImg);  // Guide
        DescribePlane(rsBuf.planes[1], &rsRGB);  // Contrast

        const phfloat_t extLambda = (phfloat_t) 0.25;
        const phfloat_t regLambda = (phfloat_t) 0.25;
        const phfloat_t tikLambda = (phfloat_t) 1e-3;
        const phfloat_t multip2 = PHFLOAT_ONE;

        GradientCalcs(polynomial, rsRGB, clImg, rsLum, wtBuf, rsHeight, rsWidth,
                      multip2, regLambda, tikLambda, extLambda);

        clImg.Release();

        for (unsigned poly = 0; poly < PHUSION_MAX_POLYNOMIALS; poly++) {
            phResult = Phusion_SetPolynomial(phHandle, Phusion_PolynomialID(poly),
                                             &polynomial[poly]);
            error = error && (phResult != Phusion_OK);
        }
    }

    if (!error && !interruptFlag) {
        inBuf.nPlanes = 1;
        DescribePlane(inBuf.planes[0], &cBitmapIn);

        inImgContrast.nChannels = 3;
        inImgContrast.planeNumber[0] = 0;
        inImgContrast.planeNumber[1] = 0;
        inImgContrast.planeNumber[2] = 0;

        // TODO - NOTE!!!
        //        This code is currently being asked to read and write luminance from/to a single
        //        plane, described within both outBuf and inBuf which is UNDEFINED behaviour really!

        phResult = Phusion_ProcessPixels(phHandle,  // Handle to library
                                         &outBuf, &wtBuf, &inBuf,    // Buffer descriptions
                                         &outImg, &inImgContrast,    // Image descriptions
                                         rsWidth, rsHeight,          // Weights supplied
                                         imWidth, imHeight,          // Stripe dimensions
                                         Phusion_ProcessFlags(Phusion_ProcSwitchConfig
                                                              | Phusion_ProcUseWeights),
                                         nullptr);                   // Blocking API call
        error = (phResult != Phusion_OK);

    }

    Bitmap cBitmapOut;
    AndroidBitmapInfo infoOut = {0};

    if (!error && !interruptFlag) {

        if ((AndroidBitmap_getInfo(env, bitmapOut, &infoOut) == ANDROID_BITMAP_RESULT_SUCCESS)) {

            FLOG("bitmapOut width: %d height: %d stride: %d format: 0x%x", infoOut.width,
                 infoOut.height, infoOut.stride, infoOut.format);

            error = !cBitmapOut.Create(infoOut.width, infoOut.height, 24);
        }

    }

    if (!error) {
        ApplyLuminance(cBitmapOut, cBitmapIn, lum, outLum, outLUT, alphaLUT, minL);
        if (pWfn != NULL) {
            delete[] pWfn;
        }
        phResult = Phusion_Finalise(phHandle);
        error = (phResult != Phusion_OK);
    }

    gLastProcessTimeUs = systemTimeUs() - timeStart;

    FLOG(">>> Finished processing in %"
                 PRId64
                 "ms", gLastProcessTimeUs / 1000UL);

    if (!error && !interruptFlag) {
        // load the clarity bitmap into the Android bitmap

        void *pixelsOut = NULL;

        if ((AndroidBitmap_lockPixels(env, bitmapOut, &pixelsOut) == ANDROID_BITMAP_RESULT_SUCCESS)
            && (infoOut.format == ANDROID_BITMAP_FORMAT_RGBA_8888)) {

            loadClarityBitmapIntoAndroidBitmap(cBitmapOut, pixelsOut, infoOut);
            AndroidBitmap_unlockPixels(env, bitmapOut);

        }
    }

    FLOG("processBitmapCPU end");

}

JNIEXPORT jlong JNICALL
Java_com_spectraledge_app_clarityapp_ImageActivityFragment_getProcessingTime(JNIEnv *env,
                                                                             jobject instance) {

    return (jlong) (gLastProcessTimeUs / 1000UL);

}

#ifdef __cplusplus
}
#endif

