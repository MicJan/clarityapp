/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        Clarity.cpp                                                   */
/* Description: Example application using the Phusion Library to implement    */
/*              Clarity for RGB images                                        */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#include <cassert>
#include <cctype>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>

// Include the Phusion Library header file
#include "phusion.h"

// We use the Bitmap handling for input/output
#include "Bitmap.hpp"

// Toolkit functions to implement Clarity processing stages
#include "Conversion.hpp"
#include "Convolution.hpp"
#include "GradientCalcs.hpp"
#include "Utils.hpp"
#include "Weights.hpp"

#ifndef min
#define min(x,y) (((x)>(y))?(y):(x))
#endif
#ifndef max
#define max(x,y) (((x)>(y))?(x):(y))
#endif

#ifndef NOF_ELEMENTS
#define NOF_ELEMENTS(x) (sizeof(x)/sizeof(*(x)))
#endif

#define MULTITHREADED 0

#ifdef _MSC_VER
#define DEFAULT_OUTPUT_FILE "..\\..\\Clarity\\output.bmp"
#define DEFAULT_INPUT_FILE  "..\\..\\Clarity\\IMG_0008.bmp"
#define COEFFS_FILE "..\\..\\Clarity\\Coeffs.dat"
#define ALPHA_FILE "..\\..\\Clarity\\alpha.dat"
#else
#if 1
#define DEFAULT_OUTPUT_FILE "output.bmp"
//#define DEFAULT_INPUT_FILE  "IMG_0008.bmp"
#define DEFAULT_INPUT_FILE  "IMG_0073.bmp"
#define COEFFS_FILE "Coeffs.dat"
#else
#define DEFAULT_OUTPUT_FILE "PixOut.bmp"
#define DEFAULT_INPUT_FILE "../../../../Test/Pixel/9344135986.jpg"
#define COEFFS_FILE "PixCoeffs.dat"
#endif
#define ALPHA_FILE "alpha.dat"
#endif

#ifdef WIN32
#define CLARITY_LOG_DIR "\\"
#else
#define CLARITY_LOG_DIR "./"
#endif

#ifndef PI
#define PI 3.1415927
#endif

#define APP_PROFILING 1

#define TEST_DIR "../../../../Test/Refs/"

#define FORCE_ALPHA   0
#define FORCE_COEFFS  0
#define FORCE_WEIGHTS 0

// Enable these when the Phusion Library API calls are working
#define LIB_CONVERSIONS 0
#define LIB_FILTERING   0

// Check that the operation completed successfully (Phusion_OK)
//    Note: this macro should not be used if the result may be Phusion_Pending
#define PH_CHECK(res) ((((res)==Phusion_OK)?0: \
                         fprintf(stderr, "Error from Phusion library at %s,%d: %s (0x%x)\n", \
                                         __FILE__, __LINE__, Phusion_ErrorText(res), res)),  \
                       assert((res)==Phusion_OK))

//static const unsigned ResamplingFactor = 5;
static const unsigned ResamplingFactor = 8;

static bool bVerbose(false);
static bool bSaveIntermediates(false);

static phfloat_t tmLinKern[2*MAX_KERNEL_DIM+1];
static phfloat_t umLinKern[2*MAX_KERNEL_DIM+1];

static const char syntax[] = "Syntax: Clarity <output> <RGB input>\n";

// Build a Phusion Plane description of the given Bitmap image
static void DescribePlane(Phusion_Plane &plane, const Bitmap *pBMP);
// Show Phusion Buffer properties as textual output
static void ShowBufferProperties(const Phusion_Buffer &buf);
// Show Phusion Image description as textual output
static void ShowImageDescription(const Phusion_ImageChannels &im);
// Show polynomail coefficients as textual output
static void ShowPolynomial(FILE *out, const Phusion_Polynomial &poly);

static void GenerateGaussian(phfloat_t linKern[], double std, unsigned kernWidth, double wgt = 1.0)
{
    double dstd2 = 2*std*std;
    double denom = sqrt(PI * dstd2);
    double wtNorm = wgt / denom;

    // Construct 1D Gaussian filter kernel
    printf("Gaussian filter kernel sideband %u pixels\n", kernWidth);
    assert(kernWidth <= MAX_KERNEL_DIM);
    for (int dx = -(int)kernWidth; dx <= (int)kernWidth; dx++)
    {
        double wt = exp(-(dx*dx)/dstd2) * wtNorm;
        linKern[dx+(int)kernWidth] = wt;
    }
}

// Build a Phusion Plane description of the given Bitmap image
void DescribePlane(Phusion_Plane &plane, const Bitmap *pBMP)
{
    plane.pData   = pBMP->GetRow(0);
    plane.iStride = pBMP->GetStride();

    switch (pBMP->GetBPP())
    {
        // 8-bit sample formats
        case 8:  plane.flags = Phusion_PlaneFlagU8;
                 plane.fmt   = Phusion_PlaneRaster1;
                 break;
        case 24: plane.flags = Phusion_PlaneFlagU8;
                 plane.fmt   = Phusion_PlaneRaster3;
                 break;
        case 32: plane.flags = Phusion_PlaneFlagU8;
                 plane.fmt   = Phusion_PlaneRaster4;
                 break;

        // 16-bit sample formats
        case 16: plane.flags = Phusion_PlaneFlagU16;
                 plane.fmt   = Phusion_PlaneRaster1;
                 break;
        case 48: plane.flags = Phusion_PlaneFlagU16;
                 plane.fmt   = Phusion_PlaneRaster3;
                 break;
        case 64: plane.flags = Phusion_PlaneFlagU16;
                 plane.fmt   = Phusion_PlaneRaster4;
                 break;

        default: assert(!"Unsupported bit depth in DescribePlane()");
                 break;
    }
}

// Show polynomial coefficients as textual output
void ShowPolynomial(FILE *out, const Phusion_Polynomial &poly)
{
    if (bVerbose)
    {
        printf("Polynomial coefficients:\n");
        for (unsigned c = 0; c < poly.nCoeffs; c++)
        {
            putchar('\t');
            for (unsigned p = 0; p < poly.nPlanes; p++)
                printf("%20.16lf%c", poly.coeffs[p][c], (p >= (unsigned)poly.nPlanes - 1) ? '\n' : ' ');
        }
    }
}

// Show Phusion Buffer properties as textual output
void ShowBufferProperties(const Phusion_Buffer &buf)
{
    if (bVerbose)
    {
        unsigned nPlanes = buf.nPlanes;
        FILE *fp = stdout;

        printf("Buffer planes: %u\n", nPlanes);
        if (nPlanes > PHUSION_MAX_PLANES)
        {
            printf("ERROR: Too many planes\n");
            nPlanes = PHUSION_MAX_PLANES;
        }
        for (unsigned p = 0; p < nPlanes; p++)
        {
            printf(" %u: pData %p iStride %d ", p, buf.planes[p].pData, buf.planes[p].iStride);

            unsigned nc = (unsigned)(buf.planes[p].fmt & Phusion_PlaneNComps);
            if (nc > 0 && nc <= 8)
                fprintf(fp, "%u component%c - ", nc, (nc == 1) ? ' ' : 's');
            else
                fprintf(fp, "\nERROR: Invalid format field %u\n", (unsigned)buf.planes[p].fmt);
            switch (buf.planes[p].flags)
            {
                case Phusion_PlaneFlagU8:  fprintf(fp, "8-bit  unsigned\n"); break;
                case Phusion_PlaneFlagU16: fprintf(fp, "16-bit unsigned\n"); break;
                case Phusion_PlaneFlagSF:  fprintf(fp, "single floats\n"); break;
                case Phusion_PlaneFlagDF:  fprintf(fp, "double floats\n"); break;
                default:
                    fprintf(fp, "\nERROR: Invalid flags field %u\n", (unsigned)buf.planes[p].flags);
                    break;
            }
        }
    }
}

// Show Phusion Image as a textual description
void ShowImageDescription(const Phusion_ImageChannels &im)
{
    if (bVerbose)
    {
        unsigned nChannels = im.nChannels;
        printf("Image channels: %u\n", nChannels);
        if (nChannels > PHUSION_MAX_CHANNELS)
        {
            printf("ERROR: Too many channels\n");
            nChannels = PHUSION_MAX_CHANNELS;
        }
        for (unsigned ch = 0; ch < nChannels; ch++)
        {
            printf(" %u: plane %u component %u\n", ch, im.planeNumber[ch], im.compNumber[ch]);
            if (im.planeNumber[ch] >= PHUSION_MAX_PLANES)
                printf("ERROR: Invalid plane number\n");
            if (im.compNumber[ch] > 4)
                printf("ERROR: Invalid component number\n");
        }
    }
}

int main(int argc, char *argv[])
{
    Phusion_Polynomial polynomial[PHUSION_MAX_POLYNOMIALS];
    Phusion_ImageChannels inImgContrast;
    Phusion_ImageChannels rsImgContrast;
    Phusion_ImageChannels inImgGuide;
    Phusion_ImageChannels outImg;
    const char *rgb_filename = nullptr;
    const char *out_filename = nullptr;
    uint16_t outLUT[0x100];
    Phusion_Features phFeatures;  // Library feature set
    Phusion_Version phVersion;  // Structure describing the library version/revision
    Phusion_Handle phHandle;  // Handle to an instance of the Phusion library
    Phusion_Result phResult;  // Result code from library call
    Phusion_Config config;
    Phusion_Buffer outBuf;
    Phusion_Buffer wtBuf;
    Phusion_Buffer rsBuf;
    Phusion_Buffer inBuf;
    Bitmap rgb, lum;
    Bitmap outLum;
    Bitmap ftLum;
    Bitmap clImg;
    Bitmap rsLum;
    Bitmap rsRGB;
    Bitmap out;

    puts("Clarity Phusion Test Application\nCopyright (C) Spectral Edge Ltd, 2017.\n");

    profile_init();

    if (argc != 4 && argc != 5)
        puts(syntax);

    // Command-line parsing
    for(int i = 1; i < argc; i++)
    {
        switch (i)
        {
            case 1:  out_filename = argv[i]; break;
            default:
                rgb_filename = argv[i];
                break;
        }
    }

    // Pick up default filenames; just a convenience for testing/debugging
    if (!out_filename) out_filename = DEFAULT_OUTPUT_FILE;
    if (!rgb_filename) rgb_filename = DEFAULT_INPUT_FILE;

    // Check the version of the Phusion library being used
    phFeatures.featSize = sizeof(phFeatures);
    phResult = Phusion_Identify(&phVersion, &phFeatures);
    PH_CHECK(phResult);

    // Initialise an instance of the library, obtaining a handle for use in subsequent operations
#if MULTITHREADED
    config = Phusion_DefaultConfig;  // CPU + SMP
#else
    config = Phusion_CfgUseCPU;      // CPU single-threaded
#endif
    phResult = Phusion_Initialise(config, &phHandle);
    if (phResult != Phusion_OK) {
        // Phusion library failed to start
        fprintf(stderr, "ERROR: Phusion library failed to initialise: %s (0x%x)\n",
                         Phusion_ErrorText(phResult), phResult);
        return 3;
    }

    // Image properties
    uint32_t imHeight;
    uint32_t imWidth;

    // Load our input images that we want the library to fuse
    printf("Loading RGB input image from '%s'\n", rgb_filename);
    if (!rgb.Load(rgb_filename))
    {
        fprintf(stderr, "ERROR: Failed to load RGB input image '%s'\n", rgb_filename);
        return 1;
    }

    imHeight = rgb.GetHeight();
    imWidth = rgb.GetWidth();

    uint8_t rgbBPP = rgb.GetBPP();

    // Create luminance buffer
    uint32_t lumWidth  = imWidth;
    uint32_t lumHeight = imHeight;
    uint8_t     lumBPP = rgbBPP / 3;

    // TODO - decide what should be considered in the overall performance measurement
    clock_t start = clock();

    if (!lum.Create(lumWidth, lumHeight, lumBPP))
    {
        fprintf(stderr, "ERROR: Insufficient memory to allocate luminance/max images of %ux%u pixels\n",
                        lumWidth, lumHeight);
        phResult = Phusion_Finalise(phHandle);
        PH_CHECK(phResult);
        return 4;
    }

    // Generate luminance image from input RGB image, returning the global non-zero minimum
    uint16_t minL = RGB2Luminance(lum, rgb);

    if (bVerbose)
        printf("minL is %u\n", minL);

    profile_stop_timing(&profile[PROF_APP_CREATELUM]);

    if (bSaveIntermediates)
        lum.Save("lum.bmp", FileFormat_Auto);

    // Create buffer for the final output image
    uint32_t outWidth  = imWidth;
    uint32_t outHeight = imHeight;
    uint8_t     outBPP = rgbBPP;

    if (bVerbose)
        printf("Output dimensions %ux%ux%ubpp\n", outWidth, outHeight, outBPP);

    if (!out.Create(outWidth, outHeight, outBPP))
    {
        fprintf(stderr, "ERROR: Insufficient memory to allocate output image of %ux%u pixels\n",
                        outWidth, outHeight);
        phResult = Phusion_Finalise(phHandle);
        PH_CHECK(phResult);
        return 4;
    }

    // Create buffer for the output luminance image
    if (!outLum.Create(outWidth, outHeight, outBPP / 3))
    {
        fprintf(stderr, "ERROR: Insufficient memory to allocate output luminance of %ux%u pixels\n",
                        outWidth, outHeight);
        phResult = Phusion_Finalise(phHandle);
        PH_CHECK(phResult);
        return 5;
    }

    outBuf.nPlanes = 1;
    DescribePlane(outBuf.planes[0], &outLum);

    // Create thumbnail buffers in memory
    uint32_t rsHeight = (imHeight + ResamplingFactor-1) / ResamplingFactor;
    uint32_t rsWidth  = (imWidth  + ResamplingFactor-1) / ResamplingFactor;

    if (!rsRGB.Create(rsWidth, rsHeight, rgbBPP) || !rsLum.Create(rsWidth, rsHeight, lumBPP) ||
        !ftLum.Create(rsWidth, rsHeight, lumBPP) || !clImg.Create(rsWidth, rsHeight, lumBPP))
    {
        fprintf(stderr, "ERROR: Insufficient memory to allocate resampled images of %ux%u pixels\n",
                        rsWidth, rsHeight);
        phResult = Phusion_Finalise(phHandle);
        PH_CHECK(phResult);
        return 5;
    }

    // Set up a description of the input image buffer
    // - 2 input planes:
    //      first plane - 3 interleaved samples, BGR (Windows DIB)
    //     second plane - 1 sample per pixel, NIR/Thermal
    inBuf.nPlanes = 2;
    DescribePlane(inBuf.planes[0], &rgb);
    DescribePlane(inBuf.planes[1], &lum);

    // Set up a description of the input image channels for each of the contrast and guide images,
    // - each image channel is mapped to a specified component of a given plane of the input buffer
    inImgContrast.nChannels = 3;
    inImgContrast.planeNumber[0] = 0;  // The first 3 components are the RGB data
    inImgContrast.planeNumber[1] = 0;
    inImgContrast.planeNumber[2] = 0;
    inImgContrast.compNumber[0]  = 0;
    inImgContrast.compNumber[1]  = 1;
    inImgContrast.compNumber[2]  = 2;

    // - similarly for the guide image which has just 1 luminance channel
    inImgGuide.nChannels = 1;
    inImgGuide.planeNumber[0] = 0;
    inImgGuide.compNumber[0] = 0;

    rsImgContrast.nChannels = 3;
    rsImgContrast.planeNumber[0] = 1;
    rsImgContrast.planeNumber[1] = 1;
    rsImgContrast.planeNumber[2] = 1;
    rsImgContrast.compNumber[0] = 0;
    rsImgContrast.compNumber[1] = 1;
    rsImgContrast.compNumber[2] = 2;

    // Describe the buffer for resampled output
    rsBuf.nPlanes = 2;
    DescribePlane(rsBuf.planes[0], &rsRGB);
    DescribePlane(rsBuf.planes[1], &rsLum);

    // NOTE: Since the RGB Guide image will be created as a direct copy of the RGB input plane
    //       we don't need a separate description of the resampled guide channels

    // Compute filter kernels
    unsigned tmKernDim = (3 * min(rsWidth, rsHeight)) / 10;
    double   tmStd = 0.10 * min(rsWidth, rsHeight);

    if (bVerbose)
    {
        printf("TM kernel dimension %u\n", tmKernDim);
        printf("Gaussian TM kernel:\n");
    }
    GenerateGaussian(tmLinKern, tmStd, tmKernDim);

    unsigned umKernDim = (3 * min(rsWidth, rsHeight)) / 20;
    double   umStd = 0.05 * min(rsWidth, rsHeight);

    if (bVerbose)
    {
        printf("UM kernel dimension %u\n", umKernDim);
        printf("Modified Gaussian UM kernel:\n");
    }
    const double multip1 = 0.3;
    GenerateGaussian(umLinKern, umStd, umKernDim, sqrt(multip1));

#if FORCE_ALPHA
    Phusion_ImageChannels alphaIm;
    Phusion_Buffer alphaBuf;
    uint32_t w, h;
    ReadMATLABdata(ALPHA_FILE, &alphaBuf, &alphaIm, &w, &h);
//  printf("Alpha W %u H %u\n", w, h);
    double *alphaLUT = (double*)alphaBuf.planes[0].pData;
#else
    const unsigned pivot = (unsigned)(0.5 * 0x400);
    double alphaLUT[0x400];
    for (unsigned in = 0; in < pivot; in++)
    {
        double ref = in / 1024.0;
        alphaLUT[in] = (4.0 * (ref - 1.0) * ref) + 1.0;
    }
    for (unsigned in = pivot; in < 0x400; in++)
    {
        // NOTE: LUT is currently indexed with [0:255]<<2, so reach unity at 0x3FC
        double out = (double)(in - pivot) / (0x3FC - pivot);
        alphaLUT[in] = (out > 1.0) ? 1.0 : out;
    }
#endif

    const double strength = 0.3;  // TODO - obviously supposed to be configurable
    for (unsigned in = 0; in < 0x100; in++)
    {
        double cStretchFact = min(strength * 1.5, 1.0);

        double x = in / 255.0;
        double thrdegpoly = -0.64*x*x*x + 0.96*x*x + 0.68*x;

        double contStretch = (thrdegpoly * cStretchFact + x * (1.0 - cStretchFact)) * 255.0;
        unsigned cst = (contStretch >= 255.0) ? 0xff : ((contStretch < 0.0) ? 0x00 : (unsigned)contStretch);
        outLUT[in] = cst;
    }

    // Lastly, a description of the output image, which is the same as the guide image, so this is easy
    outImg = inImgGuide;

    if (bVerbose)
        printf("Create grayscale guide thumbnail\n");

    Bitmap rsGuide;
    if (!rsGuide.Create(rsWidth, rsHeight, rgbBPP/3))
    {
        fprintf(stderr, "ERROR: Insufficient memory to allocate resampled guide of %ux%u pixels\n",
                        rsWidth, rsHeight);
        return 6;
    }

#if FORCE_COEFFS
{
    Phusion_ImageChannels im;
    Phusion_Buffer coeffBuf;
    uint32_t w, h;

    puts("FORCING COEFFS from file " COEFFS_FILE "\n");
    bool bOK = ReadMATLABdata(COEFFS_FILE, &coeffBuf, &im, &w, &h);
    assert(bOK);
    assert(w == 1);
    assert(h == 72);

    memset(polynomial, 0xff, sizeof(polynomial));

    for (unsigned poly = 0; poly < PHUSION_MAX_POLYNOMIALS; poly++)
    {
        polynomial[poly].nPlanes = w;
        polynomial[poly].nCoeffs = h / PHUSION_MAX_POLYNOMIALS;

        for (unsigned p = 0; p < polynomial[poly].nPlanes; p++)
            for (unsigned c = 0; c < polynomial[poly].nCoeffs; c++)
                polynomial[poly].coeffs[p][c] = (((double*)coeffBuf.planes[0].pData)
                            + (c + (poly*polynomial[poly].nCoeffs) * coeffBuf.planes[0].iStride / sizeof(double)))[p];
    }
}
#endif

    /*********************************************************************************************/
    /*            Start performance measurements here; this is the per-frame processing          */
    /*********************************************************************************************/
    //clock_t start = clock();

    // Resample the two input images to produce the RGB and Luminance thumbnail images
    printf("Resampling\n");
    phResult = Phusion_ResampleImages(phHandle,
                    &rsBuf, &inBuf,
                    rsWidth, rsHeight,
                    imWidth, imHeight,
                    Phusion_ResBox, NULL);
    PH_CHECK(phResult);

    // Convert the RGB thumbnail to grayscale
    profile_start_timing(&profile[PROF_APP_RGB2GRAY]);
    RGB2Gray(rsGuide, rsRGB);
    profile_stop_timing(&profile[PROF_APP_RGB2GRAY]);

    if (bSaveIntermediates)
    {
        // TODO - obviously this should be moved back to the end of the code
        // Save the thumbnails
        if (!rsRGB.Save("rsRGB.bmp", FileFormat_Auto))
        {
            fprintf(stderr, "ERROR: Unable to save RGB thumbnail image\n");
            return 4;
        }
        if (!rsLum.Save("rsLum.bmp", FileFormat_Auto))
        {
            fprintf(stderr, "ERROR: Unable to save NIR thumbnail image\n");
            return 4;
        }
        if (!rsGuide.Save("rsGuide.bmp", FileFormat_Auto))
        {
            fprintf(stderr, "ERROR: Unable to save Guide thumbnail image\n");
            return 4;
        }
    }

    // Create weights from the downsampled RGB input image
    const unsigned nWeights = 8;
// const unsigned nWeights = 6;
//  const unsigned nWeights = 4;

    phpoly_t *pWws = new phpoly_t [(nWeights/2) * rsHeight * rsWidth];  // Planar scratch workspace
    // TODO - !!!! Bodged height for now because we lazily run off the bottom!
    phpoly_t *pWfn = new phpoly_t [(rsHeight+10) * rsWidth * nWeights];      // Interleaved weights

    assert(pWws && pWfn);
    unsigned mwKernWidth = (min(rsWidth, rsHeight) * 4) / 5;
    mwKernWidth += !(mwKernWidth & 1);
    MakeWeights(pWfn, pWws, rsGuide, mwKernWidth, mwKernWidth, nWeights >> 1);
    delete [] pWws;

#if FORCE_WEIGHTS
Phusion_ImageChannels wtImg;
uint32_t wtW, wtH;
bool bOK = ReadMATLABdata("../../../../Test/Refs/Weights.dat", &wtBuf, &wtImg, &wtW, &wtH);
assert(bOK);
printf("Weights read : %u x %u, %u planes\n", wtW, wtH, wtBuf.nPlanes);

#else
    // Describe the weights for use in pixel-processing
    wtBuf.nPlanes = nWeights;
    for (unsigned p = 0; p < nWeights; p++)
    {
        wtBuf.planes[p].pData   = pWfn + (p * rsHeight * rsWidth);
// TODO - stride!!
        wtBuf.planes[p].iStride = rsWidth * sizeof(phpoly_t);
        wtBuf.planes[p].fmt     = Phusion_PlaneRaster1;
#if PHUSION_SINGLE_PRECISION
        wtBuf.planes[p].flags   = Phusion_PlaneFlagSF;
#else
        wtBuf.planes[p].flags   = Phusion_PlaneFlagDF;
#endif
    }
#endif
    // Tone mapping
    int32_t maxTM = 0;
    int32_t minTM = 0x100;

    profile_start_timing(&profile[PROF_APP_TM_CONVOLUTION]);
    ConvolveImage(ftLum, rsLum, tmLinKern, tmKernDim, tmKernDim, maxTM, minTM, 0, false);
    profile_stop_timing(&profile[PROF_APP_TM_CONVOLUTION]);

    if (bVerbose)
        printf("Tone mapping min/max: %x %x\n", minTM, maxTM);

    if (bSaveIntermediates)
        ftLum.Save("ftLum.bmp", FileFormat_Auto);

    assert(maxTM >= 0);
    assert(minTM >= 0);
    ToneMapping(ftLum, rsLum, (unsigned)maxTM, (unsigned)minTM, strength);

    if (bSaveIntermediates)
        ftLum.Save("tmLum.bmp", FileFormat_Auto);

    // Unsharp masking
    int32_t maxUM;
    int32_t minUM;

    profile_start_timing(&profile[PROF_APP_UM_CONVOLUTION]);
    ConvolveImage(clImg, ftLum, umLinKern, umKernDim, umKernDim, maxUM, minUM,
                    (int32_t)((1.0 + multip1) * (1 << 28)), true);
    profile_stop_timing(&profile[PROF_APP_UM_CONVOLUTION]);
    ftLum.Release();

    if (bSaveIntermediates)
        clImg.Save("clImg.bmp", FileFormat_Auto);

    // Describe the buffer for input to standalone gradient calculations
    rsBuf.nPlanes = 2;
    DescribePlane(rsBuf.planes[0], &clImg);  // Guide
    DescribePlane(rsBuf.planes[1], &rsRGB);  // Contrast

    ShowBufferProperties(rsBuf);
    ShowImageDescription(inImgGuide);
    ShowImageDescription(rsImgContrast);

#if FORCE_COEFFS
    profile_start_timing(&profile[PROF_APP_GRADCALCS]);

    // Configuration for performing gradient calculations on the thumbnail images
    Phusion_GradientConfig gradConfig;

    // Per-channel gains for the guide and contrast images
    for (unsigned ch = 0; ch < 3; ch++)
        gradConfig.contGains[ch]  = 1.0;
    gradConfig.guideGains[0] = 1.0;

    // Regularisation parameters
    gradConfig.lambda    = 1e-2;
    gradConfig.tikLambda = 1e-6;

    Phusion_Polynomial firstPoly;
    phResult = Phusion_CalculateGradient(phHandle,
                    &firstPoly, &rsBuf, &rsImgContrast, &inImgGuide,
                    rsWidth, rsHeight, &gradConfig,
                    Phusion_GradDefault, NULL);
    PH_CHECK(phResult);

    profile_stop_timing(&profile[PROF_APP_GRADCALCS]);
#else
    const phfloat_t extLambda = (phfloat_t)0.25;
    const phfloat_t regLambda = (phfloat_t)0.25;
    const phfloat_t tikLambda = (phfloat_t)1e-3;
    const phfloat_t multip2   = PHFLOAT_ONE;

    GradientCalcs(polynomial, rsRGB, clImg, rsLum, wtBuf, rsHeight, rsWidth,
                  multip2, regLambda, tikLambda, extLambda);
#endif
    clImg.Release();

//    ShowPolynomial(stdout, poly);
//    phResult = Phusion_SetPolynomial(phHandle, (Phusion_PolynomialID)0, &poly);
//    PH_CHECK(phResult);

    for (unsigned poly = 0; poly < PHUSION_MAX_POLYNOMIALS; poly++)
    {
#if FORCE_COEFFS
        printf("FORCING POLYNOMIAL %u\n", poly);
#endif
        printf("Setting polynomial %u\n", poly);
        ShowPolynomial(stdout, polynomial[poly]);

        phResult = Phusion_SetPolynomial(phHandle, Phusion_PolynomialID(poly), &polynomial[poly]);
        PH_CHECK(phResult);
    }

    if (bVerbose)
    {
        printf("Input buffer:\n");
        ShowBufferProperties(inBuf);
        printf("Full guide image:\n");
        ShowImageDescription(inImgGuide);
        printf("Full contrast image:\n");
        ShowImageDescription(inImgContrast);
        printf("Output buffer:\n");
        ShowBufferProperties(outBuf);
        printf("Output image:\n");
        ShowImageDescription(outImg);
    }

    inBuf.nPlanes = 1;
    DescribePlane(inBuf.planes[0], &rgb);

    inImgContrast.nChannels = 3;
    inImgContrast.planeNumber[0] = 0;
    inImgContrast.planeNumber[1] = 0;
    inImgContrast.planeNumber[2] = 0;
#if FORCE_COEFFS
    // Coeffs have come from MATLAB which puts R first
    inImgContrast.compNumber[0]  = 2;
    inImgContrast.compNumber[1]  = 1;
    inImgContrast.compNumber[2]  = 0;
#else
    inImgContrast.compNumber[0]  = 0;
    inImgContrast.compNumber[1]  = 1;
    inImgContrast.compNumber[2]  = 2;
#endif

    // TODO - NOTE!!!
    //        This code is currently being asked to read and write luminance from/to a single
    //        plane, described within both outBuf and inBuf which is UNDEFINED behaviour really!

    if (bVerbose)
        printf("Processing pixels\n");

    // Apply the polynomial coefficients to the contrast image channels to produce the output image
    profile_start_timing(&profile[PROF_APP_PROCESS_PIXELS]);
    phResult = Phusion_ProcessPixels(phHandle,  // Handle to library
                    &outBuf, &wtBuf, &inBuf,    // Buffer descriptions
                    &outImg, &inImgContrast,    // Image descriptions
                    rsWidth, rsHeight,          // Weights supplied
                    imWidth, imHeight,          // Stripe dimensions
                    Phusion_ProcessFlags(Phusion_ProcSwitchConfig
                                       | Phusion_ProcUseWeights),
                    nullptr);                   // Blocking API call
    profile_stop_timing(&profile[PROF_APP_PROCESS_PIXELS]);

/*  TODO - in time, a single API call couples gradient calculations and pixel-processing
    phResult = Phusion_ProcessImage(phHandle,                       // Handle to library
                    &outBuf, &inBuf,                                // Buffer descriptions
                    &outImg, &inImgContrast, &inImgGuide,           // Image descriptions
                    imWidth, imHeight,                              // Image dimensions
                    Phusion_ResDefault, NULL, Phusion_GradDefault,  // Default settings
                    (Phusion_ProcessFlags)0, NULL);                 // Blocking API call
*/

    if (bSaveIntermediates)
        outLum.Save("phused.bmp", FileFormat_Auto);

    // And now just steal the luminance back into the RGB components, writing into out
    profile_start_timing(&profile[PROF_APP_APPLY_LUM]);
    ApplyLuminance(out, rgb, lum, outLum, outLUT, alphaLUT, minL);
    profile_stop_timing(&profile[PROF_APP_APPLY_LUM]);

    PH_CHECK(phResult);

    delete [] pWfn;

    /*********************************************************************************************/
    /*            End performance measurements here; that was the per-frame processing           */
    /*********************************************************************************************/
    clock_t end = clock();
    printf("Took %lfs (est. %lffps)\n", (double)(end - start) / CLOCKS_PER_SEC,
                                        (double)CLOCKS_PER_SEC / (end - start));
    printf("  (%ld %ld %ld)\n", (long)end, (long)start, (long)CLOCKS_PER_SEC);

    profile_report(CLARITY_LOG_DIR "Clarity_perf.txt");

    // Finished with the Phusion Library
    phResult = Phusion_Finalise(phHandle);
    PH_CHECK(phResult);

    // The entire processed output is now available in our output buffer, so emit/perform further processing etc..
    printf("Saving output image to '%s'\n", out_filename);
    if (!out.Save(out_filename, FileFormat_Auto))
    {
        fprintf(stderr, "ERROR: Unable to save output image to '%s'\n", out_filename);
        return 3;
    }

    return 0;
}
