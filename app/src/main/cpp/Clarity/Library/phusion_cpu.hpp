/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_cpu.hpp                                               */
/* Description: CPU-based implementation of the Phusion processing            */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __phusion_cpu_hpp
#define __phusion_cpu_hpp
#ifndef __phusion_h
#include "phusion.h"
#endif
// Basic types used within the Phusion_State structure
#ifndef __phusion_types_hpp
#include "phusion_types.hpp"
#endif

#ifdef PHUSION_USE_CPU

// Maximum number of concurrent worker threads used for pixel processing
#define PHUSION_CPU_MAX_THREADS 0x12

// Resampling kernels use pre-computed column and row positions for efficiency,
//    operating upon rectangular blocks of the output image if these static
//    limits are exceeded; limits are chosen to accommodate practical image sizes
#define PHUSION_CPU_MAX_COLW 2048
#define PHUSION_CPU_MAX_ROWH 1536

// Number of pixels converted as a single chunk of data
#define PHUSION_CPU_CONV_CHUNK 256

// Maximum number of pre-calculated vertical interpolations per polynomial
//#define PHUSION_CPU_MAX_VSUMS 64
// TODO - ABSOLUTELY MUST DO SOMETHING TO REDUCE THIS
#define PHUSION_CPU_MAX_VSUMS 6120

// Type used for polynomial coefficients internally
// TODO - may not want to tie this indefinitely to the type of phfloat_t
#if PHUSION_SINGLE_PRECISION
#define PHPOLY_ZERO 0.0F
#define PHPOLY_ONE  1.0F
typedef float phpoly_t;
#else
#define PHPOLY_ZERO 0.0
#define PHPOLY_ONE  1.0
typedef double phpoly_t;
#endif

// Thread-local state for CPU worker thread
typedef struct
{
    // Horizontal source scanlines for resampling input images
    bool bGotHOffsets;  // Indicates whether pre-calculated offsets are available
    struct              // Configuration for which we have pre-calculated offsets
    {
        uint32_t rsPixPerLine;
        uint32_t pixPerLine;
        Phusion_PlaneFormat srcFmt;
        Phusion_PlaneFlags srcFlags;
    } offCfg;

    // Byte offsets into source scanline
    uint32_t hOffsets[PHUSION_CPU_MAX_COLW+1];
    // Scanline offsets into source stripe
    // uint32_t vOffsets[PHUSION_CPU_MAX_ROWH+1];

    //  TODO - workspace sharing is possible here too

    // Scratch space for data during colour space conversion operations
    phfloat_t    convData[PHUSION_MAX_CHANNELS][PHUSION_CPU_CONV_CHUNK];
    phfloat_t convResults[PHUSION_MAX_CHANNELS][PHUSION_CPU_CONV_CHUNK];

    // TODO - INTRODUCE MACRO, SHARE STORAGE AND DECIDE UPON DIMENSIONS
    phfloat_t   inSamples[PHUSION_MAX_CHANNELS][32];
// TODO -
phpoly_t svfCoeffs[PHUSION_MAX_COEFFS][PHUSION_MAX_PLANES][32];
// ouch! 44 X 8 X 32 X 4 = 512kB!


    // Temporary workspace, shared amongst the various processing operations
    //   because at any one time, the thread is only ever performing one type
    union
    {
#if PHUSION_INT_WEIGHTS
        uint16_t vSums[PHUSION_MAX_POLYNOMIALS][PHUSION_CPU_MAX_VSUMS];  // Vertical interpolations
#else
        phpoly_t vSums[PHUSION_MAX_POLYNOMIALS][PHUSION_CPU_MAX_VSUMS];  // Vertical interpolations
#endif
    } ws;

    // Configuration sequence number for the most recently-converted
    //    polynomial coefficients
    uint32_t lastCfgSeq;

    // Polynomial coefficients in appropriate form for pixel-processing kernel
    union
    {
        int32_t  iwcoeffs[PHUSION_MAX_COEFFS][PHUSION_MAX_PLANES];
        int32_t  icoeffs[PHUSION_MAX_COEFFS][PHUSION_MAX_PLANES];
        phpoly_t fcoeffs[PHUSION_MAX_COEFFS][PHUSION_MAX_PLANES];
    } c;
    phpoly_t   mpfcoeffs[PHUSION_MAX_COEFFS][PHUSION_MAX_PLANES][PHUSION_MAX_POLYNOMIALS];

    // State information for this worker thread
    Phusion_ThreadState worker;
} Phusion_CPUThreadState;

// Complete state information for all CPU-based Phusion processing
typedef struct
{
    // Threading information
    unsigned nThreads;

    Phusion_CPUThreadState states[PHUSION_CPU_MAX_THREADS];
} Phusion_CPUState;

// Initialise CPU-based processing
Phusion_Result phusion_cpu_init(Phusion_State *ph_state, int nthreads);

// Finalise CPU-based processing
Phusion_Result phusion_cpu_fin(Phusion_State *ph_state);
#endif

#endif
