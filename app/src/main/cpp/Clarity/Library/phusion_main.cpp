/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_main.cpp                                              */
/* Description: Main interface to Phusion image processing library            */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#include <stdio.h>
#include <string.h>

#include "phusion.h"
#include "phusion_cpu.hpp"
#include "phusion_defs.hpp"
#include "phusion_hw.hpp"
#include "phusion_linsys.hpp"
#include "phusion_main.hpp"
#include "phusion_maths.hpp"
#include "phusion_sampler.hpp"
#include "phusion_utils.hpp"
#include "phusion_worker.hpp"

// Just a single state structure for now, but the code is structured for ready extension
//      to multiple dynamically-allocated states if it should ever be required
static Phusion_State state;

// Allocate an IOP for processing a stripe of pixels
static Phusion_IOPUnit *AllocateIOP(Phusion_State *ph_state);
// Calculate the weights required for bicubic filtering
static void CalculateWeights(int16_t filtWeights[][PHUSION_UWFILT_PTS], unsigned n);
// Find and validate an IOP, given its handle
static Phusion_IOPUnit *FindIOP(Phusion_State *ph_state, Phusion_IOPHandle iop);
// Release an IOP, updating internal state
static void ReleaseIOP(Phusion_State *ph_state, Phusion_IOPUnit *pIOP);
// Scale the per-channel gains before performing the gradient calculations
static void ScaleChannelGains(phfloat_t scaledGains[PHUSION_MAX_CHANNELS], const double gains[PHUSION_MAX_CHANNELS],
                              const Phusion_Buffer &bufIn, const Phusion_ImageChannels &imChans);
// Validate the buffer properties for the given sample data
static bool ValidateBuffer(const Phusion_Buffer *pBuf, uint32_t numLines);
// Validate the list of image channels, w.r.t. the given physical buffer,
//    assuing that the latter has already been validated
static bool ValidateImage(const Phusion_ImageChannels *pImg, const Phusion_Buffer *pBuf);

//---------------------------------------------------------------------------------------------
//  Return version number and feature set of the Phusion library
//---------------------------------------------------------------------------------------------
Phusion_Result Phusion_Identify(Phusion_Version *pVersion, Phusion_Features *pFeatures)
{
    Phusion_FeatureFlags flags = Phusion_FeatureFlags(Phusion_FeatU8Samples | Phusion_FeatU16Samples);
#ifdef PHUSION_USE_CPU
    // CPU backend offers all sample formats
    flags = Phusion_FeatureFlags(flags | Phusion_FeatSFSamples | Phusion_FeatDFSamples);
#endif
    if (pVersion)
    {
        pVersion->majorVersion = PHUSION_MAJOR_VERSION;
        pVersion->minorVersion = PHUSION_MINOR_VERSION;
        pVersion->revision     = PHUSION_REVISION;
    }

    // Build switches should have been set appropriately for the target platform
    //  when compiling the Phusion library; we cannot at this point test the availability
    //  of these processing resources because the backends have not been initialised
#ifdef PHUSION_USE_CPU
    flags = Phusion_FeatureFlags(flags | Phusion_FeatSupportsCPU);
#ifdef PHUSION_USE_SMP
    flags = Phusion_FeatureFlags(flags | Phusion_FeatSupportsSMP);
#endif
#endif
#ifdef PHUSION_USE_GPU
    flags = Phusion_FeatureFlags(flags | Phusion_FeatSupportsGPU);
#endif
#ifdef PHUSION_USE_HW
    flags = Phusion_FeatureFlags(flags | Phusion_FeatSupportsHW);
#endif

    // The _Features structure contains an indication of the size expected by the client code
    //    so that we may extend the structure in future versions of the library without causing
    //    incompatibility; future versions shall return only as much information as the client
    //    expects to receive, whilst replacing the 'featSize' field with the total size of the
    //    features
    if (pFeatures && pFeatures->featSize >= sizeof(Phusion_Features))
    {
        pFeatures->featSize         = sizeof(Phusion_Features);
        pFeatures->flags            = flags;
        pFeatures->maxPlanes        = PHUSION_MAX_PLANES;
        pFeatures->maxChannels      = PHUSION_MAX_CHANNELS;
        pFeatures->maxPolynomials   = PHUSION_MAX_POLYNOMIALS;
    }

    return Phusion_OK;
}

//---------------------------------------------------------------------------------------------
// Initialise Phusion library
//---------------------------------------------------------------------------------------------
Phusion_Result Phusion_Initialise(Phusion_Config config, Phusion_Handle *pHandle)
{
    Phusion_Result res = Phusion_OK;
    Phusion_State *ph_state;
    int err;

    // Set up any conversions required when reading/writing sample data
    Phusion_Sampler::Initialise();

    PHUSION_ASSERT(pHandle);
    if (state.bInitialised)
        return Phusion_TooManyInstances;
    ph_state = &state;

    memset(ph_state, 0, sizeof(*ph_state));
    ph_state->config = config;

    profile_init(ph_state);

    // Set up default polynomials
    ph_state->reqPoly[0].nPlanes = PHUSION_MAX_PLANES;
    ph_state->reqPoly[0].nCoeffs = PHUSION_MAX_COEFFS;
    for(unsigned p = 0; p < PHUSION_MAX_PLANES; p++)
        for(unsigned c = 0; c < PHUSION_MAX_COEFFS; c++)
            ph_state->reqPoly[0].coeffs[p][c] = (c == p) ? 1.0 : 0.0;
    CopyPolynomial(&ph_state->currPoly[0], &ph_state->reqPoly[0]);
    for(unsigned id = 1; id < NOF_ELEMENTS(ph_state->reqPoly); id++)
    {
        CopyPolynomial(&ph_state->reqPoly[id],  &ph_state->reqPoly[0]);
        CopyPolynomial(&ph_state->currPoly[id], &ph_state->reqPoly[0]);
    }

    // Calculate the weights required for bicubic filtering
    CalculateWeights(ph_state->uwFilt, 1U << PHUSION_UWFILT_WBITS);

    ph_state->currentCfgSeq = 0x000cf65e;  // Just avoid starting at zero

    // Create our IPC resources for communication with worker threads
    err = pthread_mutex_init(&ph_state->workMutex, NULL);
    if (!err)
    {
        err = pthread_mutex_init(&ph_state->iopMutex, NULL);
        if (!err)
        {
            err = pthread_cond_init(&ph_state->waitCond, NULL);
            if (!err)
            {
                err = pthread_cond_init(&ph_state->workCond, NULL);
                if (err)
                    pthread_cond_destroy(&ph_state->waitCond);
            }
            if (err)
                pthread_mutex_destroy(&ph_state->iopMutex);
        }
        if (err)
            pthread_mutex_destroy(&ph_state->workMutex);
    }
    if (err)
    {
        fprintf(stderr, "ERROR: Unable to create IPC resource(s), error %d\n", err);
        return Phusion_InitFailed;
    }

#ifdef PHUSION_USE_HW
    // Initialise dedicated HW block if requested
    if (config & Phusion_UseHW)
        res = phusion_hw_init(ph_state, PHUSION_HW_BASE);
#endif
#ifdef PHUSION_USE_GPU
    // Initialise GPU processing if requested
    if (res == Phusion_OK && (config & Phusion_CfgUseGPU))
    {
        res = phusion_gpu_init(ph_state);
        if (res != Phusion_OK)
        {
#ifdef PHUSION_USE_HW
            if (config & Phusion_CfgUseHW))
                (void)phusion_hw_fin(ph_state);
#endif
        }
    }
#endif

#ifdef PHUSION_USE_CPU
    if (res == Phusion_OK && (config & Phusion_CfgUseCPU))
    {
        int nthreads = 1;

#ifdef PHUSION_USE_SMP
        // Initialise Symmetric Multi-Processing if requested
        if (config & Phusion_CfgUseSMP)
            nthreads = -1;
#endif

        res = phusion_cpu_init(ph_state, nthreads);
        if (res != Phusion_OK)
        {
#ifdef PHUSION_USE_GPU
            if (config & Phusion_CfgUseGPU)
                (void)phusion_gpu_fin(ph_state);
#endif
#ifdef PHUSION_USE_HW
            if (config & Phusion_CfgUseHW)
                (void)phusion_hw_fin(ph_state);
#endif
        }
    }
#endif
    if (res != Phusion_OK)
    {
        (void)pthread_cond_destroy(&ph_state->workCond);
        (void)pthread_cond_destroy(&ph_state->waitCond);
        (void)pthread_mutex_destroy(&ph_state->iopMutex);
        (void)pthread_mutex_destroy(&ph_state->workMutex);
        return res;
    }

    ph_state->bInitialised = true;
    *pHandle = ph_state;

    return res;
}

//---------------------------------------------------------------------------------------------
// Finalise Phusion library
//---------------------------------------------------------------------------------------------
Phusion_Result Phusion_Finalise(Phusion_Handle ph)
{
    Phusion_State *ph_state = ValidStateHandle(ph);

    if (!ph_state)
        return Phusion_BadHandle;

    // Finalise processing backends
    ph_state->bFinalising = true;

    // Cancel all IOPs
    pthread_mutex_lock(&ph_state->iopMutex);

    for(unsigned i = 0; i < NOF_ELEMENTS(ph_state->IOPs); i++)
        ph_state->IOPs[i].bCancel = true;

    pthread_mutex_unlock(&ph_state->iopMutex);

    // Wake all threads so that they spot the finalisation request
    pthread_cond_broadcast(&ph_state->workCond);

#ifdef PHUSION_USE_CPU
    Phusion_Result res = phusion_cpu_fin(ph_state);
    if (res != Phusion_OK)
        return res;
#endif
#ifdef PHUSION_USE_GPU
    if (ph_state->config & Eyeteq_CfgUseGPU)
    {
        Phusion_Result res = phusion_gpu_fin(ph_state);
        if (res != Phusion_OK)
            return res;
    }
#endif
#ifdef PHUSION_USE_HW
    if (ph_state->config & Phusion_CfgUseHW)
    {
        Phusion_Result res = phusion_hw_fin(ph_state);
        if (res != Phusion_OK)
            return res;
    }
#endif

    // Release all IPC resources
    (void)pthread_cond_destroy(&ph_state->workCond);
    (void)pthread_cond_destroy(&ph_state->waitCond);
    (void)pthread_mutex_destroy(&ph_state->iopMutex);
    (void)pthread_mutex_destroy(&ph_state->workMutex);

    profile_report(ph_state);

    // Restore state structure to its initial condition
    // NOTE: Importantly this also clearly bInitialised
    memset(ph_state, 0, sizeof(*ph_state));

    // If support for multiple concurrent states is introduced, a dynamically-allocated state
    //    structure must be released at this point; we have only a single static structure

    return Phusion_OK;
}

//---------------------------------------------------------------------------------------------
// Validate the buffer properties for the given image format
//---------------------------------------------------------------------------------------------
bool ValidateBuffer(const Phusion_Buffer *pBuf, uint32_t numLines)
{
    // Check the plane count and then each of the planes in turn
    if (!pBuf || !pBuf->nPlanes || pBuf->nPlanes > PHUSION_MAX_PLANES)
    {
        fprintf(stderr, "ERROR: Null buffer (%p) or invalid plane count (%u)\n",
                        pBuf, (pBuf ? 0 : pBuf->nPlanes));
        return false;
    }

    for(unsigned p = 0; p < pBuf->nPlanes; p++)
    {
        // Known format?
        if (pBuf->planes[p].fmt < Phusion_PlaneRaster1 ||
            pBuf->planes[p].fmt > Phusion_PlaneRaster8)
        {
            fprintf(stderr, "ERROR: Invalid/unknown format for plane %u of buffer\n", p);
            return false;
        }

        // Check alignment requirements are met; natural alignment only for now,
        //   but particular processing backends may have more stringent requirements
        uint32_t align = 1;
        switch (pBuf->planes[p].flags)
        {
            case Phusion_PlaneFlagU8:  align = 1; break;
            case Phusion_PlaneFlagU16: align = 2; break;
            case Phusion_PlaneFlagSF:  align = 4; break;
            case Phusion_PlaneFlagDF:  align = 8; break;
            default:
                fprintf(stderr, "ERROR: Invalid/unknown flags for plane %u of buffer\n", p);
                return false;
        }

        if (!pBuf->planes[p].pData || ((uintptr_t)(pBuf->planes[p].pData) & (align - 1)))
        {
            fprintf(stderr, "ERROR: Data pointer for plane %u is invalid/unaligned\n", p);
            return false;
        }
        if (pBuf->planes[p].iStride & (align - 1))
        {
            fprintf(stderr, "ERROR: Stride for plane %u is invalid/unaligned\n", p);
            return false;
        }

#if PHUSION_VALIDATE_MEMORY
        // Attempt to read from the initial and final byte of each scanline within this plane
        //    to catch invalid memory accesses now rather than during processing

        volatile uint8_t *initPtr = (uint8_t*)pBuf->planes[p].pData;
        volatile uint8_t *finPtr  = initPtr + pBuf->planes[p].iStride - 1;
        volatile uint32_t a = 0;
        volatile uint32_t b = 0;

        for (uint32_t y = 0; y < numLines; y++)
        {
            a = (a << 1) ^ *initPtr;
            b = (b << 1) ^ *finPtr;
            initPtr += pBuf->planes[p].iStride;
            finPtr  += pBuf->planes[p].iStride;
        }
#else
        UNUSED(numLines);
#endif
    }

    return true;
}

//---------------------------------------------------------------------------------------------
// Validate the list of image channels, w.r.t. the given physical buffer,
//    assuing that the latter has already been validated
//---------------------------------------------------------------------------------------------
bool ValidateImage(const Phusion_ImageChannels *pImg, const Phusion_Buffer *pBuf)
{
    // Check the channel count and then each channel in turn
    if (!pImg || !pImg->nChannels || pImg->nChannels > PHUSION_MAX_CHANNELS)
        return false;

    for(unsigned c = 0; c < pImg->nChannels; c++)
    {
        uint8_t p = pImg->planeNumber[c];
        if (p >= pBuf->nPlanes)
        {
            fprintf(stderr, "ERROR: Image channel %u has an invalid plane number %u\n", c, p);
            return false;
        }

        // Ascertain the component count for this plane
        uint8_t nc = 1;
        switch (pBuf->planes[p].fmt)
        {
            case Phusion_PlaneRaster1: nc = 1; break;
            case Phusion_PlaneRaster2: nc = 2; break;
            case Phusion_PlaneRaster3: nc = 3; break;
            default:
                PHUSION_ASSERT(pBuf->planes[p].fmt == Phusion_PlaneRaster4);
                nc = 4;
                break;
        }

        if (pImg->compNumber[c] >= nc)
        {
            fprintf(stderr, "ERROR: Image channel %u has an invalid component number %u\n",
                                    c, pImg->compNumber[c]);
            return false;
        }
    }

    return true;
}

//---------------------------------------------------------------------------------------------
// Filter an image with the supplied filter kernel weights to produce an output image
//---------------------------------------------------------------------------------------------
Phusion_Result Phusion_FilterPixels(Phusion_Handle              ph,          // Handle to Phusion instance
                                    const Phusion_Buffer      *pBufOut,      // Output buffer
                                    const Phusion_Buffer      *pBufIn,       // Input buffer
                                    uint32_t                    pixPerLine,  // Pixels per line
                                    uint32_t                    numLines,    // Number of scanlines
                                    const Phusion_FilterKernel *pKernel,     // Filter kernel to be applied
                                    Phusion_FilterFlags         flags,       // Flags affecting filtering
                                    Phusion_IOPHandle          *pIOP)        // Returned for overlapped I/O
{
    Phusion_State *ph_state;
    Phusion_Result res;

    ph_state = ValidStateHandle(ph);
    if (!ph_state)
        return Phusion_BadHandle;

    // Validate the input and output buffers
    if (!ValidateBuffer(pBufOut, numLines))
        return Phusion_InvalidParameters;
    if (!ValidateBuffer(pBufIn, numLines))
        return Phusion_InvalidParameters;

    // Validate the flags
    if (flags == Phusion_FiltDefault)
        flags = Phusion_FilterFlags(0);  // and NOT overlapped
    else
        return Phusion_InvalidFilterFlags;
/*
    {
        if ((flags & ~Phusion_FiltOverlapped) != )
            return Phusion_InvalidFilterFlags;
    }
*/

    Phusion_IOPUnit *iop = AllocateIOP(ph_state);
    if (!iop)
        return Phusion_TooBusy;

    // Complete the details of the IOP
    iop->pixPerLine = pixPerLine;
    iop->workFlags = (1U << WorkAction_Filter);  // Only filtering required

    if (0)
    {
        res = QueueIOP(ph_state, iop, WorkAction_Filter, numLines, 0, 0, numLines,
                       pBufOut, nullptr, pBufIn);
        switch (res)
        {
            case Phusion_OK: break;
            case Phusion_Pending:
                if (flags & Phusion_FiltOverlapped)
                    *pIOP = iop;
                else
                {
                    res = Phusion_WaitComplete(ph, iop, Phusion_NO_TIMEOUT);
                    if (res != Phusion_OK)
                        (void)ReleaseIOP(ph_state, iop);
                }
                break;
            default:
                (void)ReleaseIOP(ph_state, iop);
                break;
        }
    }
    else
        res = Phusion_NotImplemented;

    return res;
}

//---------------------------------------------------------------------------------------------
// Image sample/colour space conversions
//---------------------------------------------------------------------------------------------
Phusion_Result Phusion_ConvertPixels(Phusion_Handle                ph,         // Handle to Phusion instance
                                     const Phusion_Buffer        *pBufOutput,  // Output buffer
                                     const Phusion_Buffer        *pBufInput,   // Input buffer
                                     const Phusion_ImageChannels *pOutputIm,   // Output image channels description
                                     const Phusion_ImageChannels *pInputIm,    // Input image channels description
                                     uint32_t                     pixPerLine,  // Pixels per line
                                     uint32_t                     numLines,    // Number of scanlines
                                     const Phusion_Conversion   *pConversion,  // Conversion parameters per output
                                     const double               *pGain,        // Gain parameter per input channel
                                     Phusion_ConversionFlags      flags,       // Flags affecting conversion
                                     Phusion_IOPHandle          *pIOP)         // Returned for overlapped I/O
{
    Phusion_State *ph_state;
    Phusion_Result res;

    ph_state = ValidStateHandle(ph);
    if (!ph_state)
        return Phusion_BadHandle;

    // Validate the input and output buffers
    if (!ValidateBuffer(pBufOutput, numLines))
        return Phusion_InvalidParameters;
    if (!ValidateBuffer(pBufInput, numLines))
        return Phusion_InvalidParameters;

    // Validate the flags
    if (flags == Phusion_ConvDefault)
        flags = Phusion_ConversionFlags(0);  // and NOT overlapped
    else
        return Phusion_InvalidConversionFlags;
/*
    {
        if ((flags & ~Phusion_ConvOverlapped) != Phusion_)
            return Phusion_InvalidConversionFlags;
    }
*/

    Phusion_IOPUnit *iop = AllocateIOP(ph_state);
    if (iop)
        return Phusion_TooBusy;

    // Complete the details of the IOP
    memcpy(&iop->imgContrast, pInputIm, sizeof(iop->imgContrast));
    memcpy(&iop->imgOutput, pOutputIm, sizeof(iop->imgOutput));
    if (flags & Phusion_ConvGain)
        memcpy(iop->convGain, pGain, pInputIm->nChannels * sizeof(double));
    if (flags & Phusion_ConvMatrix)
        memcpy(&iop->conversion, pConversion, pOutputIm->nChannels * sizeof(*pConversion));
    iop->pixPerLine = pixPerLine;
    iop->workFlags = (1U << WorkAction_Convert);  // Only conversion required

    res = QueueIOP(ph_state, iop, WorkAction_Convert, numLines, 0, 0, numLines,
                   pBufOutput, nullptr, pBufInput);
    switch (res)
    {
        case Phusion_OK: break;
        case Phusion_Pending:
            if (flags & Phusion_ResOverlapped)
                *pIOP = iop;
            else
            {
                res = Phusion_WaitComplete(ph, iop, Phusion_NO_TIMEOUT);
                if (res != Phusion_OK)
                    (void)ReleaseIOP(ph_state, iop);
            }
            break;
        default:
            (void)ReleaseIOP(ph_state, iop);
            break;
    }

    return res;
}

//---------------------------------------------------------------------------------------------
// Compute resampled images from the given input images using the specified resampling filter,
//   for subsequent use in the gradient calculation stage
//---------------------------------------------------------------------------------------------
Phusion_Result Phusion_ResampleImages(Phusion_Handle        ph,
                                      const Phusion_Buffer *pBufOutput,     // Output buffer
                                      const Phusion_Buffer *pBufInput,      // Input buffer
                                      uint32_t              outPixPerLine,  // Output pixels per line
                                      uint32_t              outNumLines,    // Output height in scanlines
                                      uint32_t              inPixPerLine,   // Input pixels per line
                                      uint32_t              inNumLines,     // Input height in scanlines
                                      Phusion_ResampleFlags flags,          // Flags affecting resampling
                                      Phusion_IOPHandle   *pIOP)            // Returned for overlapped I/O
{
    Phusion_State *ph_state;
    Phusion_Result res;

    ph_state = ValidStateHandle(ph);
    if (!ph_state)
        return Phusion_BadHandle;

    // Validate the input and output buffers
    if (!ValidateBuffer(pBufOutput, outNumLines))
        return Phusion_InvalidParameters;
    if (!ValidateBuffer(pBufInput, inNumLines))
        return Phusion_InvalidParameters;

    // Check that the input and output buffers have the same format for each used plane
    if (pBufOutput->nPlanes > pBufInput->nPlanes)
       return Phusion_IncompatibleBuffers;
    for(unsigned p = 0; p < pBufOutput->nPlanes; p++)
       if (pBufOutput->planes[p].fmt != pBufInput->planes[p].fmt)
           return Phusion_IncompatibleBuffers;

    // Validate the flags
    if (flags == Phusion_ResDefault)
       flags = Phusion_ResNearest;  // and NOT overlapped
    else
    {
        switch (flags & ~Phusion_ResOverlapped)
        {
            case Phusion_ResNearest:
            case Phusion_ResBox:
                break;
            default:
                return Phusion_InvalidResampleFlags;
        }
    }

    // Allocate, complete and queue this stripe of pixels for processing
    Phusion_IOPUnit *iop = AllocateIOP(ph_state);
    if (!iop)
        return Phusion_TooBusy;

    // Complete the details of the IOP
    iop->rsPixPerLine = outPixPerLine;
    iop->pixPerLine = inPixPerLine;
    iop->rsFlags = flags;
    iop->workFlags = (1U << WorkAction_Resample);  // Only resampling required

    res = QueueIOP(ph_state, iop, WorkAction_Resample, outNumLines, 0, 0, inNumLines,
                   pBufOutput, nullptr, pBufInput);
    switch (res)
    {
        case Phusion_OK: break;
        case Phusion_Pending:
            if (flags & Phusion_ResOverlapped)
                *pIOP = iop;
            else
            {
                res = Phusion_WaitComplete(ph, iop, Phusion_NO_TIMEOUT);
                if (res != Phusion_OK)
                    (void)ReleaseIOP(ph_state, iop);
            }
            break;
        default:
            (void)ReleaseIOP(ph_state, iop);
            break;
    }

    return res;
}

//---------------------------------------------------------------------------------------------
// Calculate the Spectral Edge gradient from the given contrast and guide (thumbnail) images
//---------------------------------------------------------------------------------------------
Phusion_Result Phusion_CalculateGradient(Phusion_Handle                 ph,
                                         Phusion_Polynomial           *pPoly,        // Output polynomial coefficients
                                         const Phusion_Buffer         *pBufIn,       // Input images
                                         const Phusion_ImageChannels  *pContrastIm,  // Contrast image channels description
                                         const Phusion_ImageChannels  *pGuideIm,     // Guide image channels description
                                         uint32_t                       pixPerLine,  // Pixels per scanline
                                         uint32_t                       numLines,    // Stripe height in scanlines
                                         const Phusion_GradientConfig *pConfig,      // Configuration settings
                                         Phusion_GradientFlags          flags,       // Flags affecting gradient calcs
                                         Phusion_IOPHandle            *pIOP)         // Returned for overlapped I/O
{
    Phusion_State *ph_state;
    Phusion_Result res;

    ph_state = ValidStateHandle(ph);
    if (!ph_state)
        return Phusion_BadHandle;

    // Validate the input buffer and images
    if (!ValidateBuffer(pBufIn, numLines))
        return Phusion_InvalidParameters;
    if (!ValidateImage(pContrastIm, pBufIn) || !ValidateImage(pGuideIm, pBufIn))
        return Phusion_InvalidParameters;

    // Validate the flags
    if (flags == Phusion_GradDefault)
        flags = Phusion_GradientFlags(Phusion_GradInitialStripe |
                                      Phusion_GradFinalStripe);  // and NOT overlapped
    else
    {
        if (flags & ~Phusion_GradientFlags(Phusion_GradResample |
                                           Phusion_GradInitialStripe |
                                           Phusion_GradFinalStripe |
                                           Phusion_GradOverlapped))
            return Phusion_InvalidGradientFlags;
    }

    // Allocate, complete and queue this stripe of pixels for processing
    Phusion_IOPUnit *iop = AllocateIOP(ph_state);
    if (!iop)
        return Phusion_TooBusy;

    // Complete the details of the IOP
    memcpy(&iop->grConfig, pConfig, sizeof(iop->grConfig));
    memcpy(&iop->imgContrast, pContrastIm, sizeof(iop->imgContrast));
    memcpy(&iop->imgGuide, pGuideIm, sizeof(iop->imgGuide));
    iop->grPolyResult = pPoly;
    iop->pixPerLine = pixPerLine;
    iop->grFlags = flags;
    iop->workFlags = (1U << WorkAction_CalculateGrad);

    // Perform the format-dependent scaling of the channel gains once before issuing the IOP rather
    //    than have each worker thread have to do that locally
    ScaleChannelGains(iop->scContGains,  iop->grConfig.contGains,  *pBufIn, *pContrastIm);
    ScaleChannelGains(iop->scGuideGains, iop->grConfig.guideGains, *pBufIn, *pGuideIm);

    uint32_t rsNumLines;
    if (flags & Phusion_GradResample)
    {
        iop->workFlags |= (1U << WorkAction_Resample);  // Resampling required too

        // Calculate the post-resampling size of this input stripe
        iop->rsPixPerLine = pixPerLine / PHUSION_DEFAULT_RS_FACTOR;  // Round-to-zero in each dimension
        rsNumLines        = numLines   / PHUSION_DEFAULT_RS_FACTOR;
    }
    else
        rsNumLines = numLines;  // Input images have already been resampled

    // Initialise the linear system contribution of this IOP
    ClearLinSysCont(iop->linSysCont);

    if (flags & Phusion_GradFinalStripe)
        iop->workFlags |= (1U << WorkAction_SolveLinSys);  // Solve linear system after this stripe

    res = QueueIOP(ph_state, iop, WorkAction_CalculateGrad, rsNumLines, 0, 0, numLines,
                   pBufIn, nullptr, pBufIn);
    switch (res)
    {
        case Phusion_OK: break;
        case Phusion_Pending:
            if (flags & Phusion_GradOverlapped)
                *pIOP = iop;
            else
            {
                res = Phusion_WaitComplete(ph, iop, Phusion_NO_TIMEOUT);
                if (res != Phusion_OK)
                    (void)ReleaseIOP(ph_state, iop);
            }
            break;
        default:
            (void)ReleaseIOP(ph_state, iop);
    }

    return res;
}

//---------------------------------------------------------------------------------------------
// Process a complete image as a single operation, performing all stages of the Phusion
//    algorithm and producing only the final processed output
//---------------------------------------------------------------------------------------------
Phusion_Result Phusion_ProcessImage(Phusion_Handle                 ph,          // Handle to Phusion instance
                                    const Phusion_Buffer         *pBufOutput,   // Output buffer
                                    const Phusion_Buffer         *pBufInput,    // Input buffer
                                    const Phusion_ImageChannels  *pOutputIm,    // Output image channels description
                                    const Phusion_ImageChannels  *pContrastIm,  // Contrast image channels description
                                    const Phusion_ImageChannels  *pGuideIm,     // Guide image channels description
                                    uint32_t                       imWidth,     // Image width, pixels
                                    uint32_t                       imHeight,    // Image height, pixels
                                    Phusion_ResampleFlags          rsFlags,     // Flags affecting resampling
                                    const Phusion_GradientConfig *pGradConfig,  // Gradient settings
                                    Phusion_GradientFlags          grFlags,     // Flags affecting gradient calcs
                                    Phusion_ProcessFlags           flags,       // Flags affecting processing
                                    Phusion_IOPHandle            *pIOP)         // Returned for overlapped I/O
{
    Phusion_State *ph_state;
    Phusion_IOPUnit *iop;
    Phusion_Result res;

    // Locate our state information
    ph_state = ValidStateHandle(ph);
    if (!ph_state)
        return Phusion_BadHandle;

    // Validate the input and output buffers
    if (pBufOutput)
    {
        if (!ValidateBuffer(pBufOutput, imHeight))
            return Phusion_InvalidParameters;
    }
    else
        pBufOutput = pBufInput;   // Use input buffer for output too, if unspecified
    if (!pOutputIm)
        pOutputIm = pGuideIm;  // Use guide image channels for output too, if unspecified

    // Validate the input images
    if (!ValidateBuffer(pBufInput, imHeight) || !ValidateImage(pContrastIm, pBufInput) ||
        !ValidateImage(pGuideIm, pBufInput)  || !ValidateImage(pOutputIm, pBufOutput))
    {
        return Phusion_InvalidParameters;
    }

    // Allocate, complete and queue this stripe of pixels for processing
    iop = AllocateIOP(ph_state);
    if (!iop)
        return Phusion_TooBusy;

    // Complete the details of the IOP
    memcpy(&iop->grConfig, pGradConfig, sizeof(iop->grConfig));
    memcpy(&iop->imgContrast, pContrastIm, sizeof(iop->imgContrast));
    memcpy(&iop->imgGuide, pGuideIm, sizeof(iop->imgGuide));
    memcpy(&iop->imgOutput, pOutputIm, sizeof(iop->imgOutput));
    iop->imgRsGuide.nChannels = 0;
    iop->imgRsContrast.nChannels = 0;
    iop->grPolyResult = nullptr;
    iop->pixPerLine = imWidth;
    iop->cfgSeq     = 0;
    iop->pcFlags    = Phusion_ProcessFlags(flags | Phusion_ProcSwitchConfig);
    iop->workFlags  = (1U << WorkAction_ProcessPixels)
                    | (1U << WorkAction_SolveLinSys)
                    | (1U << WorkAction_CalculateGrad);

    res = QueueIOP(ph_state, iop, WorkAction_CalculateGrad, imHeight, 0, 0, imHeight,
                   pBufOutput, nullptr, pBufInput);
    switch (res)
    {
        case Phusion_OK: break;
        case Phusion_Pending:
            if (flags & Phusion_ProcOverlapped)
                *pIOP = iop;
            else
            {
                res = Phusion_WaitComplete(ph, iop, Phusion_NO_TIMEOUT);
                if (res != Phusion_OK)
                    (void)ReleaseIOP(ph_state, iop);
            }
            break;
        default:
            (void)ReleaseIOP(ph_state, iop);
    }

    return res;
}

//---------------------------------------------------------------------------------------------
// Process a stripe of pixel data; processing may be performed in-situ by supplying NULL pFBOut
// if the input and output image formats are identical. Pixel processing may be performed
// using 'overlapped I/O', returning control to the calling code before the operation completes
// asynchronously at a later time.
//---------------------------------------------------------------------------------------------
Phusion_Result Phusion_ProcessPixels(Phusion_Handle                ph,
                                     const Phusion_Buffer        *pBufOutput,   // Output buffer
                                     const Phusion_Buffer        *pBufWeights,  // Weights buffer
                                     const Phusion_Buffer        *pBufInput,    // Input buffer
                                     const Phusion_ImageChannels *pOutputIm,    // Output image channels description
                                     const Phusion_ImageChannels *pContrastIm,  // Contrast image channels description
                                     uint32_t                      weightsX,    // Weights per line or X fraction+scaling
                                     uint32_t                      weightsY,    // Lines of weights or Y fraction+scaling
                                     uint32_t                      pixPerLine,  // Pixels per scanline
                                     uint32_t                      numLines,    // Stripe height in scanlines
                                     Phusion_ProcessFlags          flags,       // Flags affecting processing
                                     Phusion_IOPHandle           *pIOP)         // Returned for overlapped I/O
{
    Phusion_State *ph_state;
    Phusion_IOPUnit *iop;
    Phusion_Result res;

    // Locate our state information
    ph_state = ValidStateHandle(ph);
    if (!ph_state)
        return Phusion_BadHandle;

    // Validate the input and output buffers
    if (pBufOutput)
    {
        if (!ValidateBuffer(pBufOutput, numLines))
            return Phusion_InvalidParameters;
    }
    else
        pBufOutput = pBufInput;   // Use input buffer for output too, if unspecified

    if (!ValidateBuffer(pBufInput, numLines) ||
        !ValidateImage(pContrastIm, pBufInput) ||
        !ValidateImage(pOutputIm, pBufOutput))
    {
        return Phusion_InvalidParameters;
    }

    // Validate the weights buffer
    uint32_t wgtLines = weightsY;
    if (flags & Phusion_ProcUseWeights)
    {
        if (flags & Phusion_ProcStripedWeights)
        {
            uint32_t wgtOffset = (((int32_t)weightsY >> Phusion_WgtScaleBits) << Phusion_WgtFractBits);
            uint32_t scLines = numLines * (weightsY & Phusion_WgtScaleMask);

            // Round up when checking the available scanlines; partially-used lines must be valid
            const uint32_t round = (1U << Phusion_WgtScaleBits) - 1U;
            wgtLines = (wgtOffset + scLines + round) >> Phusion_WgtScaleBits;
        }
printf("ProcessPixels: wgtLines %u\n", wgtLines);

        if (!ValidateBuffer(pBufWeights, wgtLines))
            return Phusion_InvalidParameters;
    }
    else
        wgtLines = 0U;

    // Collect the appropriate polynomial coefficients for this operation
    uint32_t cfgSeq = ph_state->currentCfgSeq;
    if (flags & Phusion_ProcSwitchConfig)
    {
        for(Phusion_PolynomialID id = 0; id < PHUSION_MAX_POLYNOMIALS; id++)
        {
            if (ph_state->reqPolyChanged & (1U << id))
                CopyPolynomial(&ph_state->currPoly[id], &ph_state->reqPoly[id]);
        }
        ph_state->reqPolyChanged = 0U;

        // Ensure that the workers reconfigure themselves where necessary; since not all workers
        //   will necessarily receive a workload for the first IOP to use a new configuration,
        //   and some may indeed miss the use of a particular configuration entirely, we maintain
        //   a sequence number. Any worker that caches configuration information shall also remember
        //   the sequence number that was then current, and invalidate it upon detecting a change.
        if (cfgSeq == ph_state->currentCfgSeq)
            ph_state->currentCfgSeq++;
    }

    // Allocate, complete and queue this stripe of pixels for processing
    iop = AllocateIOP(ph_state);
    if (!iop)
        return Phusion_TooBusy;

    // Complete the details of the IOP
    memcpy(&iop->imgContrast, pContrastIm, sizeof(iop->imgContrast));
    memcpy(&iop->imgOutput, pOutputIm, sizeof(iop->imgOutput));
    iop->imgGuide.nChannels = 0;
    iop->imgRsGuide.nChannels = 0;
    iop->imgRsContrast.nChannels = 0;
    for(unsigned id = 0; id < NOF_ELEMENTS(iop->poly); id++)
        CopyPolynomial(&iop->poly[id], &ph_state->currPoly[id]);

    iop->pixPerLine = pixPerLine;
    iop->cfgSeq     = cfgSeq;
    iop->pcFlags    = flags;
    iop->workFlags  = (1U << WorkAction_ProcessPixels);  // Only pixel-processing required

if (weightsX && weightsY)
{
// TODO
printf("FUDGING WEIGHTS X/Y\n");
// Round down when calculating scales factors; required to ensure that we don't try to
//    read beyond the available weights in each dimension
int wXScale = pixPerLine / weightsX;
int wYScale = numLines / weightsY;

weightsX = (weightsX << Phusion_WgtScaleBits) / pixPerLine;
weightsY = (weightsY << Phusion_WgtScaleBits) / numLines;

weightsX |= (-(wXScale << (Phusion_WgtFractBits)) / 2) << Phusion_WgtScaleBits;
weightsY |= (-(wYScale << (Phusion_WgtFractBits)) / 2) << Phusion_WgtScaleBits;

printf("  -> %x : %x\n", weightsX, weightsY);
}
    res = QueueIOP(ph_state, iop, WorkAction_ProcessPixels,
                   numLines, weightsX, weightsY, numLines,
                   pBufOutput, wgtLines ? pBufWeights : 0, pBufInput);
    switch (res)
    {
        case Phusion_OK: break;
        case Phusion_Pending:
            if (flags & Phusion_ProcOverlapped)
                *pIOP = iop;
            else
            {
                res = Phusion_WaitComplete(ph, iop, Phusion_NO_TIMEOUT);
                if (res != Phusion_OK)
                    (void)ReleaseIOP(ph_state, iop);
            }
            break;
        default:
            (void)ReleaseIOP(ph_state, iop);
    }

    return res;
}

//---------------------------------------------------------------------------------------------
// Specify the polynomial coefficients to be used for subsequent pixel-processing
//---------------------------------------------------------------------------------------------
Phusion_Result Phusion_SetPolynomial(Phusion_Handle             ph,    // Handle to Phusion instance
                                     Phusion_PolynomialID       id,    // ID of polynomial to be set
                                     const Phusion_Polynomial *pPoly)  // Polynomial coefficients
{
    Phusion_State *ph_state;

    ph_state = ValidStateHandle(ph);
    if (!ph_state)
        return Phusion_BadHandle;
    if (!pPoly || pPoly->nPlanes > PHUSION_MAX_PLANES || pPoly->nCoeffs > PHUSION_MAX_COEFFS ||
        id >= PHUSION_MAX_POLYNOMIALS)
    {
        PHUSION_ASSERT(!"Missing/excessive polynomial coefficients");
        return Phusion_InvalidParameters;
    }

    // Record these coefficients as the most-recently requested
    CopyPolynomial(&ph_state->reqPoly[id], pPoly);
    ph_state->reqPolyChanged |= (1U << id);

    return Phusion_OK;
}

//---------------------------------------------------------------------------------------------
// Retrieve the most-recently set polynomial coefficients
//---------------------------------------------------------------------------------------------
Phusion_Result Phusion_GetPolynomial(Phusion_Handle         ph,    // Handle to Phusion instance
                                     Phusion_PolynomialID   id,    // ID of polynomial to retrieve
                                     Phusion_Polynomial   *pPoly)  // Receives polynomial coefficients

{
    Phusion_State *ph_state;

    ph_state = ValidStateHandle(ph);
    if (!ph_state)
        return Phusion_BadHandle;
    if (!pPoly || pPoly->nPlanes > PHUSION_MAX_PLANES || pPoly->nCoeffs > PHUSION_MAX_COEFFS ||
        id >= PHUSION_MAX_POLYNOMIALS)
    {
        PHUSION_ASSERT(!"Missing/excessive planes");
        return Phusion_InvalidParameters;
    }

    // Retrieve the most-recently supplied set of coefficients
    CopyPolynomial(pPoly, &ph_state->reqPoly[id]);

    return Phusion_OK;
}

//---------------------------------------------------------------------------------------------
// Allocate an IOP structure
//---------------------------------------------------------------------------------------------
Phusion_IOPUnit *AllocateIOP(Phusion_State *ph_state)
{
    Phusion_IOPUnit *pIOP = NULL;
    unsigned i;

    pthread_mutex_lock(&ph_state->iopMutex);

    // Search round-robin, looking for an unallocated IOP
    i = ph_state->oldestIOP;
    do
    {
        if (!ph_state->IOPs[i].bAllocated)
        {
            pIOP = &ph_state->IOPs[i];
            pIOP->bCancel    = false;
            pIOP->bAllocated = true;
            break;
        }
        if (++i >= NOF_ELEMENTS(ph_state->IOPs))
            i = 0;
    } while (i != ph_state->oldestIOP);

    pthread_mutex_unlock(&ph_state->iopMutex);

    return pIOP;
}

//---------------------------------------------------------------------------------------------
// Release an IOP, updating internal state
//
// NOTE: Caller must already have locked the IOP mutex
//---------------------------------------------------------------------------------------------
void ReleaseIOP(Phusion_State *ph_state, Phusion_IOPUnit *pIOP)
{
    // IOP structure is no longer required and may be reused
    PHUSION_ASSERT(pIOP >= ph_state->IOPs && pIOP <= &ph_state->IOPs[PHUSION_MAX_IOPS-1]);

    pIOP->bAllocated = false;
    pIOP->bCancel = false;
    pIOP->bValid = false;

    // If we have just cancelled the oldest IOP, advance to the next
    if (pIOP == &ph_state->IOPs[ph_state->oldestIOP])
    {
        unsigned nextIOP = ph_state->oldestIOP;
        if (++nextIOP >= NOF_ELEMENTS(ph_state->IOPs))
            nextIOP = 0;
        ph_state->oldestIOP = nextIOP;
    }
}

//---------------------------------------------------------------------------------------------
// Find and validate an IOP, given its handle
//
// NOTE: Caller must already have locked the IOP mutex
//---------------------------------------------------------------------------------------------
Phusion_IOPUnit *FindIOP(Phusion_State *ph_state, Phusion_IOPHandle iop)
{
    if (iop >= &ph_state->IOPs[0] && iop <= &ph_state->IOPs[PHUSION_MAX_IOPS-1])
    {
        Phusion_IOPUnit *pIOP = iop;
        if (pIOP->bValid)
            return pIOP;
    }
    return NULL;
}

//---------------------------------------------------------------------------------------------
// Queue an IOP for subsequent processing
//---------------------------------------------------------------------------------------------
Phusion_Result QueueIOP(Phusion_State *ph_state, Phusion_IOPUnit *pIOP, WorkerAction action,
                        uint32_t outNumLines, uint32_t wgtX, uint32_t wgtY, uint32_t inNumLines,
                        const Phusion_Buffer *pBufOut, const Phusion_Buffer *pBufWgt,
                        const Phusion_Buffer *pBufIn)
{
    // Partition the workload vertically according to the measured performance of each of
    //    the worker threads of each of the active backends
    unsigned nworkers = 0;
    unsigned w = 0;

    nworkers = ph_state->nextThreadNum;
    if (!nworkers)
    {
        PHUSION_ASSERT(!"No enabled processing backends");
        return Phusion_BadConfig;
    }

    if (action == WorkAction_SolveLinSys)
    {
        // Only the CPU backend has the ability to solve the linear system at present
        Phusion_Result res = AssignIndivisibleTask(ph_state, Worker_CPU, &w);
        if (res != Phusion_OK)
        {
            PHUSION_ASSERT(!"No suitable worker threads for solving linear system");
            return res;
        }

        // Deactivate lower-numbered worker threads
        for (unsigned t = 0; t < w; t++)
        {
            pIOP->unit[t].numLines = 0;
            pIOP->unit[t].bCompleted = true;
        }

        // Just one worker thread involved
        pIOP->unit[w].numLines   = 1;
        pIOP->unit[w].availLines = 1;
        pIOP->unit[w].rsNumLines = 1;
        pIOP->unit[w].bCompleted = false;
        pIOP->nUnits = 1;
        w++;
    }
    else
    {
        // Vertical scaling factor from output to input, and fractional scanline
        uint32_t vScaleFact = ((inNumLines << 16) / outNumLines);
        uint32_t fY = 0;

        // printf("%u scanlines to %u scanlines, fact %x\n", inNumLines, outNumLines, vScaleFact);

        // Work out how many scanlines of work to request of each worker thread
        AllocateWork(ph_state, pIOP, pIOP->pixPerLine, outNumLines);

        Phusion_Buffer bufOut = *pBufOut;
        Phusion_Buffer bufIn  = *pBufIn;
        Phusion_Buffer bufWgt;
        if (pBufWgt)
            bufWgt = *pBufWgt;
        else
            bufWgt.nPlanes = 0;

        // Remember the original input buffers (for pixel-processing stage of _ProcessImage API)
        pIOP->bufOut = bufOut;
        pIOP->bufIn  = bufIn;

        // Keep a count of how many workers have work to do for this IOP,
        //   so that the last over the finish line can signal its completion
        pIOP->nUnits = 0;

        // Construct a work unit for each of the available workers; we apportion the
        //   output scanlines amongst the workers, determining the input scanlines
        //   required to produce that output.
        //
        // For a more conventional image processing system it would probably be true
        //   that whichever of the input pixel count and the output pixel count is the
        //   larger number, it is that which limits the performance. For Phusion since
        //   we're employing nearest-neighbour subsampling and the gradient calculations
        //   (the only resize operation) are mathematically intensive, it is reasonable to
        //   use the output pixel count as the measure of work load.

        unsigned outY = 0;
        unsigned inY = 0;

printf("resampling %u to %u\n", inNumLines, outNumLines);

        while (outY < outNumLines && w < nworkers)
        {
            uint32_t outStripeHeight;
            uint32_t inStripeHeight;
            uint32_t availLines;

            PHUSION_ASSERT(w < nworkers);

            // Number of scanlines to be processed by this worker
            outStripeHeight = pIOP->unit[w].numLines;
printf("outStripeHeight is %u\n", outStripeHeight);
            if (outStripeHeight)
            {
                uint32_t endY;
                unsigned p;

                PHUSION_ASSERT(outStripeHeight <= outNumLines - outY);

                // TODO - this constraint is not yet imposed
                // PHUSION_ASSERT(!(stripeHeight & 1));  // Simplifies handling of subsampled formats

                // If we're performing on-the-fly resampling in the gradient calculations then
                //    we must still handle the relationship between input and post-rs co-ords
                if (action == WorkAction_Resample ||
                    (action == WorkAction_CalculateGrad && (pIOP->grFlags & Phusion_GradResample)))
                {
//printf("endY %u inY %u fY %u vScaleFact %u\n", endY, inY, fY, vScaleFact);
                    endY = (inY << 16) + fY + (vScaleFact * outStripeHeight);
                    inStripeHeight = ((endY + 0xffff) >> 16) - inY;
                }
                else
                {
                    // By default, there's a 1:1 mapping between input and output co-ordinate spaces
                    inStripeHeight = outStripeHeight;
                    endY = (inY + inStripeHeight) << 16;
                }
                printf("after scaling %u %x %x\n", inStripeHeight, endY, fY);

                // Although the gradient calculations still require knowledge of whether there's
                //   vertical context below the input stripe
                availLines = inNumLines - inY;
                if (availLines > outStripeHeight)
                    availLines = outStripeHeight + 1;

                // Buffers for this worker
                pIOP->unit[w].bufOut = bufOut;
                pIOP->unit[w].bufWgt = bufWgt;
                pIOP->unit[w].bufIn  = bufIn;

                // Set addresses of input/output scanlines as needed by this work unit
                for (p = 0; p < bufOut.nPlanes; p++)
                    pIOP->unit[w].bufOut.planes[p].pData = (uint8_t*)bufOut.planes[p].pData
                                                         + ((int32_t)outY * bufOut.planes[p].iStride);
                for (p = 0; p < bufIn.nPlanes; p++)
                    pIOP->unit[w].bufIn.planes[p].pData  = (uint8_t*)bufIn.planes[p].pData
                                                         + ((int32_t)inY  * bufIn.planes[p].iStride);

                // Work unit properties
                pIOP->unit[w].numLines   = inStripeHeight;
                pIOP->unit[w].rsNumLines = outStripeHeight;
                pIOP->unit[w].availLines = availLines;
                pIOP->unit[w].bCompleted = false;
                pIOP->unit[w].wgtX       = wgtX;
                pIOP->unit[w].wgtY       = wgtY;

printf("work unit %u %u %u %u from %u,%u\n", pIOP->unit[w].numLines,
pIOP->unit[w].rsNumLines,
pIOP->unit[w].availLines,
pIOP->unit[w].bCompleted,
inY, outY);

                outY += outStripeHeight;
                inY   = (endY >> 16);
                fY    = (endY & 0xffff);

                // Track how many workers have been activated
                pIOP->nUnits++;
            }
            else
                inNumLines = availLines = 0;

            //printf("Worker %u : lines [%u,%u) avail to %u -> output [%u,%u)\n", w,
            //       inY - inStripeHeight, inY, (inY + availLines - inStripeHeight),
            //       outY - outStripeHeight, outY);

            w++;
        }

        // TODO - catch any failed allocation attempts here and check arithmetic
        printf("After allocation, %u of %u lines used\n", inY, inNumLines);
    }

    // Deactivate all higher-numbered workers/work units
    while (w < PHUSION_MAX_WORKERS)
    {
        pIOP->unit[w].numLines   = 0;
        pIOP->unit[w].rsNumLines = 0;
        pIOP->unit[w].bCompleted = true;
        w++;
    }

    // Present the IOP to the worker threads, waking any sleeping threads that we want to use
    pthread_mutex_lock(&ph_state->iopMutex);

    // Mark this IOP has suitable for acceptance by worker threads
    pIOP->workAction = action;
    pIOP->bCancel    = false;
    pIOP->bValid     = true;
    printf("Queuing work %d %u %u\n", action, inNumLines, outNumLines);
    pthread_cond_broadcast(&ph_state->workCond);

    pthread_mutex_unlock(&ph_state->iopMutex);

    return Phusion_Pending;
}

//---------------------------------------------------------------------------------------------
// Wait for completion of a processing operation
//---------------------------------------------------------------------------------------------
Phusion_Result Phusion_WaitComplete(Phusion_Handle ph, Phusion_IOPHandle iop, uint32_t timeout)
{
    Phusion_Result res = Phusion_OK;
    Phusion_State *ph_state;
    Phusion_IOPUnit *pIOP;

    ph_state = ValidStateHandle(ph);
    if (!ph_state)
        return Phusion_BadHandle;

    pthread_mutex_lock(&ph_state->iopMutex);

    pIOP = FindIOP(ph_state, iop);
    if (pIOP)
    {
        uint32_t prevTime = CurrentTime();
        while (pIOP->nUnits && timeout > 0)
        {
            uint32_t timeNow;
            int rc;

            if (timeout == Phusion_NO_TIMEOUT)
                rc = pthread_cond_wait(&ph_state->waitCond, &ph_state->iopMutex);
            else
            {
                struct timespec ts;
#ifdef WIN32
                // This ugliness is necessary because Win32 does not have the
                //    non-ISO clock_gettime() function used below
                GetRealTimeAsTimespec(&ts);
#else
                uint32_t secs, nsecs;
                clock_gettime(CLOCK_REALTIME, &ts);

                nsecs = ts.tv_nsec + (timeout * 1000);
                secs  = nsecs / 1000000000;
                ts.tv_nsec = nsecs - (secs * 1000000000);
                ts.tv_sec += secs;
#endif
                rc = pthread_cond_timedwait(&ph_state->waitCond, &ph_state->iopMutex, &ts);

                timeNow = CurrentTime();

                timeout -= (timeNow - prevTime);
                prevTime = timeNow;
            }

            if (rc)
            {
                fprintf(stderr, "ERROR: pthread_cond_(timed)wait returned error %d\n", rc);
                res = Phusion_WaitFailed;
                break;
            }
        }

        if (res == Phusion_OK)
        {
            if (pIOP->nUnits)
            {
                // Still in progress
                res = Phusion_Pending;
            }
            else
            {
                // Return final results of gradient calculation
                if (pIOP->workAction == WorkAction_CalculateGrad)
                {
                    for(unsigned id = 0; id < NOF_ELEMENTS(pIOP->grPolyResult); id++)
                        CopyPolynomial(&pIOP->grPolyResult[id], &pIOP->poly[id]);
                }

                // IOP complete, release it for reuse
                ReleaseIOP(ph_state, pIOP);
                res = Phusion_OK;
            }
        }
    }
    else
        res = Phusion_BadIOPHandle;

    pthread_mutex_unlock(&ph_state->iopMutex);

    return res;
}

//---------------------------------------------------------------------------------------------
// Cancel a processing operation
//---------------------------------------------------------------------------------------------
Phusion_Result Phusion_Cancel(Phusion_Handle ph, Phusion_IOPHandle iop)
{
    Phusion_State *ph_state = (Phusion_State*)ph;
    Phusion_Result res = Phusion_OK;
    Phusion_IOPUnit *pIOP;

    if (!ValidStateHandle(ph))
        return Phusion_BadHandle;

    pthread_mutex_lock(&ph_state->iopMutex);

    pIOP = FindIOP(ph_state, iop);
    if (pIOP)
    {
        // Instruct all worker threads to complete this IOP early
        pIOP->bCancel = true;

        // In order to ensure that all worker threads make no further accesses to
        //    the associated input and output buffers upon return, we really have no
        //    choice to wait around

        while (pIOP->nUnits > 0)
        {
            int rc = pthread_cond_wait(&ph_state->waitCond, &ph_state->iopMutex);
            if (rc)
            {
                fprintf(stderr, "ERROR: pthread_cond_wait returned error %d\n", rc);
                res = Phusion_CancelFailed;
            }
        }

        if (res == Phusion_OK)
            ReleaseIOP(ph_state, pIOP);
    }
    else
        res = Phusion_BadIOPHandle;

    pthread_mutex_unlock(&ph_state->iopMutex);

    return res;
}

//---------------------------------------------------------------------------------------------
// Utility function that returns a textual error description
//---------------------------------------------------------------------------------------------
const char *Phusion_ErrorText(Phusion_Result res)
{
    static const struct
    {
        Phusion_Result res;
        const char   *text;
    } errors[] =
    {
        // Success/in-progress responses
        { Phusion_OK,                    "OK" },
        { Phusion_Pending,               "Pending" },

        // Error conditions
        { Phusion_BadHandle,             "Invalid handle passed to Phusion library" },
        { Phusion_OutOfMemory,           "Not enough memory available" },
        { Phusion_InvalidParameters,     "Invalid parameters passed to Phusion library" },
        { Phusion_TooManyInstances,      "Attempt to create too many instances of Phusion library" },
        { Phusion_TooBusy,               "Busy (too many processing operations in progress" },
        { Phusion_InitFailed,            "Phusion library failed to inialise" },
        { Phusion_FinFailed,             "Phusion library failed to finalise" },
        { Phusion_BadIOPHandle,          "Invalid IOP handle passed to Phusion library" },
        { Phusion_BadConfig,             "Invalid library configuration" },
        { Phusion_WaitFailed,            "An error occurred whilst awaiting completion of processing" },
        { Phusion_CancelFailed,          "An error occurred when trying to cancel processing" },
        { Phusion_IncompatibleBuffers,   "Input and output buffers are incompatible" },
        { Phusion_InvalidResampleFlags,  "Resampling flags are invalid/unrecognised" },
        { Phusion_InvalidGradientFlags,  "Gradient flags are invalid/unrecognised" },
        { Phusion_InvalidProcessFlags,   "Processing flags are invalid/unrecognised" },

        // Hardware-related error conditions
        { Phusion_HW_NotFound,           "Phusion hardware not found at expected address" },

        // Miscellaneous
        { Phusion_NotImplemented,        "Not yet implemented within Phusion library" }
    };
    unsigned idx = 0;
    while (idx < NOF_ELEMENTS(errors))
    {
        if (res == errors[idx].res)
            return errors[idx].text;
        idx++;
    }
    return "Unknown error";
}

//---------------------------------------------------------------------------------------------
// Copy a set of polynomial coefficients from source to destination
//---------------------------------------------------------------------------------------------
void CopyPolynomial(Phusion_Polynomial *d, const Phusion_Polynomial *s)
{
    d->nPlanes = s->nPlanes;
    d->nCoeffs = s->nCoeffs;
    d->res2 = d->res1 = 0;

    // Copy used polynomial coefficients, initialising all the others as a safety measure,
    //   remembering that we mustn't use memset because these are floating-point values
    for(unsigned p = 0; p < PHUSION_MAX_PLANES; p++)
        for(unsigned c = 0; c < PHUSION_MAX_COEFFS; c++)
            d->coeffs[p][c] = (p >= s->nPlanes || c >= s->nCoeffs) ? 0.0 : s->coeffs[p][c];
}

//---------------------------------------------------------------------------------------------
// Internal utility function for checking the (likely) validity of a state handle from other translation units
// without exporting the state structure (potential linkage issues when statically-linked with client code)
//---------------------------------------------------------------------------------------------
Phusion_State *ValidStateHandle(Phusion_Handle ph)
{
    Phusion_State *ph_state = (Phusion_State*)ph;
    PHUSION_ASSERT(ph_state);
    return (ph_state && ph_state == &state && ph_state->bInitialised) ? ph_state : NULL;
}

//---------------------------------------------------------------------------------------------
// Internal utility function for scaling the per-channel gains before performing the gradient
// calculations
//---------------------------------------------------------------------------------------------
void ScaleChannelGains(phfloat_t scaledGains[PHUSION_MAX_CHANNELS], const double gains[PHUSION_MAX_CHANNELS],
                       const Phusion_Buffer &bufIn, const Phusion_ImageChannels &imChans)
{
    for (unsigned ch = 0; ch < imChans.nChannels; ch++)
    {
        unsigned planeNumber = imChans.planeNumber[ch];
        Phusion_PlaneFlags flags = bufIn.planes[planeNumber].flags;
        switch (flags)
        {
            // No adjustment required for floating-point types
            case Phusion_PlaneFlagSF:
            case Phusion_PlaneFlagDF:
                scaledGains[ch] = (phfloat_t)gains[ch];
                break;

            // Adjust the gains to factor in the conversion from integer to floating-point
            case Phusion_PlaneFlagU16:
                scaledGains[ch] = (phfloat_t)(gains[ch] / 65535.0);
                break;
            default:
                PHUSION_ASSERT(flags == Phusion_PlaneFlagU8);
                scaledGains[ch] = (phfloat_t)(gains[ch] / 255.0);
                break;
        }
    }
}

//---------------------------------------------------------------------------------------------
// Calculate the weights required for bicubic filtering
//---------------------------------------------------------------------------------------------
void CalculateWeights(int16_t filtWeights[][PHUSION_UWFILT_PTS], unsigned n)
{
    // Lagrange polynomial interpolation
    for(unsigned fr = 0; fr < n; fr++)
    {
        double f = (double)fr / n;
        filtWeights[fr][0] = (int16_t)floor(0.5 + f * (f - 1.0) * (f - 2.0) * 4096.0 / (-1.0 * -2.0 * -3.0));
        filtWeights[fr][1] = (int16_t)floor(0.5 + (f + 1.0) * (f - 1.0) * (f - 2.0) * 4096.0 / (1.0 * -1.0 * -2.0));
        filtWeights[fr][2] = (int16_t)floor(0.5 + (f + 1.0) * f * (f - 2.0) * 4096.0 / (2.0 * 1.0 * -1.0));
        filtWeights[fr][3] = (int16_t)floor(0.5 + (f + 1.0) * f * (f - 1.0) * 4096.0 / (3.0 * 2.0 * 1.0));

        // TODO - there is a slight ripple in the sums that it should really be possible to eliminate with careful rounding
// printf("FR : %d : Weights %d %d %d %d -> %d\n", fr,
//        filtWeights[fr][0], filtWeights[fr][1], filtWeights[fr][2], filtWeights[fr][3],
//        filtWeights[fr][0] + filtWeights[fr][1] + filtWeights[fr][2] + filtWeights[fr][3]);
    }
}
