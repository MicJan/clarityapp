/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_worker.hpp                                            */
/* Description: Management of worker threads                                  */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __phusion_worker_hpp
#define __phusion_worker_hpp
#ifndef __phusion_internal_hpp
#include "phusion_internal.hpp"
#endif

// Create a worker thread to perform pixel-processing or to manage GPU/HW-based processing
bool CreateWorkerThread(Phusion_State *ph_state, Phusion_ThreadState *t_state,
                        void *(*func)(void *), const pthread_attr_t *pAttr,
                        bool bBind, unsigned cpuNumber);

// Join a worker thread that has previously been asked to terminate
Phusion_Result JoinWorkerThread(Phusion_State *ph_state, Phusion_ThreadState *t_state);

// Blocks the calling worker thread until a new work unit is available
//    (returns a pointer to the IOP containing the work unit for this thread), or
//    until the thread is requested to stop execution (returns NULL)
WorkerAction CollectWorkUnit(Phusion_State *ph_state, Phusion_ThreadState *state, void **ppData);

// Called by a worker thread to signal completion of a work unit
void CompleteWorkUnit(Phusion_State *ph_state, Phusion_ThreadState *state, Phusion_IOPUnit *pIOP);

// Allocate an appropriate amount of work to each of the available worker threads to try
//     to ensure the earliest possible completion time for the whole processing operation
void AllocateWork(Phusion_State *ph_state, Phusion_IOPUnit *pIOP, uint32_t pixPerLine, uint32_t newLines);

// Assign a single, indivisible task to the first available worker thread of the given type.
Phusion_Result AssignIndivisibleTask(Phusion_State *ph_state, WorkerType workers, unsigned *pThreadNum);

#endif
