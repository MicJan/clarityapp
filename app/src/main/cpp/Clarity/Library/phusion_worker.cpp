/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_worker.cpp                                            */
/* Description: Management of worker threads                                  */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#include "phusion_defs.hpp"
#include "phusion_linsys.hpp"
#include "phusion_main.hpp"
#include "phusion_utils.hpp"
#include "phusion_worker.hpp"

// Create a worker thread to perform pixel-processing or to manage GPU/HW-based processing
bool CreateWorkerThread(Phusion_State *ph_state, Phusion_ThreadState *t_state,
                        void *(*func)(void *), const pthread_attr_t *pAttr,
                        bool bBind, unsigned cpuNumber)
{
#ifdef PHUSION_PTHREAD_BINDING
    cpu_set_t cpuset;
#endif
    int rc;

    t_state->pState = ph_state;
    t_state->threadNumber = ph_state->nextThreadNum++;
    rc = pthread_create(&t_state->threadHandle, pAttr, func, t_state);
    if (rc != 0)
        return false;

#ifdef PHUSION_PTHREAD_BINDING
    // set each thread to have unique affinity; assumption is that one thread/core is
    //     yields maximum performance because we can consume all available CPU power
    CPU_ZERO(&cpuset);

    CPU_SET(cpuNumber, &cpuset);

    int s = pthread_setaffinity_np(t_state->threadHandle, sizeof(cpu_set_t), &cpuset);
    if (s != 0)
        printf("setaffinity error %d\n", s);

    /* Check the actual affinity mask assigned to the thread */

    s = pthread_getaffinity_np(t_state->threadHandle, sizeof(cpu_set_t), &cpuset);
    if (s != 0)
        printf("getaffinity error %d\n", s);

    printf("Set returned by pthread_getaffinity_np() contained:\n");
    for (int j = 0; j < CPU_SETSIZE; j++)
        if (CPU_ISSET(j, &cpuset))
            printf("    CPU %d\n", j);
#endif

    return true;
}

// Join a worker thread that has previously been asked to terminate
Phusion_Result JoinWorkerThread(Phusion_State *ph_state, Phusion_ThreadState *t_state)
{
    Phusion_Result res = Phusion_OK;
    void *status;
    int rc;

    rc = pthread_join(t_state->threadHandle, &status);
    if (rc != 0)
    {
        fprintf(stderr, "ERROR: pthread_join returned error %d\n", rc);
        res = Phusion_FinFailed;
    }

    return res;
}

//---------------------------------------------------------------------------------------------
// Called by a backend worker thread to collect a work unit for processing
//---------------------------------------------------------------------------------------------
WorkerAction CollectWorkUnit(Phusion_State *ph_state, Phusion_ThreadState *state, void **ppData)
{
    WorkerAction action = WorkAction_Quit;
    void *pData = NULL;

    PHUSION_ASSERT(state->threadNumber < PHUSION_MAX_WORKERS);
    PHUSION_ASSERT(ppData);

    pthread_mutex_lock(&ph_state->iopMutex);

    // Look for a pending work unit or a termination request
    while (!ph_state->bFinalising)
    {
        volatile Phusion_IOPUnit *pIOP = NULL;
        unsigned i;

        // Start searching from the oldest known IOP
        i = ph_state->oldestIOP;
        do
        {
            volatile Phusion_WorkUnit *pUnit;

            pIOP = &ph_state->IOPs[i];
            pUnit = &pIOP->unit[state->threadNumber];

            // Is this a valid IOP for which this worker thread has something to do?
            if (pIOP->bValid && !pIOP->bCancel && pUnit->rsNumLines && !pUnit->bCompleted)
            {
                // printf("Thread %d processing %d lines\n", state->threadNumber, pUnit->numLines);
                state->workStartTime = CurrentTime();
                break;
            }

            // Try the next IOP in the list
            pIOP = NULL;
            if (++i >= NOF_ELEMENTS(ph_state->IOPs))
                i = 0;

        } while (i != ph_state->oldestIOP);

        // Did we find anything?
        if (pIOP)
        {
            pData = (void *)pIOP;
            action = pIOP->workAction;
            break;
        }

        // Nothing for us to do, go back to sleep
        pthread_cond_wait(&ph_state->workCond, &ph_state->iopMutex);
    }

    pthread_mutex_unlock(&ph_state->iopMutex);

    *ppData = pData;
    return action;
}

//---------------------------------------------------------------------------------------------
// Called by a worker thread to signal completion of a work unit
//---------------------------------------------------------------------------------------------
void CompleteWorkUnit(Phusion_State *ph_state, Phusion_ThreadState *state, Phusion_IOPUnit *pIOP)
{
    const Phusion_WorkUnit *pUnit;
    Phusion_WorkerState *pWorker;
    bool bReissue(false);
    uint32_t elapsedTime;
    uint32_t avWorkRate;
    uint32_t numLines;
    uint32_t workRate;
    unsigned rateIdx;
    uint32_t load;

    PHUSION_ASSERT(ph_state && state && pIOP);
    PHUSION_ASSERT(state->threadNumber < NOF_ELEMENTS(pIOP->unit));

    // Compute work rate for this unit and compute the new rolling average;
    //   NOTE: since only the worker thread itself changes the average, this can
    //         be done outside the critical section

    pUnit = &pIOP->unit[state->threadNumber];
    numLines = pUnit->rsNumLines;
    load = numLines * pIOP->pixPerLine;

    elapsedTime = CurrentTime() - state->workStartTime;
    workRate = elapsedTime ? (load / elapsedTime) : load;

    pWorker = &ph_state->workLoad[state->threadNumber];

    // Update the rolling average
    rateIdx = pWorker->rateIdx;
    avWorkRate = pWorker->avWorkRate + workRate - pWorker->workRates[rateIdx];
    pWorker->workRates[rateIdx] = workRate;
    if (++rateIdx >= NOF_ELEMENTS(pWorker->workRates))
        rateIdx = 0;
    pWorker->rateIdx = rateIdx;

    // ---------- Critical section updating the workload monitoring -----------
    pthread_mutex_lock(&ph_state->workMutex);

    PHUSION_ASSERT(pWorker->totalLoad >= load);
    pWorker->totalLoad -= load;
    pWorker->avWorkRate = avWorkRate;

    pthread_mutex_unlock(&ph_state->workMutex);
    // ------------------------ End of critical section -----------------------


    // --------------- Critical section updating IOP completion ---------------

    // Ensure that we have write access to the IOP
    pthread_mutex_lock(&ph_state->iopMutex);

    // Per-IOP work that must be performed by each worker whilst holding exclusive
    //    access to the IOP; so, keep this minimal!
    switch (pIOP->workAction)
    {
        case WorkAction_CalculateGrad:
            if (pIOP->grFlags & Phusion_GradFinalStripe)
            {
                // Sum our contribution into the linear system of this IOP
                SumLinSysCont(pIOP->linSysCont, state->linSysCont,
                              pIOP->imgGuide.nChannels, pIOP->imgContrast.nChannels);

                // If this is the final contribution, release the lock whilst
                //     we solve the linear system and then re-claim
                if (pIOP->nUnits <= 1)
                {
                    // Release the IOP
                    pthread_mutex_unlock(&ph_state->iopMutex);

                    profile_start_timing(&state->profile[PROF_CPU_SOLVELINSYS]);

                    // Solve linear system and output the resulting polynomial coefficients
                    SolveLinSys(pIOP->grPolyResult, pIOP->linSysCont, pIOP->grConfig,
                                pIOP->imgGuide.nChannels, pIOP->imgContrast.nChannels);

                    profile_stop_timing(&state->profile[PROF_CPU_SOLVELINSYS]);

                    // Ensure that we have write access to the IOP
                    pthread_mutex_lock(&ph_state->iopMutex);
                }
            }
            break;
        default:  // Placate compiler
            break;
    }

    // Signal that we have completed our contribution
    pIOP->unit[state->threadNumber].bCompleted = true;

    // Is this the final contribution?
//  uint32_t outNumLines = 0;
//  uint32_t inNumLines = 0;
    if (pIOP->nUnits <= 1)
    {
        // ProcessImage operations consist of three chained operations
        //   (Gradient calcs, Linear system solving and Pixel processing)
        // and CalculateGradient operations consist of the first two
        //
        // Dedicated resampling operations, as opposed to the implicit,
        //   default resampling offered by the 'CalculateGrad' stage,
        //   are always performed standalone (no chaining)

        switch (pIOP->workAction)
        {
            case WorkAction_CalculateGrad:
                // if (pIOP->workFlags & (1U << WorkAction_SolveLinSys))
                break;
/*
            case WorkAction_SolveLinSys:
                if (pIOP->workFlags & (1U << WorkAction_ProcessPixels))
                {
                    pIOP->workAction = WorkAction_ProcessPixels;
                    outNumLines = inNumLines = 0;
                    bReissue = true;
                }
                break;
*/
            default:
             // printf("Signalling completion of %p\n", pIOP);
                break;
        }

        // If we're reissuing this IOP we must leave the nUnits count as non-zero
        //    so that the client code does not consider it complete yet
        if (!bReissue)
        {
            pIOP->nUnits = 0;
            pthread_cond_broadcast(&ph_state->waitCond);
        }
    }
    else
        pIOP->nUnits--;

    // Release the IOP
    pthread_mutex_unlock(&ph_state->iopMutex);

    // -------------------------- End of critical section ---------------------

    if (bReissue)
    {
//        Phusion_Result res;
        // TODO
        //res = QueueIOP(ph_state, pIOP, pIOP->workAction, outNumLines, inNumLines,);
    }
}

//---------------------------------------------------------------------------------------------
// Allocate an appropriate amount of work to each of the available worker threads to try
//     to ensure the earliest possible completion time for the whole processing operation
//---------------------------------------------------------------------------------------------
void AllocateWork(Phusion_State *ph_state, Phusion_IOPUnit *pIOP,
                  uint32_t pixPerLine, uint32_t numLines)
{
    // Snapshots of current worker state
    uint32_t workerLoads[PHUSION_MAX_WORKERS];
    uint32_t workerRates[PHUSION_MAX_WORKERS];
    // Additional load we are about to allocate
    uint32_t issueLoads[PHUSION_MAX_WORKERS];
    uint32_t issuedLines = 0;
    // Aggregate average work rate of all workers
    uint32_t totalRate = 0;
    uint32_t totalLoad;
    unsigned nworkers;
    uint32_t newLoad;
    uint32_t minUnit;
    unsigned ncands;
    unsigned w;

    // TODO - if/when we need to handle vertically-subsampled image formats, there will
    //        be an additional constraint on the work allocation; for simplicity, always
    //        ensure that scanline allocations are even?

    // Validate input parameters
    PHUSION_ASSERT(ph_state && pIOP && pixPerLine > 0 && numLines > 0);

    // Total new workload across all activated workers
    newLoad = pixPerLine * numLines;

    totalLoad = newLoad;

    nworkers = ph_state->nextThreadNum;
    PHUSION_ASSERT(nworkers > 0);

    // Pretty arbitary minimum work unit to dispatch, but we must have some form of minimum
    //    because it will likely be more expensive to start more workers on smaller loads
    //    than few workers each on a larger unit
    minUnit = pixPerLine << 3;

    // Compute the total workload already committed, and the aggregate average work rate
    pthread_mutex_lock(&ph_state->workMutex);
    for (w = 0; w < nworkers; w++)
    {
        // Steal a copy of the average work rates and total loads whilst they are not
        //    susceptible to change
        workerLoads[w] = ph_state->workLoad[w].totalLoad;
        workerRates[w] = ph_state->workLoad[w].avWorkRate;

        totalLoad += workerLoads[w];
        totalRate += workerRates[w];
    }
    pthread_mutex_unlock(&ph_state->workMutex);

    // On the first iteration, allocate the new work, skipping any workers that are falling behind
    // Then iterate again, allocating the excess work
    for (w = 0; w < nworkers && issuedLines < numLines; w++)
    {
#if PHUSION_LOAD_BALANCING
        uint32_t scaledLoad = totalRate ? (uint32_t)(((uint64_t)totalLoad * workerRates[w]) / totalRate)
                                        : (totalLoad / nworkers);  // Share evenly at startup
        if (scaledLoad < workerLoads[w] ||           // falling behind
           (scaledLoad - workerLoads[w]) < minUnit)  // inefficient to issue such a small work unit
        {
            issueLoads[w] = 0;
        }
        else
        {
            // But we can only dispatch complete scanlines...
            unsigned nlines = (scaledLoad - workerLoads[w] + pixPerLine / 2) / pixPerLine;
#else
        {
            unsigned nlines = (numLines + nworkers-1) / nworkers;
#endif
            if (nlines > numLines - issuedLines)
                nlines = numLines - issuedLines;
            issuedLines += nlines;

            issueLoads[w] = nlines * pixPerLine;
        }
    }
    while (w < PHUSION_MAX_WORKERS)
        issueLoads[w++] = 0;

    if (issuedLines < numLines)
    {
        // Divide up the excess load among a subset of the workers, behind or not, aiming
        //    to ensure that none is dispatched with an unit that's inefficiently small

        uint32_t excessLoad = (numLines - issuedLines) * pixPerLine;
        uint32_t loadLeft = (excessLoad + nworkers - 1) / nworkers;
        if (loadLeft < minUnit)
        {
            ncands = excessLoad / minUnit;  // Rounding down, and demanding more of each
                                            //   to avoid issuing less than the minimum unit
            if (!ncands)
                ncands = 1;                 // But somebody has to do it...
            loadLeft = minUnit;
            PHUSION_ASSERT(ncands > 0 && ncands <= nworkers);
        }
        else
        {
            // Round up to a multiple of the line length so that we only issue complete lines
            loadLeft = pixPerLine * ((loadLeft + pixPerLine-1) / pixPerLine);
            ncands = nworkers;
        }

        w = ph_state->firstCand;  // Round-robin record of where we started last time,
                                  //   so that we're not biased towards particular workers
        PHUSION_ASSERT(w < nworkers);
        while (ncands-- > 0 && excessLoad > 0)
        {
            // Apportionate evenly as planned, but we must avoid over-/under-running given that we
            //    have rounded up the load to complete scanlines (overrun), and we have only a
            //    finite number of workers (underrun hazard)
            uint32_t thisLoad = (ncands && loadLeft < excessLoad) ? loadLeft : excessLoad;

            PHUSION_ASSERT(thisLoad >= 0);
            issueLoads[w] += thisLoad;
            if (++w >= nworkers)
                w = 0;

            excessLoad -= thisLoad;
        }
        ph_state->firstCand = w;
        PHUSION_ASSERT(!excessLoad);  // All work shall have been issued
    }

    // Final judgement, scaling the work loads to scanlines and recording our decision in the IOP
    for(w = 0; w < PHUSION_MAX_WORKERS; w++)
    {
        PHUSION_ASSERT(issueLoads[w] == pixPerLine * (issueLoads[w] / pixPerLine));
        pIOP->unit[w].numLines = issueLoads[w] / pixPerLine;
    }

    // Claim the mutex again now that we have a verdict, then quickly record the additional loads;
    //    we must do this here rather than at the point of workers collecting and commencing work
    //    units, to respond promptly to slowdowns

    pthread_mutex_lock(&ph_state->workMutex);
    for (w = 0; w < nworkers; w++)
        ph_state->workLoad[w].totalLoad += issueLoads[w];
    pthread_mutex_unlock(&ph_state->workMutex);
}

//---------------------------------------------------------------------------------------------
// Assign a single, indivisible task to the first available worker thread of the given type.
//---------------------------------------------------------------------------------------------
Phusion_Result AssignIndivisibleTask(Phusion_State *ph_state, WorkerType worker, unsigned *pThreadNum)
{
#if 0
    unsigned w;

    pthread_mutex_lock(&ph_state->iopMutex);

    w = ph_state->firstCand;  // Round-robin record of where we started last time,
                              //   so that we're not biased towards particular workers
    PHUSION_ASSERT(w < nworkers);
    for (w

    for (ncands-- > 0 && )
    {
        // On the first pass, ignore any worker that is lagging behind
        if ()
        {
        }
    }
    if ()
    {
        // If still no decision, then we just
        do
        {
        } while ();
    }

    pthread_mutex_unlock(&ph_state->iopMutex);

    if ()
    {
        // No suitable candidate workers
        return Phusion_BadConfig;
    }
#endif
*pThreadNum = 0;
    return Phusion_OK;
}
