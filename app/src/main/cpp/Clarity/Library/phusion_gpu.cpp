/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_gpu.cpp                                               */
/* Description: GPU-based implementation of the Phusion processing            */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#include <stdio.h>

#include "phusion_gpu.hpp"

#ifdef PHUSION_USE_GPU
// TODO - Placeholder for actual implementation code, using OpenCL/similar

// Initialise GPU-based processing
Phusion_Result phusion_gpu_init(Phusion_State *ph_state)
{
    fprintf(stderr, "ERROR: GPU initialisation not implemented\n");
    return Phusion_NotImplemented;
}

// Finalise GPU-based processing
Phusion_Result phusion_gpu_fin(Phusion_State *ph_state)
{
    fprintf(stderr, "ERROR: GPU finalisation not implemented\n");
    return Phusion_NotImplemented;
}

#endif
