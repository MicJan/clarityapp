/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_utils.hpp                                             */
/* Description: Phusion library utility functions                             */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __phusion_utils_hpp
#define __phusion_utils_hpp
#ifndef __phusion_types_hpp
#include "phusion_types.hpp"
#endif

#if PHUSION_PROFILING
// Performance profiling (development)
void profile_init(Phusion_State *phState);
void profile_report(Phusion_State *phState);

// Start/stop recording of the time consumed by a particular operation
void profile_start_timing(profile_info *pf);
void profile_stop_timing(profile_info *pf);

// Returns the current value of a microsecond-resolution monotonic timer
//    or the best available approximation thereof
uint64_t time_now_us(void);
#else
#define profile_init(x)
#define profile_report(x)

#define profile_start_timing(x)
#define profile_stop_timing(x)
#endif

// Internal memory management routines, mirroring malloc/free but possibly indirected
//    through client code, and accepting a statically-allocated textual description of
//    the block usage which may be diagnostically useful

void *mem_alloc(size_t n, const char *pDesc);
void  mem_free(void *pBlk, const char *pDesc);

// Utility functions
Phusion_Result ReturnError(Phusion_Result res, const char *pDesc);

// Basic text logging
#if PHUSION_LOGGING
int log_printf(const char *fmt, ...);
#else
inline int do_nothing(const char *fmt, ...) { return 0; }
#define log_printf(...)
#endif

// Return current value of a monotonic timer in milliseconds
uint32_t CurrentTime(void);

// Return a count of the number of processor cores
unsigned NumberOfProcessors(void);

#ifdef WIN32
// Return the current real time as a struct timespec that is compatible
//   with the 'abstime' implementation of Pthreads-win32
void GetRealTimeAsTimespec(struct timespec *ts);
#endif

#endif
