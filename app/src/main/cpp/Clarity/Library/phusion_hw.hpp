/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_hw.hpp                                                */
/* Description: Implementation of Phusion processing using dedicated hardware */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __phusion_hw_hpp
#define __phusion_hw_hpp
#ifndef __phusion_h
#include "phusion.h"
#endif

#ifdef PHUSION_USE_HW
typedef struct
{
    // Hardware information
    uint32_t    baseAddress;
    uint32_t    version;

    // IPC resources for communication with the interrupt handler
    pthread_mutex_t intMutex;
    pthread_cond_t  intCond;

    Phusion_ThreadState state;
} Phusion_HWState;

// Initialise HW-based processing
Phusion_Result phusion_hw_init(Phusion_State *ph_state);

// Finalise HW-based processing
Phusion_Result phusion_hw_fin(Phusion_State *ph_state);
#endif

#endif
