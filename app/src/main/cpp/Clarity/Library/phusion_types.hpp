/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_types.hpp                                             */
/* Description: Internal, low-level types common to all backends and the core */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __phusion_types_hpp
#define __phusion_types_hpp
#ifndef __phusion_h
#include "phusion.h"
#endif
#ifndef __phusion_defs_hpp
#include "phusion_defs.hpp"
#endif
// We use the pthread library for managing our worker threads
extern "C"
{
#include "pthread.h"
};

#if PHUSION_SINGLE_PRECISION
// Use single-precision floats for lower memory footprint and
//    possible higher performance
typedef float phfloat_t;
#define PHFLOAT_ZERO (phfloat_t)0.0F
#define PHFLOAT_ONE  (phfloat_t)1.0F
#else
// Use double-precision for higher precision and exact concordance with
//    MATLAB-based reference implementations
typedef double phfloat_t;
#define PHFLOAT_ZERO (phfloat_t)0.0
#define PHFLOAT_ONE  (phfloat_t)1.0
#endif

#if PHUSION_PROFILING
// Profiling categories, must be kept in step with 'profiling' array
enum
{
    PROF_CPU_RESAMPLING    = 0,
    PROF_CPU_CALCGRADIENT,
    PROF_CPU_SOLVELINSYS,
    PROF_CPU_PROCPIXELS,
    PROF_CPU_CONVERTING,
    PROF_CPU_FILTERING,

    PROF_COUNT
};

// Profiling information, per thread per operation
typedef struct
{
    uint64_t start_time;
    uint64_t elapsed_time;
} profile_info;
#endif

// Actions that a worker thread may perform
typedef enum
{
    WorkAction_Quit = 0,
    WorkAction_Filter,
    WorkAction_Convert,
    WorkAction_Resample,
    WorkAction_CalculateGrad,
    WorkAction_SolveLinSys,
    WorkAction_ProcessPixels
} WorkerAction;

// Types of worker thread (may be merged to form a bitmap)
typedef enum
{
    Worker_CPU = 1,
    Worker_GPU = 2,
    Worker_HW  = 4
} WorkerType;

// Contribution to linear system
typedef struct
{
    phfloat_t Lreg[PHUSION_MAX_COEFFS*(PHUSION_MAX_COEFFS+1)/2];
    phfloat_t   Ls[PHUSION_MAX_COEFFS*(PHUSION_MAX_COEFFS+1)/2];
    phfloat_t Rreg[PHUSION_MAX_COEFFS*PHUSION_MAX_CHANNELS];
    phfloat_t   Rs[PHUSION_MAX_COEFFS*PHUSION_MAX_CHANNELS];
} Phusion_LinSysCont;

typedef struct Phusion_State Phusion_State;

// Thread-local state for processing of the current work unit
typedef struct
{
    // Information about this thread
    pthread_t           threadHandle;
    uint32_t            threadNumber;

    // Library instance for which the thread is working
    Phusion_State     *pState;

    // Time at which processing started on current work unit
    uint32_t            workStartTime;

    // Contribution to linear system
    Phusion_LinSysCont  linSysCont;

#if PHUSION_PROFILING
    // Performance profiling (development use)
    profile_info        profile[PROF_COUNT];
#endif

} Phusion_ThreadState;

#endif
