/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_linsys.cpp                                            */
/* Description: Linear system solving                                         */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2017. All Rights Reserved.                */
/******************************************************************************/

#include <cstdio>

#include "Eigen/Dense"

#include "phusion_linsys.hpp"

#if PHUSION_LOGGING
static const bool bVerbose(true);
#else
static const bool bVerbose(false);
#endif

// Solve the complete linear system to produce a set of polynomial coefficients
void SolveLinSys(Phusion_Polynomial *outPoly, Phusion_LinSysCont &linSys,
                 const Phusion_GradientConfig &grConfig,
                 unsigned guideChan, unsigned contChan)
{
    unsigned pchan = contChan + (contChan * (contChan+1))/2;
    const unsigned P = guideChan;
    unsigned i, j;

    if (bVerbose)
    {
        for(i = 0; i < (pchan * (pchan+1) / 2); i++)
            printf("  L%u: %lf %lf\n", i, linSys.Ls[i], linSys.Lreg[i]);
        for(i = 0; i < (pchan * P); i++)
            printf("  R%u: %lf %lf\n", i, linSys.Rs[i], linSys.Rreg[i]);
    }

    // Calculate the mean of the diagonal. This is useful to regularise the solution of the system.
    phfloat_t avgDiag = PHFLOAT_ZERO;
    for(i = 0; i < pchan; i++) {
        // Same Lapack convention as above, it would be i+i*(i+1)/2 because I'm accessing the diagonal elements.
        avgDiag += linSys.Ls[i*(i + 3) / 2];
    }

    avgDiag /= (phfloat_t)pchan;

    // Sum the left-hand side matrices to regularise the linear system.
    for(i = 0; i < pchan; i++) {
        for(j = i; j < pchan; j++) {
            linSys.Ls[i + j*(j + 1) / 2] += (phfloat_t)grConfig.lambda * linSys.Lreg[i + j*(j + 1) / 2];
        }
    }

    // Tikhonov regularisation: add a small value to the diagonal of the matrix.
    for(i = 0; i < pchan; i++) {
        linSys.Ls[i*(i + 3) / 2] += avgDiag * (phfloat_t)grConfig.tikLambda;
    }

    // Add the right-hand side matrices to regularise the linear system.
    for(j = 0; j < P; j++) {
        for(i = 0; i < pchan; i++) {
            linSys.Rs[i + j*pchan] += (phfloat_t)grConfig.lambda * linSys.Rreg[i + j*pchan];
        }
    }

    /* A - left-hand side matrix of the linear system.
    *  b - right-hand side matrix of the linear system.
    *  b   is overwritten.
    */
#if PHUSION_SINGLE_PRECISION
    Eigen::MatrixXf A((int)pchan, (int)pchan);
    Eigen::MatrixXf b((int)pchan, (int)P); // 'pchan' rows, P columns
#else
    Eigen::MatrixXd A((int)pchan, (int)pchan);
    Eigen::MatrixXd b((int)pchan, (int)P); // 'pchan' rows, P columns
#endif

    // Eigen needs the full matrix so duplicate input upper right triangle
    //    in the bottom left of A
    unsigned index = 0;
    unsigned x, y;
    for (x = 0; x < pchan; x++)
    {
        for (y = 0; y < x; y++)
        {
            A(y, x) = linSys.Ls[index];
            A(x, y) = linSys.Ls[index++];
        }
        A(y, x) = linSys.Ls[index++];
    }

    // copy RHS matrix
    for (x = 0; x < P; x++)
    {
        for (y = 0; y < pchan; y++)
            b(y, x) = linSys.Rs[y + (x * pchan)];
    }

    // Solve matrix equation Ax = B where A and B are known
#if PHUSION_SINGLE_PRECISION
    Eigen::ColPivHouseholderQR<Eigen::MatrixXf> decomp(A);
    Eigen::MatrixXf X = decomp.solve(b);
#else
    Eigen::ColPivHouseholderQR<Eigen::MatrixXd> decomp(A);
    Eigen::MatrixXd X = decomp.solve(b);
#endif

    if (bVerbose)
    {
        for (y = 0; y < pchan; y++)
        {
            for (x = 0; x < P; x++)
                printf(" %lf", X(y, x));
            putchar('\n');
        }
    }

    // Populate the client buffer with the computed polynomial coefficients
    outPoly->nPlanes = P;
    outPoly->nCoeffs = pchan;

    for (unsigned plane = 0; plane < PHUSION_MAX_PLANES; plane++)
        for (unsigned coeff = 0; coeff < PHUSION_MAX_COEFFS; coeff++)
            outPoly->coeffs[plane][coeff] = (plane < P && coeff < pchan) ? X(coeff, plane) : 0.0;
}

