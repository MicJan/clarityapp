/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_maths.hpp                                             */
/* Description: Generic mathematical routines used by the SE fusion gradients */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2017. All Rights Reserved.                */
/******************************************************************************/

#ifndef __phusion_maths_hpp
#define __phusion_maths_hpp
#include <cmath>
#ifndef __phusion_types_hpp
#include "phusion_types.hpp"
#endif

#define THRESH ((phfloat_t)1e-11)

#ifndef POW2
#define POW2(x) ((x)*(x))
#endif

// Type definitions
typedef struct
{
    phfloat_t g11, g12, g22;
} dizenzo;

typedef struct
{
    phfloat_t dx[PHUSION_MAX_CHANNELS];
    phfloat_t dy[PHUSION_MAX_CHANNELS];
} gradient;

typedef struct
{
    phfloat_t lambda1, lambda2;
    phfloat_t v1x, v1y;
    phfloat_t v2x, v2y;
} eigen;

typedef struct
{
    phfloat_t r11, r12, r21, r22;
} m2x2;

inline phfloat_t fsign(phfloat_t x)
{
    return (phfloat_t)((PHFLOAT_ZERO < x) - (x < PHFLOAT_ZERO));
/*
    phfloat_t s;

    if (x < PHFLOAT_ZERO)
        s = -1.0;
    else if (x == PHFLOAT_ZERO)
        s = PHFLOAT_ZERO;
    else
        s = 1.0;

    return s;
*/
}

inline void eigenvalue_decomposition(const dizenzo *CT, eigen *dec)
{
    phfloat_t sqr;
    phfloat_t g11, g12, g22;
    phfloat_t lambda1, lambda2;
    phfloat_t v1x, v1y;
    phfloat_t v2x, v2y;
    phfloat_t norm;

    g11 = CT->g11;
    g12 = CT->g12;
    g22 = CT->g22;

    sqr = (phfloat_t)sqrt(POW2(g11 - g22) + 4.0*POW2(g12));
    lambda1 = (phfloat_t)0.5F * (g11 + g22 + sqr);
    lambda2 = (phfloat_t)0.5F * (g11 + g22 - sqr);

    v1x = v2x = g12;
    v1y = lambda1 - g11;
    v2y = lambda2 - g11;

    if (ABS(g12) < THRESH)
    {
        if (ABS(g11) < THRESH && ABS(g22) < THRESH)
        {
            lambda1 = lambda2 = THRESH;
            v1x = v2y = PHFLOAT_ONE;
            v2x = v1y = PHFLOAT_ZERO;
        }
        else if (g11 > g22)
        {
            // re-assign, to get rid of rounding errors
            lambda1 = g11;
            lambda2 = g22;
            v1x = PHFLOAT_ONE;
            v1y = PHFLOAT_ZERO;
            v2x = PHFLOAT_ZERO;
            v2y = PHFLOAT_ONE;
        }
        else
        {
            // re-assign, to get rid of rounding errors
            lambda1 = g22;
            lambda2 = g11;
            v1x = PHFLOAT_ZERO;
            v1y = PHFLOAT_ONE;
            v2x = PHFLOAT_ONE;
            v2y = PHFLOAT_ZERO;
        }
    }
    else
    {
        norm = sqrt(POW2(v1x) + POW2(v1y));
        v1x /= norm;
        v1y /= norm;

        norm = sqrt(POW2(v2x) + POW2(v2y));
        v2x /= norm;
        v2y /= norm;
    }

    /* The square root here isn't part of the eigenvalue decomposition, but
    * is necessary to compute the magnitude. */
    dec->lambda1 = sqrt(ABS(lambda1));
    dec->lambda2 = sqrt(ABS(lambda2));
    dec->v1x = v1x;
    dec->v1y = v1y;
    dec->v2x = v2x;
    dec->v2y = v2y;
}

/*inline void m2x2mult(const m2x2 *x, const m2x2 *y, m2x2 *p)
{
    p->r11 = x->r11 * y->r11 + x->r12 * y->r21;
    p->r12 = x->r11 * y->r12 + x->r12 * y->r22;
    p->r21 = x->r21 * y->r11 + x->r22 * y->r21;
    p->r22 = x->r21 * y->r12 + x->r22 * y->r22;
}
*/

inline void m2x2multMD(const m2x2 *x, const dizenzo *y, m2x2 *p)
{
    p->r11 = x->r11 * y->g11 + x->r12 * y->g12;
    p->r12 = x->r11 * y->g12 + x->r12 * y->g22;
    p->r21 = x->r21 * y->g11 + x->r22 * y->g12;
    p->r22 = x->r21 * y->g12 + x->r22 * y->g22;
}

inline void m2x2multDM(const dizenzo *x, const m2x2 *y, m2x2 *p)
{
    p->r11 = x->g11 * y->r11 + x->g12 * y->r21;
    p->r12 = x->g11 * y->r12 + x->g12 * y->r22;
    p->r21 = x->g12 * y->r11 + x->g22 * y->r21;
    p->r22 = x->g12 * y->r12 + x->g22 * y->r22;
}

inline void m2x2multDD(const dizenzo *x, const dizenzo *y, m2x2 *p)
{
    p->r11 = x->g11 * y->g11 + x->g12 * y->g12;
    p->r12 = x->g11 * y->g12 + x->g12 * y->g22;
    p->r21 = x->g12 * y->g11 + x->g22 * y->g12;
    p->r22 = x->g12 * y->g12 + x->g22 * y->g22;
}

inline void vector_findrot(const dizenzo *Zc, const dizenzo *Zg, m2x2 *rot)
{
    m2x2 temp;

    /* Although apparently the Matlab code does Zc*Zg instead of Zg*Zc, this is the
    * correct formulation. */
    m2x2multDD(Zg, Zc, &temp);

    // Inner and Outer products, temporary.
    dizenzo it, ot;
    // T*T'
    it.g11 = POW2(temp.r11) + POW2(temp.r12);
    it.g12 = temp.r21*temp.r11 + temp.r12*temp.r22;
    it.g22 = POW2(temp.r21) + POW2(temp.r22);

    // T'*T
    ot.g11 = POW2(temp.r11) + POW2(temp.r21);
    ot.g12 = temp.r11*temp.r12 + temp.r21*temp.r22;
    ot.g22 = POW2(temp.r12) + POW2(temp.r22);

    // Eigenvalue decompositions of Inner and Outer products.
    eigen deI, deO;
    eigenvalue_decomposition(&it, &deI);
    eigenvalue_decomposition(&ot, &deO);

// TODO - worth doing?
#if 1
    phfloat_t sign = fsign(deI.v1x*deO.v1x + deI.v1y*deO.v1y);
    deO.v1x *= sign;
    deO.v1y *= sign;

    sign = fsign(deI.v2x*deO.v2x + deI.v2y*deO.v2y);
    deO.v2x *= sign;
    deO.v2y *= sign;
#else
    phfloat_t sum = deI.v1x*deO.v1x + deI.v1y*deO.v1y;
    if (sum < PHFLOAT_ZERO)
    {
        de0.v1x = -de0.v1x;
        de0.v1y = -de0.v1y;
    }
    sum = deI.v2x*deO.v2x + deI.v2y*deO.v2y;
    if (sum < PHFLOAT_ZERO)
    {
        de0.v1x = -de0.v1x;
        de0.v1y = -de0.v1y;
    }
#endif

    rot->r11 = deO.v1x*deI.v1x + deO.v2x*deI.v2x;
    rot->r12 = deO.v1x*deI.v1y + deO.v2x*deI.v2y;
    rot->r21 = deO.v1y*deI.v1x + deO.v2y*deI.v2x;
    rot->r22 = deO.v1y*deI.v1y + deO.v2y*deI.v2y;
}

#endif
