/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_linsys.hpp                                            */
/* Description: Linear system solving                                         */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2017. All Rights Reserved.                */
/******************************************************************************/

#ifndef __phusion_linsys_hpp
#define __phusion_linsys_hpp
#ifndef __phusion_defs_hpp
#include "phusion_defs.hpp"
#endif
#ifndef __phusion_internal_hpp
#include "phusion_internal.hpp"
#endif

inline void ClearLinSysCont(Phusion_LinSysCont &linSysCont)
{
    PHUSION_ASSERT(NOF_ELEMENTS(linSysCont.Ls) == NOF_ELEMENTS(linSysCont.Lreg));
    PHUSION_ASSERT(NOF_ELEMENTS(linSysCont.Rs) == NOF_ELEMENTS(linSysCont.Rreg));
    for(unsigned i = 0; i < NOF_ELEMENTS(linSysCont.Ls); i++)
    {
        linSysCont.Ls[i] = PHFLOAT_ZERO;
        linSysCont.Lreg[i] = PHFLOAT_ZERO;
    }
    for(unsigned i = 0; i < NOF_ELEMENTS(linSysCont.Rreg); i++)
    {
        linSysCont.Rs[i] = PHFLOAT_ZERO;
        linSysCont.Rreg[i] = PHFLOAT_ZERO;
    }
}

inline void SumLinSysCont(Phusion_LinSysCont &sum, Phusion_LinSysCont &cont,
                          unsigned guideChan,  // Number of channels of guide input (=output channels)
                          unsigned contChan)   // Number of channels in contrast input
{
    const unsigned pchan = contChan + (contChan * (contChan+1))/2;
    const unsigned P = guideChan;

    // Accumulate the linear system contribution into the resultant sum
    for (unsigned i = 0; i < (pchan * (pchan+1) / 2); i++)
    {
    //  printf("  L%u: %lf %lf\n", i, cont.Ls[i], cont.Lreg[i]);
        sum.Ls[i]   += cont.Ls[i];
        sum.Lreg[i] += cont.Lreg[i];
    //  printf("sums %lf %lf\n", sum.Ls[i], sum.Lreg[i]);
    }
    for (unsigned i = 0; i < (pchan * P); i++)
    {
    //  printf("  R%u: %lf %lf\n", i, cont.Rs[i], cont.Rreg[i]);
        sum.Rs[i]   += cont.Rs[i];
        sum.Rreg[i] += cont.Rreg[i];
    //  printf("sums %lf %lf\n", sum.Rs[i], sum.Rreg[i]);
    }
}

// Solve the complete linear system to produce a set of polynomial coefficients
void SolveLinSys(Phusion_Polynomial *outPoly, Phusion_LinSysCont &linSys,
                 const Phusion_GradientConfig &grConfig,
                 unsigned guideChan,  // Number of channels in guide input (=ouptut channels)
                 unsigned contChan);  // Number of channels in contrast input

#endif

