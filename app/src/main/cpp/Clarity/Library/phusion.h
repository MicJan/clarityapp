/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion.h                                                     */
/* Description: Main interface to Phusion image processing library            */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __phusion_h
#define __phusion_h
#ifdef __cplusplus
// The library may be useed with either C or C++ semantics
extern "C"
{
#endif
#ifndef _MSC_VER
#include <stdbool.h>
#endif
#include <stddef.h>
#include <stdint.h>

// Build linkage
#ifdef PHUSION_DLL_BUILD
#define PH_ENTRY __declspec (dllexport)
#elif defined(PHUSION_USE_DLL)
#define PH_ENTRY __declspec (dllimport)
#else
#define PH_ENTRY
#endif

// The following configuration options may be overridden within the
//    build system as appropriate for the target platform
#ifndef PHUSION_CONFIGURED

// Recruit at least one CPU to assist with pixel processing
#define PHUSION_USE_CPU

// Recruit all CPU(s) to perform Symmetric Multi-Processing
#define PHUSION_USE_SMP

// Recruit the GPU via OpenCL - NOT YET AVAILABLE
//#define PHUSION_USE_GPU

// Recruit dedicated HW block - NOT YET AVAILABLE
// #define PHUSION_USE_HW

// Double-precision arithmetic is the default
#define PHUSION_SINGLE_PRECISION 0
#endif

// Constant definitions
#define Phusion_NO_TIMEOUT (~0U)

// Maximum number of planes describing a buffer passed to/from the library
#define PHUSION_MAX_PLANES   8

// Maximum channels in an image passed to/from the library
#define PHUSION_MAX_CHANNELS 8

// Maximum number of polynomials in use concurrently
#ifndef PHUSION_MAX_POLYNOMIALS
#define PHUSION_MAX_POLYNOMIALS 1
#endif

// Maximum number of polynomial coefficients resulting from the input channels
#define PHUSION_MAX_COEFFS (PHUSION_MAX_CHANNELS*(PHUSION_MAX_CHANNELS+3)/2)

// Maximum filter kernel coefficients (= 1 + floor((width or height)/2))
#define PHUSION_MAX_FILT_COEFFS 33

// Result codes from Phusion API functions
typedef enum
{
    // Success/in-progress responses
    Phusion_OK                     = 0,
    Phusion_Pending                = 1,

    // Error conditions
    Phusion_BadHandle              = 2,
    Phusion_OutOfMemory            = 3,
    Phusion_InvalidParameters      = 4,
    Phusion_TooManyInstances       = 5,
    Phusion_TooBusy                = 6,
    Phusion_InitFailed             = 7,
    Phusion_FinFailed              = 8,
    Phusion_BadIOPHandle           = 9,
    Phusion_BadConfig              = 10,
    Phusion_WaitFailed             = 11,
    Phusion_CancelFailed           = 12,
    Phusion_IncompatibleBuffers    = 13,
    Phusion_InvalidResampleFlags   = 14,
    Phusion_InvalidGradientFlags   = 15,
    Phusion_InvalidProcessFlags    = 16,
    Phusion_InvalidConversionFlags = 17,
    Phusion_InvalidFilterFlags     = 18,

    // Hardware-related error conditions
    Phusion_HW_NotFound            = 0x80,

    Phusion_NotImplemented         = 0xff
} Phusion_Result;

// Library version number
typedef struct
{
    uint8_t majorVersion;  // Major functional changes
    uint8_t minorVersion;  // Minor changes to function/features
    uint8_t revision;      // Small, perfective changes and bug-fixes
} Phusion_Version;

// Library feature flags
typedef enum
{
    Phusion_FeatSupportsCPU  = 1,         // Supports CPU-based processing
    Phusion_FeatSupportsSMP  = 2,         // Supports Symmetric Multi-Processing
    Phusion_FeatSupportsSIMD = 4,         // Supports SIMD multimedia extensions
    Phusion_FeatSupportsGPU  = 8,         // Supports GPU-based processing
    Phusion_FeatSupportsHW   = 0x10,      // Supports processing in dedicated Hardware block

    Phusion_FeatU8Samples    = 0x1000,    // Supports 8-bit unsigned sample data
    Phusion_FeatU16Samples   = 0x2000,    // Supports 16-bit unsigned sample data
    Phusion_FeatSFSamples    = 0x4000,    // Supports single-precision floating point samples
    Phusion_FeatDFSamples    = 0x8000,    // Supports double-precision floating point samples
} Phusion_FeatureFlags;

// Library features
typedef struct
{
    uint8_t              featSize;        // Size of this Phusion_Features structure
    uint8_t              maxPlanes;       // Maximum number of planes per input/output buffer
    uint8_t              maxChannels;     // Maximum number of channels per input image
    uint8_t              maxPolynomials;  // Maximum number of polynomials supported
    Phusion_FeatureFlags flags;           // Library feature flags
} Phusion_Features;

// Configuration flags
typedef enum
{
    Phusion_DefaultConfig   = ~0U,  // Default configuration, using all processing resources

    Phusion_CfgUseCPU       = 1,    // Use CPU-based pixel processing
    Phusion_CfgUseSMP       = 2,    // Use Symmetric Multi-Processing, if available
    Phusion_CfgUseSIMD      = 4,    // Use SIMD multimedia extensions, if available
    Phusion_CfgUseGPU       = 8,    // Use GPU-based processing, if available
    Phusion_CfgUseHW        = 0x10  // Use dedicated Hardware block, if available
} Phusion_Config;

// Flags affecting filtering
typedef enum
{
    Phusion_FiltOverlapped   = 0x80000000U,  // Overlapped processing

    Phusion_FiltDefault      =         ~0U   // Use default filtering behaviour
} Phusion_FilterFlags;

// Flags affecting conversion
typedef enum
{
    Phusion_ConvMatrix       =           1,  // Apply conversion matrix for each output
    Phusion_ConvGain         =           2,  // Apply gain parameter to each input channel

    Phusion_ConvOverlapped   = 0x80000000U,  // Overlapped processing

    Phusion_ConvDefault      =         ~0U   // Use default conversion behaviour
} Phusion_ConversionFlags;

// Flags affecting resampling
typedef enum
{
    Phusion_ResNearest       =          0U,  // Nearest-neighbour resampling
    Phusion_ResBox           =          1U,  // Box filter resampling
    Phusion_ResFiltType      =        0xFU,  // Bits 3:0 specify the filter algorithm

    Phusion_ResOverlapped    = 0x80000000U,  // Overlapped processing

    Phusion_ResDefault       =         ~0U   // Use default resampling behaviour
} Phusion_ResampleFlags;

// Flags affecting gradient calculation
typedef enum
{
    Phusion_GradResample      = 1,   // Perform default resampling on the inputs which are the
                                     //   fully-sampled contrast and guide images, not yet downsampled
    Phusion_GradInitialStripe = 2,   // Initial stripe of an image => initialise coefficient calculation
    Phusion_GradFinalStripe   = 4,   // Final stripe of an image => emit final coefficients upon completion

    Phusion_GradOverlapped    = 0x80000000U,  // Overlapped processing

    Phusion_GradDefault       = ~0U  // Use default behaviour
} Phusion_GradientFlags;

// Configuration settings for gradient calculation
typedef struct
{
    // Per-channel Phusion strengths
    double contGains[PHUSION_MAX_CHANNELS];
    double guideGains[PHUSION_MAX_CHANNELS];

    // Regularisation parameters
    double lambda;
    double tikLambda;
} Phusion_GradientConfig;

// Flags affecting pixel-processing
typedef enum
{
    Phusion_ProcSwitchConfig   = 1,           // Switch to new configuration settings at the start of this stripe
    Phusion_ProcUseWeights     = 2,           // Spatially-varying weights supplied
    Phusion_ProcStripedWeights = 4,           // Weights are supplied as stripe (use fraction/scale factor)
    Phusion_ProcOverlapped     = 0x80000000U  // Overlapped processing; completion is asynchronous
} Phusion_ProcessFlags;

// Plane format flags - data type for each component sample within the plane
typedef enum
{
    // Fully-enumerated sample types for convenience
    Phusion_PlaneFlagU8     = 0,    // 8-bit  unsigned integer samples
    Phusion_PlaneFlagU16    = 1,    // 16-bit unsigned integer samples
    Phusion_PlaneFlagSF     = 2,    // Single-precision floating point samples
    Phusion_PlaneFlagDF     = 3,    // Double-precision floating point samples

    // Sample types just for internal use (presently)
    Phusion_PlaneFlagI8     = 4,    // 8-bit  signed integer samples
    Phusion_PlaneFlagI16    = 5,    // 16-bit signed integer samples
    Phusion_PlaneFlagU32    = 8,    // 32-bit unsigned integer samples
    Phusion_PlaneFlagI32    = 0xC,  // 32-bit signed integer samples

    // Sample types are formed from these flags
    Phusion_PlaneFlagWide   = 1,    // Wide data (16-bits/double-precision, not 8/single)
    Phusion_PlaneFlagFloat  = 2,    // Floating-point data
    Phusion_PlaneFlagSigned = 4,    // Signed integral data, not unsigned
    Phusion_PlaneFlagLong   = 8     // Long data (32-bits/long double-precision)
} Phusion_PlaneFlags;

// Plane formats - colour space and component packing/ordering
typedef enum
{
    Phusion_PlaneNComps   = 0x00FF,  // Component count
    Phusion_PlaneOrdering = 0x0F00,

    // Raster-scan formats
    Phusion_PlaneRaster1  = 1,  // single component
    Phusion_PlaneRaster2,       // two interleaved components
    Phusion_PlaneRaster3,
    Phusion_PlaneRaster4,
    Phusion_PlaneRaster5,
    Phusion_PlaneRaster6,
    Phusion_PlaneRaster7,
    Phusion_PlaneRaster8
} Phusion_PlaneFormat;

// Single plane, consisting of one more components
typedef struct
{
    void               *pData;    // Pointer to start of plane (top-left of image data)
    int32_t             iStride;  // Address offset from one scanline to the next
    Phusion_PlaneFlags  flags;    // Format flags, specifying the sample data type
    Phusion_PlaneFormat fmt;      // Plane format => sample count and ordering
} Phusion_Plane;

// Pixel buffer, consisting of one or more data planes
typedef struct
{
    uint8_t       nPlanes;                     // Number of used planes
    uint8_t       res1, res2, res3;            // Reserved fields (SBZ)
    Phusion_Plane planes[PHUSION_MAX_PLANES];  // Properties of each plane
} Phusion_Buffer;

// Description of the channels of an input/output image
typedef struct
{
    uint8_t nChannels;                          // Number of image channels
    uint8_t res1, res2, res3;                   // Reserved fields (SBZ)
    uint8_t planeNumber[PHUSION_MAX_CHANNELS];  // Plane number for each channel
    uint8_t compNumber[PHUSION_MAX_CHANNELS];   // Component within that plane
} Phusion_ImageChannels;

// Polynomial coefficients for pixel-processing
typedef struct
{
    uint8_t nPlanes;     // Number of used planes
    uint8_t nCoeffs;     // Number of coefficients for each used plane
    uint8_t res1, res2;  // Reserved fields (SBZ)
    double  coeffs[PHUSION_MAX_PLANES][PHUSION_MAX_COEFFS];
} Phusion_Polynomial;

// Filter kernel coefficients for image filtering
typedef struct
{
    uint8_t kernSize;
    double  coeff[PHUSION_MAX_FILT_COEFFS];
} Phusion_FilterKernel;

// Conversion parameters for a single channel of format/colour space conversion
typedef struct
{
    double matrix[PHUSION_MAX_CHANNELS];
    double offset;
} Phusion_Conversion;

// Weights pixel offset (upper bits)/scaling (lower) constants
#define Phusion_WgtOffsetBits 16U
#define Phusion_WgtFractBits   8U  // Offset is s8.8, bits [31:16]
#define Phusion_WgtScaleBits (32U  - Phusion_WgtOffsetBits)  // [15:0]
#define Phusion_WgtScaleMask ((1U << Phusion_WgtScaleBits) - 1U)

// Callback functions from the library
typedef struct
{
    // Memory management routines
    void *(*mem_alloc)(size_t n, const char *pDesc);
    void  (*mem_free)(void *, const char *pDesc);

    // Diagnostic routines
    void  (*log_printf)(const char *pFmt, ...);
    void  (*error)(Phusion_Result res, const char *pDesc);
} Phusion_Callbacks;

// ID of polynomial
typedef unsigned Phusion_PolynomialID;

// Opaque handle to an overlapped processing operation
typedef struct Phusion_IOPUnit *Phusion_IOPHandle;

// Opaque handle to Phusion library state
typedef struct Phusion_State *Phusion_Handle;

//---------------------------------------------------------------------------------------------
// Return version number and feature set of the Phusion library
//---------------------------------------------------------------------------------------------
PH_ENTRY
Phusion_Result Phusion_Identify(Phusion_Version *pVersion, Phusion_Features *pFeatures);

//---------------------------------------------------------------------------------------------
// Initialise Phusion library
//---------------------------------------------------------------------------------------------
PH_ENTRY
Phusion_Result Phusion_Initialise(Phusion_Config config, Phusion_Handle *pHandle);

//---------------------------------------------------------------------------------------------
// Finalise Phusion library
//---------------------------------------------------------------------------------------------
PH_ENTRY
Phusion_Result Phusion_Finalise(Phusion_Handle ph);

//---------------------------------------------------------------------------------------------
// Filter an image with the supplied filter kernel weights to produce an output image
//---------------------------------------------------------------------------------------------
PH_ENTRY
Phusion_Result Phusion_FilterPixels(Phusion_Handle              ph,          // Handle to Phusion instance
                                    const Phusion_Buffer      *pBufOut,      // Output buffer
                                    const Phusion_Buffer      *pBufIn,       // Input buffer
                                    uint32_t                    pixPerLine,  // Pixels per line
                                    uint32_t                    numLines,    // Number of scanlines
                                    const Phusion_FilterKernel *pKernel,     // Filter kernel to be applied
                                    Phusion_FilterFlags         flags,       // Flags affecting filtering
                                    Phusion_IOPHandle          *pIOP);       // Returned for overlapped I/O

//---------------------------------------------------------------------------------------------
// Image sample/colour space conversions
//---------------------------------------------------------------------------------------------
PH_ENTRY
Phusion_Result Phusion_ConvertPixels(Phusion_Handle                ph,         // Handle to Phusion instance
                                     const Phusion_Buffer        *pBufOut,     // Output buffer
                                     const Phusion_Buffer        *pBufIn,      // Input buffer
                                     const Phusion_ImageChannels *pOutIm,      // Output image channels description
                                     const Phusion_ImageChannels *pInIm,       // Input image channels description
                                     uint32_t                     pixPerLine,  // Pixels per line
                                     uint32_t                     numLines,    // Number of scanlines
                                     const Phusion_Conversion   *pConversion,  // Conversion parameters per output
                                     const double               *pGain,        // Gain parameter per input
                                     Phusion_ConversionFlags      flags,       // Flags affecting conversion
                                     Phusion_IOPHandle          *pIOP);        // Returned for overlapped I/O

//---------------------------------------------------------------------------------------------
// Compute resampled images from the given input images using the specified resampling filter,
//    for subsequent use in the gradient calculation stage
//---------------------------------------------------------------------------------------------
PH_ENTRY
Phusion_Result Phusion_ResampleImages(Phusion_Handle         ph,            // Handle to Phusion instance
                                      const Phusion_Buffer *pBufOut,        // Output buffer
                                      const Phusion_Buffer *pBufIn,         // Input buffer
                                      uint32_t               outPixPerLine, // Output pixels per line
                                      uint32_t               outNumLines,   // Output height in scanlines
                                      uint32_t               inPixPerLine,  // Input pixels per line
                                      uint32_t               inNumLines,    // Input height in scanlines
                                      Phusion_ResampleFlags  flags,         // Flags affecting resampling
                                      Phusion_IOPHandle     *pIOP);         // Returned for overlapped I/O

//---------------------------------------------------------------------------------------------
// Calculate the Spectral Edge gradient from the given contrast and guide (thumbnail) images
//---------------------------------------------------------------------------------------------
PH_ENTRY
Phusion_Result Phusion_CalculateGradient(Phusion_Handle                 ph,          // Handle to Phusion instance
                                         Phusion_Polynomial           *pPoly,        // Output polynomial coefficients
                                         const Phusion_Buffer         *pBufIn,       // Input images
                                         const Phusion_ImageChannels  *pContrastIm,  // Contrast image channels description
                                         const Phusion_ImageChannels  *pGuideIm,     // Guide image channels description
                                         uint32_t                       pixPerLine,  // Pixels per scanline
                                         uint32_t                       numLines,    // Stripe height in scanlines
                                         const Phusion_GradientConfig *pConfig,      // Configuration settings
                                         Phusion_GradientFlags          flags,       // Flags affecting gradient calcs
                                         Phusion_IOPHandle            *pIOP);        // Returned for overlapped I/O

//---------------------------------------------------------------------------------------------
// Process a complete image as a single operation, performing all stages of the Phusion
//    algorithm and producing only the final processed output
//---------------------------------------------------------------------------------------------
PH_ENTRY
Phusion_Result Phusion_ProcessImage(Phusion_Handle                 ph,          // Handle to Phusion instance
                                    const Phusion_Buffer         *pBufOut,      // Output buffer
                                    const Phusion_Buffer         *pBufIn,       // Input buffer
                                    const Phusion_ImageChannels  *pOutIm,       // Output image channels description
                                    const Phusion_ImageChannels  *pContrastIm,  // Contrast image channels description
                                    const Phusion_ImageChannels  *pGuideIm,     // Guide image channels description
                                    uint32_t                       imWidth,     // Image width, pixels
                                    uint32_t                       imHheight,   // Image height, pixels
                                    Phusion_ResampleFlags          rsFlags,     // Flags affecting resampling
                                    const Phusion_GradientConfig *pGradConfig,  // Gradient settings
                                    Phusion_GradientFlags          grFlags,     // Flags affecting gradient calcs
                                    Phusion_ProcessFlags           flags,       // Flags affecting processing
                                    Phusion_IOPHandle            *pIOP);        // Returned for overlapped I/O

//---------------------------------------------------------------------------------------------
// Process a stripe of pixel data; processing may be performed in-situ by supplying NULL pFBOut
// if the input and output image formats are identical. Pixel processing may be performed
// using 'overlapped I/O', returning control to the calling code before the operation completes
// asynchronously at a later time.
//---------------------------------------------------------------------------------------------
PH_ENTRY
Phusion_Result Phusion_ProcessPixels(Phusion_Handle                ph,          // Handle to Phusion instance
                                     const Phusion_Buffer        *pBufOut,      // Output buffer
                                     const Phusion_Buffer        *pBufWgt,      // Weights buffer
                                     const Phusion_Buffer        *pBufIn,       // Input buffer
                                     const Phusion_ImageChannels *pOutIm,       // Output image channels description
                                     const Phusion_ImageChannels *pContrastIm,  // Contrast image channels description
                                     uint32_t                      wgtX,        // Weights per line, or X fraction+scaling
                                     uint32_t                      wgtY,        // Lines of weights, or Y fraction+scaling
                                     uint32_t                      pixPerLine,  // Pixels per scanline
                                     uint32_t                      numLines,    // Stripe height in scanlines
                                     Phusion_ProcessFlags          flags,       // Flags affecting processing
                                     Phusion_IOPHandle           *pIOP);        // Returned for overlapped I/O

//---------------------------------------------------------------------------------------------
// Specify the polynomial coefficients to be used for subsequent pixel-processing
//---------------------------------------------------------------------------------------------
PH_ENTRY
Phusion_Result Phusion_SetPolynomial(Phusion_Handle             ph,     // Handle to Phusion instance
                                     Phusion_PolynomialID       id,     // ID of polynomial to set
                                     const Phusion_Polynomial *pPoly);  // Polynomial coefficients

//---------------------------------------------------------------------------------------------
// Retrieve the most-recently set polynomial coefficients
//---------------------------------------------------------------------------------------------
PH_ENTRY
Phusion_Result Phusion_GetPolynomial(Phusion_Handle             ph,     // Handle to Phusion instance
                                     Phusion_PolynomialID       id,     // ID of polynomial to retrieve
                                     Phusion_Polynomial       *pPoly);  // Receives polynomial coefficients

//---------------------------------------------------------------------------------------------
// Wait for completion of a processing/gradient calculation operation
//---------------------------------------------------------------------------------------------
PH_ENTRY
Phusion_Result Phusion_WaitComplete(Phusion_Handle ph, Phusion_IOPHandle iop, uint32_t timeout);

//---------------------------------------------------------------------------------------------
// Cancel a processing operation
//---------------------------------------------------------------------------------------------
PH_ENTRY
Phusion_Result Phusion_Cancel(Phusion_Handle ph, Phusion_IOPHandle iop);

//---------------------------------------------------------------------------------------------
// Utility function that registers callback functions for use by the Phusion library
//---------------------------------------------------------------------------------------------
PH_ENTRY
Phusion_Result Phusion_SetCallback(const Phusion_Callbacks,  // List of callback functions
                                   size_t sz);               // Size of callback structure

//---------------------------------------------------------------------------------------------
// Utility function that returns a textual error description
//---------------------------------------------------------------------------------------------
PH_ENTRY
const char *Phusion_ErrorText(Phusion_Result res);

#ifdef __cplusplus
};
#endif

#endif
