/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_cpu_resample.hpp                                      */
/* Description: CPU-based implementation of the Phusion resampling stage      */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __phusion_cpu_resample_hpp
#define __phusion_cpu_resample_hpp
#ifndef __phusion_cpu_hpp
#include "phusion_cpu.hpp"
#endif

// Resampling of input buffers to produce downsampled input for gradient calculations
void cpu_resampling(Phusion_State *ph_state, Phusion_CPUThreadState *state, const Phusion_IOPUnit *pIOP,
                    const Phusion_Buffer *pBufRs, const Phusion_Buffer *pBufIn,
                    uint32_t rsPixPerLine, uint32_t rsNumLines,
                    uint32_t pixPerLine, uint32_t numLines,
                    Phusion_ResampleFlags rsFlags,
                    volatile bool *pCancel);

#endif
