/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_cpu.cpp                                               */
/* Description: CPU-based implementation of the Phusion pixel processing      */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#include <cmath>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#include "phusion_cpu.hpp"
#include "phusion_cpu_resample.hpp"
#include "phusion_linsys.hpp"
#include "phusion_main.hpp"
#include "phusion_maths.hpp"
#include "phusion_sampler.hpp"
#include "phusion_utils.hpp"
#include "phusion_worker.hpp"

// Test mode for load-balancing SMP code; each threads tints its output pixels
//    according to its thread number, providing a visual indication of work load
// #define PHUSION_TEST_MODE

#ifdef PHUSION_USE_CPU

#if PHUSION_LOGGING
static const bool bVerbose(true);
#else
static const bool bVerbose(false);
#endif

// TODO! - lose the hard-coded numbers!
const int Q = PHUSION_Q;

// Gradient calculations
static void cpu_calc_gradient(Phusion_State *ph_state, Phusion_CPUThreadState *state, const Phusion_IOPUnit *pIOP,
                              Phusion_LinSysCont *pLinSys, const Phusion_Buffer *pBufIn,
                              uint32_t numLines,   uint32_t pixPerLine,  // Pixels for which gradient must be calculated
                              uint32_t availLines, uint32_t availPix,    // Pixels for which information is available
                              volatile bool *pCancel);

// Filtering operations
static void cpu_filtering(Phusion_State *ph_state, Phusion_CPUThreadState *state, const Phusion_IOPUnit *pIOP,
                          const Phusion_Buffer *pBufOut, const Phusion_Buffer *pBufIn,
                          uint32_t pixPerLine, uint32_t numLines, volatile bool *pCancel);

// Main processing function for CPU-based pixel processing
static void cpu_process_pixels(Phusion_State *ph_state, Phusion_CPUThreadState *state, const Phusion_IOPUnit *pIOP,
                               const Phusion_Buffer *pBufOut, const Phusion_Buffer *pBufWgt, const Phusion_Buffer *pBufIn,
                               uint32_t wgtX, uint32_t wgtY, uint32_t pixPerLine, uint32_t numLines,
                               volatile bool *pCancel);

// Worker thread function
static void *cpu_worker_thread(void *);

// Fixed-point implementation, visually identical to the floating-point implementation, but not bit-exact
static void PhusionApply_Fixed(uint8_t *dp, const uint8_t *p, const uint8_t *p1, uint32_t w,
                               const int32_t icoeffs[][PHUSION_MAX_PLANES]);

// Wide (64-bit) fixed-point implementation, can be bit exact with the floating-point implementation
static void PhusionApply_FixedW(uint8_t *dp, const uint8_t *p, const uint8_t *p1, uint32_t w,
                                const int32_t iwcoeffs[][PHUSION_MAX_PLANES]);

// Floating-point version, either single- or double-precision (just define phpoly_t appropriately)
static void PhusionApply_Float(uint8_t *dp, const Phusion_Buffer *pInBuf, uint32_t w,
                               const phpoly_t coeffs[][PHUSION_MAX_PLANES]);

// Spatially-varying, multi-polynomial coefficient application
static void PhusionApply_Spatial(const Phusion_State *ph_state, Phusion_CPUThreadState *state, const Phusion_IOPUnit *pIOP,
                                 const Phusion_Buffer &bufOut, const Phusion_Buffer *pBufWgt, const Phusion_Buffer &bufIn,
                                 uint32_t wgtX, uint32_t wgtY, uint32_t numLines, uint32_t pixPerLine, unsigned nPoly,
                                 volatile bool *pCancel);


#ifdef PHUSION_TEST_MODE
// Test code for monitoring the load-balancing algorithm
static uint32_t test_tint(uint32_t pix, unsigned testMode);
#endif

/* Initialise CPU-based pixel-processing
   - create IPC resources and worker threads
   - start all worker threads in anticipation of the first frame */
Phusion_Result phusion_cpu_init(Phusion_State *ph_state, int nthreads)
{
    pthread_attr_t attr;
    unsigned nCPUs = 0;

    if (nthreads < 0)
    {
        // Work out how many CPU cores we have
        nCPUs = NumberOfProcessors();
#ifdef WIN32
        nthreads = nCPUs << 1;  // Assume presence of hyperthreading
#else
        nthreads = nCPUs;       // Assume no hyperthreading
#endif
        // Allow for the presence of one UI thread in the caller, for example
    //    nthreads--;

        if (nthreads > PHUSION_CPU_MAX_THREADS)
            nthreads = PHUSION_CPU_MAX_THREADS;
    }

    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    // Create and start each of our worker threads
    unsigned cpuNumber = 0;
    unsigned t = 0;
    while (t < (unsigned)nthreads)
    {
        if (!CreateWorkerThread(ph_state, &ph_state->cpu.states[t].worker, cpu_worker_thread, &attr, true, cpuNumber))
        {
            fprintf(stderr, "ERROR: Failed to create CPU worker thread %d\n", t);
            break;
        }
        if (++cpuNumber >= nCPUs)
            cpuNumber = 0;
        ph_state->cpu.nThreads = ++t;
    }

    pthread_attr_destroy(&attr);

    return Phusion_OK;
}

// Finalise CPU-based pixel processing
// - stop all worker threads
Phusion_Result phusion_cpu_fin(Phusion_State *ph_state)
{
    for(unsigned t = 0; t < ph_state->cpu.nThreads; t++)
    {
        Phusion_Result phResult = JoinWorkerThread(ph_state, &ph_state->cpu.states[t].worker);
        if (phResult != Phusion_OK)
            return phResult;
    }

    return Phusion_OK;
}

// Gradient calculations, operating upon a horizontal stripe of an input image and computing its contribution
//    to the complete linear system. The contribution is accumulated into thread local storage and the final
//    linear system is accumulated only when the final worker thread

void cpu_calc_gradient(Phusion_State *ph_state, Phusion_CPUThreadState *state, const Phusion_IOPUnit *pIOP,
                       Phusion_LinSysCont *pLinSys, const Phusion_Buffer *pBufIn,
                       uint32_t numLines,   uint32_t pixPerLine,  // Pixels for which gradient must be calculated
                       uint32_t availLines, uint32_t availPix,    // Pixels for which information is available
                       volatile bool *pCancel)
{
    // TODO - suspect this amount of stacked data is bad news....move this into the state structure?
    phfloat_t poly_nextr[PHUSION_MAX_CHANNELS];
    phfloat_t poly_nextc[PHUSION_MAX_CHANNELS];
    phfloat_t poly_curr[PHUSION_MAX_COEFFS];
    phfloat_t poly_gx[PHUSION_MAX_COEFFS];
    phfloat_t poly_gy[PHUSION_MAX_COEFFS];
    phfloat_t temp_gx[PHUSION_MAX_CHANNELS];
    phfloat_t temp_gy[PHUSION_MAX_CHANNELS];
    phfloat_t temp_g[PHUSION_MAX_CHANNELS];
//  uint32_t pixLeft = pixPerLine;
    // TODO

    profile_start_timing(&state->worker.profile[PROF_CPU_CALCGRADIENT]);

    const phfloat_t normFact = (phfloat_t)sqrt(1.0 / 3.0);
    const unsigned Q = pIOP->imgContrast.nChannels;
    const unsigned P = pIOP->imgGuide.nChannels;
    const unsigned pchan = Q + Q*(Q+1)/2;

    // TODO
    // Operate in columns across the width of this stripe, if necessary
    // - this is only necessary/beneficial when performing on-the-fly resampling since we can
    //   pick up our input samples much faster if we have pre-calculated horizontal offsets
    //   a la the standalone resampling in 'cpu_resampling'

    unsigned i, j, m, n, ii;

// TODO - tidy and eliminate; these settings are applicable only to RGB+Thermal|NIR Phusion
#define PRESCALED 0
#define NOSCALE   0

#if !NOSCALE
    // TODO - scale the poly_ values because rgb_thermal does not scale the thermal component of the contrast
    //        for the linear system solution and polynomial computation/application
    phfloat_t iscThermal = (phfloat_t)(1.0 / pIOP->grConfig.contGains[Q-1]);
#endif

    if (bVerbose)
    {
        printf("Grad calcs on %u lines of %u pixels\n"
               "   (%u lines of %u pixels available)\n", numLines, pixPerLine, availLines, availPix);
        for (unsigned ch = 0; ch < Q; ch++)
            printf("%d: guide: plane %x, comp %x - contrast: plane %x, comp %x\n",
                ch,
                (ch >= P) ? ~0U : pIOP->imgGuide.planeNumber[ch],
                (ch >= P) ? ~0U : pIOP->imgGuide.compNumber[ch],
                pIOP->imgContrast.planeNumber[ch], pIOP->imgContrast.compNumber[ch]);

        // Report the per-channel gains, scaled gain and Thermal channel inverse gain
        printf("Guide gains:\n");
        for (unsigned ch = 0; ch < pIOP->imgGuide.nChannels; ch++)
            printf("  ch %u: %lf scaled to %lf\n", ch, pIOP->grConfig.guideGains[ch], (double)pIOP->scGuideGains[ch]);
        printf("Contrast gains:\n");
        for (unsigned ch = 0; ch < pIOP->imgContrast.nChannels; ch++)
            printf("  ch %u: %lf scaled to %lf\n", ch, pIOP->grConfig.contGains[ch], (double)pIOP->scContGains[ch]);
        printf("Thermal inverse gain: %lf\n", (double)iscThermal);
    }

    /* The core loop calculates the Spectral Edge gradient and sets up
    * some data for the reintegration that happens later. */
    for (j = 0; j < numLines; j++)
    {
        int nextRow = (j < availLines - 1);

        for (i = 0; i < pixPerLine; i++) {
            int nextCol = (i < availPix - 1);

            // Z (Di Zenzo matrix) for Guide and for Contrast.
            dizenzo Zg, Zc;

            // x and y gradients for Guide and for Contrast.
            gradient xyG, xyC;

            // Compute the colour tensors pixel-wise.
            Zg.g11 = Zg.g12 = Zg.g22 = PHFLOAT_ZERO;
            Zc.g11 = Zc.g12 = Zc.g22 = PHFLOAT_ZERO;

            for (unsigned ch = 0; ch < Q; ch++)
            {
                // TODO - obviously we do NOT want to be incurring all this overhead on every pixel;
                //        compute up-front which sample reader(s) we shall require
                unsigned plane  = pIOP->imgContrast.planeNumber[ch];
                unsigned comp   = pIOP->imgContrast.compNumber[ch];
                uint8_t nComps  = (int)(pBufIn->planes[plane].fmt);  // temp
                int32_t iStride = pBufIn->planes[plane].iStride;

                switch (pBufIn->planes[plane].flags)
                {
                    case Phusion_PlaneFlagU16:
                    {
                        // TODO - obviously due to be re-expressed
                        const uint16_t *pCurr = (uint16_t*)((uint8_t*)pBufIn->planes[plane].pData + j*iStride + (i*nComps+comp)*2);
                        const phfloat_t sc = pIOP->scContGains[ch];

                        poly_curr[ch]  = pCurr[0] * sc;
                        poly_nextc[ch] = pCurr[nextCol*nComps] * sc;
                        poly_nextr[ch] = pCurr[nextRow*iStride/2] * sc;
                    }
                    break;

                    case Phusion_PlaneFlagU8:
                    {
                        const uint8_t *pCurr = (uint8_t*)pBufIn->planes[plane].pData + i*nComps+comp + j*iStride;
                        const phfloat_t sc = pIOP->scContGains[ch];

                        if (0) // i < 1 && j < 10)
                            printf("Contrast %u: %u,%u: %lf %lf %lf (%d %d at %p)\n", ch, i, j, pCurr[0] * sc, pCurr[nextCol*nComps] * sc,
                                                                                      pCurr[nextRow*iStride] * sc,
                                                                                      nextCol * nComps, nextRow * iStride, pCurr);
                        poly_curr[ch]  = pCurr[0] * sc;
                        poly_nextc[ch] = pCurr[nextCol*nComps]  * sc;
                        poly_nextr[ch] = pCurr[nextRow*iStride] * sc;
                    }
                    break;

                    case Phusion_PlaneFlagDF:
                    {
                        const double *pCurr = (double*)((uint8_t*)pBufIn->planes[plane].pData + j*iStride + (i*nComps+comp)*sizeof(double));
                        const phfloat_t sc = pIOP->scContGains[ch];

                        poly_curr[ch]  = (phfloat_t)pCurr[0] * sc;
                        poly_nextc[ch] = (phfloat_t)pCurr[nextCol*nComps] * sc;
                        poly_nextr[ch] = (phfloat_t)pCurr[nextRow*iStride/sizeof(double)] * sc;
                    }
                    break;

                    case Phusion_PlaneFlagSF:
                    {
                        const float *pCurr = (float*)((uint8_t*)pBufIn->planes[plane].pData + j*iStride + (i*nComps+comp)*sizeof(float));
                        const phfloat_t sc = pIOP->scContGains[ch];

                        poly_curr[ch]  = pCurr[0] * sc;
                        poly_nextc[ch] = pCurr[nextCol*nComps] * sc;
                        poly_nextr[ch] = pCurr[nextRow*iStride/sizeof(float)] * sc;
                    }
                    break;

                    default:
                        PHUSION_ASSERT(!"Unimplemented/unsupported input sample format for Contrast image\n");
                        break;
                }

                poly_gx[ch] = xyC.dx[ch] = poly_nextc[ch] - poly_curr[ch];
                poly_gy[ch] = xyC.dy[ch] = poly_nextr[ch] - poly_curr[ch];

                Zc.g11 += POW2(xyC.dx[ch]);
                Zc.g12 += xyC.dx[ch] * xyC.dy[ch];
                Zc.g22 += POW2(xyC.dy[ch]);

                // TODO - for the 'latest' RGB + Thermal case we may not assume that the guide forms a subset
                //        of the contrast... but we should optimise this case when possible
                //      - probably worth decomposing this loop into two
                if (ch < P)
                {
                    // ....so we must compute the guide gradients separately
                    unsigned plane  = pIOP->imgGuide.planeNumber[ch];
                    unsigned comp   = pIOP->imgGuide.compNumber[ch];
                    uint8_t nComps  = (int)(pBufIn->planes[plane].fmt);  // temp
                    int32_t iStride = pBufIn->planes[plane].iStride;

                    switch (pBufIn->planes[plane].flags)
                    {
                        case Phusion_PlaneFlagU16:
                        {
                            const uint8_t *pCurr = (uint8_t*)pBufIn->planes[plane].pData + i*nComps+comp + j*iStride;
                            const phfloat_t sc = pIOP->scGuideGains[ch];

                            temp_g[ch] = pCurr[0] * sc;
                            xyG.dx[ch] = ((int)pCurr[nextCol*nComps]  - pCurr[0]) * sc;
                            xyG.dy[ch] = ((int)pCurr[nextRow*iStride] - pCurr[0]) * sc;
                        }
                        break;

                        case Phusion_PlaneFlagU8:
                        {
                            const uint8_t *pCurr = (uint8_t*)pBufIn->planes[plane].pData + i*nComps+comp + j*iStride;
                            const phfloat_t sc = pIOP->scGuideGains[ch];

                            temp_g[ch] = pCurr[0] * sc;
                            xyG.dx[ch] = ((int)pCurr[nextCol*nComps]  - pCurr[0]) * sc;
                            xyG.dy[ch] = ((int)pCurr[nextRow*iStride] - pCurr[0]) * sc;
                        }
                        break;

                        case Phusion_PlaneFlagDF:
                        {
                            const double *pCurr = (double*)((uint8_t*)pBufIn->planes[plane].pData + j*iStride + (i*nComps+comp)*sizeof(double));
                            const phfloat_t sc = pIOP->scGuideGains[ch];

                            temp_g[ch] = (phfloat_t)pCurr[0] * sc;
                            xyG.dx[ch] = (phfloat_t)(pCurr[nextCol*nComps] - pCurr[0]) * sc;
                            xyG.dy[ch] = (phfloat_t)(pCurr[nextRow*iStride/sizeof(double)] - pCurr[0]) * sc;
                        }
                        break;

                        case Phusion_PlaneFlagSF:
                        {
                            const float *pCurr = (float*)((uint8_t*)pBufIn->planes[plane].pData + j*iStride + (i*nComps+comp)*sizeof(float));
                            const phfloat_t sc = pIOP->scGuideGains[ch];

                            temp_g[ch] = pCurr[0] * sc;
                            xyG.dx[ch] = (pCurr[nextCol*nComps] - pCurr[0]) * sc;
                            xyG.dy[ch] = (pCurr[nextRow*iStride/sizeof(float)] - pCurr[0]) * sc;
                        }
                        break;

                        default:
                            PHUSION_ASSERT(!"Unimplemented/unsupported input sample format for Contrast image\n");
                            break;
                    }

                    Zg.g11 += POW2(xyG.dx[ch]);
                    Zg.g12 += xyG.dx[ch] * xyG.dy[ch];
                    Zg.g22 += POW2(xyG.dy[ch]);
                }
            }

            /* TODO - do we actually need to vary this parameter?
            const double kludge = 1.0;
            Zc.g11 *= kludge;
            Zc.g12 *= kludge;
            Zc.g22 *= kludge;
            */

            // Eigenvalue Decomposition for Guide and Contrast.
            eigen decG, decC;

            eigenvalue_decomposition(&Zg, &decG);
            eigenvalue_decomposition(&Zc, &decC);

            // Square roots of Di Zenzo matrices for Guide and Contrast.
            dizenzo Zsqg, Zsqc;

            Zsqg.g11 = decG.lambda1 * POW2(decG.v1x) + decG.lambda2 * POW2(decG.v2x);
            Zsqg.g22 = decG.lambda1 * POW2(decG.v1y) + decG.lambda2 * POW2(decG.v2y);
            Zsqg.g12 = decG.lambda1 * decG.v1x * decG.v1y + decG.lambda2 * decG.v2x * decG.v2y;

            Zsqc.g11 = decC.lambda1 * POW2(decC.v1x) + decC.lambda2 * POW2(decC.v2x);
            Zsqc.g22 = decC.lambda1 * POW2(decC.v1y) + decC.lambda2 * POW2(decC.v2y);
            Zsqc.g12 = decC.lambda1 * decC.v1x * decC.v1y + decC.lambda2 * decC.v2x * decC.v2y;

            m2x2 rot;
            vector_findrot(&Zsqc, &Zsqg, &rot);

            phfloat_t lambda1inv = ((decG.lambda1 >= THRESH) ? ((phfloat_t)1.0 / decG.lambda1) : PHFLOAT_ZERO);
            phfloat_t lambda2inv = ((decG.lambda2 >= THRESH) ? ((phfloat_t)1.0 / decG.lambda2) : PHFLOAT_ZERO);

            // Inverse of the Di Zenzo matrix for Guide.
            dizenzo Zinv;

            Zinv.g11 = lambda1inv * POW2(decG.v1x)      + lambda2inv * POW2(decG.v2x);
            Zinv.g12 = lambda1inv * decG.v1x * decG.v1y + lambda2inv * decG.v2x * decG.v2y;
            Zinv.g22 = lambda1inv * POW2(decG.v1y)      + lambda2inv * POW2(decG.v2y);

            // Multiply two 2x2 matrices.
            m2x2 AVEC_p, AVEC;
            m2x2multDM(&Zsqc, &rot, &AVEC_p);
            m2x2multMD(&AVEC_p, &Zinv, &AVEC);

#if !NOSCALE
            poly_curr[Q-1]  *= iscThermal;
            poly_nextc[Q-1] *= iscThermal;
            poly_nextr[Q-1] *= iscThermal;
            poly_gx[Q-1]    *= iscThermal;
            poly_gy[Q-1]    *= iscThermal;
#endif

// TODO - NIR_grad at this point steals thermal/NIR gradient across to all gradients if the input guide image gradients
//        sum to less than a threshold

            phfloat_t abs_g = PHFLOAT_ZERO;
            for (unsigned ch = 0; ch < P; ch++)
                abs_g += ABS(xyG.dx[ch]) + ABS(xyG.dy[ch]);

            const double kNIRgrad = 0.025;
            if (abs_g <= kNIRgrad)
            {
                phfloat_t sc_gx = poly_gx[Q-1] * normFact;
                phfloat_t sc_gy = poly_gy[Q-1] * normFact;

                for (unsigned ch = 0; ch < P; ch++)
                {
                    temp_gx[ch] = sc_gx;
                    temp_gy[ch] = sc_gy;
                }
            }
            else
            {
                for (unsigned ch = 0; ch < P; ch++)
                {
                    temp_gx[ch] = xyG.dx[ch] * AVEC.r11 + xyG.dy[ch] * AVEC.r12;
                    temp_gy[ch] = xyG.dx[ch] * AVEC.r21 + xyG.dy[ch] * AVEC.r22;
                }
            }

            // ------ END OF GRADIENT CALCULATION ------

            /* Quadratic terms. I'm expanding the image into a polynomial, and then
             * calculating its gradient. */
            unsigned jj = 0, kk = 0;
            for (ii = Q; ii < pchan; ii++)
            {
                phpoly_t prod = poly_curr[jj] * poly_curr[kk];
                poly_curr[ii] = prod;
#if 1
                poly_gx[ii]   = (poly_nextc[jj] * poly_nextc[kk]) - prod;
                poly_gy[ii]   = (poly_nextr[jj] * poly_nextr[kk]) - prod;
#else
phpoly_t tempc = (poly_nextc[jj] * poly_nextc[kk]);
phpoly_t tempr = (poly_nextr[jj] * poly_nextr[kk]);
                poly_gx[ii]   = tempc - prod;
                poly_gy[ii]   = tempr - prod;
#endif
                if (++kk >= Q)
                    kk = ++jj;
            }

            /* The left-hand side of the linear system is in the form A^T * A (because it's a
            * linear least-squares problem). This means the matrix is symmetric (i.e., equal to
            * its transpose) and I can use a Lapack routine that exploits this. Therefore, I use
            * an array that doesn't contain the whole matrix, and I only store its upper-triangular
            * using Lapack's packed-storage convention. */
            for (m = 0; m < pchan; m++)
            {
                for (n = m; n < pchan; n++)
                {
                    // The indices for Ls and Lreg follow the Lapack convention for packed storage.
                    // http://www.netlib.org/lapack/lug/node123.html
                    pLinSys->Ls[m   + n*(n + 1) / 2] += poly_gx[n]   * poly_gx[m];
                    pLinSys->Ls[m   + n*(n + 1) / 2] += poly_gy[n]   * poly_gy[m];
                    pLinSys->Lreg[m + n*(n + 1) / 2] += poly_curr[n] * poly_curr[m];
// TODO - simplify addressing
                }
            }

//0 -> 0
//1 -> 1
//2 -> 3
//3 -> 6
//4 -> 10

//(k+1)*(k+2)/2 = (k*k+3k+2)/2
//              = k*(k+3)/2 + 1
//              = k*(k+1)/2 + k + 1
//0,1,2,3,4,5
//0

            /* Matrices on the right of the system. Actually, I'm solving for P linear system in which
            * the known vector is one of the columns of Rs. Lapack solves all of them simultaneously. */
// TODO - addressing is wasteful here!
//            for (unsigned ch = 0; ch < P; ch++)
            unsigned ch = 0;
            ii = 0;
            while (ii < P * pchan)
            {
                for (kk = 0; kk < pchan; kk++)
                {
// TODO - the following is mathematically equivalent, but because of the wonders of floating-point arithmetic
//        it breaks comparison against earlier reference data
//                  pLinSys->Rs[ii]   += (poly_gx[kk] * temp_gx[ch]) + (poly_gy[kk] * temp_gy[ch]);
                    pLinSys->Rs[ii]   += poly_gx[kk]   * temp_gx[ch];
                    pLinSys->Rs[ii]   += poly_gy[kk]   * temp_gy[ch];
                    pLinSys->Rreg[ii] += poly_curr[kk] * temp_g[ch];
                    ii++;
                }
                if (++ch >= P)
                    ch = 0;
            }
        }
    }

    profile_stop_timing(&state->worker.profile[PROF_CPU_CALCGRADIENT]);
}

// Main pixel-processing function for CPU thread
void cpu_process_pixels(Phusion_State *ph_state, Phusion_CPUThreadState *state, const Phusion_IOPUnit *pIOP,
                        const Phusion_Buffer *pBufOut, const Phusion_Buffer *pBufWgt, const Phusion_Buffer *pBufIn,
                        uint32_t wgtX, uint32_t wgtY, uint32_t pixPerLine, uint32_t numLines,
                        volatile bool *pCancel)
{
    Phusion_Sampler destState[PHUSION_MAX_CHANNELS];
    Phusion_Sampler srcState[PHUSION_MAX_CHANNELS];

    // TODO - just temporary code for bring up with fixed-format buffers etc
    const uint8_t *sp  = (uint8_t*)pBufIn->planes[0].pData;
    const uint8_t *sp1 = (uint8_t*)pBufIn->planes[1].pData;
    uint8_t *dp = (uint8_t*)pBufOut->planes[0].pData;
    int32_t s1_stride = pBufIn->planes[1].iStride;
    int32_t d_stride = pBufOut->planes[0].iStride;
    int32_t srcStride = pBufIn->planes[0].iStride;
    const unsigned Q = pIOP->imgContrast.nChannels;

    PHUSION_ASSERT(ph_state && state);

    profile_start_timing(&state->worker.profile[PROF_CPU_PROCPIXELS]);

#if 0
    // TODO - the pixel-processing code has not yet been generalised
    PHUSION_ASSERT(pBufIn->nPlanes == 2 && pBufOut->nPlanes == 1);
    PHUSION_ASSERT(pBufIn->planes[0].fmt == Phusion_PlaneRaster3  && pBufIn->planes[0].flags == Phusion_PlaneFlagU8);
    PHUSION_ASSERT(pBufIn->planes[1].fmt == Phusion_PlaneRaster1  && pBufIn->planes[1].flags == Phusion_PlaneFlagU8);
    PHUSION_ASSERT(pBufOut->planes[0].fmt == Phusion_PlaneRaster3 && pBufOut->planes[0].flags == Phusion_PlaneFlagU8);
    PHUSION_ASSERT(pIOP->imgOutput.nChannels == 3);
    PHUSION_ASSERT(pIOP->imgContrast.nChannels == 4);
#endif

    // Determine the number of polynomials being used for the pixel-processing
    unsigned nPoly = 1;
    if (pIOP->pcFlags & Phusion_ProcUseWeights)
{
        nPoly = (unsigned)pBufWgt->planes[0].fmt & Phusion_PlaneNComps;

// TODO - we're running with planar for now at least until a decision is made
        nPoly = pBufWgt->nPlanes;
}
    else
        pBufWgt = nullptr;
printf("%u weight planes\n", nPoly);

    PHUSION_ASSERT(nPoly >= 1 && nPoly <= PHUSION_MAX_POLYNOMIALS);

    // Has the configuration changed since the last work that we did;
    //    do we need to calculate new coefficients?

    // Decide which kernel should be used based upon the current configuration
    //   and the properties of the requested operation

    // TODO - ensure that changes in those properties are considered configuration changes from
    //        the perspective of the code below

#ifndef PHUSION_KERNEL_NUMBER
    const int kernNum = 0;
#else
    const int kernNum = PHUSION_KERNEL_NUMBER;
#endif
    printf("Using processing kernel %u\n", kernNum);
    if (ph_state->currentCfgSeq != state->lastCfgSeq)
    {
// POISON ALL COEFFICIENTS
memset(state->mpfcoeffs, 0xff, sizeof(state->mpfcoeffs));

        for (unsigned poly = 0; poly < nPoly; poly++)
        {
            const Phusion_Polynomial *pPoly = &pIOP->poly[poly];
            switch (kernNum)
            {
                case 0:
                    for(unsigned c = 0; c < pPoly->nCoeffs; c++)
                        for(unsigned p = 0; p < pPoly->nPlanes; p++)
                        {
                            // Scale the coefficients to .12 fixed-point format, which permits us to
                            //    accumulate up to 16 partial products of (8-bit sample)^2 * coeff into a
                            //    single register, normalising by 8 bits for a maximum error of 1/16th output-
                            //    pixel precision per product, and thus no error in the summation or final rounding

                            //    This means that usable coefficients are in the range [-16,+16) and may be
                            //    represented to 1/4096 (2^-12).
                            const double sc = (double)(1U << 12);
                            state->c.icoeffs[c][p] = (int32_t)(pPoly->coeffs[p][c] * sc);
                        }
                    break;

                case 1:
                    for(unsigned c = 0; c < pPoly->nCoeffs; c++)
                        for(unsigned p = 0; p < pPoly->nPlanes; p++)
                        {
                            //    TODO - comment!
                            const double scq = (double)(1U << 20);
                            const double scl = scq * 255.0;  // Pre-scale the linear coefficients to match quadratic ones
                            state->c.iwcoeffs[c][p] = (int32_t)(pPoly->coeffs[p][c] * ((c < Q) ? scl : scq));
                        }
                    break;

                case 2:
printf("CPU thread %u setting polynomial %u\n", state->worker.threadNumber, poly);
                    for(unsigned c = 0; c < pPoly->nCoeffs; c++)
                        for(unsigned p = 0; p < pPoly->nPlanes; p++)
{
                            // Floating-point implementations run on single-precision coefficients because
                            //    they are somewhat faster on account of greater parallelism
                            state->mpfcoeffs[c][p][poly] = (phpoly_t)pPoly->coeffs[p][c];
printf("%f\n", state->mpfcoeffs[c][p][poly]);
}
                    break;

                default:
                    PHUSION_ASSERT(kernNum == 3);
                    for(unsigned c = 0; c < pPoly->nCoeffs; c++)
                        for(unsigned p = 0; p < pPoly->nPlanes; p++)
                            // Floating-point implementations run on single-precision coefficients because
                            //    they are somewhat faster on account of greater parallelism
                            state->c.fcoeffs[c][p] = (phpoly_t)pPoly->coeffs[p][c];
                    break;
            }
        }
        state->lastCfgSeq = ph_state->currentCfgSeq;
    }

    // We are sampling from the contrast channels, applying the polynomial(s)
    //    and writing to the output channels
    unsigned destChans = pIOP->imgOutput.nChannels;
    unsigned srcChans = pIOP->imgContrast.nChannels;
    unsigned ch;

    // Initialise samplers
    for (ch = 0; ch < destChans; ch++)
    {
        const Phusion_Plane &plane = pBufOut->planes[pIOP->imgOutput.planeNumber[ch]];
        InitSampler(destState[ch], plane.pData, plane.iStride, plane.flags, plane.fmt,
                    pIOP->imgOutput.compNumber[ch]);
    }
    for (ch = 0; ch < srcChans; ch++)
    {
        const Phusion_Plane &plane = pBufIn->planes[pIOP->imgContrast.planeNumber[ch]];
        InitSampler(srcState[ch], plane.pData, plane.iStride, plane.flags, plane.fmt,
                    pIOP->imgContrast.compNumber[ch]);
    }

    switch (kernNum)
    {
        case 0:
            while (!*pCancel && numLines-- > 0)
            {
                PhusionApply_Fixed(dp, sp, sp1, pixPerLine, state->c.icoeffs);
                dp  += d_stride;
                sp  += srcStride;
                sp1 += s1_stride;
            }
            break;

        case 1:
            while (!*pCancel && numLines-- > 0)
            {
                PhusionApply_FixedW(dp, sp, sp1, pixPerLine, state->c.iwcoeffs);
                dp  += d_stride;
                sp  += srcStride;
                sp1 += s1_stride;
            }
            break;

        case 2:
            PhusionApply_Spatial(ph_state, state, pIOP, *pBufOut, pBufWgt, *pBufIn, wgtX, wgtY,
                                 numLines, pixPerLine, nPoly, pCancel);
            break;

        default:
        {
            Phusion_Buffer bufIn = *pBufIn;

            PHUSION_ASSERT(kernNum == 3);
            while (!*pCancel && numLines-- > 0)
            {
                PhusionApply_Float(dp, &bufIn, pixPerLine, state->c.fcoeffs);

                dp  += d_stride;
                bufIn.planes[0].pData = (uint8_t*)bufIn.planes[0].pData + srcStride;
                bufIn.planes[1].pData = (uint8_t*)bufIn.planes[1].pData + s1_stride;
            }
        }
        break;
    }

    profile_stop_timing(&state->worker.profile[PROF_CPU_PROCPIXELS]);
}

// CPU-based pixel-processing worker thread
// - Awaits upon receipt of work units from foreground calls to the library API,
//   until asked to stop, or stopped abruptly during library shutdown.

void *cpu_worker_thread(void *t)
{
    Phusion_ThreadState *worker = (Phusion_ThreadState*)t;
    Phusion_State *ph_state = worker->pState;
    Phusion_CPUThreadState *state;

    // Local our CPU thread state from the generic worker thread state
    state = (Phusion_CPUThreadState*)((uint8_t*)worker
          - offsetof(Phusion_CPUThreadState, worker));

    if (bVerbose)
        printf("CPU Thread %x is running\n", worker->threadNumber);

    while (true)
    {
        Phusion_WorkUnit *pUnit;
        Phusion_IOPUnit *pIOP;
        WorkerAction action;
        void *pData;

        // Await receipt of a work unit
        action = CollectWorkUnit(ph_state, &state->worker, &pData);
        if (action == WorkAction_Quit)
            break;

        pIOP = (Phusion_IOPUnit *)pData;
        pUnit = &pIOP->unit[worker->threadNumber];

        if (bVerbose)
            printf("CPU thread %u got work action %u\n", worker->threadNumber, action);

        switch (action)
        {
            case WorkAction_Filter:
                cpu_filtering(ph_state, state, pIOP, &pUnit->bufOut, &pUnit->bufIn,
                              pIOP->pixPerLine, pUnit->numLines, &pIOP->bCancel);

                if (bVerbose)
                    printf("CPU Thread %x has completed filtering\n", worker->threadNumber);

                // Signal completion of this work unit
                CompleteWorkUnit(ph_state, &state->worker, pIOP);
                break;

            case WorkAction_Convert:
                profile_start_timing(&state->worker.profile[PROF_CPU_CONVERTING]);

                // We can perform this entire operation using a generic internal routine
                ConvertStripe(state, &pUnit->bufOut, &pIOP->bufIn, pIOP->imgOutput, pIOP->imgContrast,
                              pUnit->numLines, pIOP->pixPerLine, pIOP->conversion, pIOP->convGain,
                              pIOP->convFlags, &pIOP->bCancel);

                profile_stop_timing(&state->worker.profile[PROF_CPU_CONVERTING]);

                if (bVerbose)
                    printf("CPU Thread %x has completed conversion\n", worker->threadNumber);

                // Signal completion of this work unit
                CompleteWorkUnit(ph_state, &state->worker, pIOP);
                break;

            case WorkAction_Resample:
                cpu_resampling(ph_state, state, pIOP, &pUnit->bufOut, &pUnit->bufIn,
                               pIOP->rsPixPerLine, pUnit->rsNumLines,
                               pIOP->pixPerLine, pUnit->numLines,
                               pIOP->rsFlags, &pIOP->bCancel);

                if (bVerbose)
                    printf("CPU Thread %x has completed resampling\n", worker->threadNumber);

                // Signal completion of this work unit
                CompleteWorkUnit(ph_state, &state->worker, pIOP);
                break;

            case WorkAction_CalculateGrad:
                if (pIOP->grFlags & Phusion_GradInitialStripe)
                    ClearLinSysCont(worker->linSysCont);

                cpu_calc_gradient(ph_state, state, pIOP, &worker->linSysCont, &pUnit->bufIn,
                                  pUnit->numLines, pIOP->pixPerLine,
                                  pUnit->availLines, pIOP->pixPerLine,
                                  &pIOP->bCancel);

                if (bVerbose)
                    printf("CPU Thread %x has completed gradient calculations\n", worker->threadNumber);

                // Signal completion of this work unit
                CompleteWorkUnit(ph_state, &state->worker, pIOP);
                break;
/*
            case WorkAction_SolveLinSys:
                cpu_solve_linsys(ph_state, &state->worker, pIOP);

                if (bVerbose)
                    printf("CPU Thread %x has completed matrix solving\n", worker->threadNumber);

                // Signal completion of this work unit
                CompleteWorkUnit(ph_state, &state->worker, pIOP);
                break;
*/
            case WorkAction_ProcessPixels:
                // Process this stripe of pixels
                cpu_process_pixels(ph_state, state, pIOP,
                                   &pUnit->bufOut, &pUnit->bufWgt, &pUnit->bufIn,
                                    pUnit->wgtX,    pUnit->wgtY,
                                    pIOP->pixPerLine, pUnit->numLines,
                                   &pIOP->bCancel);

                if (bVerbose)
                    printf("CPU Thread %x has completed pixel processing\n", worker->threadNumber);

                // Signal completion of this work unit
                CompleteWorkUnit(ph_state, &state->worker, pIOP);
                break;

            default:
                PHUSION_ASSERT(!"Unknown work action in CPU worker thread\n");
                break;
        }
    }

    pthread_exit(NULL);
    return NULL;
}


void PhusionApply_Fixed(uint8_t *dp, const uint8_t *p, const uint8_t *p1, uint32_t w,
                        const int32_t icoeffs[][PHUSION_MAX_PLANES])
{
    // Break the processing into chunks of a fixed size, allowing the compiler
    //    to parallelise the code using vector extensions such as AVX/NEON
    const int N = 32;
    int32_t r[N], g[N], b[N];
    int32_t terms[Q][N];
    const int32_t *cp;

    while (w >= N)
    {
        int k = 0;
        int j = 0;
        int pix;

        w -= N;

        // Read inputs
        for(pix = 0; pix < N; pix++)
        {
            terms[0][pix] = p[pix*3+0];  // R
            terms[1][pix] = p[pix*3+1];  // G
            terms[2][pix] = p[pix*3+2];  // B
            terms[3][pix] = p1[pix];
        }
        p += N*3;
        p1 += N;

        // First linear term initialises results
        for(pix = 0; pix < N; pix++)
        {
            r[pix] = (terms[0][pix] * icoeffs[0][0]) >> 8;  // R
            g[pix] = (terms[0][pix] * icoeffs[0][1]) >> 8;  // G
            b[pix] = (terms[0][pix] * icoeffs[0][2]) >> 8;  // B
        }
        cp = icoeffs[1];

        // Remaining linear terms require no product calculation or indexing
        for(int i = 1; i < Q; i++)
        {
            for(pix = 0; pix < N; pix++)
            {
                r[pix] += (terms[i][pix] * cp[0]) >> 8;
                g[pix] += (terms[i][pix] * cp[1]) >> 8;
                b[pix] += (terms[i][pix] * cp[2]) >> 8;
            }
            cp += PHUSION_MAX_PLANES;
        }

        // Quadratic terms
        while (k < Q)
        {
            for(pix = 0; pix < N; pix++)
            {
                int32_t prod = terms[k][pix] * terms[j][pix];
                r[pix] += (prod * cp[0]) >> 16;
                g[pix] += (prod * cp[1]) >> 16;
                b[pix] += (prod * cp[2]) >> 16;
            }
            cp += PHUSION_MAX_PLANES;

            if (++j >= Q)
                j = ++k;
        }

        // Clamp and write out final pixels
        for(pix = 0; pix < N; pix++)
        {
            dp[pix*3+0] = (r[pix] < 0) ? 0 : ((r[pix] >= 0xff0) ? 0xff : ((r[pix] + 8) >> 4));
            dp[pix*3+1] = (g[pix] < 0) ? 0 : ((g[pix] >= 0xff0) ? 0xff : ((g[pix] + 8) >> 4));
            dp[pix*3+2] = (b[pix] < 0) ? 0 : ((b[pix] >= 0xff0) ? 0xff : ((b[pix] + 8) >> 4));
        }
        dp += N*3;
    }

    // Tail code mops up the remaining pixels, processing one at a time
    while (w-- > 0)
    {
        const int32_t *cp;
        int32_t terms[Q];
        int32_t r, g, b;
        int k = 0;
        int j = 0;

        // Collect contrast samples
        terms[0] = p[0];   // R
        terms[1] = p[1];   // G
        terms[2] = p[2];   // B
        terms[3] = p1[0];  // NIR
        p1++;
        p += 3;

        // Compute output samples
        r = (terms[0] * icoeffs[0][0]) >> 8;
        g = (terms[0] * icoeffs[0][1]) >> 8;
        b = (terms[0] * icoeffs[0][2]) >> 8;
        cp = icoeffs[1];

        for(int i = 1; i < Q; i++)
        {
            r += (terms[i] * cp[0]) >> 8;
            g += (terms[i] * cp[1]) >> 8;
            b += (terms[i] * cp[2]) >> 8;

            cp += PHUSION_MAX_PLANES;
        }

        while (k < Q)
        {
            int32_t prod = terms[k] * terms[j];
            if (++j >= Q)
                j = ++k;

            r += (prod * cp[0]) >> 16;
            g += (prod * cp[1]) >> 16;
            b += (prod * cp[2]) >> 16;

            cp += PHUSION_MAX_PLANES;
        }
        dp[0] = (r < 0) ? 0 : ((r >= 0xff0) ? 0xff : ((r + 8) >> 4));
        dp[1] = (g < 0) ? 0 : ((g >= 0xff0) ? 0xff : ((g + 8) >> 4));
        dp[2] = (b < 0) ? 0 : ((b >= 0xff0) ? 0xff : ((b + 8) >> 4));
        dp += 3;
    }
}

void PhusionApply_FixedW(uint8_t *dp, const uint8_t *p, const uint8_t *p1, uint32_t w,
                         const int32_t iwcoeffs[][PHUSION_MAX_PLANES])
{
    // Break the processing into chunks of a fixed size, allowing the compiler
    //    to parallelise the code using vector extensions such as AVX/NEON
    const int N = 32;
    int64_t r[N], g[N], b[N];
    const int32_t *cp;

    Phusion_Sampler reader;
    Phusion_Sampler writer;
    int16_t terms[Q][N];
    uint16_t out[Q-1][N];
    InitSampler(reader, terms[0], 0, Phusion_PlaneFlagU16, Phusion_PlaneRaster1, 0);
    InitSampler(writer, out, 0, Phusion_PlaneFlagU16, Phusion_PlaneRaster1, 0);

    while (w >= N)
    {
        int k = 0;
        int j = 0;
        int pix;

        w -= N;

        // Read inputs
        for(pix = 0; pix < N; pix++)
        {
            terms[0][pix] = p[pix*3+0];  // B
            terms[1][pix] = p[pix*3+1];  // G
            terms[2][pix] = p[pix*3+2];  // R
            terms[3][pix] = p1[pix];
        }
        p  += N * 3;
        p1 += N;

        // First linear term initialises results
        for(pix = 0; pix < N; pix++)
        {
            r[pix] = (int64_t)terms[0][pix] * iwcoeffs[0][0];  // R
            g[pix] = (int64_t)terms[0][pix] * iwcoeffs[0][1];  // G
            b[pix] = (int64_t)terms[0][pix] * iwcoeffs[0][2];  // B
        }
        cp = iwcoeffs[1];

        // Remaining linear terms require no product calculation or indexing
        for(int i = 1; i < Q; i++)
        {
            for(pix = 0; pix < N; pix++)
            {
                r[pix] += (int64_t)terms[i][pix] * cp[0];
                g[pix] += (int64_t)terms[i][pix] * cp[1];
                b[pix] += (int64_t)terms[i][pix] * cp[2];
            }
            cp += PHUSION_MAX_PLANES;
        }

        // Quadratic terms
        while (k < Q)
        {
            for(pix = 0; pix < N; pix++)
            {
                int32_t prod = terms[k][pix] * terms[j][pix];
                r[pix] += (int64_t)prod * cp[0];
                g[pix] += (int64_t)prod * cp[1];
                b[pix] += (int64_t)prod * cp[2];
            }
            cp += PHUSION_MAX_PLANES;

            if (++j >= Q)
                j = ++k;
        }

        // Clamp and write out final pixels
        for(pix = 0; pix < N; pix++)
        {
            dp[pix*3+0] = (r[pix] < 0) ? 0 : ((r[pix] >= 0xff0000000ll) ? 0xff : (uint8_t)((r[pix] + 0x8000000) >> 28));
            dp[pix*3+1] = (g[pix] < 0) ? 0 : ((g[pix] >= 0xff0000000ll) ? 0xff : (uint8_t)((g[pix] + 0x8000000) >> 28));
            dp[pix*3+2] = (b[pix] < 0) ? 0 : ((b[pix] >= 0xff0000000ll) ? 0xff : (uint8_t)((b[pix] + 0x8000000) >> 28));
        }
        dp += N*3;
    }

    // Tail code mops up the remaining pixels, processing one at a time
    while (w-- > 0)
    {
        const int32_t *cp;
        int32_t terms[Q];
        int64_t r, g, b;
        int k = 0;
        int j = 0;

        // Collect contrast samples
        terms[0] = p[0];   // B
        terms[1] = p[1];   // G
        terms[2] = p[2];   // R
        terms[3] = *p1++;  // NIR
        p += 3;

        // Compute output samples
        r = (int64_t)terms[0] * iwcoeffs[0][0];
        g = (int64_t)terms[0] * iwcoeffs[0][1];
        b = (int64_t)terms[0] * iwcoeffs[0][2];
        cp = iwcoeffs[1];

        // Remaining linear terms require no product calculation or indexing
        for(int i = 1; i < Q; i++)
        {
            r += (int64_t)terms[i] * cp[0];
            g += (int64_t)terms[i] * cp[1];
            b += (int64_t)terms[i] * cp[2];

            cp += PHUSION_MAX_PLANES;
        }

        // Quadratic terms
        while (k < Q)
        {
            int32_t prod = terms[k] * terms[j];
            if (++j >= Q)
                j = ++k;

            r += (int64_t)prod * cp[0];
            g += (int64_t)prod * cp[1];
            b += (int64_t)prod * cp[2];

            cp += PHUSION_MAX_PLANES;
        }

        // Clamp and write out final pixels
        dp[0] = (r < 0) ? 0 : ((r >= 0xff0000000ll) ? 0xff : (uint8_t)((r + 0x8000000) >> 28));
        dp[1] = (g < 0) ? 0 : ((g >= 0xff0000000ll) ? 0xff : (uint8_t)((g + 0x8000000) >> 28));
        dp[2] = (b < 0) ? 0 : ((b >= 0xff0000000ll) ? 0xff : (uint8_t)((b + 0x8000000) >> 28));
        dp += 3;
    }
}

void PhusionApply_Float(uint8_t *dp, const Phusion_Buffer *pInBuf, uint32_t w,
                        const phpoly_t coeffs[][PHUSION_MAX_PLANES])
{
    // Break the processing into chunks of a fixed size, allowing the compiler
    //    to parallelise the code using vector extensions such as AVX/NEON
    const int N = 32;
    phpoly_t r[N], g[N], b[N];
    const phpoly_t sc = (phpoly_t)255;
    phpoly_t terms[Q][N];
    const uint8_t *p  = (uint8_t*)pInBuf->planes[0].pData;
    const uint8_t *p1 = (uint8_t*)pInBuf->planes[1].pData;

    while (w >= N)
    {
        const phpoly_t *cp;
        int k = 0;
        int j = 0;
        int pix;

        w -= N;

        // Read inputs
        for(pix = 0; pix < N; pix++)
        {
            terms[0][pix] = p[pix*3+0]  / sc;
            terms[1][pix] = p[pix*3+1]  / sc;
            terms[2][pix] = p[pix*3+2]  / sc;
            terms[3][pix] = p1[pix] / sc;  // NIR
        }
        p  += N * 3;
        p1 += N;

        // First linear term initialises results
        for(pix = 0; pix < N; pix++)
        {
            r[pix] = terms[0][pix] * coeffs[0][0];  // R
            g[pix] = terms[0][pix] * coeffs[0][1];  // G
            b[pix] = terms[0][pix] * coeffs[0][2];  // B
        }
        cp = coeffs[1];

        // Remaining linear terms require no product calculation or indexing
        for(int i = 1; i < Q; i++)
        {
            for(pix = 0; pix < N; pix++)
            {
                r[pix] += terms[i][pix] * cp[0];
                g[pix] += terms[i][pix] * cp[1];
                b[pix] += terms[i][pix] * cp[2];
            }
            cp += PHUSION_MAX_PLANES;
        }

        // Quadratic terms
        while (k < Q)
        {
            for(pix = 0; pix < N; pix++)
            {
                phpoly_t prod = terms[k][pix] * terms[j][pix];
                r[pix] += prod * cp[0];
                g[pix] += prod * cp[1];
                b[pix] += prod * cp[2];
            }
            cp += PHUSION_MAX_PLANES;

            if (++j >= Q)
                j = ++k;
        }

        // Clamp and write out final pixels
        for(pix = 0; pix < N; pix++)
        {
            dp[pix*3+0] = (r[pix] < (phpoly_t)0) ? 0 : ((r[pix] >= (phpoly_t)1.0) ? 0xff : (uint8_t)(r[pix] * sc));
            dp[pix*3+1] = (g[pix] < (phpoly_t)0) ? 0 : ((g[pix] >= (phpoly_t)1.0) ? 0xff : (uint8_t)(g[pix] * sc));
            dp[pix*3+2] = (b[pix] < (phpoly_t)0) ? 0 : ((b[pix] >= (phpoly_t)1.0) ? 0xff : (uint8_t)(b[pix] * sc));
        }
        dp += N*3;
    }

    // Tail code implementation mops up the remaining pixels, processing one at a time
    while (w-- > 0)
    {
        const phpoly_t sc = (phpoly_t)255;
        const phpoly_t *cp;
        phpoly_t terms[Q];
        phpoly_t r, g, b;
        int k = 0;
        int j = 0;

        // Read inputs
        terms[0] = (phpoly_t)p[0]  / sc;  // R
        terms[1] = (phpoly_t)p[1]  / sc;  // G
        terms[2] = (phpoly_t)p[2]  / sc;  // B
        terms[3] = (phpoly_t)p1[0] / sc;  // NIR
        p += 3;
        p1++;

        // First linear term initialises results
        r = terms[0] * coeffs[0][0];  // R
        g = terms[0] * coeffs[0][1];  // G
        b = terms[0] * coeffs[0][2];  // B
        cp = coeffs[1];

        // Remaining linear terms require no product calculation or indexing
        for(int i = 1; i < Q; i++)
        {
            r += terms[i] * cp[0];
            g += terms[i] * cp[1];
            b += terms[i] * cp[2];
            cp += PHUSION_MAX_PLANES;
        }

        // Quadratic terms
        while (k < Q)
        {
            phpoly_t prod = terms[k] * terms[j];
            r += prod * cp[0];
            g += prod * cp[1];
            b += prod * cp[2];
            cp += PHUSION_MAX_PLANES;

            if (++j >= Q)
                j = ++k;
        }

        // Clamp and write out final pixels
        dp[0] = (r < (phpoly_t)0) ? 0 : ((r >= (phpoly_t)1.0) ? 0xff : (uint8_t)(r * sc));
        dp[1] = (g < (phpoly_t)0) ? 0 : ((g >= (phpoly_t)1.0) ? 0xff : (uint8_t)(g * sc));
        dp[2] = (b < (phpoly_t)0) ? 0 : ((b >= (phpoly_t)1.0) ? 0xff : (uint8_t)(b * sc));
        dp += 3;
    }
}

// Spatially-varying, multi-polynomial coefficient application
//
// TODO - see whether the bicubic kernel really offers any benefit at all over bilinear; then make a decision or provide a configuration switch?

void PhusionApply_Spatial(const Phusion_State *ph_state, Phusion_CPUThreadState *state, const Phusion_IOPUnit *pIOP,
                          const Phusion_Buffer &bufOut, const Phusion_Buffer *pBufWgt, const Phusion_Buffer &bufIn,
                          uint32_t wgtX, uint32_t wgtY, uint32_t numLines, uint32_t pixPerLine, unsigned nPoly,
                          volatile bool *pCancel)
{
    const unsigned nWts = pBufWgt ? ((unsigned)pBufWgt->nPlanes) : 0;  //planes[0].fmt & Phusion_PlaneNComps) : 0;
    bool bWeighted = (pIOP->pcFlags & Phusion_ProcUseWeights) != 0;
    const phpoly_t *pWt[PHUSION_MAX_POLYNOMIALS][PHUSION_UWFILT_PTS];
    Phusion_Sampler destState[PHUSION_MAX_CHANNELS];
    Phusion_Sampler srcState[PHUSION_MAX_CHANNELS];
    const unsigned inChan  = pIOP->imgContrast.nChannels;  // Contrast image supplies input channels
    const unsigned outChan = pIOP->imgOutput.nChannels;    // Output image determines number of outputs
    const unsigned nCoeffs = inChan + inChan*(inChan+1)/2;
#if PHUSION_SINGLE_PRECISION
    const Phusion_PlaneFlags flags = Phusion_PlaneFlagSF;
#else
    const Phusion_PlaneFlags flags = Phusion_PlaneFlagDF;
#endif
    const unsigned fBits = 12;
    const int onePixel = 1U << fBits;
    const int fMask = onePixel - 1;
    Phusion_Sampler readerState;
    Phusion_Sampler writerState;
    const bool bVerbose(true);
// TODO - vary this parameter and set appropriately for target
    const int N = 6;
// TODO - this is still 1KB of data, perhaps it too needs relocating?!
        phpoly_t sums[PHUSION_MAX_CHANNELS][N];

    PHUSION_ASSERT(!pBufWgt || (nWts > 0 && nWts <= PHUSION_MAX_POLYNOMIALS));

    // Initialise samplers for reading from contrast image and converting to internal type in thread-local storage
    for (unsigned ch = 0; ch < inChan; ch++)
        InitSampler(srcState[ch], bufIn, pIOP->imgContrast, ch);
    InitSampler(readerState, state->inSamples, sizeof(state->inSamples[0]), flags, Phusion_PlaneRaster1, 0);

    // Initialise samplers from converting from internal storage to output image
    for (unsigned ch = 0; ch < outChan; ch++)
        InitSampler(destState[ch], bufOut, pIOP->imgOutput, ch);
    InitSampler(writerState, sums, sizeof(sums[0]), flags, Phusion_PlaneRaster1, 0);

    /* TODO - weights are currently phpoly_t, planar; suspect we may want to read them instead using
              sampler code and perhaps even interpolate vertically on a per-plane basis with no knowledge
              of component count?
    */
    int32_t inY  = ((int32_t)wgtY >> Phusion_WgtScaleBits) << (fBits - Phusion_WgtFractBits);
    int32_t incY = (wgtY & Phusion_WgtScaleMask) >> (Phusion_WgtScaleBits - fBits);
    int32_t incX = (wgtX & Phusion_WgtScaleMask) >> (Phusion_WgtScaleBits - fBits);

    if (bVerbose)
    {
        printf("Apply_Spatial called for %ux%u pixels\n", pixPerLine, numLines);
        printf("  Input channel(s): %u Output %u Poly %u Coeffs %u\n", inChan, outChan, nPoly, nCoeffs);
        printf("  %x,%x,%x\n", inY, incX, incY);
    }

    // Outputs from upsampling filter
    phpoly_t filtOut[PHUSION_MAX_POLYNOMIALS][N];
    if (!bWeighted)
    {
        // If no weights supplied, all polynomials are weighted equally
        const phpoly_t weight = PHPOLY_ONE / nPoly;
        for (unsigned poly = 0; poly < nPoly; poly++)
            for (unsigned pix = 0; pix < N; pix++)
                filtOut[poly][pix] = weight;
    }

    bool bNewWeightLines(bWeighted);
    while (numLines-- > 0)
    {
        // Set up scanline pointers for weights inputs
#if PHUSION_PROTO_BILINEAR
        const unsigned filtPts = 2;
#else
        const unsigned filtPts = PHUSION_UWFILT_PTS;
#endif
        if (bNewWeightLines)
        {
            for (unsigned dy = 0; dy < filtPts; dy++)
            {
                int32_t iy = (int)dy + (inY >> fBits) - 1;
                if (iy < 0)
                    iy = 0;
                for (unsigned poly = 0; poly < nPoly; poly++)
                    pWt[poly][dy] = (phpoly_t*)((uint8_t*)pBufWgt->planes[poly].pData + (iy * pBufWgt->planes[poly].iStride));
            }
        }

        // Set up line pointers for sample inputs and outputs
        for (unsigned ch = 0; ch < inChan; ch++)
            srcState[ch].StartLine();
        for (unsigned ch = 0; ch < outChan; ch++)
            destState[ch].StartLine();

//      unsigned vEnd = PHUSION_CPU_MAX_VSUMS;
        unsigned vNext = 0;
        unsigned vIdx = 0;

        int32_t inX = ((int32_t)wgtX >> Phusion_WgtScaleBits) << (fBits - Phusion_WgtFractBits);
        int32_t ixNext = (inX & ~fMask) - onePixel;

        // Break the processing into chunks of a fixed size, allowing the compiler
        //    to parallelise the code using vector extensions such as AVX/NEON

        uint32_t w = pixPerLine;
        uint32_t ox = 0;
        while (w >= N)
        {
            // Read input samples from the contrast image
            for (unsigned ch = 0; ch < inChan; ch++)
            {
                readerState.InitLine(state->inSamples[ch]);
                ConvertSamples(readerState, srcState[ch], N);
//printf("Samples %f %f %f %f %f\n", state->inSamples[ch][0], state->inSamples[ch][1], state->inSamples[ch][2],
//state->inSamples[ch][3], state->inSamples[ch][4]);
            }

            if (bWeighted)
            {
                const phpoly_t normUW = (phpoly_t)(1 / 4096.0);  // TODO - our bicubic filter weights are currently s4.12
                unsigned idxPix[N];
                uint16_t fxPix[N];

                // Calculate for each 'pixel pipe' its new horizontal phase and starting index within the vertical sums
                for (unsigned pix = 0; pix < N; pix++)
                {
                    int32_t ix = inX + (pix * incX);
                    idxPix[pix] = vIdx + (ix >> fBits) - (inX >> fBits);
                    fxPix[pix] = ix & fMask;
                }

                // Do we need any more columns from the weights buffer?
                while (inX + (incX * (N-1)) + (PHUSION_UWFILT_PTS-2)*onePixel >= ixNext)
                {
                    // TODO - we need to ensure that we don't read too far to the right
                    unsigned fy = inY & fMask;

                    // Generate replicated left/right context when required
                    int32_t ix = ixNext >> fBits;
                    if (ix < 0)
                        ix = 0;
                    else if (ix >= (int)pixPerLine)
                        ix = pixPerLine - 1;

                    // Compute new vertical interpolations
                    // TODO - do we want to support different weight data formats at this point?
                    for (unsigned poly = 0; poly < nPoly; poly++)
                    {
                        // Initialise vertical sums
                        for (unsigned pix = 0; pix < N; pix++)
                            state->ws.vSums[poly][vNext+pix] = PHPOLY_ZERO;

                        // Accumulate contributions from each line of the filter kernel
                        for (unsigned dy = 0; dy < filtPts; dy++)
                            for (unsigned pix = 0; pix < N; pix++)
                                state->ws.vSums[poly][vNext+pix] += (ph_state->uwFilt[fy][dy] * normUW) * pWt[poly][dy][vNext+pix];
                    }

                    ixNext += N * onePixel;
                    vNext += N;
                }
/* TODO - we CANNOT have so many vertical sums needlessly stored in thread-local storage! */

                // Horizontal interpolation to produce the appropriate weights for each polynomial+pixel
                for (unsigned poly = 0; poly < nPoly; poly++)
                {
                    for (unsigned pix = 0; pix < N; pix++)
                        filtOut[poly][pix] = PHPOLY_ZERO;

                    for (unsigned dx = 0; dx < filtPts; dx++)
                        for (unsigned pix = 0; pix < N; pix++)
                        {
                            unsigned vIdx = idxPix[pix];
                            unsigned fx = fxPix[pix];
                            filtOut[poly][pix] += (ph_state->uwFilt[fx][dx] * normUW) * state->ws.vSums[poly][vIdx+dx];
                        }
                }
            }

            // Compute the set of polynomial coefficients from the weighted input polynomials
            for (unsigned c = 0; c < nCoeffs; c++)
                for (unsigned ch = 0; ch < outChan; ch++)
                {
                    for (unsigned pix = 0; pix < N; pix++)
                        state->svfCoeffs[c][ch][pix] = filtOut[0][pix] * state->mpfcoeffs[c][ch][0];

                    for (unsigned poly = 1; poly < nPoly; poly++)
                        for (unsigned pix = 0; pix < N; pix++)
                            state->svfCoeffs[c][ch][pix] += filtOut[poly][pix] * state->mpfcoeffs[c][ch][poly];
                }

            // ---------------------- Apply the coefficients -----------------------

            // First linear term initialises the results
            for (unsigned ch = 0; ch < outChan; ch++)
                for (unsigned pix = 0; pix < N; pix++)
                    sums[ch][pix] = state->inSamples[0][ox+pix] * state->svfCoeffs[0][ch][pix];

            // Remaining linear terms require no product calculation or indexing
            for (unsigned ch = 0; ch < outChan; ch++)
                for (unsigned in = 1; in < inChan; in++)
                    for (unsigned pix = 0; pix < N; pix++)
                        sums[ch][pix] += state->inSamples[in][ox+pix] * state->svfCoeffs[in][ch][pix];

            // Quadratic terms require the calculation of an intermediate product term
            unsigned k = 0;
            unsigned j = 0;
            for (unsigned c = inChan; c < nCoeffs; c++)
            {
                for (unsigned ch = 0; ch < outChan; ch++)
                    for (unsigned pix = 0; pix < N; pix++)
                    {
                        phpoly_t prod = state->inSamples[k][ox+pix] * state->inSamples[j][ox+pix];
                        sums[ch][pix] += prod * state->svfCoeffs[c][ch][pix];
//TEMP
//sums[ch][pix] = 1.5 * state->inSamples[0][ox+pix];
//sums[ch][pix] = filtOut[ch][pix];
                    }

                if (++j >= inChan)
                    j = ++k;
            }

            // ------------- Clamp the results and emit output pixels --------------
            for (unsigned ch = 0; ch < outChan; ch++)
            {
                writerState.InitLine(sums[ch]);
                ConvertSamples(destState[ch], writerState, N);
            }

            int32_t nextX = inX + N * incX;
            vIdx += (nextX >> fBits) - (inX >> fBits);
            inX = nextX;
// TODO - we do not bulk read atm
//          ox += N;
            w -= N;
        }

#if 0
// TODO - it would be useful for development purposes if we could use the tail code across the entire image
        if (w > 0)
        {
            // We can use the knowledge that we have less than a batch left to do...
            PHUSION_ASSERT(w < N);

            // Read input samples from the contrast image
            for (unsigned ch = 0; ch < inChan; ch++)
            {
                readerState.initLine(state->inSamples[ch]);
                ConvertSamples(renderState, srcState[ch], w);
            }

            if (bWeighted)
            {
            }

            // Horizontal interpolation to produce the appropriate weights for each remaining pixel
            for (unsigned
            {
// TODO - go straight to polynomial coefficients from vsums?

                // Compute the set of polynomial coefficients to be used for the weighted input polynomials
                for (unsigned c = 0; c < ; c++)
                    {
                    }
            }

            // Tail code, producing one pixel/iteration
            while (w-- > 0)
            {
                // First linear term initialises the results
                for (unsigned ch = 0; ch < outChan; ch++)
                    sums[ch] = ;

                // Remaining linear terms require no production calculation or indexing
                for (unsigned ch = 0; ch < outChan; ch++)
                    sums[ch][pix] += state->inSamples[in][ox+pix]

                // Quadratic terms require the calculation of an intermediate product term
                unsigned k = 0;
                unsigned j = 0;
                for (unsigned c = inChan; c < nCoeffs; c++)
                {
                    for (unsigned ch = 0; ch < outChan; ch++)
                        phpoly_t prod = state->inSamples[k][ox] * state->inSamples[j][ox];
                        sums[ch][0] += prod
                }


            }
#endif

//      printf("Done scanline, %u left\n", numLines);

        // Advance scanline pointers
        for (unsigned ch = 0; ch < inChan; ch++)
            srcState[ch].AdvanceLine();
        for (unsigned ch = 0; ch < outChan; ch++)
            destState[ch].AdvanceLine();

        int32_t nextY = inY + incY;
        bNewWeightLines = bWeighted && (nextY >> fBits) != (inY >> fBits);
        inY = nextY;
    }
}

#ifdef PHUSION_TEST_MODE
uint32_t test_tint(uint32_t pix, unsigned testMode)
{
    uint32_t b = (pix & 0xffU);
    uint32_t g = (pix & 0xff00U);
    uint32_t r = (pix & 0xff0000U);
    uint32_t a = (pix & 0xff000000U);

    if (testMode & 1) r += (r >> 1);
    if (testMode & 2) g += (g >> 1);
    if (testMode & 4) b += (b >> 1);

    return a | ((r >= 0x1000000U) ? 0xff0000U : (r & 0xff0000U))
             | ((g >= 0x10000U) ? 0xff00U : (g & 0xff00U))
             | ((b >= 0x100U) ? 0xffU : b);
}
#endif

// Filtering operations
void cpu_filtering(Phusion_State *ph_state, Phusion_CPUThreadState *state, const Phusion_IOPUnit *pIOP,
                   const Phusion_Buffer *pBufOut, const Phusion_Buffer *pBufIn,
                   uint32_t pixPerLine, uint32_t numLines, volatile bool *pCancel)
{
#if 0
    Phusion_Sampler destState[PHUSION_MAX_CHANNELS];
    Phusion_Sampler srcState[PHUSION_MAX_CHANNELS];
    unsigned destChan = pBufOut->nPlanes;
    unsigned srcChan = pBufIn->nPlanes;

    PHUSION_ASSERT(ph_state && state);

    profile_start_timing(&state->worker[PROF_CPU_FILTERING]);

    // Set up read and write samplers for this scanline
    for (unsigned ch = 0; ch < pBufIn->nChannels; ch++)
        InitSampler(srcState[ch],  );
    for (unsigned ch = 0; ch < pBufOut->nChannels; ch++)
        InitSampler(destState[ch], );

    PHUSION_ASSERT(pBufIn->nPlanes <= pBufOut->nPlanes);

    const uint32_t maxBlkWidth  = 0x100;  // TODO
    const uint32_t maxBlkHeight = 0x100;

    // Process each output plane in turn, filtering each of the components within the plane independently,
    //      but in an interleaved fashion to avoid the performance hit of performing multiple passes
    //      of images that may be too large to be accommodated within the caches. Oweing to the 2D nature
    //      of the convolution filtering operation, cacheing is likely to be very important.

    for (unsigned p = 0; p < pBufOut->nPlanes; p++)
    {
        uint32_t y = 0;

        // NOTE: We opt to read all the samples within this plane as if they constituted a single component
        //       because this makes the reading and conversion more efficient, and we need to access them
        //       in a vertical arrangement to attain maximum efficiency from the filter kernel itself

        InitSampler(srcState, srcPlane.pData, srcPlane.iStride, srcPlane.flags, Phusion_PlaneRaster1, 0);

        while (y < numLines && !*pCancel)
        {
            uint32_t blkHeight = min(numLines - y, maxBlkHeight);

            uint32_t x = 0;
            while (x < pixPerLine)
            {
                uint32_t blkWidth = min(pixPerLine - x, maxBlkWidth);

                for (uint32_t by = 0; by < blkHeight; by++)
                {
                    // Expressed to aid vectorising compilers
                    const unsigned N = 16;

                    for (uint32_t bx = 0; bx < blkWidth & ~(N-1); bx++)
                    {
                        // Do we need more input samples yet?
                        if ()
                        {
                            for (uint32_t dy = 0; dy < ; dy++)
                            {
                                // Read input samples for the new scanline
                                readerState.StartLine();
                                ConvertSamples(readerState, srcState, blkWidth * nComps, );
                                readerState.AdvanceLine();
                            }
                        }

                        // Calculate the contribution of the new vertical column to the kernel for each compoent
                        double sum[] = 0;
                        for (unsigned comp = 0; comp < nComps; comp++)
                        {
                            for (unsigned dy = 0; dy < kernHeight; dy++)
                                sum[comp] += pKernel[dy] * state->sample[][];
                        }

                        // Calculate the new output from the horizontal filter
                        for ()
                        {
                        }


                        // Write out samples
                        ConvertSamples();
                    }


                        double sum = 0;
                        for (comp = 0; comp < nComps; comp++)
                            sum += pKernel[dy] * readerState[comp].ReadPixel();



                    }
                    while (bx < blkWidth)
                    {
                        bx++;
                    }
                }

                x += blkWidth;
            }
            y += blkHeight;
        }
    }

    profile_stop_timing(&state->worker.profile[PROF_CPU_FILTERING]);
#endif
}

#endif  // PHUSION_USE_CPU
