/******************************************************************************/
/* Project:     Phusion Library                                               */
/* File:        phusion_defs.hpp                                              */
/* Description: Global definitions                                            */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __phusion_defs_hpp
#define __phusion_defs_hpp
#include <cassert>
#ifndef _MSC_VER
#include <cstdbool>
#endif
#include <cstdint>
#include <cstdio>

#ifndef NOF_ELEMENTS
#define NOF_ELEMENTS(x) (sizeof(x)/sizeof(*(x)))
#endif

#ifndef UNUSED
#define UNUSED(x) ((x)=(x))
#endif

#ifndef ABS
#define ABS(x) (((x)<0)?-(x):(x))
#endif

// Indirect our application-local assertions so that the behaviour may be changed
#ifndef PHUSION_ASSERT
#define PHUSION_ASSERT assert
#endif

#ifndef min
#define min(x,y) (((x)<(y))?(x):(y))
#endif
#ifndef max
#define max(x,y) (((y)<(x))?(x):(y))
#endif

// Define to non-zero for 32-bit target platforms
#ifndef PHUSION_32BIT
#ifdef _MSC_VER
#define PHUSION_32BIT 1
#else
#define PHUSION_32BIT 0
#endif
#endif

// Define to use floating-point types for Phusion arithmetic
//#define PHUSION_FLOATING_POINT
// Define to use double-precision types if using floating-point arithmetic
//#define PHUSION_DOUBLES

#ifndef PHUSION_FLOATING_POINT
#define PHUSION_PRECISION 12
#endif

// Define to zero to disable load-balancing, for diagnostics/fault-finding
#define PHUSION_LOAD_BALANCING 1

// Define to non-zero to enforce in-order completion of gradient calculations,
//    PUREly ofr diagnostics/fault-finding; this is detrimental to performance
#define PHUSION_CALCGRAD_ORDERING 0

// Define to non-zero to introduce logging/diagnostics
#define PHUSION_LOGGING 0

// Define to non-zero to introduce performance profiling
#define PHUSION_PROFILING 0

// Validate memory addresses passed into the Phusion Library by inducing
//    an access violation/segmentation fault at a defined point
#define PHUSION_VALIDATE_MEMORY 0

#endif
