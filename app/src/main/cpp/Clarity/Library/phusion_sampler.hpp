/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_sampler.hpp                                           */
/* Description: Phusion sample reading/writing/conversion                     */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2017. All Rights Reserved.                */
/******************************************************************************/

#ifndef __phusion_sampler_hpp
#define __phusion_sampler_hpp
#include <cmath>
#include <cstddef>
#include <cstring>
#ifndef __phusion_h
#include "phusion.h"
#endif
#ifndef __phusion_cpu_hpp
#include "phusion_cpu.hpp"
#endif
#ifndef __phusion_defs_hpp
#include "phusion_defs.hpp"
#endif

/* Much of the implementation of the sampling code is in the header file so that it may be inlined
   within the calling code, and optimised away for processing routines that expect a fixed format;
   this is important for the performance of full-image pixel-processing code.
*/

// State information for sample reader/writer
class Phusion_Sampler
{
public:
    uint8_t           *pRow;      // Pointer to first sample within current row
    uint8_t           *pData;     // Pointer to current sample
    int32_t            iStride;   // Inter-row address offset, bytes
    ptrdiff_t          skip;      // Horizontal distance between samples, bytes
    unsigned           elemSize;  // Element size, bytes
    Phusion_PlaneFlags flags;     // Describes sample format

    // Single global invocation to set up linear<->log space conversions
    static void         Initialise();

    void                InitLine(void *pLine) { pData = pRow = (uint8_t*)pLine; }
    void                StartLine()           { pData = pRow;    }
    void                AdvanceLine()         { pRow += iStride; }

    // Read in a single sample value, converted to the return type, and advance
    double              ReadSampleDF();
    int32_t             ReadSampleI32();

    // Write out a single sample value and advance
    void                WriteSampleDF(double);
    void                WriteSampleI32(int32_t);
    void                WriteSampleLogI32(int32_t);

    // Clear destination samples to zero
    inline void         ClearSamples(uint32_t n);

    // Convert source samples, applying optional gain and writing out to this destination
    inline void         ConvertSamples(Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);

    // Read source samples, applying optional gain, and sum into the destination
    inline void         SumSamples(Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);

    // Specialised sample format conversion functions Conv<dest><src> with optional gain parameter
    static void      ConvertU8U8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void     ConvertU8U16(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void     ConvertU8I32(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void      ConvertU8SF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void      ConvertU8DF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);

    static void     ConvertU16U8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void    ConvertU16U16(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void     ConvertU16SF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void     ConvertU16DF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);

    static void      ConvertSFU8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void     ConvertSFU16(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void      ConvertSFSF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void      ConvertSFDF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);

    static void      ConvertDFU8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void     ConvertDFU16(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void      ConvertDFSF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void      ConvertDFDF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);

    static void  ConvertU8LogI32(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);

    // Specialised sample summation functions Sum<dest><src> with optional gain parameter
    static void         SumU16U8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);

    static void         SumI32U8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void        SumI32U16(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void         SumI32SF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void         SumI32DF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);

    static void          SumSFU8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void         SumSFU16(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void          SumSFSF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void          SumSFDF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);

    static void          SumDFU8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void         SumDFU16(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void          SumDFSF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
    static void          SumDFDF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);

    static void      SumLogI32U8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
//  static void       SumLogSFU8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);
//  static void       SumLogDFU8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);

    // For use where no conversion/summation function is available/scheduled for implementation
    static void     NoConversion(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain = nullptr);

    // Linear<->Log-space conversions
    static int32_t      LogI32U8(uint8_t s) { PHUSION_ASSERT(bInitialised); return logU8[s]; }

private:
    static bool    bInitialised;

    // Conversion from U8 linear input to log space
    static int32_t logU8[0x100];
};

// Sample conversion function
typedef void (*Phusion_SamplerConvFunc)(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain);

// Sample summation function
typedef void (*Phusion_SamplerSumFunc)(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain);

#ifndef NDEBUG
// Alignment tests for the given plane properties
void CheckSampleAlignment(const void *pData, int32_t iStride, Phusion_PlaneFlags flags, Phusion_PlaneFormat fmt);
#endif

// Initialise a sample reader/writer
inline void InitSampler(Phusion_Sampler &st, void *pData, int32_t iStride,
                        Phusion_PlaneFlags flags, Phusion_PlaneFormat fmt,
                        unsigned compNumber)
{
#ifndef NDEBUG
    // Alignment and format tests
    CheckSampleAlignment(pData, iStride, flags, fmt);

    PHUSION_ASSERT(fmt == Phusion_PlaneRaster1 || fmt == Phusion_PlaneRaster2 ||
                   fmt == Phusion_PlaneRaster3 || fmt == Phusion_PlaneRaster4);
    PHUSION_ASSERT(compNumber < (unsigned)fmt);
#endif

    // Set up format-specific parameters for horizontal advancement
    switch (flags)
    {
        case Phusion_PlaneFlagI8:
        case Phusion_PlaneFlagU8:
            st.skip = (unsigned)fmt;
            st.elemSize = 1;
            break;

        case Phusion_PlaneFlagI16:
        case Phusion_PlaneFlagU16:
            st.elemSize = 2;
            st.skip = (unsigned)fmt << 1;
            break;

        case Phusion_PlaneFlagI32:
        case Phusion_PlaneFlagU32:
            st.elemSize = 4;
            st.skip = (unsigned)fmt << 2;
            break;

        case Phusion_PlaneFlagSF:
            st.elemSize = sizeof(float);
            st.skip = (unsigned)fmt * sizeof(float);
            break;

        default:
            PHUSION_ASSERT(flags == Phusion_PlaneFlagDF);
            st.elemSize = sizeof(double);
            st.skip = (unsigned)fmt * sizeof(double);
            break;
    }

    // Initialise pointers to the first sample on the first row, and remember other properties
    pData = (uint8_t*)pData + (compNumber * st.elemSize);

    st.pData   = (uint8_t*)pData;
    st.pRow    = (uint8_t*)pData;
    st.iStride = iStride;
    st.flags   = flags;
//  printf("InitSampler %p %p %d %u %ld %u\n", st.pData, st.pRow, st.iStride, st.elemSize, st.skip, st.flags);
}

// Initialise a sample reader/writer for the given component of a Phusion Buffer plane
inline void InitSampler(Phusion_Sampler &st, const Phusion_Plane &plane, unsigned compNumber)
{
    PHUSION_ASSERT(compNumber < (unsigned)plane.fmt);
    InitSampler(st, plane.pData, plane.iStride, plane.flags, plane.fmt, compNumber);
}

// Initialise a sample reader/writer for the given channel of a Phusion Image
inline void InitSampler(Phusion_Sampler &st, const Phusion_Buffer &buf,
                        const Phusion_ImageChannels &img, unsigned chanNumber)
{
    PHUSION_ASSERT(chanNumber < img.nChannels);
    const Phusion_Plane &plane = buf.planes[img.planeNumber[chanNumber]];
    unsigned compNumber = img.compNumber[chanNumber];
    InitSampler(st, plane.pData, plane.iStride, plane.flags, plane.fmt, compNumber);
}

// Clear destination samples to zero
inline void Phusion_Sampler::ClearSamples(uint32_t n)
{
    if ((flags & Phusion_PlaneFlagFloat) != (skip != 1))
    {
        const unsigned N = 16;
        ptrdiff_t wrSkip = skip;
        unsigned p;

        switch (flags)
        {
            case Phusion_PlaneFlagI8:
            case Phusion_PlaneFlagU8:
                for (p = 0; p < (n & ~(N-1)); p++)
                    pData[p * skip] = 0U;

                while (p < n)
                {
                    pData[p * skip] = 0U;
                    p++;
                }
                break;

            case Phusion_PlaneFlagI16:
            case Phusion_PlaneFlagU16:
                wrSkip >>= 1;

                for (p = 0; p < (n & ~(N-1)); p++)
                    ((uint16_t*)pData)[p * wrSkip] = 0U;

                while (p < n)
                {
                    ((uint16_t*)pData)[p * wrSkip] = 0U;
                    p++;
                }
                break;

            case Phusion_PlaneFlagI32:
            case Phusion_PlaneFlagU32:
                wrSkip >>= 2;

                for (p = 0; p < (n & ~(N-1)); p++)
                    ((uint32_t*)pData)[p * wrSkip] = 0U;

                while (p < n)
                {
                    ((uint32_t*)pData)[p * wrSkip] = 0U;
                    p++;
                }
                break;

            case Phusion_PlaneFlagSF:
                wrSkip /= sizeof(float);

                for (p = 0; p < (n & ~(N-1)); p++)
                    ((float*)pData)[p * wrSkip] = 0.0F;

                while (p < n)
                {
                    ((float*)pData)[p * wrSkip] = 0.0F;
                    p++;
                }
                break;

            default:
                PHUSION_ASSERT(flags == Phusion_PlaneFlagDF);
                wrSkip /= sizeof(double);

                for (p = 0; p < (n & ~(N-1)); p++)
                    ((double*)pData)[p * wrSkip] = 0.0;

                while (p < n)
                {
                    ((double*)pData)[p * wrSkip] = 0.0;
                    p++;
                }
                break;
        }
    }
    else
        memset(pData, 0, n * elemSize);

    pData += n * skip;
}

// Convert a run of samples from one format to another
inline void ConvertSamples(Phusion_Sampler &wrState, Phusion_Sampler &rdState,
                           uint32_t n, const phfloat_t *pGain = nullptr)
{
    if (rdState.flags == wrState.flags && !pGain)
    {
        size_t elemSize = (wrState.flags == Phusion_PlaneFlagDF ) ? sizeof(double) :
                         ((wrState.flags == Phusion_PlaneFlagSF ) ? sizeof(float)  :
                         ((wrState.flags == Phusion_PlaneFlagU16) ? 2 : 1));

        memmove(wrState.pData, rdState.pData, n * elemSize);

        // Advance pointers
        rdState.pData += n * elemSize;
        wrState.pData += n * elemSize;
    }
    else
    {
#if 0

        // TODO - should we have a short array of intermediates here to reduce the loop overhead?
        //        the fastest way to perform the conversion would surely be to have 12 format conversions
        uint16_t uSample(0);  // Placate compiler
        double dSample(0.0);  // Placate compiler

        while (n-- > 0)
        {
            // Read the input sample into the appropriate type (integral/floating-point)
            switch (rdState.flags)
            {
                case Phusion_PlaneFlagU8:  uSample = (*rdState.pData << 8) | *rdState.pData; break;
                case Phusion_PlaneFlagU16: uSample = *(uint16_t*)rdState.pData; break;
                case Phusion_PlaneFlagSF:  dSample = *(float*)rdState.pData; break;
                default:                   dSample = *((double*)rdState.pData); break;
            }

            // Horizontal advance
            rdState.pData += rdState.skip;

// TODO - use gain values!
            switch (rdState.flags)
            {
                // Conversion from unsigned integer to destination format
                case Phusion_PlaneFlagU8:
                case Phusion_PlaneFlagU16:
                    switch (wrState.flags)
                    {
                        case Phusion_PlaneFlagU8:  *wrState.pData = uSample >> 8; break;
                        case Phusion_PlaneFlagU16: *(uint16_t*)wrState.pData = uSample; break;
                        case Phusion_PlaneFlagSF:  *(float*)wrState.pData  = (float)uSample / 65535.0F; break;
                        default:                   *(double*)wrState.pData = (double)uSample / 65535.0; break;
                    }
                    break;

                // Conversion from floating-point to destination format
                case Phusion_PlaneFlagSF:
                default:
                    switch (wrState.flags)
                    {
                        case Phusion_PlaneFlagU8:
                            *wrState.pData = (dSample >= 1.0) ? 0xffU :
                                            ((dSample <  0.0) ? 0x00U :
                                    (uint8_t)(dSample * 255.0 + 0.5));
                            break;
                        case Phusion_PlaneFlagU16:
                            *(uint16_t*)wrState.pData = (dSample >= 1.0) ? 0xffffU :
                                                       ((dSample <  0.0) ? 0x0000U :
                                              (uint16_t)(dSample * 65535.0));
                            break;

                        // TODO: Decide whether we should be introducing (optional) clipping here
                        case Phusion_PlaneFlagSF:
                            *(float*)wrState.pData = (float)dSample; break;
                        default:
                            *(double*)wrState.pData = dSample;
                            break;
                    }
                    break;
            }
            // Horizontal advance
            wrState.pData += wrState.skip;
        }
#else
        static const Phusion_SamplerConvFunc convFunctions[][4] =
        {
            { Phusion_Sampler::ConvertU8U8,  Phusion_Sampler::ConvertU8U16,  Phusion_Sampler::ConvertU8SF,  Phusion_Sampler::ConvertU8DF  },
            { Phusion_Sampler::ConvertU16U8, Phusion_Sampler::ConvertU16U16, Phusion_Sampler::ConvertU16SF, Phusion_Sampler::ConvertU16DF },
            { Phusion_Sampler::ConvertSFU8,  Phusion_Sampler::ConvertSFU16,  Phusion_Sampler::ConvertSFSF,  Phusion_Sampler::ConvertSFDF  },
            { Phusion_Sampler::ConvertDFU8,  Phusion_Sampler::ConvertDFU16,  Phusion_Sampler::ConvertDFSF,  Phusion_Sampler::ConvertDFDF  }
        };
        Phusion_SamplerConvFunc convFunc;
//      void (*convFunc)(Phusion_Sampler &wrState, Phusion_Sampler &src, uint32_t n, pGain);

        // Determine conversion function to be used
        PHUSION_ASSERT(rdState.flags < NOF_ELEMENTS(convFunctions[0]));
        PHUSION_ASSERT(wrState.flags < NOF_ELEMENTS(convFunctions));
        convFunc = convFunctions[wrState.flags][rdState.flags];

        // Invoke conversion function
        convFunc(wrState, rdState, n, pGain);

        // Advance read and write pointers
        rdState.pData += n * rdState.skip;
        wrState.pData += n * wrState.skip;
#endif
    }
}

// Convert a run of optionally-weighted samples from one format to another
inline void Phusion_Sampler::ConvertSamples(Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    if (flags == rd.flags && skip == elemSize && !pGain)
    {
        memmove(pData, rd.pData, n * elemSize);

        // Advance pointers
        rd.pData += n * elemSize;
        pData    += n * elemSize;
    }
    else
    {
        static const Phusion_SamplerConvFunc convFunctions[][5] =
        {
            { Phusion_Sampler::ConvertU8U8,  Phusion_Sampler::ConvertU8U16,  Phusion_Sampler::ConvertU8SF,  Phusion_Sampler::ConvertU8DF,  Phusion_Sampler::ConvertU8I32 },
            { Phusion_Sampler::ConvertU16U8, Phusion_Sampler::ConvertU16U16, Phusion_Sampler::ConvertU16SF, Phusion_Sampler::ConvertU16DF, Phusion_Sampler::NoConversion },
            { Phusion_Sampler::ConvertSFU8,  Phusion_Sampler::ConvertSFU16,  Phusion_Sampler::ConvertSFSF,  Phusion_Sampler::ConvertSFDF,  Phusion_Sampler::NoConversion },
            { Phusion_Sampler::ConvertDFU8,  Phusion_Sampler::ConvertDFU16,  Phusion_Sampler::ConvertDFSF,  Phusion_Sampler::ConvertDFDF,  Phusion_Sampler::NoConversion },
        };
        Phusion_PlaneFlags rdFlags = (rd.flags == Phusion_PlaneFlagI32) ? Phusion_PlaneFlags(4) : rd.flags;
        Phusion_SamplerConvFunc convFunc;

        // Determine conversion function to be used
        PHUSION_ASSERT(rdFlags < NOF_ELEMENTS(convFunctions[0]));
        PHUSION_ASSERT(flags   < NOF_ELEMENTS(convFunctions));
        convFunc = convFunctions[flags][rdFlags];

        // Invoke conversion function
        convFunc(*this, rd, n, pGain);

        // Advance read and write pointers
        rd.pData += n * rd.skip;
        pData    += n * skip;
    }
}

// Sum a run of optionally-weighted samples into running totals; intended for vertical filtering operations.
// NOTE: Only a subset of destination types are supported.
inline void Phusion_Sampler::SumSamples(Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    if (flags == Phusion_PlaneFlagU16)
    {
        PHUSION_ASSERT(rd.flags == Phusion_PlaneFlagU8);
        SumU16U8(*this, rd, n, pGain);
    }
    else
    {
        static const Phusion_SamplerSumFunc sumFunctions[][4] =
        {
            { Phusion_Sampler::SumSFU8,  Phusion_Sampler::SumSFU16,  Phusion_Sampler::SumSFSF,  Phusion_Sampler::SumSFDF  },
            { Phusion_Sampler::SumDFU8,  Phusion_Sampler::SumDFU16,  Phusion_Sampler::SumDFSF,  Phusion_Sampler::SumDFDF  },
            { Phusion_Sampler::SumI32U8, Phusion_Sampler::SumI32U16, Phusion_Sampler::SumI32SF, Phusion_Sampler::SumI32DF },
        };
        Phusion_PlaneFlags wrFlags = Phusion_PlaneFlags((flags == Phusion_PlaneFlagI32) ? 2 : (flags == Phusion_PlaneFlagDF));
        Phusion_SamplerSumFunc sumFunc;

        // Determine summation function to be used
        PHUSION_ASSERT(flags == Phusion_PlaneFlagSF || flags == Phusion_PlaneFlagDF || flags == Phusion_PlaneFlagI32);
        PHUSION_ASSERT(rd.flags < NOF_ELEMENTS(sumFunctions[0]));
        sumFunc = sumFunctions[wrFlags][rd.flags];

        // Invoke summation function
        sumFunc(*this, rd, n, pGain);

        // Advance read and write pointers
        rd.pData += n * rd.skip;
        pData    += n * skip;
    }
}

inline void Phusion_Sampler::WriteSampleI32(int32_t s)
{
    switch (flags)
    {
        case Phusion_PlaneFlagU8:
        {
            // Round-to-nearest
            s = (s < 0) ? 0U : ((s >= 0xFF80) ? 0xFFU : ((s + 0x80) >> 8));
            pData[0] = (uint8_t)s;
            pData += skip;
        }
        break;

        default:
            PHUSION_ASSERT(!!"Sample writing not yet implemented");
            break;
    }
}

inline void Phusion_Sampler::WriteSampleLogI32(int32_t s)
{
    switch (flags)
    {
        case Phusion_PlaneFlagU8:
        {
            const double normFact = 1.0 / (double)(1 << 26);
            // Round-to-nearest
            s = (int32_t)(0.5 + 255.0 * exp((double)s * normFact));
            pData[0] = (s >= 0x100 ? 0xFFU : (uint8_t)s);
            pData += skip;
        }
        break;

        default:
            PHUSION_ASSERT(!!"Sample writing not yet implemented");
            break;
    }
}

// Conversion from source image/buffer to destination image/buffer with
//    sample format and/or colour space conversions
void ConvertStripe(Phusion_CPUThreadState     *pState,
                   const Phusion_Buffer       *pDestBuf,
                   const Phusion_Buffer       *pSrcBuf,
                   const Phusion_ImageChannels &destIm,
                   const Phusion_ImageChannels &srcIm,
                   uint32_t                     numLines,
                   uint32_t                     pixPerLine,
                   const Phusion_Conversion   *pConversion,
                   const double               *pGain,
                   Phusion_ConversionFlags      flags,
                   volatile bool              *pCancel);

#endif

