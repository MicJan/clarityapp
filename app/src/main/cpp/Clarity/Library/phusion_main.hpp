/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_main.hpp                                              */
/* Description: Main interface code of the Phusion library                    */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __phusion_main_hpp
#define __phusion_main_hpp
#ifndef __phusion_worker_hpp
#include "phusion_worker.hpp"
#endif

// Check the validity of a state handle, returning access or NULL
Phusion_State *ValidStateHandle(Phusion_Handle ph);

//---------------------------------------------------------------------------------------------
// Copy a set of polynomial coefficients from source to destination
//---------------------------------------------------------------------------------------------
void CopyPolynomial(Phusion_Polynomial *d, const Phusion_Polynomial *s);

//---------------------------------------------------------------------------------------------
// Queue an IOP for subsequent processing
//---------------------------------------------------------------------------------------------
Phusion_Result QueueIOP(Phusion_State *ph_state, Phusion_IOPUnit *pIOP, WorkerAction action,
                        uint32_t outNumLines, uint32_t wgtX, uint32_t wgtY, uint32_t inNumLines,
                        const Phusion_Buffer *pBufOut, const Phusion_Buffer *pBufWgt,
                        const Phusion_Buffer *pBufIn);

#endif
