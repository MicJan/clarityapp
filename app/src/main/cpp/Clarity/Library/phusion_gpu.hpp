/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_gpu.hpp                                               */
/* Description: GPU-based implementation of the Phusion processing            */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __phusion_gpu_hpp
#define __phusion_gpu_hpp
#ifndef __phusion_h
#include "phusion.h"
#endif

#ifdef PHUSION_USE_GPU
// GPU-related state information
typedef struct
{
    Phusion_ThreadState state;
} Phusion_GPUState;

// Initialise GPU-based processing
Phusion_Result phusion_gpu_init(Phusion_State *ph_state);

// Finalise GPU-based processing
Phusion_Result phusion_gpu_fin(Phusion_State *ph_state);
#endif

#endif
