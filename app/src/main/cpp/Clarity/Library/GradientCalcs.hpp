/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        GradientCalcs.hpp                                             */
/* Description: Calculation of gradient space polynomials, and construction   */
/*              and solution of linear system                                 */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __GradientCalcs_hpp
#define __GradientCalcs_hpp
#include "phusion.h"
#ifndef __Bitmap_hpp
#include "Bitmap.hpp"
#endif
#ifndef __Toolkit_Types_hpp
#include "Types.hpp"
#endif

// Calcuation of gradient space polynomials, and construction and solution of linear system
void GradientCalcs(Phusion_Polynomial poly[], const Bitmap &smCont, const Bitmap &smGuide, const Bitmap &xtGuide,
                   const Phusion_Buffer &wtBuf, uint32_t numLines, uint32_t pixPerLine,
                   phfloat_t multip2, phfloat_t regLambda, phfloat_t tikLambda, phfloat_t extLambda);

#endif
