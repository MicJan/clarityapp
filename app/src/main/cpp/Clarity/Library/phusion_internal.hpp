/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_internal.hpp                                          */
/* Description: Phusion library internals (suject to change;                  */
/*              NOT to be included directly by client code)                   */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __phusion_internal_hpp
#define __phusion_internal_hpp
#ifndef __phusion_h
#include "phusion.h"
#endif
// Basic types used within the Phusion_State structure
#ifndef __phusion_types_hpp
#include "phusion_types.hpp"
#endif

// Configuration-dependent processing backends
#ifdef PHUSION_USE_CPU
#include "phusion_cpu.hpp"
#endif
#ifdef PHUSION_USE_GPU
#include "phusion_gpu.hpp"
#endif
#ifdef PHUSION_USE_HW
#include "phusion_hw.hpp"
#endif

// TODO - Temporarily define the number of input and output channels for the Phusion processing
#define PHUSION_Q 4
#define PHUSION_P (PHUSION_Q-1)
#define PHUSION_PCHAN (PHUSION_Q+(PHUSION_Q*(PHUSION_Q+1))/2)

// Version and revision of the Phusion library itself
// - Major changes of function
#define PHUSION_MAJOR_VERSION  0
// - Minor changes of function
#define PHUSION_MINOR_VERSION  1
// - Bugfixes and perfective/non-functional changes
#define PHUSION_REVISION       0

#if defined(PHUSION_USE_CPU) && !defined(PHUSION_CPU_CONFIG_DEFINED)
#ifdef PHUSION_TARGET_ARM
// Define to use the NEON multimedia coprocessors, if present
#define PHUSION_USE_NEON
#else
// Define to use the SSE/AVX multimedia SIMD extensions, if present
#define PHUSION_USE_SSE
#endif

// ... Options relating to other CPU implementations

#if !defined(WIN32) && !defined(__ANDROID__)
// Define to use non-portable pthreads extensions for setting thread CPU affinity
#define PHUSION_PTHREAD_BINDING
#endif
#endif

// Number of workers performing processing
//    (= threads + OpenCL + hardware, if everything present and usable)
#define PHUSION_MAX_WORKERS  (PHUSION_CPU_MAX_THREADS + 2)

// Maximum number of concurrent IOP units
#define PHUSION_MAX_IOPS 0x10

// Number of work rate entries contributing to rolling average
#define PHUSION_WORK_RATE_ENTRIES 0x10

// Minimum size of work unit to be allocated to a worker thread
#define PHUSION_MIN_WORK_UNIT 0x2000

// Default resampling factor when not specified explicitly
#define PHUSION_DEFAULT_RS_FACTOR 5

// Weight upsampling filter
#define PHUSION_UWFILT_PTS   4
#define PHUSION_UWFILT_WBITS 12

// Description of current workload and performance of a worker thread
typedef struct
{
    uint32_t      totalLoad;   // Current total work load (pixels queued but not completed)
    uint32_t      avWorkRate;  // Average work rate (pixels/ms)

    // Rolling average of work rates
    uint8_t       rateIdx;
    uint32_t      workRates[PHUSION_WORK_RATE_ENTRIES];
} Phusion_WorkerState;

// Description of a unit of work performed by a thread
//   (The properties of the operation that are common to all worker threads
//    remain in the parent structure 'Phusion_IOPUnit')
typedef struct
{
    bool           bCompleted;
    // Number of input/output lines
    uint32_t        numLines;
    // Number of input lines available for use (includes context)
    uint32_t        availLines;
    // Number of resampled output lines to be processed
    uint32_t        rsNumLines;
    // Upsampling of weights for spatially-varying pixel processing
    uint32_t        wgtX;
    uint32_t        wgtY;
    // Input, weights and output buffers with worker-specific data pointers
    Phusion_Buffer  bufOut;
    Phusion_Buffer  bufWgt;
    Phusion_Buffer  bufIn;
} Phusion_WorkUnit;

// Overlapped processing operation
struct Phusion_IOPUnit
{
    // IOP structure has already been allocated
    //   (being completed or processing in-progress)
    bool                    bAllocated;

    // Validity of IOP structure, indicating whether workers should consult it
    bool                    bValid;

    // IOP cancellation request
    bool                    bCancel;

    // Complete input and output buffers for the operation
    Phusion_Buffer          bufIn;
    Phusion_Buffer          bufOut;

    // TODO - now that we have more processing operations, there's call for an union here

    // Properties common to all workers contributing to this IOP
    Phusion_ImageChannels   imgContrast;    // Description of constrast image input
    Phusion_ImageChannels   imgGuide;       // Description of guide image input
    Phusion_ImageChannels   imgRsContrast;  // Description of resampled constrast image in/out
    Phusion_ImageChannels   imgRsGuide;     // Description of resampled guide image in/out
    Phusion_ImageChannels   imgOutput;      // Description of output from pixel-processing
    uint32_t                rsPixPerLine;   // Width (pixels) of resampled in/out
    uint32_t                pixPerLine;
    uint32_t                cfgSeq;
    Phusion_Polynomial     *grPolyResult;   // Buffer to receive polynomial coefficients
    Phusion_Polynomial      poly[PHUSION_MAX_POLYNOMIALS];
    Phusion_LinSysCont      linSysCont;
    Phusion_FilterKernel    filtKernel;
    phfloat_t               scContGains[PHUSION_MAX_CHANNELS];
    phfloat_t               scGuideGains[PHUSION_MAX_CHANNELS];
    double                  convGain[PHUSION_MAX_CHANNELS];
    Phusion_Conversion      conversion[PHUSION_MAX_CHANNELS];
    Phusion_GradientConfig  grConfig;
    Phusion_ConversionFlags convFlags;
    Phusion_FilterFlags     filtFlags;
    Phusion_ResampleFlags   rsFlags;
    Phusion_GradientFlags   grFlags;
    Phusion_ProcessFlags    pcFlags;

    // Bitmap of WorkActions to be performed on this IOP (see phusion_worker.hpp)
    uint32_t                workFlags;
    WorkerAction            workAction;

    // Count of the number of incomplete work units for this IOP
    unsigned                nUnits;

    // Details specific to each worker
    Phusion_WorkUnit        unit[PHUSION_MAX_WORKERS];
};

// Internal state of library instance
struct Phusion_State
{
    // Current configuration settings
    Phusion_Config         config;

    // Current state
    bool                   bInitialised;
    bool                   bFinalising;

    // Requested polynomial coefficients
    uint32_t              reqPolyChanged;
    Phusion_Polynomial    reqPoly[PHUSION_MAX_POLYNOMIALS];

    // Current polynomial coefficients
    Phusion_Polynomial    currPoly[PHUSION_MAX_POLYNOMIALS];

    uint32_t              currentCfgSeq;  // Sequence number of current configuration

    unsigned              nextThreadNum;

    // First candidate thread for handling any excess load; workers are
    //   assigned additional load round-robin to prevent bias
    unsigned              firstCand;

    // Monitoring of current workloads and worker performance
    pthread_mutex_t       workMutex;
    Phusion_WorkerState   workLoad[PHUSION_MAX_WORKERS];

    // Pending/in-progress IOPs and communication with workers
    pthread_mutex_t       iopMutex;   // Controls acecss to all IOP units
    pthread_cond_t        waitCond;   // Waiting for completion
    pthread_cond_t        workCond;   // Waiting for work
    unsigned              oldestIOP;  // Round-robin allocation within the IOP list
    Phusion_IOPUnit       IOPs[PHUSION_MAX_IOPS];

    // Filter weights for bicubic upsampling filter kernel
    int16_t               uwFilt[1U << PHUSION_UWFILT_WBITS][PHUSION_UWFILT_PTS];

    // Local information for each include processing backend
#ifdef PHUSION_USE_CPU
    Phusion_CPUState      cpu;
#endif
#ifdef PHUSION_USE_GPU
    Phusion_GPUState      gpu;
#endif
#ifdef PHUSION_USE_HW
    Phusion_HWState       hw;
#endif
};

#endif
