/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_cpu_resample.cpp                                      */
/* Description: CPU-based implementation of the Phusion resampling stage      */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#include "phusion_cpu_resample.hpp"
#include "phusion_defs.hpp"
#include "phusion_utils.hpp"

#ifdef PHUSION_USE_CPU

// Container for additional parameters passed to the more involved filter routines
struct ResampleFilter_State
{
    // Sample formats of destination and source buffers
    Phusion_PlaneFlags dstFlags;
    Phusion_PlaneFlags srcFlags;

    // Normalisation factors for box filter kernel
    uint32_t    normFactU[2][2];
    float      normFactSF[2][2];
    double     normFactDF[2][2];

    float    normFactSFU8[2][2];
    double   normFactDFU8[2][2];
    float   normFactSFU16[2][2];
    double  normFactDFU16[2][2];

    double   normFactU8DF[2][2];
    double  normFactU16DF[2][2];
};

// Resampling filter function
typedef void (*ResampleFunc)(uint8_t *, const uint8_t *, int32_t,
                             unsigned, unsigned, const uint32_t *, unsigned,
                             const ResampleFilter_State &state);

// Nearest-neighbour resampling functions
static void ResampleNearest_CopyU8(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                                   unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                                   const ResampleFilter_State &state);
static void ResampleNearest_CopyU16(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                                    unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                                    const ResampleFilter_State &state);
static void ResampleNearest_CopySF(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                                   unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                                   const ResampleFilter_State &state);
static void ResampleNearest_CopyDF(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                                   unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                                   const ResampleFilter_State &state);
static void ResampleNearest_Convert(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                                    unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                                    const ResampleFilter_State &state);

// Box filter resampling functions
static void ResampleBox_CopyU8(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                               unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                               const ResampleFilter_State &state);
static void ResampleBox_CopyU16(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                                unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                                const ResampleFilter_State &state);
static void ResampleBox_CopySF(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                               unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                               const ResampleFilter_State &state);
static void ResampleBox_CopyDF(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                               unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                               const ResampleFilter_State &state);
static void ResampleBox_Convert(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                                unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                                const ResampleFilter_State &state);

// Nearest-neighbour resampling functions
static const ResampleFunc nnFuncs[] =
{
    ResampleNearest_CopyU8,
    ResampleNearest_CopyU16,
    ResampleNearest_CopySF,
    ResampleNearest_CopyDF,
    ResampleNearest_Convert
};

// Box-filter resampling functions
static const ResampleFunc boxFuncs[] =
{
    ResampleBox_CopyU8,
    ResampleBox_CopyU16,
    ResampleBox_CopySF,
    ResampleBox_CopyDF,
    ResampleBox_Convert
};

#ifdef __GNUC__
// Compiler's data flow analysis is not smart enough and this pixel-processing code needs
//     to avoid the performance penalty of needless initialisations.
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif

// ---------------------------------------- Nearest-neighbour resampling functions ------------------------------------

// Nearest-neighbour copying of interleaved 8-bit samples
void ResampleNearest_CopyU8(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                            unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                            const ResampleFilter_State &state)
{
    while (w-- > 0)
    {
        const uint8_t *sp = (uint8_t*)(pSrcRow + *pHOff++);
        for (unsigned c = 0; c < nc; c++)
            pDestRow[c] = sp[c];
        pDestRow += nc;
    }
}

// Nearest-neighbour copying of interleaved 16-bit samples
void ResampleNearest_CopyU16(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                             unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                             const ResampleFilter_State &state)
{
    uint16_t *dp = (uint16_t*)pDestRow;
    while (w-- > 0)
    {
        const uint16_t *sp = (uint16_t*)(pSrcRow + *pHOff++);
        for (unsigned c = 0; c < nc; c++)
            dp[c] = sp[c];
        dp += nc;
    }
}

// Nearest-neighbour copying of interleaved single-fp samples
void ResampleNearest_CopySF(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                            unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                            const ResampleFilter_State &state)
{
    float *dp = (float*)pDestRow;
    while (w-- > 0)
    {
        const float *sp = (float*)(pSrcRow + *pHOff++);
        for (unsigned c = 0; c < nc; c++)
        {
            float f = sp[c];
            dp[c] = (f >= 1.0F) ? 1.0F : ((f < 0.0F) ? 0.0F : f);
        }
        dp += nc;
    }
}

// Nearest-neighbour copying of interleaved double-fp samples
void ResampleNearest_CopyDF(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                            unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                            const ResampleFilter_State &state)
{
    double *dp = (double*)pDestRow;
    while (w-- > 0)
    {
        const double *sp = (double*)(pSrcRow + *pHOff++);
        for (unsigned c = 0; c < nc; c++)
        {
            double d = sp[c];
            dp[c] = (d >= 1.0) ? 1.0 : ((d < 0.0) ? 0.0 : d);
        }
        dp += nc;
    }
}

// Nearest-neighbour resampling to interleaved unsigned samples
void ResampleNearest_Convert(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                             unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                             const ResampleFilter_State &state)
{
    uint8_t *dp = pDestRow;

    // This generic routine should be used only when conversion is required
    PHUSION_ASSERT(state.dstFlags != state.srcFlags);

    while (w-- > 0)
    {
        const uint8_t *sp = pSrcRow + *pHOff++;
        uint16_t sU[PHUSION_MAX_CHANNELS];
        double sD[PHUSION_MAX_CHANNELS];

        // Read source samples and convert to unsigned 16-bit integers
        switch (state.srcFlags)
        {
            case Phusion_PlaneFlagU16:
                for (unsigned c = 0; c < nc; c++)
                    sU[c] = ((uint16_t*)sp)[c];
                break;

            case Phusion_PlaneFlagSF:
                for (unsigned c = 0; c < nc; c++)
                {
                    float f = ((float*)sp)[c];
                    sD[c] = (f >= 1.0F) ? 1.0 : ((f < 0.0F) ? 0.0 : (double)f);
                }
                break;

            case Phusion_PlaneFlagDF:
                for (unsigned c = 0; c < nc; c++)
                {
                    double d = ((double*)sp)[c];
                    sD[c] = (d >= 1.0) ? 1.0 : ((d < 0.0) ? 0.0 : d);
                }
                break;

            default:
                PHUSION_ASSERT(state.srcFlags == Phusion_PlaneFlagU8);
                for (unsigned c = 0; c < nc; c++)
                    sU[c] = sp[c];
                break;
        }

        // Scale input samples as the point of output
        switch (state.dstFlags)
        {
            case Phusion_PlaneFlagU16:
                switch (state.srcFlags)
                {
                    case Phusion_PlaneFlagSF:
                    case Phusion_PlaneFlagDF:
                        for (unsigned c = 0; c < nc; c++)
                            ((uint16_t*)dp)[c] = (uint16_t)(65535.0 * sD[c]);
                        break;
                    default:
                        for (unsigned c = 0; c < nc; c++)
                            ((uint16_t*)dp)[c] = (sU[c] << 8) | sU[c];
                        break;
                }
                dp += nc << 1;
                break;
            case Phusion_PlaneFlagSF:
                switch (state.srcFlags)
                {
                    case Phusion_PlaneFlagU16:
                        for (unsigned c = 0; c < nc; c++)
                            ((float*)dp)[c] = (float)sU[c] / 65535.0F;
                        break;
                    case Phusion_PlaneFlagDF:
                        for (unsigned c = 0; c < nc; c++)
                            ((float*)dp)[c] = (float)sU[c];
                        break;
                    default:
                        for (unsigned c = 0; c < nc; c++)
                            ((float*)dp)[c] = (float)sU[c] / 255.0F;
                        break;
                }
                dp += nc * sizeof(float);
                break;
            case Phusion_PlaneFlagDF:
                switch (state.srcFlags)
                {
                    case Phusion_PlaneFlagU16:
                        break;
                    case Phusion_PlaneFlagSF:
                        for (unsigned c = 0; c < nc; c++)
                            ((double*)dp)[c] = sD[c];
                        break;
                    default:
                        for (unsigned c = 0; c < nc; c++)
                            ((double*)dp)[c] = (double)sU[c] / 255.0;
                        break;
                }
                dp += nc * sizeof(double);
                break;
            default:
                PHUSION_ASSERT(state.dstFlags == Phusion_PlaneFlagU8);
                switch (state.srcFlags)
                {
                    case Phusion_PlaneFlagSF:
                    case Phusion_PlaneFlagDF:
                        for (unsigned c = 0; c < nc; c++)
                            dp[c] = (uint8_t)(255.0 * sD[c]);
                        break;
                    default:
                        for (unsigned c = 0; c < nc; c++)
                            dp[c] = sU[c] >> 8;
                        break;
                }
                dp += nc;
                break;
        }
    }
}

// ------------------------------------------- Box-filter resampling functions ----------------------------------------

// Box-filter resampling of interleaved 8-bit samples
void ResampleBox_CopyU8(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                        unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                        const ResampleFilter_State &state)
{
    while (w-- > 0)
    {
        const uint8_t *sp = pSrcRow + pHOff[0];
        uint32_t sums[PHUSION_MAX_CHANNELS];
        unsigned dw = pHOff[1] - pHOff[0];

        for (unsigned c = 0; c < nc; c++)
            sums[c] = 0;

        for (unsigned dy = 0; dy < dh; dy++)
        {
            const uint8_t *nextRow = sp + srcStride;
            const uint8_t *spEnd = sp + dw;
            while (sp < spEnd)
            {
                for (unsigned c = 0; c < nc; c++)
                    sums[c] += sp[c];
                sp += nc;
            }
            sp = nextRow;
        }

        for (unsigned c = 0; c < nc; c++)
            pDestRow[c] = ((uint64_t)sums[c] * state.normFactU[dh&1][dw&1]) >> 28;
        pDestRow += nc;

        pHOff++;
    }
}

// Box-filter resampling of interleaved 16-bit samples
void ResampleBox_CopyU16(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                         unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                         const ResampleFilter_State &state)
{
    uint16_t *dp = (uint16_t*)pDestRow;
    while (w-- > 0)
    {
        const uint8_t *sp = pSrcRow + pHOff[0];
        uint16_t sums[PHUSION_MAX_CHANNELS];
        unsigned dw = pHOff[1] - pHOff[0];

        for (unsigned c = 0; c < nc; c++)
            sums[c] = 0;

        for (unsigned dy = 0; dy < dh; dy++)
        {
            const uint8_t *nextRow = sp + srcStride;
            const uint8_t *spEnd = sp + dw;
            while (sp < spEnd)
            {
                for (unsigned c = 0; c < nc; c++)
                    sums[c] += ((uint16_t*)sp)[c];
                sp += nc << 1;
            }
            sp = nextRow;
        }

        for (unsigned c = 0; c < nc; c++)
            dp[c] = ((uint64_t)sums[c] * state.normFactU[dh&1][dw&1]) >> 28;
        dp += nc;

        pHOff++;
    }
}

// Box-filter resampling of interleaved single-fp samples
void ResampleBox_CopySF(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                        unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                        const ResampleFilter_State &state)
{
    float *dp = (float*)pDestRow;
    while (w-- > 0)
    {
        const uint8_t *sp = pSrcRow + pHOff[0];
        float sums[PHUSION_MAX_CHANNELS];
        unsigned dw = pHOff[1] - pHOff[0];

        for (unsigned c = 0; c < nc; c++)
            sums[c] = 0.0F;

        for (unsigned dy = 0; dy < dh; dy++)
        {
            const uint8_t *nextRow = sp + srcStride;
            const uint8_t *spEnd = sp + dw;
            while (sp < spEnd)
            {
                for (unsigned c = 0; c < nc; c++)
                {
                    float f = ((float*)sp)[c];
                    sums[c] += (f >= 1.0F) ? 1.0F : ((f < 0.0F) ? 0.0F : f);
                }
                sp += nc * sizeof(float);
            }
            sp = nextRow;
        }

        for (unsigned c = 0; c < nc; c++)
            dp[c] = sums[c] * state.normFactSF[dh&1][dw&1];
        dp += nc;

        pHOff++;
    }
}

// Box-filter resampling of interleaved double-fp samples
void ResampleBox_CopyDF(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                        unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                        const ResampleFilter_State &state)
{
    double *dp = (double*)pDestRow;
    while (w-- > 0)
    {
        const uint8_t *sp = pSrcRow + pHOff[0];
        double sums[PHUSION_MAX_CHANNELS];
        unsigned dw = pHOff[1] - pHOff[0];

        for (unsigned c = 0; c < nc; c++)
            sums[c] = 0.0;

        for (unsigned dy = 0; dy < dh; dy++)
        {
            const uint8_t *nextRow = sp + srcStride;
            const uint8_t *spEnd = sp + dw;
            while (sp < spEnd)
            {
                for (unsigned c = 0; c < nc; c++)
                {
                    double d = ((double*)sp)[c];
                    sums[c] += (d >= 1.0) ? 1.0 : ((d < 0.0) ? 0.0 : d);
                }
                sp += nc * sizeof(double);
            }
            sp = nextRow;
        }

        for (unsigned c = 0; c < nc; c++)
            dp[c] = sums[c] * state.normFactDF[dh&1][dw&1];
        dp += nc;

        pHOff++;
    }
}

// Box-filter resampling with sample type conversion
void ResampleBox_Convert(uint8_t *pDestRow, const uint8_t *pSrcRow, int32_t srcStride,
                         unsigned dh, unsigned w, const uint32_t *pHOff, unsigned nc,
                         const ResampleFilter_State &state)
{
    uint8_t *dp = pDestRow;

    // This generic routine should be used only when conversion is required
    PHUSION_ASSERT(state.dstFlags != state.srcFlags);

    while (w-- > 0)
    {
        const uint8_t *sp = pSrcRow + pHOff[0];
        uint16_t sumsU[PHUSION_MAX_CHANNELS];
        double sumsD[PHUSION_MAX_CHANNELS];
        unsigned dw = pHOff[1] - pHOff[0];

        // Initialise per-component sums for current output
        if (state.srcFlags & Phusion_PlaneFlagFloat)
        {
            for (unsigned c = 0; c < nc; c++)
                sumsD[c] = 0;
        }
        else
        {
            for (unsigned c = 0; c < nc; c++)
                sumsU[c] = 0;
        }

        // Calculate sums using the input type (integral/fp)
        for (unsigned dy = 0; dy < dh; dy++)
        {
            const uint8_t *nextRow = sp + srcStride;
            const uint8_t *spEnd = sp + dw;
            switch (state.srcFlags)
            {
                case Phusion_PlaneFlagU16:
                    while (sp < spEnd)
                    {
                        for (unsigned c = 0; c < nc; c++)
                            sumsU[c] += ((uint16_t*)sp)[c];
                        sp += nc << 1;
                    }
                    break;
                case Phusion_PlaneFlagSF:
                    while (sp < spEnd)
                    {
                        for (unsigned c = 0; c < nc; c++)
                        {
                            float f = ((float*)sp)[c];
                            sumsD[c] += (f > 1.0F) ? 1.0 : ((f < 0.0F) ? 0.0 : (double)f);
                        }
                        sp += nc * sizeof(float);
                    }
                    break;
                case Phusion_PlaneFlagDF:
                    while (sp < spEnd)
                    {
                        for (unsigned c = 0; c < nc; c++)
                        {
                            double d = ((double*)sp)[c];
                            sumsD[c] += (d > 1.0) ? 1.0 : ((d < 0.0) ? 0.0 : d);
                        }
                        sp += nc * sizeof(double);
                    }
                    break;
                default:
                    PHUSION_ASSERT(state.srcFlags == Phusion_PlaneFlagU8);
                    while (sp < spEnd)
                    {
                        for (unsigned c = 0; c < nc; c++)
                            sumsU[c] += sp[c];
                        sp += nc;
                    }
                    break;
            }
            sp = nextRow;
        }

        // Scale accumulated input samples once per-output rather than once per-input,
        //    whilst emitting output components for the current pixel
        //
        // TODO - consider selecting an output function based upon dstFlags? This is the ugliness
        //        that phusion_sampler was intended to address
        switch (state.dstFlags)
        {
            case Phusion_PlaneFlagU16:
                switch (state.srcFlags)
                {
                    case Phusion_PlaneFlagSF:
                    case Phusion_PlaneFlagDF:
                        for (unsigned c = 0; c < nc; c++)
                            ((uint16_t*)dp)[c] = (uint16_t)(sumsD[c] * state.normFactU16DF[dh&1][dw&1]);
                        break;
                    default:
                        for (unsigned c = 0; c < nc; c++)
                            dp[c] = ((uint16_t)(sumsU[c] + (sumsU[c] << 8)) * state.normFactU[dh&1][dw&1]) >> 28;
                        break;
                }
                dp += nc << 1;
                break;

            case Phusion_PlaneFlagSF:
                switch (state.srcFlags)
                {
                    case Phusion_PlaneFlagU16:
                        for (unsigned c = 0; c < nc; c++)
                            ((float*)dp)[c] = (float)sumsU[c] * state.normFactSFU16[dh&1][dw&1];
                        break;
                    case Phusion_PlaneFlagDF:
                        for (unsigned c = 0; c < nc; c++)
                            ((float*)dp)[c] = (float)sumsD[c] * state.normFactSF[dh&1][dw&1];
                        break;
                    default:
                        for (unsigned c = 0; c < nc; c++)
                            ((float*)dp)[c] = (float)sumsU[c] * state.normFactSFU8[dh&1][dw&1];
                        break;
                }
                dp += nc * sizeof(float);
                break;

            case Phusion_PlaneFlagDF:
                switch (state.srcFlags)
                {
                    case Phusion_PlaneFlagU16:
                        for (unsigned c = 0; c < nc; c++)
                            ((double*)dp)[c] = (double)sumsU[c] * state.normFactDFU16[dh&1][dw&1];
                        break;
                    case Phusion_PlaneFlagSF:
                        for (unsigned c = 0; c < nc; c++)
                            ((double*)dp)[c] = sumsD[c] * state.normFactDF[dh&1][dw&1];
                        break;
                    default:
                        for (unsigned c = 0; c < nc; c++)
                            ((double*)dp)[c] = (double)sumsU[c] * state.normFactDFU8[dh&1][dw&1];
                        break;
                }
                dp += nc * sizeof(double);
                break;

            default:
                PHUSION_ASSERT(state.dstFlags == Phusion_PlaneFlagU8);
                switch (state.srcFlags)
                {
                    case Phusion_PlaneFlagSF:
                    case Phusion_PlaneFlagDF:
                        for (unsigned c = 0; c < nc; c++)
                            dp[c] = (uint8_t)(sumsD[c] * state.normFactU8DF[dh&1][dw&1]);
                        break;
                    default:
                        for (unsigned c = 0; c < nc; c++)
                        {
                            const uint64_t round = (uint64_t)1U << 35;
                            dp[c] = (uint8_t)(((uint64_t)sumsU[c] * state.normFactU[dh&1][dw&1] + round) >> 36);
                        }
                        break;
                }
                dp += nc;
                break;
        }
    }
}

//=====================================================================================================================
// Resampling of input buffers to produce downsampled input for gradient calculations
//
//      Resampling is performed by pre-calculating the column and row offsets from output:input
//      to avoid repeated calculations during the pixel summing/selection. Statically-sized arrays
//      are used to store these column/row offsets, and thus resampling is performed in blocks
//      to avoid imposing restrictions upon the maximum dimensions of the output image.
//
//      The two resampling kernels currently available (nearest-neighbour and box-filter) are intended
//      only for downsampling operations, although the former may be employed (rather inefficiently)
//      for upsampling also.
//=====================================================================================================================

void cpu_resampling(Phusion_State *ph_state, Phusion_CPUThreadState *state, const Phusion_IOPUnit *pIOP,
                    const Phusion_Buffer *pBufRs, const Phusion_Buffer *pBufIn,
                    uint32_t rsPixPerLine, uint32_t rsNumLines,
                    uint32_t pixPerLine, uint32_t numLines,
                    Phusion_ResampleFlags rsFlags,
                    volatile bool *pCancel)
{
    profile_start_timing(&state->worker.profile[PROF_CPU_RESAMPLING]);

    // Compatibility of the buffers should have been ensured at the point of issue
    PHUSION_ASSERT(pBufRs->nPlanes <= pBufIn->nPlanes);

    printf("CPU thread %u resampling %ux%u to %ux%u\n", state->worker.threadNumber, pixPerLine, numLines, rsPixPerLine, rsNumLines);
    // Compute scale factors
    for(unsigned p = 0; p < pBufRs->nPlanes && !*pCancel; p++)
    {
        uint8_t *pColStart = (uint8_t*)pBufRs->planes[p].pData;
        Phusion_PlaneFormat  dstFmt = pBufRs->planes[p].fmt;
        Phusion_PlaneFormat  srcFmt = pBufIn->planes[p].fmt;
        ResampleFilter_State filtState;
        uint32_t pixLeft = rsPixPerLine;
        uint32_t colStart = 0;
        unsigned nc = 1;

        filtState.dstFlags = pBufRs->planes[p].flags;
        filtState.srcFlags = pBufIn->planes[p].flags;

        // Compatibility of the planes should have been ensured at the point of issue
        PHUSION_ASSERT(srcFmt == dstFmt);

        // Determine the component count from the buffer format
        nc = (unsigned)(pBufRs->planes[p].fmt & Phusion_PlaneNComps);
        PHUSION_ASSERT(nc > 0 && nc <= 8);  // Reasonable value

        // Scale factors
        unsigned hFact = pixPerLine / rsPixPerLine;
        unsigned vFact = numLines / rsNumLines;

        // Select filter type and set up any required information
        bool bCopy(filtState.dstFlags == filtState.srcFlags);
        ResampleFunc filterFunc;
        switch (rsFlags & 0xf)
        {
            case Phusion_ResBox:
                filterFunc = boxFuncs[bCopy ? (filtState.dstFlags & 3) : 4];

                // We store 2x2 normalisation factors for the 4 cases of even/odd filter kernel size in each dimension
                //    (in general the resampling factor is non-integral, so the two possible width/heights are
                //     ceil(fact) and floor(fact), one of which will be an even-valued size, and the other odd)
                for (unsigned fv = 0; fv < 2; fv++)
                    for (unsigned fh = 0; fh < 2; fh++)
                    {
                        unsigned area = (hFact + fh) * (vFact + fv);
                        unsigned y = (vFact ^ fv) & 1;
                        unsigned x = (hFact ^ fh) & 1;
                        // Normalisation factors for straight copy operations are always required;
                        //     those for conversion are only required in the more complex cases
                        switch (filtState.dstFlags)
                        {
                            case Phusion_PlaneFlagSF:
                                filtState.normFactSF[y][x] = 1.0F / (float)area;
                                if (!bCopy)
                                {
                                    filtState.normFactSFU8[y][x]  = filtState.normFactSF[y][x] / 255.0F;
                                    filtState.normFactSFU16[y][x] = filtState.normFactSF[y][x] / 65535.0F;
                                }
                                break;
                            case Phusion_PlaneFlagDF:
                                filtState.normFactDF[y][x] = 1.0 / (double)area;
                                if (!bCopy)
                                {
                                    filtState.normFactDFU8[y][x]  = filtState.normFactDF[y][x] / 255.0;
                                    filtState.normFactDFU16[y][x] = filtState.normFactDF[y][x] / 65535.0;
                                }
                                break;
                            default:
                                filtState.normFactU[y][x] = ((1U << 28) + area/2) / area;
                                if (!bCopy)
                                {
                                    filtState.normFactU8DF[y][x]  =   255.0 * filtState.normFactDF[y][x];
                                    filtState.normFactU16DF[y][x] = 65535.0 * filtState.normFactDF[y][x];
                                }
                                break;
                        }
                    }
                break;
            default:
                PHUSION_ASSERT(!"Invalid or unsupported resampling filter type");
                // no break
            case Phusion_ResNearest:
                filterFunc = nnFuncs[bCopy ? (filtState.dstFlags & 3) : 4];
                break;
        }

        // Determine the number of bytes per source pixel
        unsigned srcPixBytes;
        switch (filtState.srcFlags)
        {
            case Phusion_PlaneFlagU16: srcPixBytes = 2; break;
            case Phusion_PlaneFlagSF:  srcPixBytes = sizeof(float); break;
            case Phusion_PlaneFlagDF:  srcPixBytes = sizeof(double); break;
            default:
                PHUSION_ASSERT(filtState.srcFlags == Phusion_PlaneFlagU8);
                srcPixBytes = 1;
                break;
        }
        switch (srcFmt)
        {
            case Phusion_PlaneRaster2: srcPixBytes *= 2; break;
            case Phusion_PlaneRaster3: srcPixBytes *= 3; break;
            case Phusion_PlaneRaster4: srcPixBytes *= 4; break;
            default:
                PHUSION_ASSERT(srcFmt == Phusion_PlaneRaster1);
                break;
        }

        // Because we have a statically-sized array of offsets into the scanline
        //    we work across the output buffer in columns
        while (pixLeft > 0)
        {
            unsigned colWidth = min(pixLeft, NOF_ELEMENTS(state->hOffsets) - 1);
//          unsigned srcColStart;
            unsigned y = 0;

            pixLeft -= colWidth;

            // TODO - this can be done incrementally but also note that colStart/Width are well-defined values
/*
            srcColStart = (colStart * pixPerLine) / rsPixPerLine;
            switch (srcFlags)
            {
                case Phusion_PlaneFlagU16: srcColStart <<= 1; break;
                case Phusion_PlaneFlagSF:  srcColStart *= sizeof(float); break;
                default: PHUSION_ASSERT(srcFmt == Phusion_PlaneFlagDF); break;
            }
            switch (srcFmt)
            {
                case Phusion_FormatRaster2: srcColStart *= 2; break;
                case Phusion_FormatRaster3: srcColStart *= 3; break;
                case Phusion_FormatRaster4: srcColStart *= 4; break;
                default: PHUSION_ASSERT(srcFmt == Phusion_Raster1); break;
            }
*/

            // Can we avoid (re-)calculating the column offsets?
            //    (This check is chiefly intended to avoid re-calculation for
            //     each stripe of a sufficiently-narrow source image)
            // TODO - is this a complete test?
            if (!state->bGotHOffsets || (pixPerLine         != state->offCfg.pixPerLine)
                                     || (rsPixPerLine       != state->offCfg.rsPixPerLine)
                                     || (srcFmt             != state->offCfg.srcFmt)
                                     || (filtState.srcFlags != state->offCfg.srcFlags))
            {
                // Compute horizontal offsets into each of the source scanlines
                // NOTE: including x == colWidth terminates the list, which is required by the
                //       box filter kernel to determine the kernel width for the final output
                for(unsigned x = 0; x <= colWidth; x++)
                {
                    // Determine the corresponding location in the input buffer
                    // TODO - temporary; do this incrementally
                    unsigned hOffset = ((colStart + x) * pixPerLine) / rsPixPerLine;

                    // Ensure that the source ordinate is valid
                    PHUSION_ASSERT(hOffset <= pixPerLine);

                    // Translate this into an address offset given sample count and type
                    state->hOffsets[x] = hOffset * srcPixBytes;
                }

                if (rsPixPerLine <= NOF_ELEMENTS(state->hOffsets))  // Single column?
                {
                    // Remember the configuration for which we have computed the column offsets
                    //    so that we may safely decide whether to reuse them in future
                    state->offCfg.rsPixPerLine = rsPixPerLine;
                    state->offCfg.pixPerLine = pixPerLine;
                    state->offCfg.srcFlags = filtState.srcFlags;
                    state->offCfg.srcFmt = srcFmt;
                    state->bGotHOffsets = true;
                }
            }

            // TODO
            uint8_t *pDestRow = pColStart;
            pColStart += colWidth * srcPixBytes;
            colStart += colWidth;

            // NOTE: we don't pre-calculate vertical offsets at the moment, although
            //       doing so may be beneficial for very wide images

            unsigned srcRow = (y * numLines) / rsNumLines;
            unsigned h = rsNumLines;

            while (!*pCancel && h-- > 0)
            {
                // Determine leftmost pixel of this column within the given source scanline
                const uint8_t *pSrcRow;
                unsigned w = colWidth;
                unsigned x = 0;

                // TODO - temporary; do this incrementally...then lose y
                unsigned nextRow = ((y + 1) * numLines) / rsNumLines;
                unsigned dh = nextRow - srcRow;

                pSrcRow = (uint8_t*)pBufIn->planes[p].pData + (srcRow * pBufIn->planes[p].iStride);

                // Invoke the filter kernel for this horizontal run of output pixels
                filterFunc(pDestRow, pSrcRow, pBufIn->planes[p].iStride, dh, w, &state->hOffsets[x], nc, filtState);
                srcRow = nextRow;

                // Next scanline within resampled output
                pDestRow += pBufRs->planes[p].iStride;
                y++;
            }
        }
    }

    profile_stop_timing(&state->worker.profile[PROF_CPU_RESAMPLING]);
}

#endif
