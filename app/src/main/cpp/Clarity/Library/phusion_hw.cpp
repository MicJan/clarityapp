/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_hw.cpp                                                */
/* Description: Hardware-based implementation of the Phusion processing       */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#include <stdio.h>

#include "phusion_hw.hpp"

#ifdef PHUSION_USE_HW
// TODO - Placeholder for actual implementation code, using dedicated hardware

// Initialise HW-based processing
Phusion_Result phusion_hw_init(Phusion_State *ph_state)
{
    fprintf(stderr, "ERROR: HW initialisation not implemented\n");
    return Phusion_NotImplemented;
}

// Finalise HW-based processing
Phusion_Result phusion_hw_fin(Phusion_State *ph_state)
{
    fprintf(stderr, "ERROR: HW finalisation not implemented\n");
    return Phusion_NotImplemented;
}

#endif
