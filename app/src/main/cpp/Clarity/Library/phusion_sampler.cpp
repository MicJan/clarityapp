/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_sampler.hpp                                           */
/* Description: Phusion sample reading/writing/conversion                     */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2017. All Rights Reserved.                */
/******************************************************************************/

#include "phusion_sampler.hpp"

const unsigned N = 16;

bool    Phusion_Sampler::bInitialised(false);
int32_t Phusion_Sampler::logU8[0x100];

#ifndef NDEBUG
// Alignment tests for the given plane properties
void CheckSampleAlignment(const void *pData, int32_t iStride, Phusion_PlaneFlags flags, Phusion_PlaneFormat fmt)
{
    uint32_t alignMask;

    switch (flags)
    {
        case Phusion_PlaneFlagI8:
        case Phusion_PlaneFlagU8:
            alignMask = 0;
            break;
        case Phusion_PlaneFlagI16:
        case Phusion_PlaneFlagU16:
            alignMask = 1;
            break;
        case Phusion_PlaneFlagI32:
        case Phusion_PlaneFlagU32:
            alignMask = 3;
            break;
        case Phusion_PlaneFlagSF:
            alignMask = sizeof(float) - 1;
            PHUSION_ASSERT(!(sizeof(float) & alignMask));
            break;
        default:
            alignMask = sizeof(double) - 1;
            PHUSION_ASSERT(!(sizeof(double) & alignMask));
            PHUSION_ASSERT(flags == Phusion_PlaneFlagDF);
            break;
    }

    // TODO - resolve this; it's the writer state of Apply_Spatial, so local variables
//    PHUSION_ASSERT(!((uintptr_t)pData & alignMask));
    PHUSION_ASSERT(!((uint32_t)iStride & alignMask));
}
#endif

// Conversion from source image/buffer to destination image/buffer with
//    sample format and/or colour space conversions
void ConvertStripe(Phusion_CPUThreadState     *pState,
                   const Phusion_Buffer       *pDestBuf,
                   const Phusion_Buffer       *pSrcBuf,
                   const Phusion_ImageChannels &destIm,
                   const Phusion_ImageChannels &srcIm,
                   uint32_t                     numLines,
                   uint32_t                     pixPerLine,
                   const Phusion_Conversion   *pConversion,
                   const double               *pGains,
                   Phusion_ConversionFlags      flags,
                   volatile bool              *pCancel)
{
    // Properties of source and destination channels
    Phusion_Sampler destState[PHUSION_MAX_CHANNELS];
    Phusion_Sampler srcState[PHUSION_MAX_CHANNELS];
    unsigned destChans = destIm.nChannels;
    unsigned srcChans = srcIm.nChannels;
    Phusion_Sampler rwState;

    PHUSION_ASSERT(srcChans >= destChans);
    PHUSION_ASSERT(srcChans <= PHUSION_MAX_CHANNELS);
    PHUSION_ASSERT(srcChans > 0);

    // Initialise samplers for writing into and reading from our local storage
    //    (used for those operations that cannot be performed as a single copy operation)
    InitSampler(rwState, pState->convData, sizeof(pState->convData[0]), Phusion_PlaneFlagDF, Phusion_PlaneRaster1, 0);

    // We use a single sampler for reading input data and writing out results (same sample format)
    PHUSION_ASSERT(sizeof(pState->convData[0]) == sizeof(pState->convResults[0]));

    // Initialiser sample readers and writers
    for (unsigned ch = 0; ch < destChans; ch++)
    {
        unsigned p = srcIm.planeNumber[ch];
        unsigned c = srcIm.compNumber[ch];
        PHUSION_ASSERT(p < pSrcBuf->nPlanes && pSrcBuf->nPlanes <= PHUSION_MAX_PLANES);
        PHUSION_ASSERT(c < (unsigned)pSrcBuf->planes[p].fmt);

        // Initialise sample reader
        const Phusion_Plane &srcPlane = pSrcBuf->planes[p];
        InitSampler(srcState[ch],  srcPlane.pData,  srcPlane.iStride,  srcPlane.flags,  srcPlane.fmt,  c);

        p = destIm.planeNumber[ch];
        c = destIm.compNumber[ch];
        PHUSION_ASSERT(p < pDestBuf->nPlanes && pDestBuf->nPlanes <= PHUSION_MAX_PLANES);
        PHUSION_ASSERT(c < (unsigned)pDestBuf->planes[p].fmt);

        // Initialise sample writer
        const Phusion_Plane &destPlane = pDestBuf->planes[p];
        InitSampler(destState[ch], destPlane.pData, destPlane.iStride, destPlane.flags, destPlane.fmt, c);
    }

    for (uint32_t y = 0; y < numLines; y++)
    {
        // In-situ conversion requires us to operate on chunks of pixels and perform two independent copy operations
        if (!pDestBuf || (pConversion && (flags & Phusion_ConvMatrix)))
        {
            uint32_t x = 0;

            while (x < pixPerLine)
            {
                uint32_t chunkPix = min(pixPerLine - x, PHUSION_CPU_CONV_CHUNK);

                // Read input samples
                rwState.InitLine(pState->convData[0]);
                for (unsigned in = 0; in < srcChans; in++)
                {
                    rwState.StartLine();

// TODO - this code needs to convert the gains if we leave it with compile-time dependent type!!!
                    // ConvertSamples(rwState, srcState[in], chunkPix, pGains);
                    rwState.AdvanceLine();
                }

                if (pConversion && (flags & Phusion_ConvMatrix))
                {
                    for (unsigned ch = 0; ch < destChans; ch++)
                    {
                        // Apply matrix to input samples for each output sample, replacing the first input
                        //     (Expressed to aid vectorising compilers)

                        unsigned dx;
                        for (dx = 0; dx < (chunkPix & ~15); dx++)
                        {
                            double sum = pConversion[ch].offset + (pState->convData[0][dx] * pConversion[ch].matrix[0]);
                            for (unsigned in = 1; in < srcChans; in++)
                                sum += pState->convData[in][dx] * pConversion[ch].matrix[in];

                            pState->convResults[ch][dx] = (sum >= PHFLOAT_ONE) ? PHFLOAT_ONE : ((sum < PHFLOAT_ZERO) ? PHFLOAT_ZERO : sum);
                        }

                        // Tail pixels
                        while (dx < chunkPix)
                        {
                            double sum = pConversion[ch].offset + (pState->convData[0][dx] * pConversion[ch].matrix[0]);
                            for (unsigned in = 1; in < srcChans; in++)
                                sum += pState->convData[in][dx] * pConversion[ch].matrix[in];

                            pState->convResults[ch][dx] = (sum >= PHFLOAT_ONE) ? PHFLOAT_ONE : ((sum < PHFLOAT_ZERO) ? PHFLOAT_ZERO : sum);
                        }
                    }
                }

                // Write output samples
                rwState.InitLine(pState->convResults[0]);
                for (unsigned ch = 0; ch < destChans; ch++)
                {
                    rwState.StartLine();
                    ConvertSamples(destState[ch], rwState, chunkPix, nullptr);
                    rwState.AdvanceLine();
                }

                x += chunkPix;
            }
        }
        else
        {

            // Destination and source buffers are disjoint, and there's no matrix arithmetic required...
            //    each channel can be processed in isolation to achieve the requested format conversion
            for (unsigned ch = 0; ch < destIm.nChannels; ch++)  // Each output channel
            {
                // Initialise sample pointers
                destState[ch].pData = destState[ch].pRow;
                srcState[ch].pData  = srcState[ch].pRow;

// TODO - see above!!!
//                ConvertSamples(destState[ch], srcState[ch], pixPerLine, pGains);

                // Advance vertically
                destState[ch].pRow += destState[ch].iStride;
                srcState[ch].pRow  += srcState[ch].iStride;
            }
        }
    }
}

// Single global invocation performed before any of the other Phusion_Sampler member functions may be used;
//    set up linear<->log conversions
void Phusion_Sampler::Initialise()
{
    // Conversion from U8 input to log space is pretty trivial
    if (!bInitialised)
    {
//        for (unsigned p = 0; p < 0x100; p++)
//            results[p] = (uint32_t)(0x120000 + (200000 * log((double)p / 255.0)));
        const double scale = (double)(1 << 26);

        // The MATLAB code employs a 'safelog' function which avoids log(0); so we mimic this here
        logU8[0] = (int32_t)(-13.815510557964274 * scale);

        for (unsigned p = 1; p < 0x100; p++)
            logU8[p] = (int32_t)(scale * log((double)p / 255.0));
//printf("Log curve:\n");
//for (unsigned p = 0; p < 0x100; p++)
//    printf("%lf\n", (double)results[p] / (double)(1<<26));
    }

    bInitialised = true;
}

void Phusion_Sampler::ConvertU8U8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    if (pGain)
    {
        // Vectorisable block conversion
        uint32_t gain = (uint32_t)(*pGain * (phfloat_t)(1U << 24));
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            wr.pData[p * wr.skip] = (rd.pData[p * rd.skip] * gain + (1U << 23)) >> 24;

        // Tail code
        while (p < n)
        {
            wr.pData[p * wr.skip] = (rd.pData[p * rd.skip] * gain + (1U << 23)) >> 24;
            p++;
        }
    }
    else
    {
        // Vectorisable block conversion
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            wr.pData[p * wr.skip] = rd.pData[p * rd.skip];

        // Tail code
        while (p < n)
        {
            wr.pData[p * wr.skip] = rd.pData[p * rd.skip];
            p++;
        }
    }
}

void Phusion_Sampler::ConvertU8U16(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t skip = rd.skip >> 1;

    if (pGain)
    {
        // Vectorisable block conversion
        uint32_t gain = (uint32_t)(*pGain * (phfloat_t)(1U << 8));
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
        {
            int32_t s = ((uint16_t*)rd.pData)[p * skip] * gain + (1U << 15);
            wr.pData[p * wr.skip] = (s < 0) ? 0 : ((s >= 0x1000000) ? 0xff : (uint8_t)(s >> 16));
        }

        // Tail code
        while (p < n)
        {
            int32_t s = ((uint16_t*)rd.pData)[p * skip] * gain + (1U << 15);
            wr.pData[p * wr.skip] = (s < 0) ? 0 : ((s >= 0x1000000) ? 0xff : (uint8_t)(s >> 16));
            p++;
        }
    }
    else
    {
        // Vectorisable block conversion
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            wr.pData[p * wr.skip] = (((uint16_t*)rd.pData)[p * skip]) >> 8;

        // Tail code
        while (p < n)
        {
            wr.pData[p * wr.skip] = (((uint16_t*)rd.pData)[p * skip]) >> 8;
            p++;
        }
    }
}

void Phusion_Sampler::ConvertU8I32(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t skip = rd.skip >> 2;

    if (pGain)
    {
        // Vectorisable block conversion
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
        {
            int32_t s = ((int32_t*)rd.pData)[p * skip];
            // Round-to-nearest
            wr.pData[p * wr.skip] = (s < 0) ? 0 : ((s >= 0xFF80) ? 0xFFU : (uint8_t)((s + 0x80) >> 8));
        }

        // Tail code
        while (p < n)
        {
            int32_t s = ((int32_t*)rd.pData)[p * skip];
            wr.pData[p * wr.skip] = (s < 0) ? 0 : ((s >= 0xFF80) ? 0xFFU : (uint8_t)((s + 0x80) >> 8));
            p++;
        }
    }
    else
    {
        // Vectorisable block conversion
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
        {
            int32_t s = ((int32_t*)rd.pData)[p * skip];
            wr.pData[p * wr.skip] = (s < 0) ? 0 : ((s >= 0xFF80) ? 0xFFU : (uint8_t)((s + 0x80) >> 8));
        }

        // Tail code
        while (p < n)
        {
            int32_t s = ((int32_t*)rd.pData)[p * skip];
            wr.pData[p * wr.skip] = (s < 0) ? 0 : ((s >= 0xFF80) ? 0xFFU : (uint8_t)((s + 0x80) >> 8));
            p++;
        }
    }
}

void Phusion_Sampler::ConvertU8SF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t skip = rd.skip / sizeof(float);

    if (pGain)
    {
        // Vectorisable block conversion
        float gain = ((float)*pGain) * 255.0F;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
        {
            float f = ((float*)rd.pData)[p * skip] * gain;
            wr.pData[p * wr.skip] = (f >= 255.0F) ? 0xFFU : ((f < 0.0F) ? 0U : (uint8_t)f);
        }

        // Tail code
        while (p < n)
        {
            float f = ((float*)rd.pData)[p * skip] * gain;
            wr.pData[p * wr.skip] = (f >= 255.0F) ? 0xFFU : ((f < 0.0F) ? 0U : (uint8_t)f);
            p++;
        }
    }
    else
    {
        // Vectorisable block conversion
        const float gain = 255.0F;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
        {
            float f = ((float*)rd.pData)[p * skip] * gain;
            wr.pData[p * wr.skip] = (f >= 255.0F) ? 0xFFU : ((f < 0.0F) ? 0U : (uint8_t)f);
        }

        // Tail code
        while (p < n)
        {
            float f = ((float*)rd.pData)[p * skip] * gain;
            wr.pData[p * wr.skip] = (f >= 255.0F) ? 0xFFU : ((f < 0.0F) ? 0U : (uint8_t)f);
            p++;
        }
    }
}

void Phusion_Sampler::ConvertU8DF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t skip = rd.skip / sizeof(double);

    if (pGain)
    {
        // Vectorisable block conversion
        double gain = *pGain * 255.0;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
        {
            double d = ((double*)rd.pData)[p * skip] * gain;
            wr.pData[p * wr.skip] = (d >= 255.0) ? 0xFFU : ((d < 0.0) ? 0U : (uint8_t)d);
        }

        // Tail code
        while (p < n)
        {
            double d = ((double*)rd.pData)[p * skip] * gain;
            wr.pData[p * wr.skip] = (d >= 255.0) ? 0xFFU : ((d < 0.0) ? 0U : (uint8_t)d);
            p++;
        }
    }
    else
    {
        // Vectorisable block conversion
        const double gain = 255.0;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
        {
            double d = ((double*)rd.pData)[p * skip] * gain;
            wr.pData[p * wr.skip] = (d >= 255.0) ? 0xFFU : ((d < 0.0) ? 0U : (uint8_t)d);
        }

        // Tail code
        while (p < n)
        {
            double d = ((double*)rd.pData)[p * skip] * gain;
            wr.pData[p * wr.skip] = (d >= 255.0) ? 0xFFU : ((d < 0.0) ? 0U : (uint8_t)d);
            p++;
        }
    }
}

void Phusion_Sampler::ConvertU16U8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample conversion not yet implemented");
}

void Phusion_Sampler::ConvertU16U16(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample conversion not yet implemented");
}

void Phusion_Sampler::ConvertU16SF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample conversion not yet implemented");
}

void Phusion_Sampler::ConvertU16DF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample conversion not yet implemented");
}

void Phusion_Sampler::ConvertSFU8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t wrSkip = wr.skip / sizeof(float);

    if (pGain)
    {
        // Vectorisable block conversion
        float gain = (float)*pGain / 255.0F;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((float*)wr.pData)[p * wrSkip] = rd.pData[p * rd.skip] * gain;

        // Tail code
        while (p < n)
        {
            ((float*)wr.pData)[p * wrSkip] = rd.pData[p * rd.skip] * gain;
            p++;
        }
    }
    else
    {
        // Vectorisable block conversion
        const float gain = 1.0F / 255.0F;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((float*)wr.pData)[p * wrSkip] = rd.pData[p * rd.skip] * gain;

        // Tail code
        while (p < n)
        {
            ((float*)wr.pData)[p * wrSkip] = rd.pData[p * rd.skip] * gain;
            p++;
        }
    }
}

void Phusion_Sampler::ConvertSFU16(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t wrSkip = wr.skip / sizeof(float);
    ptrdiff_t rdSkip = rd.skip >> 1;

    if (pGain)
    {
        // Vectorisable block conversion
        float gain = (float)*pGain / 65535.0F;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((float*)wr.pData)[p * wrSkip] = ((uint16_t*)rd.pData)[p * rdSkip] * gain;

        // Tail code
        while (p < n)
        {
            ((float*)wr.pData)[p * wrSkip] = ((uint16_t*)rd.pData)[p * rdSkip] * gain;
            p++;
        }
    }
    else
    {
        // Vectorisable block conversion
        const float gain = 1.0F / 65535.0F;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((float*)wr.pData)[p * wrSkip] = ((uint16_t*)rd.pData)[p * rdSkip] * gain;

        // Tail code
        while (p < n)
        {
            ((float*)wr.pData)[p * wrSkip] = ((uint16_t*)rd.pData)[p * rdSkip] * gain;
            p++;
        }
    }
}

void Phusion_Sampler::ConvertSFSF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t wrSkip = wr.skip / sizeof(float);
    ptrdiff_t rdSkip = rd.skip / sizeof(float);

    if (pGain)
    {
        // Vectorisable block conversion
        float gain = (float)*pGain;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((float*)wr.pData)[p * wrSkip] = ((float*)rd.pData)[p * rdSkip] * gain;

        // Tail code
        while (p < n)
        {
            ((float*)wr.pData)[p * wrSkip] = ((float*)rd.pData)[p * rdSkip] * gain;
            p++;
        }
    }
    else
    {
        // Vectorisable block conversion
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((float*)wr.pData)[p * wrSkip] = ((float*)rd.pData)[p * rdSkip];

        // Tail code
        while (p < n)
        {
            ((float*)wr.pData)[p * wrSkip] = ((float*)rd.pData)[p * rdSkip];
            p++;
        }
    }
}

void Phusion_Sampler::ConvertSFDF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample conversion not yet implemented");
}

void Phusion_Sampler::ConvertDFU8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t wrSkip = wr.skip / sizeof(double);

    if (pGain)
    {
        double gain = (double)*pGain / 255.0;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((double*)wr.pData)[p * wrSkip] = rd.pData[p * rd.skip] * gain;

        // Tail code
        while (p < n)
        {
            ((double*)wr.pData)[p * wrSkip] = rd.pData[p * rd.skip] * gain;
            p++;
        }
    }
    else
    {
        const double gain = 1.0 / 255.0;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((double*)wr.pData)[p * wrSkip] = rd.pData[p * rd.skip] * gain;

        // Tail code
        while (p < n)
        {
            ((double*)wr.pData)[p * wrSkip] = rd.pData[p * rd.skip] * gain;
            p++;
        }
    }
}

void Phusion_Sampler::ConvertDFU16(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample conversion not yet implemented");
}

void Phusion_Sampler::ConvertDFSF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample conversion not yet implemented");
}

void Phusion_Sampler::ConvertDFDF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample conversion not yet implemented");
}

void Phusion_Sampler::ConvertU8LogI32(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t rdSkip = rd.skip >> 2;

    // TODO - this needs speeding up; it will NOT be fast! Can we avoid exp()?

    if (pGain)
    {
        // Vectorisable block conversion
        const double gain = (double)*pGain;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
        {
            int32_t s = ((int32_t*)rd.pData)[p * rdSkip];
            // Round-to-nearest
            s = (int32_t)(0.5 + 255.0 * exp((double)s * gain));
            wr.pData[p * wr.skip] = (s >= 0x100 ? 0xFFU : (uint8_t)s);
        }

        // Tail code
        while (p < n)
        {
            int32_t s = ((int32_t*)rd.pData)[p * rdSkip];
            s = (int32_t)(0.5 + 255.0 * exp((double)s * gain));
            wr.pData[p * wr.skip] = (s >= 0x100 ? 0xFFU : (uint8_t)s);
        }
    }
    else
    {
        // Vectorisable block conversion
        const double normFact = 1.0 / (double)(1 << 26);
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
        {
            int32_t s = ((int32_t*)rd.pData)[p * rdSkip];
            s = (int32_t)(0.5 + 255.0 * exp((double)s * normFact));
            wr.pData[p * wr.skip] = (s >= 0x100 ? 0xFFU : (uint8_t)s);
        }

        // Tail code
        while (p < n)
        {
            int32_t s = ((int32_t*)rd.pData)[p * rdSkip];
            s = (int32_t)(0.5 + 255.0 * exp((double)s * normFact));
            wr.pData[p * wr.skip] = (s >= 0x100 ? 0xFFU : (uint8_t)s);
        }
    }
}

void Phusion_Sampler::SumU16U8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample summation not yet implemented");
}

void Phusion_Sampler::SumI32U8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t wrSkip = wr.skip >> 2;

    if (pGain)
    {
        // Vectorisable block summation
        int32_t gain = (int32_t)((float)*pGain * ((float)(1 << 24) / 255.0F));
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((int32_t*)wr.pData)[p * wrSkip] += (rd.pData[p * rd.skip] * gain) >> 8;

        // Tail code
        while (p < n)
        {
            ((int32_t*)wr.pData)[p * wrSkip] += (rd.pData[p * rd.skip] * gain) >> 8;
            p++;
        }
    }
    else
    {
        // Vectorisable block summation
        const int32_t gain = (1 << 16) / 255;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((int32_t*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;

        // Tail code
        while (p < n)
        {
            ((int32_t*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;
            p++;
        }
    }
}

void Phusion_Sampler::SumI32U16(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample summation not yet implemented");
}

void Phusion_Sampler::SumI32SF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample summation not yet implemented");
}

void Phusion_Sampler::SumI32DF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample summation not yet implemented");
}

void Phusion_Sampler::SumSFU8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t wrSkip = wr.skip / sizeof(float);

    if (pGain)
    {
        // Vectorisable block summation
        float gain = (float)*pGain / 255.0F;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((float*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;

        // Tail code
        while (p < n)
        {
            ((float*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;
            p++;
        }
    }
    else
    {
        // Vectorisable block summation
        const float gain = 1.0F / 255.0F;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((float*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;

        // Tail code
        while (p < n)
        {
            ((float*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;
            p++;
        }
    }
}

void Phusion_Sampler::SumSFU16(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample summation not yet implemented");
}

void Phusion_Sampler::SumSFSF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample summation not yet implemented");
}

void Phusion_Sampler::SumSFDF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample summation not yet implemented");
}

void Phusion_Sampler::SumDFU8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t wrSkip = wr.skip / sizeof(double);

    if (pGain)
    {
        // Vectorisable block summation
        double gain = (double)*pGain / 255.0;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((double*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;

        // Tail code
        while (p < n)
        {
            ((double*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;
            p++;
        }
    }
    else
    {
        // Vectorisable block summation
        const double gain = 1.0 / 255.0;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((double*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;

        // Tail code
        while (p < n)
        {
            ((double*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;
            p++;
        }
    }
}

void Phusion_Sampler::SumDFU16(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample summation not yet implemented");
}

void Phusion_Sampler::SumDFSF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample summation not yet implemented");
}

void Phusion_Sampler::SumDFDF(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample summation not yet implemented");
}

void Phusion_Sampler::SumLogI32U8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t wrSkip = wr.skip >> 2;

    PHUSION_ASSERT(bInitialised);

    if (pGain)
    {
        // Vectorisable block conversion
        const float gain = (float)*pGain;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
{
            ((int32_t*)wr.pData)[p * wrSkip] += (int32_t)(gain * logU8[rd.pData[p * rd.skip]]);
//printf("Conversion %u, gain %f -> %d\n", rd.pData[p], gain, ((int32_t*)wr.pData)[p * wrSkip]);
}

        // Tail code
        while (p < n)
        {
            ((int32_t*)wr.pData)[p * wrSkip] += (int32_t)(gain * logU8[rd.pData[p * rd.skip]]);
            p++;
        }
    }
    else
    {
        // Vectorisable block conversion
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((int32_t*)wr.pData)[p * wrSkip] += logU8[rd.pData[p * rd.skip]];

        // Tail code
        while (p < n)
        {
            ((int32_t*)wr.pData)[p * wrSkip] += logU8[rd.pData[p * rd.skip]];
            p++;
        }
    }
}

/* TODO - just one small detail overlooked here; the log space thing!!!
void Phusion_Sampler::SumLogSFU8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t wrSkip = wr.skip / sizeof(float);

    if (pGain)
    {
        // Vectorisable block summation
        float gain = (float)*pGain / 255.0F;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((float*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;

        // Tail code
        while (p < n)
        {
            ((float*)wr.pData)[p * wrSkip] += rd.pData[p * wr.skip] * gain;
            p++;
        }
    }
    else
    {
        // Vectorisable block summation
        const float gain = 1.0F / 255.0F;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((float*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;

        // Tail code
        while (p < n)
        {
            ((float*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;
            p++;
        }
    }
}

void Phusion_Sampler::SumLogDFU8(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    ptrdiff_t wrSkip = wr.skip / sizeof(double);

    if (pGain)
    {
        // Vectorisable block summation
        double gain = (double)*pGain / 255.0;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((double*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;

        // Tail code
        while (p < n)
        {
            ((double*)wr.pData)[p * wrSkip] += rd.pData[p * wr.skip] * gain;
            p++;
        }
    }
    else
    {
        // Vectorisable block summation
        const double gain = 1.0 / 255.0;
        unsigned p;
        for (p = 0; p < (n & ~(N-1)); p++)
            ((double*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;

        // Tail code
        while (p < n)
        {
            ((double*)wr.pData)[p * wrSkip] += rd.pData[p * rd.skip] * gain;
            p++;
        }
    }
}
*/

void Phusion_Sampler::NoConversion(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t n, const phfloat_t *pGain)
{
    PHUSION_ASSERT(!"Sample format conversion not supported");
}
