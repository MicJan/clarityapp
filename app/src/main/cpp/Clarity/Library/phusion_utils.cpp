/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        phusion_utils.hpp                                             */
/* Description: Phusion library utility functions                             */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "phusion_defs.hpp"
#include "phusion_internal.hpp"
#include "phusion_utils.hpp"

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN  // Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include "pthread.h"
#define PHUSION_LOG_DIR "\\"
#else
#include <sys/time.h>
#define PHUSION_LOG_DIR "./"
#endif

// Callbacks from the library into the client code
static Phusion_Callbacks cb;

#if PHUSION_PROFILING
// Textual names for processing operations; must be kept in step with
static const char *profNames[PROF_COUNT] =
{
    "Resampling",
    "CalcGradient",
    "SolveLinSys",
    "ProcessPixels",
    "ConvertPixels",
    "FilterPixels"
};

static volatile bool gotFreq = false;
static volatile uint64_t timerFreq;  // timer requency in kHz

// Initialise performance profiling
void profile_init(Phusion_State *phState)
{
    UNUSED(phState);

#ifdef WIN32
    // Easier - and provides more accurate performance information - than introducing
    // threadsafing in time_now_us() which could cause interlocking/context switches etc
    if (!gotFreq)
    {
        LARGE_INTEGER freq;
        BOOL ok = QueryPerformanceFrequency(&freq);
        PHUSION_ASSERT(ok);
        timerFreq = ((uint64_t)freq.HighPart << 32U) | freq.LowPart;
        log_printf("Performance counter is %lluHz\n", timerFreq);
        timerFreq = (timerFreq + 500) / 1000;
        gotFreq = !!ok;
    }
#endif
}

// Returns the current value of a microsecond-resolution monotonic timer
//    or the best available approximation thereof
uint64_t time_now_us(void)
{
#ifdef WIN32
    LARGE_INTEGER timeNow;
    uint64_t t;
    BOOL ok;

    PHUSION_ASSERT(gotFreq);  // profile_init() should have been invoked
    if (!gotFreq)
        return 0;

    ok = QueryPerformanceCounter(&timeNow);
    PHUSION_ASSERT(ok);
    t = ((uint64_t)timeNow.HighPart << 32U) | timeNow.LowPart;
    return ok ? (t * 1000) / timerFreq : 0;
#else
    struct timeval time_now;

    gettimeofday(&time_now, NULL);
    return ((uint64_t)time_now.tv_sec * 1000000) + time_now.tv_usec;
#endif
}

// Record initiation of a particular operation
void profile_start_timing(profile_info *pf)
{
    pf->start_time = time_now_us();
}

// Record completion of a particular operation
void profile_stop_timing(profile_info *pf)
{
//  printf("elapsed time %llu (now %lu, start %lu)\n", (uint64_t)(time_now_us() - pf->start_time),
//                                                     time_now_us(), pf->start_time);
    pf->elapsed_time += (time_now_us() - pf->start_time);
}

// Report performance profiling information for all worker threads
void profile_report(Phusion_State *phState)
{
    // Calculate the total elapsed time for all operations,
    const char *perfLog = PHUSION_LOG_DIR "perf_log.txt";
    FILE *fp;

    fp = fopen(perfLog, "w");
    if (fp)
    {
        for (unsigned t = 0; t < phState->cpu.nThreads; t++)
        {
            Phusion_ThreadState *pState = &phState->cpu.states[t].worker;
            uint64_t total_elapsed = 0;

            fprintf(fp, "Performance profiling for thread %u:\n", t);

            for(unsigned idx = 0; idx < PROF_COUNT; idx++)
               total_elapsed += pState->profile[idx].elapsed_time;

            // Report the total elapsed time for each operation and its percentage
            for(unsigned idx = 0; idx < PROF_COUNT; idx++)
            {
                const unsigned frames = 1;
#if PHUSION_32BIT
                fprintf(fp, "%-24s: %lluus %u%%\n", profNames[idx], pState->profile[idx].elapsed_time / frames,
                                       (unsigned)((pState->profile[idx].elapsed_time * 100) / total_elapsed));
#else
                fprintf(fp, "%-24s: %luus %u%%\n", profNames[idx], pState->profile[idx].elapsed_time / frames,
                                       (unsigned)((pState->profile[idx].elapsed_time * 100) / total_elapsed));
#endif
                pState->profile[idx].elapsed_time = 0;
            }
        }
        fclose(fp);
    }
    else
        fprintf(stderr, "ERROR: Unable to write to performance log '%s'\n", perfLog);
}
#endif

// Internal memory management routines, mirroring malloc/free but possibly indirected
//    through client code, and accepting a statically-allocated textual description of
//    the block usage that which be diagnostically useful

void *mem_alloc(size_t n, const char *pDesc)
{
    if (cb.mem_alloc)
        return cb.mem_alloc(n, pDesc);
    else
    {
        void *pBlk;
        log_printf("Phusion library: mem_alloc %x bytes for '%s'\n", n, pDesc);
        pBlk = malloc(n);
        if (pBlk)
            log_printf("  using block at address 0x%p\n", pBlk);
        else
            log_printf("  allocation failed\n");
        return pBlk;
    }
}

void mem_free(void *pBlk, const char *pDesc)
{
    if (cb.mem_free)
        cb.mem_free(pBlk, pDesc);
    else
    {
        log_printf("Phusion library: mem_free called for address 0x%p, '%s'\n", pBlk, pDesc);
        free(pBlk);
    }
}

// Utility function for informing client code of error conditions detected by the Phusion library;
//    pDesc references a statically-allocated textual description of the error condition
Phusion_Result ReturnError(Phusion_Result res, const char *pDesc)
{
    if (cb.error)
        cb.error(res, pDesc);
    return res;
}

// Return current value of a monotonic timer in milliseconds
uint32_t CurrentTime(void)
{
#ifdef WIN32
    clock_t t = clock();
    PHUSION_ASSERT(CLOCKS_PER_SEC == 1000);
    return t;
#else
    struct timeval time_now;

    gettimeofday(&time_now, NULL);
    return (uint32_t)(((uint64_t)time_now.tv_sec * 1000) + time_now.tv_usec / 1000);
#endif
}

// Return a count of the number of processor cores
unsigned NumberOfProcessors(void)
{
#ifdef WIN32
    SYSTEM_INFO sysInfo;
    GetSystemInfo(&sysInfo);
    return sysInfo.dwNumberOfProcessors;
#else
    // TODO - Discover an API call that will provide this information on Linux platforms
    return 4;
#endif
}

#ifdef WIN32
/*
 * time between jan 1, 1601 and jan 1, 1970 in units of 100 nanoseconds
 */
#define PTW32_TIMESPEC_TO_FILETIME_OFFSET \
      ( ((int64_t) 27111902 << 32) + (int64_t) 3577643008 )

// Return the current real time as a struct timespec that is compatible
//   with the 'abstime' implementation of Pthreads-win32
void GetRealTimeAsTimespec(struct timespec *ts)
{
   FILETIME ft;
   uint64_t t;

   GetSystemTimeAsFileTime(&ft);
   t = ft.dwLowDateTime | ((uint64_t)ft.dwHighDateTime << 32);

   ts->tv_sec  = (int)((t - PTW32_TIMESPEC_TO_FILETIME_OFFSET) / 10000000);
   ts->tv_nsec = (int)((t - PTW32_TIMESPEC_TO_FILETIME_OFFSET) % 10000000) * 100;
}
#endif

#if PHUSION_LOGGING
// Basic text logging to file, particularly intended for Windowing systems that
//   do not supply basic console output
int log_printf(const char *fmt, ...)
{
    FILE *logfp = fopen(PHUSION_LOG_DIR "log.txt", "a");
    if (logfp)
    {
       va_list va;
       int len;

       va_start(va, fmt);
       len = vfprintf(logfp, fmt, va);
       va_end(va);
       fclose(logfp);

       return len;
    }

    return 0;
}
#endif
