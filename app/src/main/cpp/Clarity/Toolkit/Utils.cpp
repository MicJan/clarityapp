/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        Utils.cpp                                                     */
/* Description: Utility and support functions                                 */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#include <cassert>
#include <cstdio>

#include "Utils.hpp"

#if TOOLKIT_PROFILING
#ifdef WIN32
#define WIN32_LEAN_AND_MEAN  // Exclude rarely-used stuff from Windows headers
#include <windows.h>
#else
#include <sys/time.h>
#endif

#if defined(_MSC_VER) || defined(ARM)
#define CLARITY_32BIT 1
#else
#define CLARITY_32BIT 0
#endif

// NOTE: Must be kept in step with PROF_ enumeration
static const char *profNames[] =
{
    "Luminance Creation",
    "RGB2Gray",
    "Averaging for Weights",
    "Weight convergence",
    "Weights calculation",
    "Downsampling",
    "ToneMapping",
    "ToneMapping Convolution",
    "Unsharp Masking Convolution",
    "Gradient Poly Expansion",
    "LinSys construction",
    "LinSys completion",
    "Solving linear system",
    "Upsampling",
    "Pixel-processing",
    "Luminance Application",

    "Temporary"
};

profile_info profile[PROF_COUNT];

static volatile bool gotFreq = false;
static volatile uint64_t timerFreq;  // timer requency in kHz

// Initialise performance profiling
void profile_init(void)
{
#ifdef WIN32
    // Easier - and provides more accurate performance information - than introducing
    // threadsafing in time_now_us() which could cause interlocking/context switches etc
    if (!gotFreq)
    {
        LARGE_INTEGER freq;
        BOOL ok = QueryPerformanceFrequency(&freq);
        assert(ok);
        timerFreq = ((uint64_t)freq.HighPart << 32U) | freq.LowPart;
#if CLARITY_32BIT
        printf("Performance counter is %lluHz\n", timerFreq);
#else
        printf("Performance counter is %luHz\n", timerFreq);
#endif
        timerFreq = (timerFreq + 500) / 1000;
        gotFreq = !!ok;
    }
#endif
}

// Returns the current value of a microsecond-resolution monotonic timer
//    or the best available approximation thereof
uint64_t time_now_us(void)
{
#ifdef WIN32
    LARGE_INTEGER timeNow;
    uint64_t t;
    BOOL ok;

    assert(gotFreq);  // profile_init() should have been invoked
    if (!gotFreq)
        return 0;

    ok = QueryPerformanceCounter(&timeNow);
    assert(ok);
    t = ((uint64_t)timeNow.HighPart << 32U) | timeNow.LowPart;
    return ok ? (t * 1000) / timerFreq : 0;
#else
    struct timeval time_now;

    gettimeofday(&time_now, NULL);
    return ((uint64_t)time_now.tv_sec * 1000000) + time_now.tv_usec;
#endif
}

// Record initiation of a particular operation
void profile_start_timing(profile_info *pf)
{
    pf->start_time = time_now_us();
}

// Record completion of a particular operation
void profile_stop_timing(profile_info *pf)
{
//  printf("elapsed time %llu (now %lu, start %lu)\n", (uint64_t)(time_now_us() - pf->start_time),
//                                                     time_now_us(), pf->start_time);
    pf->elapsed_time += (time_now_us() - pf->start_time);
}

// Report performance profiling information for all worker threads
void profile_report(const char *perfLog)
{
    // Calculate the total elapsed time for all operations,
    FILE *fp;

    fp = fopen(perfLog, "w");
    if (fp)
    {
        fprintf(fp, "Clarity Prototype performance profiling:\n");

        uint64_t total_elapsed = 0;
        for(unsigned idx = 0; idx < PROF_COUNT; idx++)
           total_elapsed += profile[idx].elapsed_time;

        // Report the total elapsed time for each operation and its percentage
        const int frames = 1;
#if CLARITY_32BIT
        printf("Total elapsed time = %lluus\n", total_elapsed);

        fprintf(fp, "Total elapsed time = %lluus\n", total_elapsed);
        for(unsigned idx = 0; idx < PROF_COUNT; idx++)
        {
            const unsigned frames = 1;
            fprintf(fp, "  %-32s: %lluus %u%%\n", profNames[idx], profile[idx].elapsed_time / frames,
                                        (unsigned)((profile[idx].elapsed_time * 100) / total_elapsed));
            profile[idx].elapsed_time = 0;
        }
#else
        printf("Total elapsed time = %luus\n", total_elapsed);

        fprintf(fp, "Total elapsed time = %luus\n", total_elapsed);
        for(unsigned idx = 0; idx < PROF_COUNT; idx++)
        {
            fprintf(fp, "  %-32s: %luus %u%%\n", profNames[idx], profile[idx].elapsed_time / frames,
                                       (unsigned)((profile[idx].elapsed_time * 100) / total_elapsed));
            profile[idx].elapsed_time = 0;
        }
#endif
        fclose(fp);
    }
    else
        fprintf(stderr, "ERROR: Unable to write to performance log '%s'\n", perfLog);
}

#endif  // TOOLKIT_PROFILING

bool ReadMATLABdata(const char *pFilename, Phusion_Buffer *pBuf, Phusion_ImageChannels *pIm,
                    uint32_t *pWidth, uint32_t *pHeight)
{
    uint32_t dims[3];
    uint32_t nDims;
    unsigned d;
    FILE *fp;

    fp = fopen(pFilename, "rb");
    if (!fp)
    {
        fprintf(stderr, "ERROR: Unable to open MATLAB data file '%s'\n", pFilename);
        fclose(fp);
        return false;
    }

    if (4 != fread(&nDims, 1, 4, fp))
    {
        fprintf(stderr, "ERROR: Failed to read from file '%s'\n", pFilename);
        fclose(fp);
        return false;
    }
    if (nDims != 2 && nDims != 3)
    {
        fprintf(stderr, "ERROR: Unexpected dimensionality (%u dimensions) in file '%s'\n", nDims, pFilename);
        fclose(fp);
        return false;
    }

    for (d = 0; d < nDims; d++)
    {
        if (4 != fread(&dims[d], 1, 4, fp))
        {
            fprintf(stderr, "ERROR: Failed to read from file '%s'\n", pFilename);
            fclose(fp);
            return false;
        }
    }
    if (nDims < 3)
        dims[2] = 1;

    // Report the dimensions of the file content
    printf("MATLAB file '%s' has content of %u dimensions\n", pFilename, nDims);
    printf(" %u row(s), %u column(s), %u plane(s)\n", dims[0], dims[1], dims[2]);

    pBuf->nPlanes  = dims[2];
    pIm->nChannels = dims[2];

    // Read in each plane in turn
    unsigned p;
    for (p = 0; p < dims[2]; p++)
    {
        pBuf->planes[p].pData = new double [dims[0] * dims[1]];
        if (!pBuf->planes[p].pData)
        {
            fprintf(stderr, "ERROR: Out of memory trying to allocate %u rows x %u columns\n", dims[0], dims[1]);
            fclose(fp);
            return false;
        }
        pBuf->planes[p].iStride = dims[1] * sizeof(double);
        pBuf->planes[p].flags   = Phusion_PlaneFlagDF;
        pBuf->planes[p].fmt     = Phusion_PlaneRaster1;

        // Convert the data into raster scan order as we go.
        //    TODO - this is obviously not efficient in access patterns,
        //           and we are relying upon fast, effective FS-layer cacheing
        double *pCol = (double*)pBuf->planes[p].pData;
        for (uint32_t x = 0; x < dims[1]; x++)
        {
            for (uint32_t y = 0; y < dims[0]; y++)
            {
                if (sizeof(double) != fread(&pCol[y * dims[1]], 1, sizeof(double), fp))
                {
                    fprintf(stderr, "ERROR: Failed to read from file '%s'\n", pFilename);

                    // TODO - release memory
                    fclose(fp);
                    return false;
                }
                // Report the value using MATLAB conventions/indices
//              printf("%u,%u: %lf\n", y+1, x+1, pCol[y * dims[1]]);
            }
            pCol++;
        }

        // Describe this channel; assume one channel per plane for now
        pIm->planeNumber[p] = p;
        pIm->compNumber[p]  = 0;
    }

    fclose(fp);

    // Optionally return plane dimensions
    if (pWidth)
        *pWidth = dims[1];
    if (pHeight)
        *pHeight = dims[0];

    return true;
}

