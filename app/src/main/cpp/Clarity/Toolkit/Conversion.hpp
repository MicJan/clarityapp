/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        Conversion.hpp                                                */
/* Description: Sample format/colour space/transfer function operations       */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __Toolkit_Conversion_hpp
#define __Toolkit_Conversion_hpp
#include <cstdint>
#ifndef __Bitmap_hpp
#include "Bitmap.hpp"
#endif

// TODO - modify these functions to eliminate their Bitmap-dependence

// Apply luminance information to RGB input
void ApplyLuminance(Bitmap &out,
                    const Bitmap &rgb, const Bitmap &lum,  // Input RGB and derived Luminance images
                    const Bitmap &outLum,  // Luminance output from Phusion processing
                    const uint16_t outLUT[0x100],
                    const double alphaLUT[0x400],
                    uint16_t minL);

// Create luminance image from input RGB image
uint16_t RGB2Luminance(Bitmap &lum, const Bitmap &rgb);

// Convert RGB thumbnail to grayscale
void RGB2Gray(Bitmap &dest, const Bitmap &src);

// Tone mapping transfer function
void ToneMapping(Bitmap &fthu, const Bitmap &lthu, unsigned max, unsigned min, const double strength);

#endif
