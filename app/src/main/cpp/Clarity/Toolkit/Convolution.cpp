/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        Convolution.cpp                                               */
/* Description: Image Convolution Filtering                                   */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#include <cassert>
#include <cmath>
#include <cstring>

#include "Convolution.hpp"
#include "Types.hpp"
#include "Utils.hpp"

#ifndef PHUSION_ASSERT
#define PHUSION_ASSERT assert
#endif

static void ReadColumns(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t nCols,
                        uint32_t aLines, int32_t aStride, uint32_t bLines, int32_t bStride,
                        const phfloat_t *pKern, bool bLogSpace);

static inline int32_t filterLin(const int32_t *pData, const int32_t *pWt, size_t n, size_t last, bool bLogSpace)
{
    int32_t sum = 0;

    if (bLogSpace)
    {
        size_t idx = ((last + 1) >= n) ? 0 : (last + 1);
        while (idx < n)
            sum += (int32_t)(((int64_t)pData[idx++] * (*pWt++)) >> 26);
        idx = 0;
        if (last < n - 1)
        {
            while (idx <= last)
                sum += (int32_t)(((int64_t)pData[idx++] * (*pWt++)) >> 26);
        }
        return sum;
    }
    else
    {
        size_t idx = ((last + 1) >= n) ? 0 : (last + 1);
        while (idx < n)
            sum += (int32_t)(((int64_t)pData[idx++] * (*pWt++) + 0x200) >> 12);
        idx = 0;
        if (last < n - 1)
        {
            while (idx <= last)
                sum += (int32_t)(((int64_t)pData[idx++] * (*pWt++) + 0x200) >> 12);
        }
        return sum >> 6;
    }
}

// Convolution of 8-bit input image data with a large uniformly-weighted kernel
// to produce smoothly-varying output FP image data (= initial weights)
//
// NOTE: kernWidth and kernHeight are the total dimensions of the input kernel

void ConvolveAverageU(phpoly_t *pDest, uint8_t *pSrc, int32_t srcStride,  // source stride, in bytes
                      uint32_t numLines, uint32_t pixPerLine,
                      unsigned kernWidth, unsigned kernHeight,
                      phpoly_t &max, phpoly_t &min)
{
    assert(pixPerLine <= PHUSION_CPU_MAX_COLW);
    assert(kernWidth  & 1);
    assert(kernHeight & 1);

    profile_start_timing(&profile[PROF_APP_WT_AVERAGE]);

    uint32_t kernUL = (kernHeight >> 1);  // Upper/lower borders; does NOT include centre pixel
    uint32_t kernLR = (kernWidth  >> 1);  // Left/right sides; does NOT include centre pixel

    printf("U8 kernel %ux%u, %u:%u\n", kernWidth, kernHeight, kernLR, kernUL);

    // Normalisation factor for output samples
    phpoly_t normFact = PHPOLY_ONE / ((phpoly_t)255.0 * (kernWidth * kernHeight));

    // Output x/y are the current output position (simple raster scan)
    uint32_t ox = 0, oy = 0;
    // Input x/y are the current input position (bottom right pixel of the input kernel, raster scan)
    uint32_t ix = 0, iy = 0;

    // Vertical colums are accumulated into this array
    // TODO - put this somewhere safe
    uint32_t sums[PHUSION_CPU_MAX_COLW];
    uint32_t kSum;

    // Design assumptions in the following implementation
    assert(numLines > kernUL && pixPerLine > kernLR);

    // Initialise vertical sums
    const unsigned N = 32;
    unsigned chunkPix = pixPerLine & ~(N-1);
    unsigned vWeight = kernUL + 2;  // The first thing we'll do is drop one
    const uint8_t *pIn = (uint8_t*)pSrc;

    // Initialise vertical sums, weighting the topmost pixel for absent vertical context
    for (unsigned pix = 0; pix < (chunkPix & ~(N-1)); pix++)
        sums[pix] = vWeight * pIn[pix];

    for (unsigned dy = 1; dy < kernUL; dy++)
    {
        pIn += srcStride;
        for (unsigned pix = 0; pix < (chunkPix & ~(N-1)); pix++)
            sums[pix] += pIn[pix];
    }

    const uint8_t *pCol = (uint8_t*)pSrc + chunkPix;
    ix = chunkPix;

    // Tail code for initialising vertical sums
    while (ix < pixPerLine)
    {
        const uint8_t *pIn = pCol;

        // Initialise vertical sums, weighting the topmost pixel for absent vertical context
        sums[ix] = vWeight * pIn[0];
        for (unsigned dy = 1; dy < kernUL; dy++)
        {
            pIn += srcStride;
            sums[ix] += pIn[0];
        }
        pCol++;
        ix++;
    }

// TODO - for this to work properly, we want N different kernel sums
    const uint8_t *pDropRow = (uint8_t*)pSrc;
    const uint8_t *pSrcRow = (uint8_t*)pSrc + kernUL * srcStride;
    uint32_t finalPix = pixPerLine - 1;
    uint32_t finalLine = numLines - 1;
    unsigned hWeight = kernLR + 1;
    iy = kernUL;
    while (oy < numLines)
    {
        const uint8_t *pDrop = pDropRow;
        const uint8_t *pIn = pSrcRow;

        // Update the vertical sums required for the line-initial condition
        ix = 0;
        for (unsigned pix = 0; pix < (kernLR & ~(N-1)); pix++)
            sums[ix+pix] += (int32_t)pIn[pix] - (int32_t)pDrop[pix];

        pIn   += kernLR & ~(N-1);
        pDrop += kernLR & ~(N-1);
        ix    += kernLR & ~(N-1);

        while (ix < kernLR)
            sums[ix++] += (int32_t)*pIn++ - (int32_t)*pDrop++;

        // Initialise kernel sum at the start of each new output line
        uint32_t partials[N];
        for (unsigned pix = 0; pix < N; pix++)
            partials[pix] = 0U;

        ix = 0;
        unsigned chunkPix = kernLR;
        while (chunkPix >= N)
        {
            for (unsigned pix = 0; pix < N; pix++)
                partials[pix] += sums[ix+pix];

            chunkPix -= N;
            ix += N;
        }
        while (chunkPix-- > 0)
            partials[0] += sums[ix++];

        kSum = 0U;
        for (unsigned pix = 0; pix < N; pix++)
            kSum += partials[pix];

        // Add additional left context
        kSum += hWeight * sums[0];

        assert(pIn   == pSrcRow  + kernLR);
        assert(pDrop == pDropRow + kernLR);

        // And we're ready to roll...
        // TODO - we need N kernel sums to be able to parallelise the following...
        bool bUpdateCol(true);
        uint32_t dx = 0;
        ox = 0;
        while (ox < pixPerLine)
        {
            if (bUpdateCol)
                sums[ix] += (int32_t)pIn[0] - (int32_t)pDrop[0];

            kSum += (int32_t)sums[ix] - (int32_t)sums[dx];
            phpoly_t out = kSum * normFact;
            assert(out >= 0.0 && out <= 1.0);

            // Update running min/max required for next step
            if (out < min)
                min = out;
            if (out > max)
                max = out;

            pDest[0] = out;
            pDest += 1;

            if (++ox > kernLR)
                dx++;
            if (ix < finalPix)
            {
                ix++;
                pIn++;
                pDrop++;
            }
            else
                bUpdateCol = false;
        }

        // Tail code is required to handle straggling pixels, but perhaps also special code for when
        //    the kernel RHS is outside the input buffer

        // Scanline advance
        if (iy < finalLine)
        {
            iy++;
            pSrcRow += srcStride;
        }
        if (++oy > kernUL+1)
            pDropRow += srcStride;
    }

    profile_stop_timing(&profile[PROF_APP_WT_AVERAGE]);
}

// Convolution of FP input weights with a large uniformly-weighted kernel
// to produce smoothly-varying output FP weights
//
// NOTE: kernWidth and kernHeight are the total dimensions of the input kernel

void ConvolveAverage(phpoly_t *pDest, size_t destSkip,  // destination skip, in elements
                     void *pSrc, int32_t srcStride,     // source stride, in bytes
                     uint32_t numLines, uint32_t pixPerLine,
                     unsigned kernWidth, unsigned kernHeight)
{
    assert(pixPerLine <= PHUSION_CPU_MAX_COLW);
    assert(kernWidth  & 1);
    assert(kernHeight & 1);

    profile_start_timing(&profile[PROF_APP_WT_AVERAGE]);

    uint32_t kernUL = (kernHeight >> 1);  // Upper/lower borders; does NOT include centre pixel
    uint32_t kernLR = (kernWidth  >> 1);  // Left/right sides; does NOT include centre pixel

    printf("FP kernel %ux%u, %u:%u\n", kernWidth, kernHeight, kernLR, kernUL);

    // Design assumptions in the following implementation
    assert(kernUL <= numLines && kernLR <= pixPerLine);

    // Normalisation factor for output samples
    phpoly_t normFact = PHPOLY_ONE / (kernWidth * kernHeight);

    // Output x/y are the current output position (simple raster scan)
    uint32_t ox = 0, oy = 0;
    // Input x/y are the current input position (bottom right pixel of the input kernel, raster scan)
    uint32_t ix = 0, iy = 0;

    // Vertical colums are accumulated into this array
    // TODO - put this somewhere safe
    phpoly_t sums[PHUSION_CPU_MAX_COLW];
    phpoly_t kSum;

    // Initialisation code assumes this
    assert(numLines > kernUL && pixPerLine > kernLR);

    // Initialise vertical sums
    const unsigned N = 32 / sizeof(phpoly_t);
    unsigned chunkPix = pixPerLine & ~(N-1);
    unsigned vWeight = kernUL + 2;  // The first thing we'll do is drop one
    const phpoly_t *pIn = (phpoly_t*)pSrc;

    // Initialise vertical sums, weighting the topmost pixel for absent vertical context
    for (unsigned pix = 0; pix < (chunkPix & ~(N-1)); pix++)
    {
        assert(pIn[pix] >= 0.0 && pIn[pix] <= 1.0);
        sums[ix+pix] = vWeight * pIn[pix];
    }

    for (unsigned dy = 1; dy < kernUL; dy++)
    {
        pIn += srcStride / sizeof(phpoly_t);
        for (unsigned pix = 0; pix < (chunkPix & ~(N-1)); pix++)
        {
            assert(pIn[pix] >= 0.0 && pIn[pix] <= 1.0);
            sums[pix] += pIn[pix];
        }
    }

    const phpoly_t *pCol = (phpoly_t*)pSrc + chunkPix;
    ix = chunkPix;

    // Tail code for initialising vertical sums
    while (ix < pixPerLine)
    {
        const phpoly_t *pIn = pCol;

        // Initialise vertical sums, weighting the topmost pixel for absent vertical context
        sums[ix] = vWeight * pIn[0];
        for (unsigned dy = 1; dy < kernUL; dy++)
        {
            pIn += srcStride / sizeof(phpoly_t);
            assert(pIn[0] >= 0.0 && pIn[0] <= 1.0);
            sums[ix] += pIn[0];
        }
        pCol++;
        ix++;
    }

// TODO - for this to work properly, we want N different kernel sums

    const uint8_t *pDropRow = (uint8_t*)pSrc;
    const uint8_t *pSrcRow = (uint8_t*)pSrc + (kernUL * srcStride);
    uint32_t finalPix = pixPerLine - 1;
    uint32_t finalLine = numLines - 1;
    unsigned hWeight = kernLR - 1;
    iy = kernUL;

    while (oy < numLines)
    {
        const phpoly_t *pDrop = (phpoly_t*)pDropRow;
        const phpoly_t *pIn = (phpoly_t*)pSrcRow;

        // Update the vertical sums required for the line-initial condition
        ix = 0;
        for (unsigned pix = 0; pix < (kernLR & ~(N-1)); pix++)
            sums[ix+pix] += pIn[pix] - pDrop[pix];

        pIn   += kernLR & ~(N-1);
        pDrop += kernLR & ~(N-1);
        ix    += kernLR & ~(N-1);

        while (ix < kernLR)
            sums[ix++] += *pIn++ - *pDrop++;

        // Initialise kernel sum at the start of each new output line
        phpoly_t partials[N];
        for (unsigned pix = 0; pix < N; pix++)
            partials[pix] = PHPOLY_ZERO;

        ix = 0;
        unsigned chunkPix = kernLR;
        while (chunkPix >= N)
        {
            for (unsigned pix = 0; pix < N; pix++)
                partials[pix] += sums[ix+pix];

            chunkPix -= N;
            ix += N;
        }
        while (chunkPix-- > 0)
            partials[0] += sums[ix++];

        kSum = PHPOLY_ZERO;
        for (unsigned pix = 0; pix < N; pix++)
            kSum += partials[pix];

        // Add additional left context
        kSum += hWeight * sums[0];

        // And we're ready to roll...
        // TODO - we need N kernel sums to be able to parallelise the following...
        bool bUpdateCol(true);
        uint32_t dx = 0;
        ox = 0;
        while (ox < pixPerLine)
        {
            if (bUpdateCol)
                sums[ix] += pIn[0] - pDrop[0];

            kSum += sums[ix] - sums[dx];

            pDest[0] = kSum * normFact;
            pDest += destSkip;

            if (++ox >= kernLR)
                dx++;
            if (ix < finalPix)
            {
                ix++;
                pIn++;
                pDrop++;
            }
            else
                bUpdateCol = false;
        }

        // Tail code is required to handle straggling pixels, but perhaps also special code for when
        //    the kernel RHS is outside the input buffer

        // Scanline advance
        if (iy < finalLine)
        {
            iy++;
            pSrcRow += srcStride;
        }
        if (++oy > kernUL+1)
            pDropRow += srcStride;
    }
    profile_stop_timing(&profile[PROF_APP_WT_AVERAGE]);
}

uint32_t SetupVerticalContext(uint32_t availLines, uint32_t y,
                              int32_t   stride, uint32_t colHeight,
                              uint32_t &aLines, int32_t &aStride,  // Properties of first stage
                              uint32_t &bLines, int32_t &bStride)  // Properties of second stage
{
    uint32_t startLine;

    PHUSION_ASSERT(colHeight & 1);

    aStride = stride;
    bStride = stride;
    aLines  = colHeight >> 1;

    if (y >= aLines)
    {
        startLine = y - aLines;

        if (availLines - y > aLines)
        {
            aLines  = (aLines << 1) + 1;
            bLines  = 0;
        }
        else
        {
            aLines += availLines - y;
            bLines  = colHeight - aLines;
            bStride = -stride;
        }
    }
    else
    {
        bLines  = y + 1 + aLines;
        aLines -= y;
        aStride = -stride;

        startLine = aLines - 1;
    }

    return startLine;
}

void ReadColumns(Phusion_Sampler &wr, Phusion_Sampler &rd, uint32_t nCols,
                 uint32_t aLines, int32_t aStride, uint32_t bLines, int32_t bStride,
                 const phfloat_t *pKern, bool bLogSpace)
{
    // We handle vertical context generation (reflected at image boundary) by reading rows of samples
    //   in two distinct phases: top downwards and bottom upwards, or vice-versa, imposing a constraint
    //   that the filter height must be less than twice the image height with the result that each
    //   output scanline may require top context- or bottom context-generation, but not

    rd.iStride = aStride;  // TODO - direct manipulation is somewhat ugly
    while (aLines-- > 0)
    {
        rd.StartLine();
        wr.StartLine();
        if (bLogSpace)
        {
            // Employ direct invocation for now since Phusion_Sampler itself has no real concept of log-space sample types
            Phusion_Sampler::SumLogI32U8(wr, rd, nCols, pKern);
        }
        else
            wr.SumSamples(rd, nCols, pKern);

        rd.AdvanceLine();
        pKern++;
    }

    rd.iStride = bStride;

// TODO - this is functional but won't do....perennial problem of when to line advance; before or after reading scanline
//        may be cleaner to have three separate loops; failing that we're going to have to check the sign of the strides?
if ((aStride < 0) != (bStride < 0))
    rd.AdvanceLine();

    while (bLines-- > 0)
    {
        rd.StartLine();
        wr.StartLine();
        if (bLogSpace)
            // Direct invocation, see above
            Phusion_Sampler::SumLogI32U8(wr, rd, nCols, pKern);
        else
            wr.SumSamples(rd, nCols, pKern);
        rd.AdvanceLine();
        pKern++;
    }
}

void FilterHRun(int32_t sums[][PHUSION_CPU_CONV_CHUNK], unsigned nSums, const int32_t *pSrc,
                const phfloat_t *pKern, unsigned width, unsigned nComps)
{
    const unsigned N = 16;

    if (nComps == 1)
    {
        // Vectorisable expression produces blocks of sums
        for (unsigned idx = 0; idx < width; idx++)
        {
            phfloat_t coeff = pKern[idx];
            for (unsigned pix = 0; pix < (nSums & ~(N-1)); pix++)
                sums[0][pix] += (int32_t)(coeff * pSrc[idx + pix]);  // TODO - produce fixed-point representation of filter gains

        }

        // Tail code produces the final few sums
        for (unsigned idx = 0; idx < width; idx++)
        {
            phfloat_t coeff = pKern[idx];
            for (unsigned pix = (nSums & ~(N-1)); pix < nSums; pix++)
                sums[0][pix] += (int32_t)(coeff * pSrc[idx + pix]);
        }
    }
    else
    {
        for (unsigned c = 0; c < nComps; c++)
        {
            for (unsigned idx = 0; idx < width; idx++)
            {
                phfloat_t coeff = pKern[idx];
                for (unsigned pix = 0; pix < (nSums & ~(N-1)); pix++)
                    sums[c][pix] += (int32_t)(coeff * pSrc[(idx + pix) * nComps + c]);
            }

            // Tail code produces the final few sums
            for (unsigned idx = 0; idx < width; idx++)
            {
                phfloat_t coeff = pKern[idx];
                for (unsigned pix = (nSums & ~(N-1)); pix < nSums; pix++)
                    sums[c][pix] += (int32_t)(coeff * pSrc[(idx + pix) * nComps + c]);
            }
        }
    }
}

void FilterHoriz(int32_t sums[][PHUSION_CPU_CONV_CHUNK], const phfloat_t *pKern, const int32_t *pSrc,
                 unsigned firstIdx, unsigned endIdx, unsigned kw,
                 unsigned nSums, unsigned nComps)
{
    // Since we hold the vertical filtering results in a circular buffer, to form the horizontal sum we will
    //    occasionally have to handle the wraparound condition and perform the horizontal filtering as two
    //    separate operations

    unsigned width = (kw > endIdx - firstIdx) ? (endIdx - firstIdx) : kw;
    const unsigned N = 16;

    for (unsigned c = 0; c < nComps; c++)
    {
        for (unsigned pix = 0; pix < (nSums & ~(N-1)); pix++)
            sums[c][pix] = 0;
        // Tail code
        for (unsigned pix = (nSums & ~(N-1)); pix < nSums; pix++)
            sums[c][pix] = 0;
    }

// TODO - are we not going to have trouble because source wraparound occurs at different points for each PE?
//        Surely that's going to be quite a serious problem for vectorisation?
    FilterHRun(sums, nSums, &pSrc[firstIdx], pKern, width, nComps);

    if (width < kw)
        FilterHRun(sums, nSums, pSrc, &pKern[width], kw - width, nComps);
}

void ConvolveImage(Bitmap &out, const Bitmap &in, const phfloat_t linKern[],
                   unsigned kernWidth, unsigned kernHeight,
                   int32_t &max, int32_t &min, int32_t cWeight,
                   bool bLogSpace)
{
    phfloat_t fcWeight = (phfloat_t)cWeight / (phfloat_t)(1U << 28);  // TODO - just accept float value?
    Phusion_Sampler destState[PHUSION_MAX_PLANES];
    Phusion_Sampler srcState[PHUSION_MAX_PLANES];
    int32_t outStride = out.GetStride();
    int32_t inStride = in.GetStride();
    uint32_t outHeight = out.GetHeight();
    uint32_t outWidth = out.GetWidth();
    uint32_t inHeight = in.GetHeight();
    uint32_t inWidth = in.GetWidth();
    bool bCentreWeighted = !!cWeight;
    const unsigned N = 16;
    int32_t partialMin[N];  // TODO - narrow?
    int32_t partialMax[N];
// TODO - separate this into two probably; writer into vertResults and reader from convResults
    Phusion_Sampler rdwr;
    bool bVerbose(false);
const phfloat_t *pHorizKern = linKern;
const phfloat_t *pVertKern = linKern;

    printf("centred %d %x\n", bCentreWeighted ? 1 : 0, cWeight);

    assert(outWidth  == inWidth);
    assert(outHeight == inHeight);

    unsigned kh = 2 * kernHeight + 1;
    unsigned kw = 2 * kernWidth + 1;
    int prevPC = -1;

    // Initialise min/max statistics of filtered output
    for (unsigned pix = 0; pix < N; pix++)
    {
        partialMin[pix] = 0x10000;
        partialMax[pix] = 0;
    }

    // TODO - this code should be generalised to multiple planes and migrated into the library for reuse

    // Initialise source sample readers
    // TODO - there's no benefit to having multiple sample readers at the moment
    const unsigned inPlanes = 1;
    for (unsigned p = 0; p < inPlanes; p++)
        InitSampler(srcState[p], in.GetRow(0), inStride, Phusion_PlaneFlagU8, Phusion_PlaneRaster1, 0);

    // Initialise destination sample writers
    const unsigned outPlanes = 1;
    for (unsigned p = 0; p < outPlanes; p++)
        InitSampler(destState[p], out.GetRow(0), outStride, Phusion_PlaneFlagU8, Phusion_PlaneRaster1, 0);

// TODO - this obviously needs moving to a safer location
//    int32_t vres[2*MAX_KERNEL_DIM+1];  // TODO - want some extra columns here too for vectorisation

// TODO - original convolution code was written entirely using integral arithmetic, and implementing here
//        as phfloat_t is most likely a backwards step; it's certainly not a good comparison

static int32_t vertResults[2*MAX_KERNEL_DIM+1 + 5200];  // MUST FIX MUST FIX; GROSSLY WASTEFUL AND ALSO PROBABLY SLOWER
static int32_t convResults[PHUSION_MAX_CHANNELS][PHUSION_CPU_CONV_CHUNK];
// TODO - convData/convResults in thread state structure?

// TODO - we don't have a 32-bit integral sample type!
    InitSampler(rdwr, vertResults, 0, Phusion_PlaneFlagI32, Phusion_PlaneRaster1, 0);

//#if PHUSION_SINGLE_PRECISION
//    InitSampler(rdwr, vertResults, 0, Phusion_PlaneFlagSF, Phusion_PlaneRaster1, 0);
//#else
//    InitSampler(rdwr, vertResults, 0, Phusion_PlaneFlagDF, Phusion_PlaneRaster1, 0);
//#endif

    const unsigned p = 0;  // TODO - single plane at the moment

    for (uint32_t y = 0; y < outHeight; y++)
    {
        int32_t aStride, bStride;
        uint32_t aLines, bLines;
        uint32_t x;

const unsigned availLines = inHeight;  // TODO - not yet set up for multi-threading

        uint32_t startLine = SetupVerticalContext(availLines, y, inStride, kh,
                                                  aLines, aStride, bLines, bStride);
        srcState[p].InitLine(in.GetRow(startLine));


        rdwr.InitLine(&vertResults[kernWidth]);
        rdwr.ClearSamples(1 + kernWidth);
        rdwr.StartLine();

        ReadColumns(rdwr, srcState[p], 1 + kernWidth, aLines, aStride, bLines, bStride,
                    pVertKern, bLogSpace);

        // Reflect the vertical sums from the right-hand context, forming the left-hand context
        for (x = 1; x <= kernWidth; x++)
            vertResults[kernWidth - x] = vertResults[kernWidth + x];

        //uint8_t *pDest = (uint8_t*)out.GetRow(y);
        destState[p].StartLine();

        uint32_t nextCol = 1 + kernWidth;  // Next column to be read
    unsigned vIdx = kw-1;   // Index into vertResults

        // Operate in runs of pixels to aid vectorisation
        x = 0;
        while (x + N <= outWidth)
        {
            uint32_t colReqd = x + N + kernWidth;  // Rightmost column required by rightmost pixel, excl.

// TODO - this needs pulling back to keep memory requirements more modest

            // Do we need more columns yet?
            while (nextCol < colReqd)
            {
                if (nextCol < inWidth)
                {
                    // One or more complete columns still to read
                    uint32_t chunkWidth = inWidth - nextCol;
                    if (chunkWidth > N)  // TODO - vary this independently of processing unit
                        chunkWidth = N;

                    srcState[p].InitLine((uint8_t*)in.GetRow(startLine) + nextCol);

                    rdwr.InitLine(&vertResults[kernWidth + nextCol]);
                    rdwr.ClearSamples(chunkWidth);
                    rdwr.StartLine();

                    ReadColumns(rdwr, srcState[p], chunkWidth, aLines, aStride, bLines, bStride,
                                pVertKern, bLogSpace);
                    nextCol += chunkWidth;
                }
                else
                {
                    // Replication of existing vertical interpolation results at image edge
                    vertResults[kernWidth + nextCol] = vertResults[kernWidth + 2 * (inWidth - 1) - nextCol];  // TODO - destination indexing
                    nextCol++;
                }
            }

            FilterHoriz(convResults, pHorizKern, vertResults, x,  // TODO - indexing
                        x + N-1 + kw, kw, N, 1);

            // Unsharp masking convolution filter requires a much stronger weighting for
            //   the central pixel which makes the kernel non-separable
            if (bCentreWeighted)
            {
                // The centre weighted kernel has been computed with a sqrt(multip1) weighting to the
                //    Gaussian filter kernel in each dimension, making the results conv(multip1 * Gaussian convolution);
                //    we now need to form (1 + multip1 * centre sample) - conv(multip1 * Gaussian)... so negate
                //    the current results before summing the 'cWeight'ed central input sample

                for (unsigned pix = 0; pix < N; pix++)
                    convResults[p][pix] = -convResults[p][pix];

                srcState[p].InitLine((uint8_t*)in.GetRow(y) + x);
                rdwr.InitLine(convResults[p]);

                if (bLogSpace)
                    Phusion_Sampler::SumLogI32U8(rdwr, srcState[p], N, &fcWeight);
                else
                    rdwr.SumSamples(srcState[p], N, &fcWeight);
            }

            rdwr.InitLine(convResults[0]);
            if (bLogSpace)
            {
                // Employ direction invocation here because Phusion_Sampler has no direct concept of log-space sample types
                Phusion_Sampler::ConvertU8LogI32(destState[p], rdwr, N);
                destState[p].pData += N * destState[p].skip;
            }
            else
            {
                // Gathering of min/max statistics for subsequent processing
                for (unsigned pix = 0; pix < N; pix++)
                {
                    int32_t s = convResults[p][pix];
                    PHUSION_ASSERT(s >= 0 && s < 0x10000);
                    if (s > partialMax[pix])
                        partialMax[pix] = s;
                    if (s < partialMin[pix])
                        partialMin[pix] = s;
                }
                destState[p].ConvertSamples(rdwr, N);
            }

            x += N;
        }

        PHUSION_ASSERT(kernWidth > N);

        // Tail code
        while (x < outWidth)
        {
            // Do we need another column? Must be replication; all reading done as pixel runs above
            while (nextCol <= x + kernWidth)
            {
                PHUSION_ASSERT(nextCol >= inWidth);

                // Replication of existing vertical interpolation results at image edge
                vertResults[kernWidth + nextCol] = vertResults[kernWidth + 2 * (inWidth - 1) - nextCol];  // TODO - destination indexing
                nextCol++;
            }

            FilterHoriz(convResults, pHorizKern, vertResults, x, x + kw, kw, 1, 1);  // TODO - indexing

            int32_t s = convResults[0][0];

            // Unsharp masking convolution filter requires a much stronger weighting for
            //   the central pixel which makes the kernel non-separable
            if (bCentreWeighted)
            {
                // See explanation about negation and weighting in the above parallelised code
                const uint8_t *sp = (uint8_t*)in.GetRow(y) + (x);
                if (bLogSpace)
                    s = (int32_t)(fcWeight * Phusion_Sampler::LogI32U8(sp[p])) - s;
                else
                    s = (int32_t)(fcWeight * ((float)(1U << 24) / 255.0F) * sp[p]) - s;
            }

            if (bLogSpace)
                destState[p].WriteSampleLogI32(s);
            else
            {
                PHUSION_ASSERT(s >= 0 && s < 0x10000);
                if (s > partialMax[0])
                    partialMax[0] = s;
                if (s < partialMin[0])
                    partialMin[0] = s;

                destState[p].WriteSampleI32(s);
            }

            x++;
        }

        destState[p].AdvanceLine();

        int pc = (y * 100) / outHeight;
        if (prevPC != pc)
        {
            prevPC = pc;
            if (bVerbose)
                printf("%u%%\n", pc);
        }
    }

    if (bLogSpace)
    {
        // We don't return min/max values in this case because they are not required
        min = 0;
        max = 0;
    }
    else
    {
        // Return the overall min/max of the output samples
        min = 0x10000;
        max = 0;
        for (unsigned pix = 0; pix < N; pix++)
        {
            if (partialMin[pix] < min)
                min = partialMin[pix];
            if (partialMax[pix] > max)
                max = partialMax[pix];
        }

        // Writing out the samples we have rounded up
        PHUSION_ASSERT(min >= 0 && min < 0x10000);
        PHUSION_ASSERT(max >= 0 && max < 0x10000);

        min = (min >= 0xFF80) ? 0xFFU : ((min + 0x80) >> 8);
        max = (max >= 0xFF80) ? 0xFFU : ((max + 0x80) >> 8);
    }
}
