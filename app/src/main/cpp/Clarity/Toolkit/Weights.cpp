/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        Weights.cpp                                                   */
/* Description: Generation of spatially-varying weight functions              */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#include <cstdint>

#include "Convolution.hpp"
#include "Utils.hpp"
#include "Weights.hpp"

// Write out a single plane of weights for importing into MATLAB

void WriteWeights(const char *pFilename, const weight_t *pWt, size_t skip, uint32_t numLines, uint32_t pixPerLine)
{
    FILE *fp = fopen(pFilename, "w");
    if (fp)
    {
        while (numLines-- > 0)
        {
            for (unsigned i = 0; i < pixPerLine; i++)
            {
                fprintf(fp, "%.12f%c", pWt[0], (i >= pixPerLine-1) ? '\n' : ',');
                pWt += skip;
            }
        }
        fclose(fp);
    }
}

// Generate 'hw' (nw/2) interleaved weights for each pixel within the given bitmap
//   (since these weights are used in the polynomial application), then
//   their inverses to obtain 'nw', iterate until convergence and then
//   scale to unity as the final stage

void MakeWeights(weight_t *pWfn, weight_t *pWws, const Bitmap &thu, unsigned kernWidth, unsigned kernHeight, unsigned hw)
{
    // Convolve the input grayscale thumbnail with a large averaging filter to obtain a strongly low-pass
    //    filtered representation of the grayscale image, at the same time converting to floating-point
    //    in preparation of weight calculation, and recording the minimum and maximum output values for
    //    subsequent scaling
    //
    // NOTE: we convolve into the mid plane of the weights structure and then run backwards,
    //       overwriting it on the final iteration. We will subsequently convolve the resulting 'hw'
    //       planes with an averaging filter to produce the first 'hw' planes in their rightful location
    //       as planes [0,hw-1].
    //
    //       Also note that weights are interleaved.

    weight_t *pWeights[PHUSION_MAX_POLYNOMIALS/2];  // Initial planes
    const uint32_t pixPerLine = thu.GetWidth();
    const uint32_t numLines = thu.GetHeight();
    const uint32_t numPix = numLines * pixPerLine;
    const unsigned nw = hw << 1;
const bool bVerbose(false);

    printf("MakeWeights %u -> %u weights\n", hw, nw);

    phpoly_t max = (phpoly_t)-1.0;
    phpoly_t min = (phpoly_t)2.0;

    // Plane pointers
    weight_t *pWt = pWws;
    for (unsigned ii = 0; ii < hw; ii++)
    {
        pWeights[ii] = pWt;
        pWt += numPix;
    }

    ConvolveAverageU(pWeights[0], (uint8_t*)thu.GetRow(0), thu.GetStride(), numLines, pixPerLine,
                     kernWidth, kernHeight, max, min);

if (bVerbose)
    printf("max/min from convolution %f %f\n", max, min);

#if 0
{
// Steal the resulting data to check that it's visually sensible
Bitmap bmp(thu.GetWidth(), thu.GetHeight(), 8);
for (uint32_t y = 0; y < thu.GetHeight(); y++)
    for (uint32_t x = 0; x < thu.GetWidth(); x++)
        ((uint8_t*)bmp.GetRow(y))[x] = (uint8_t)(pWws[y * pixPerLine + x] * 255.0);
bmp.Save("avThumb.bmp", FileFormat_Auto);
}
#endif

    // Compute initial values of weights, scaling appropriately for the weight plane in question, and clamping
    profile_start_timing(&profile[PROF_APP_WT_CALCULATE]);

    phpoly_t lo[PHUSION_MAX_POLYNOMIALS/2];
    phpoly_t sc[PHUSION_MAX_POLYNOMIALS/2];
    phpoly_t div = PHPOLY_ONE / hw;
    phpoly_t vr = (phpoly_t)0.4 * div;  // Make sure we have some overlap between weighting functions
                                        //   (helps against large-scale halos)
    for (unsigned ii = 0; ii < hw; ii++)
    {
        phpoly_t hi = (ii + 1) * div + ((ii >= hw - 1) ? PHPOLY_ZERO : vr);
        lo[ii] = ii ? (ii * div - vr) : 0;
        sc[ii] = PHPOLY_ONE / (hi - lo[ii]);
        if (bVerbose)
            printf("%u: %f %f (from %f)\n", ii, lo[ii], sc[ii], hi);
    }

    phpoly_t gain = PHPOLY_ONE / (max - min);
    uint32_t idx = numPix;
    while (idx-- > 0)
    {
        phpoly_t Wt = gain * (pWeights[0][idx] - min);
        assert(pWeights[0][idx] >= min && pWeights[0][idx] <= max);
        assert(Wt >= 0.0 && Wt <= 1.0);

        // See above about running backwards
        for (int ii = (int)hw-1; ii >= 0; ii--)
        {
            phpoly_t w1 = (Wt - lo[ii]) * sc[ii];
            w1 = (w1 >= PHPOLY_ONE) ? PHPOLY_ONE : ((w1 < PHPOLY_ZERO) ? PHPOLY_ZERO: w1);
            assert(w1 >= 0.0 && w1 <= 1.0);
            pWeights[ii][idx] = w1;
        }
    }
    profile_stop_timing(&profile[PROF_APP_WT_CALCULATE]);

    // Convolve the 4 independent planes in the scratch workspace to produce 4 (of 8) interleaved in the final buffer
    const int32_t iStride = pixPerLine * sizeof(phpoly_t);
    for (unsigned ii = 0; ii < hw; ii++)
        ConvolveAverage(pWfn + ii*numPix, 1, pWeights[ii], iStride, numLines, pixPerLine, kernWidth, kernHeight);

    // Produce inverse weight planes from the filtered first half, computing per-plane sums as we go
    phpoly_t sums[PHUSION_MAX_POLYNOMIALS];
    for (unsigned ii = 0; ii < nw; ii++)
        sums[ii] = PHPOLY_ZERO;

    pWt = pWfn;
    idx = numPix;
    while (idx-- > 0)
    {
        for (unsigned ii = 0; ii < hw; ii++)
        {
            sums[ii] += pWt[ii*numPix];

            // Inverse weights plane
            phpoly_t inv = PHPOLY_ONE - pWt[ii*numPix];
            pWt[(hw+ii)*numPix] = inv;
            sums[hw+ii] += inv;
        }
        pWt++;
    }

if (bVerbose)
{
    printf("Plane sums are:\n");
    for (unsigned ii = 0; ii < nw; ii++)
    printf("  %u: %f\n", ii, sums[ii]);
}
    // Per-plane max/min values for the final remapping stage
    double remapMax[PHUSION_MAX_POLYNOMIALS];
    double remapMin[PHUSION_MAX_POLYNOMIALS];

    // Iterate for convergence
    // TODO - investigate how many iterations are actually required for convergence
    profile_start_timing(&profile[PROF_APP_WT_CONVERGE]);
    for (int iter = 0; iter < 10; iter++)
    {
        // Each plane of weights is divided by the sum across that plane
        pWt = pWfn;
        for (unsigned ii = 0; ii < nw; ii++)
        {
            const unsigned N = 32;
            idx = numPix;
            while (idx >= N)
            {
                for (unsigned pix = 0; pix < N; pix++)
                    pWt[pix] /= sums[ii];
                pWt += N;
                idx -= N;
            }
            while (idx-- > 0)
                *pWt++ /= sums[ii];
        }

        for (unsigned ii = 0; ii < nw; ii++)
        {
            sums[ii] = PHPOLY_ZERO;
            remapMax[ii] = -1.0;
            remapMin[ii] =  2.0;
        }

        // Now divide each set of weights by its individual sum,
        //    computing per-plane sums for the next iteration or min/max for
        //    the final remapping stage below
// TODO - separate the above two cases?
        pWt = pWfn;
        idx = numPix;
        while (idx-- > 0)
        {
            double wtSum = PHPOLY_ZERO;

// TODO - this parallelises pretty easily too
            for (unsigned ii = 0; ii < nw; ii++)
                wtSum += pWt[ii * numPix];

            for (unsigned ii = 0; ii < nw; ii++)
            {
                pWt[ii * numPix] /= wtSum;

                sums[ii] += pWt[ii * numPix];
                if (pWt[ii * numPix] < remapMin[ii])
                    remapMin[ii] = pWt[ii * numPix];
                if (pWt[ii * numPix] > remapMax[ii])
                    remapMax[ii] = pWt[ii * numPix];
            }

            pWt++;
        }
    }
    profile_stop_timing(&profile[PROF_APP_WT_CONVERGE]);

    // Compute gains for remapping
    double remapGains[PHUSION_MAX_POLYNOMIALS];
    for (unsigned ii = 0; ii < nw; ii++)
        remapGains[ii] = PHPOLY_ONE / (remapMax[ii] - remapMin[ii]);

    // Remap all weights planes to unity
    pWt = pWfn;
    idx = numPix;
    while (idx-- > 0)
    {
        for (unsigned ii = 0; ii < nw; ii++)
        {
            assert(remapMin[ii] <= pWt[ii * numPix] && pWt[ii * numPix] <= remapMax[ii]);
            pWt[ii * numPix] = remapGains[ii] * (pWt[ii * numPix] - remapMin[ii]);
        }

        pWt++;
    }

    if (false)
        for (unsigned ii = 0; ii < nw; ii++)
        {
            char outFilename[32];
            sprintf(outFilename, "weights%u", ii);
            WriteWeights(outFilename, pWfn + ii*numPix, 1, numLines, pixPerLine);
        }
}
