/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        Utils.hpp                                                     */
/* Description: Utility and support functions                                 */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __Toolkit_Utils_hpp
#define __Toolkit_Utils_hpp
#include <cstdint>
#include "phusion.h"

#ifndef min
#define min(x,y) (((x)<(y))?(x):(y))
#endif
#ifndef max
#define max(x,y) (((x)<(y))?(y):(x))
#endif

#if TOOLKIT_PROFILING

// App-local performance profiling
enum
{
    PROF_APP_CREATELUM = 0,
    PROF_APP_RGB2GRAY,
    PROF_APP_WT_AVERAGE,
    PROF_APP_WT_CONVERGE,
    PROF_APP_WT_CALCULATE,
    PROF_APP_DOWNSAMPLE,
    PROF_APP_TONE_MAPPING,
    PROF_APP_TM_CONVOLUTION,
    PROF_APP_UM_CONVOLUTION,
    PROF_APP_GRADPOLY,
    PROF_APP_LSCONSTRUCT,
    PROF_APP_LSCOMPLETE,
    PROF_APP_SOLVELINSYS,
    PROF_APP_UPSAMPLE,
    PROF_APP_PROCESS_PIXELS,
    PROF_APP_APPLY_LUM,

    PROF_APP_TEMP,

    PROF_COUNT
};

// Profiling information, per thread per operation
typedef struct
{
    uint64_t start_time;
    uint64_t elapsed_time;
} profile_info;

extern profile_info profile[];

// Performance profiling (development)
void profile_init(void);
void profile_report(const char *perfLog);

// Start/stop recording of the time consumed by a particular operation
void profile_start_timing(profile_info *pf);
void profile_stop_timing(profile_info *pf);

#else

#define profile_init()
#define profile_report(x)
#define profile_start_timing(x)
#define profile_stop_timing(x)

#endif

bool ReadMATLABdata(const char *pFilename, Phusion_Buffer *pBuf, Phusion_ImageChannels *pIm,
                    uint32_t *pWidth, uint32_t *pHeight);

#endif
