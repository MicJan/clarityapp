/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        GradientCalcs.cpp                                             */
/* Description: Calculation of gradient space polynomials, and construction   */
/*              and solution of linear system                                 */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#include <cassert>
#include <ctime>

#include "Eigen/Dense"

#include "GradientCalcs.hpp"
#include "Utils.hpp"

#define FORCE_GRAD_INPUTS 0

#define GRADCALC_DEBUG 0

#ifndef NOF_ELEMENTS
#define NOF_ELEMENTS(x) (sizeof(x)/sizeof(*(x)))
#endif

// move these out of GradientCalcs so they don't overflow the stack
phfloat_t TCont[72][72];  // TODO
phfloat_t TregCont[72][72];
phfloat_t fCont[72][PHUSION_MAX_CHANNELS];
phfloat_t fregCont[72][PHUSION_MAX_CHANNELS];
phfloat_t prodRho[PHUSION_MAX_COEFFS*(PHUSION_MAX_COEFFS+1)/2];
phfloat_t prodRhoxy[PHUSION_MAX_COEFFS*(PHUSION_MAX_COEFFS+1)/2];
phfloat_t prodRhoG[PHUSION_MAX_COEFFS*PHUSION_MAX_CHANNELS];
phfloat_t prodRhoGxy[PHUSION_MAX_COEFFS*PHUSION_MAX_CHANNELS];

// Calcualtion of gradient space polynomials, and construction and solution of linear system
void GradientCalcs(Phusion_Polynomial poly[], const Bitmap &smCont, const Bitmap &smGuide, const Bitmap &xtGuide,
                   const Phusion_Buffer &wtBuf, uint32_t numLines, uint32_t pixPerLine,
                   phfloat_t multip2, phfloat_t regLambda, phfloat_t tikLambda, phfloat_t extLambda)
{
    const bool bSaveIntermediates(false);
    const bool bVerbose(false);

#if FORCE_GRAD_INPUTS
    Phusion_ImageChannels smcImg, smgImg, xtgImg; //, wtImg;
    Phusion_Buffer smcBuf, smgBuf, xtgBuf; //, wtBuf;
    uint32_t w, h, gw, gh, wtW, wtH, xtW, xtH;

    // Read small contrast image
    bool bOK = ReadMATLABdata(TEST_DIR "smcont.dat", &smcBuf, &smcImg, &w, &h);
    assert(bOK);

    // Read small guide image
    bOK = ReadMATLABdata(TEST_DIR "smguide.dat", &smgBuf, &smgImg, &gw, &gh);
    assert(bOK);

    // Read a.n.other image
    bOK = ReadMATLABdata(TEST_DIR "xtguide.dat", &xtgBuf, &xtgImg, &xtW, &xtH);
    assert(bOK);

    assert(w == gw && gw == pixPerLine);
    assert(h == gh && gh == numLines);

//    assert(w == gw && gw == wtW);
//    assert(h == gh && gh == wtH);
#endif
//    unsigned inChan = smcBuf.nPlanes;
//    unsigned outChan = smgBuf.nPlanes;

    unsigned inChan = smCont.GetBPP() / 8;
    unsigned outChan = smGuide.GetBPP() / 8;
    const unsigned pchan = (inChan * (inChan + 3)) / 2;
    unsigned nPoly = wtBuf.nPlanes;

    if (bVerbose)
    {
        printf("Gradient calculations:\n");
        printf("Input channels: %u\n", inChan);
        printf("Ouput channels: %u\n", outChan);
        printf("Weight planes: %u\n", nPoly);

        printf("Thumbnail dimensions: %u lines of %u pixels\n", numLines, pixPerLine);
    }



#if GRADCALC_DEBUG
    // POISON
    memset(TCont,    0xff, sizeof(TCont));
    memset(fCont,    0xff, sizeof(fCont));
    memset(TregCont, 0xff, sizeof(TregCont));
    memset(fregCont, 0xff, sizeof(fregCont));
#endif

// TODO - there's no need to initialise all elements since we're only populating 'half' then copying
    for (uint32_t j = 0; j < nPoly * pchan; j++)
    {
        for (uint32_t i = 0; i < nPoly * pchan; i++)
        {
            TCont[j][i]   = PHFLOAT_ZERO;
            TregCont[j][i] = PHFLOAT_ZERO;
        }
        for (uint32_t i = 0; i < outChan; i++)
        {
            fCont[j][i]    = PHFLOAT_ZERO;
            fregCont[j][i] = PHFLOAT_ZERO;
        }
    }

    const phfloat_t adjExt = extLambda - 1.0;

    // clock_t start = clock();

    for (uint32_t j = 0; j < numLines; j++)
    {
        uint32_t nextRow = j + (j < numLines - 1);

        for (uint32_t i = 0; i < pixPerLine; i++)
        {
            uint32_t nextCol = i + (i < pixPerLine - 1);

            phfloat_t g[PHUSION_MAX_CHANNELS];
            phfloat_t gx[PHUSION_MAX_CHANNELS];
            phfloat_t gy[PHUSION_MAX_CHANNELS];
            phfloat_t nextc[PHUSION_MAX_COEFFS];
            phfloat_t nextr[PHUSION_MAX_COEFFS];
            phfloat_t rhox[PHUSION_MAX_COEFFS];
            phfloat_t rhoy[PHUSION_MAX_COEFFS];
            phfloat_t rho[PHUSION_MAX_COEFFS];

            // profile_start_timing(&profile[PROF_APP_GRADPOLY]);

            // Contrast
            for (unsigned ch = 0; ch < inChan; ch++)
            {
#if FORCE_GRAD_INPUTS
                rho[ch]   = ((double*)((uint8_t*)smcBuf.planes[inChan-1-ch].pData + j * smcBuf.planes[inChan-1-ch].iStride))[i];
                nextc[ch] = ((double*)((uint8_t*)smcBuf.planes[inChan-1-ch].pData + j * smcBuf.planes[inChan-1-ch].iStride))[nextCol];
                nextr[ch] = ((double*)((uint8_t*)smcBuf.planes[inChan-1-ch].pData + nextRow * smcBuf.planes[inChan-1-ch].iStride))[i];
#else
                rho[ch]   = ((uint8_t*)smCont.GetRow(j))[i*inChan+ch] / 255.0;
//                ((double*)((uint8_t*)smcBuf.planes[ch].pData + j * smcBuf.planes[ch].iStride))[i];
                nextc[ch] = ((uint8_t*)smCont.GetRow(j))[nextCol*inChan+ch] / 255.0;
//                ((double*)((uint8_t*)smcBuf.planes[ch].pData + j * smcBuf.planes[ch].iStride))[nextCol];
                nextr[ch] = ((uint8_t*)smCont.GetRow(nextRow))[i*inChan+ch] / 255.0;
//                ((double*)((uint8_t*)smcBuf.planes[ch].pData + nextRow * smcBuf.planes[ch].iStride))[i];
#endif
                // Gradient and weighted sum of current
                rhox[ch]  = nextc[ch] + (adjExt * rho[ch]);
                rhoy[ch]  = nextr[ch] + (adjExt * rho[ch]);
            }

//a = b - c
//z = a + 0.25c
//z = b + (0.25 - 1).c

            // Guide
            for (unsigned ch = 0; ch < outChan; ch++)
            {
#if FORCE_GRAD_INPUTS
                phfloat_t eXt = ((double*)((uint8_t*)xtgBuf.planes[ch].pData + j * xtgBuf.planes[ch].iStride))[i]
                              * extLambda;

                g[ch]  = ((double*)((uint8_t*)smgBuf.planes[ch].pData + j * smgBuf.planes[ch].iStride))[i];

                gx[ch] = multip2 * (((double*)((uint8_t*)smgBuf.planes[ch].pData + j * smgBuf.planes[ch].iStride))[nextCol] - g[ch])
                       + eXt;

                gy[ch] = multip2 * (((double*)((uint8_t*)smgBuf.planes[ch].pData + nextRow * smgBuf.planes[ch].iStride))[i] - g[ch])
                       + eXt;
#else
                phfloat_t eXt = (((uint8_t*)xtGuide.GetRow(j))[i*outChan+ch] / 255.0)
                //((double*)((uint8_t*)xtgBuf.planes[ch].pData + j * xtgBuf.planes[ch].iStride))[i]
                              * extLambda;

                g[ch]  = ((uint8_t*)smGuide.GetRow(j))[i*outChan+ch] / 255.0;
                //((double*)((uint8_t*)smgBuf.planes[ch].pData + j * smgBuf.planes[ch].iStride))[i];

                gx[ch] = multip2 * (((uint8_t*)smGuide.GetRow(j))[nextCol*outChan+ch] / 255.0 - g[ch])
                //(((double*)((uint8_t*)smgBuf.planes[ch].pData + j * smgBuf.planes[ch].iStride))[nextCol] - g[ch])
                       + eXt;

                gy[ch] = multip2 * (((uint8_t*)smGuide.GetRow(nextRow))[i*outChan+ch] / 255.0 - g[ch])
                //(((double*)((uint8_t*)smgBuf.planes[ch].pData + nextRow * smgBuf.planes[ch].iStride))[i] - g[ch])
                       + eXt;
#endif
            }

#if PHUSION_INT_WEIGHTS
            // Weights for this pixel
            uint16_t wt[PHUSION_MAX_POLYNOMIALS];
            for (unsigned ii = 0; ii < nPoly; ii++)
                wt[ii] = ((uint16_t*)((uint8_t*)wtBuf.planes[ii].pData + j * wtBuf.planes[ii].iStride))[i];
#else
            // Weights for this pixel
            phfloat_t wt[PHUSION_MAX_POLYNOMIALS];
//printf("Weights: %u,%u\n", j, i);
            for (unsigned ii = 0; ii < nPoly; ii++)
{
                wt[ii] = (phfloat_t)(((double*)((uint8_t*)wtBuf.planes[ii].pData + j * wtBuf.planes[ii].iStride))[i]);
//printf(" %lf", wt[ii]);
}
#endif
//memset(wt, 0, sizeof(wt));
//if (!i && !j) wt[0] = 1.0;

            // profile_stop_timing(&profile[PROF_APP_GRADPOLY]);

            // profile_start_timing(&profile[PROF_APP_LSCONSTRUCT]);

            // TODO - re-express all of the above code with greater regard for memory access patterns and nesting order etc

            unsigned jj = 0, kk = 0;
            for (unsigned ii = inChan; ii < pchan; ii++)
            {
                phfloat_t prod = rho[jj] * rho[kk];
//printf("%u,%u: %lf %lf %lf\n", jj, kk, rho[jj], rho[kk], prod);

                rho[ii]  = prod;
                rhox[ii] = (nextc[jj] * nextc[kk]) + (adjExt * prod);
                rhoy[ii] = (nextr[jj] * nextr[kk]) + (adjExt * prod);
//printf("  Leading to %lf %lf %lf\n",

                if (++kk >= inChan)
                    kk = ++jj;
            }

            unsigned idx = 0;
            for (unsigned ii = 0; ii < pchan; ii++)
                for (unsigned jj = 0; jj <= ii; jj++)
                {
                    assert(idx < NOF_ELEMENTS(prodRho) && idx < NOF_ELEMENTS(prodRhoxy));
                    prodRho[idx]   =  rho[ii] * rho[jj];
                    prodRhoxy[idx] = rhox[ii] * rhox[jj] + rhoy[ii] * rhoy[jj];
                    idx++;
                }

            idx = 0;
            for (unsigned ii = 0; ii < pchan; ii++)
                for (unsigned jj = 0; jj < outChan; jj++)
                {
                    assert(idx < NOF_ELEMENTS(prodRhoG) && idx < NOF_ELEMENTS(prodRhoGxy));
                    //printf("%u,%u,%u : %lf %lf -> %lf\n", ii, jj, idx, rho[ii], g[jj], rho[ii] * g[jj]);
                    prodRhoG[idx]   =  rho[ii] *  g[jj];
                    prodRhoGxy[idx] = rhox[ii] * gx[jj] + rhoy[ii] * gy[jj];
                    idx++;
                }

            // Linear system contribution
            for (unsigned tii = 0; tii < nPoly; tii++)
            {
                for (unsigned tjj = 0; tjj <= tii; tjj++)
                {
#if PHUSION_INT_WEIGHTS
                    const phfloat_t normWt = 1 / (phfloat_t)(65535.0 * 65535.0);
                    phfloat_t wij = (phfloat_t)((uint32_t)wt[tii] * wt[tjj]) * normWt;
#else
                    phfloat_t wij = wt[tii] * wt[tjj];
#endif
                    phfloat_t *pTrC = &TregCont[tii*pchan][tjj*pchan];
                    phfloat_t *pTC  = &TCont[tii*pchan][tjj*pchan];
                    unsigned idx = 0;
                    unsigned ii = 0;
                    unsigned jj = 0;

                    // Sum contributions into lower triangle of sub-block
                    while (ii < pchan)
                    {
                        pTrC[jj] += wij *   prodRho[idx];
                        pTC[jj]  += wij * prodRhoxy[idx];
                        if (++jj > ii)
                        {
                            pTrC += sizeof(TregCont[0]) / sizeof(TregCont[0][0]);
                            pTC  += sizeof(TCont[0]) / sizeof(TCont[0][0]);
                            ii++;
                            jj = 0;
                        }
                        idx++;
                    }
                }
#if PHUSION_INT_WEIGHTS
                const phfloat_t normW = 1 / (phfloat_t)65535.0;
                phfloat_t wii = wt[tii] * normW;
#else
                phfloat_t wii = wt[tii];
#endif
                unsigned idx = 0;
                for (unsigned ii = 0; ii < pchan; ii++)
                    for (unsigned jj = 0; jj < outChan; jj++)
                    {
                        fCont[tii*pchan+ii][jj]    += wii * prodRhoGxy[idx];
                        fregCont[tii*pchan+ii][jj] += wii * prodRhoG[idx];
                        idx++;
                    }
            }

           // profile_stop_timing(&profile[PROF_APP_LSCONSTRUCT]);
        }
//      printf("%u\n", j);
    }

    //profile_start_timing(&profile[PROF_APP_LSCOMPLETE]);

    phfloat_t diagSum = TCont[0][0];
    for (unsigned i = 1; i < nPoly * pchan; i++)
        diagSum += TCont[i][i];

    phfloat_t meanDiag = (diagSum / (pchan * nPoly));  // TODO
    phfloat_t tikMnDiag = tikLambda * meanDiag;

    // Reflect the symmetric system into the upper triangular portion of the matrix too,
    //    calculating the mean of the leading diagonal terms as we go
    for (unsigned tii = 0; tii < nPoly; tii++)
    {
        for (unsigned tjj = 0; tjj <= tii; tjj++)
            for (unsigned ii = 0; ii < pchan; ii++)
                for (unsigned jj = 0; jj <= ii; jj++)
                {
                    bool bDiag = (tii == tjj && ii == jj);  // TODO - partially invariant

                    // Regularisation
                    phfloat_t finalCont = TCont[tii*pchan+ii][tjj*pchan+jj];

                    finalCont += regLambda * TregCont[tii*pchan+ii][tjj*pchan+jj];
                    if (bDiag)
                        finalCont += tikMnDiag;

                    TCont[tii*pchan+ii][tjj*pchan+jj] = finalCont;

                    // Reflections, first within sub-block, and then across global leading diagonal
                    if (ii != jj)
                        TCont[tii*pchan+jj][tjj*pchan+ii] = finalCont;
                    if (tii != tjj)
                    {
                        TCont[tjj*pchan+ii][tii*pchan+jj] = finalCont;
                        TCont[tjj*pchan+jj][tii*pchan+ii] = finalCont;
                    }
                }

        for (unsigned ii = 0; ii < pchan; ii++)
            for (unsigned jj = 0; jj < outChan; jj++)
                fCont[tii*pchan+ii][jj] += regLambda * fregCont[tii*pchan+ii][jj];
    }

    //profile_stop_timing(&profile[PROF_APP_LSCOMPLETE]);

    //clock_t end = clock() - start;
    //if (bVerbose)
        //printf("Constructing system took %lfs\n", (end - start) / (double)CLOCKS_PER_SEC);

    if (bSaveIntermediates)
    {
        FILE *fp = fopen("Ls.txt", "w");
        for (unsigned jj = 0; jj < nPoly * pchan; jj++)
            for (unsigned ii = 0; ii < nPoly * pchan; ii++)
                fprintf(fp, "%8.4lf%s", TCont[jj][ii], (ii >= nPoly * pchan - 1) ? "\n" : ", ");
        fclose(fp);

        fp = fopen("Rs.txt", "w");
        for (unsigned jj = 0; jj < nPoly * pchan; jj++)
            for (unsigned ii = 0; ii < outChan; ii++)
                fprintf(fp, "%8.4lf%s", fCont[jj][ii], (ii >= outChan - 1) ? "\n" : ",");
        fclose(fp);
    }



    // Build Eigen matrices
#if PHUSION_SINGLE_PRECISION
    Eigen::MatrixXf A((int)(pchan*nPoly), (int)(pchan*nPoly));
    Eigen::MatrixXf b((int)(pchan*nPoly), (int)outChan); // 'pchan*nPoly' rows, 'outChan' columns
#else
    Eigen::MatrixXd A((int)(pchan*nPoly), (int)(pchan*nPoly));
    Eigen::MatrixXd b((int)(pchan*nPoly), (int)outChan); // 'pchan*nPoly' rows, 'outChan' columns
#endif

    // Populate the Eigen matrices
    for (unsigned j = 0; j < pchan*nPoly; j++)
    {
        for (unsigned i = 0; i < pchan*nPoly; i++)
            A(j, i) = TCont[j][i];
        for (unsigned ch = 0; ch < outChan; ch++)
            b(j, ch) = fCont[j][ch];
    }

    profile_start_timing(&profile[PROF_APP_SOLVELINSYS]);

    {
        // Solve the matrix equation Ax = B where A and B are known
#if PHUSION_SINGLE_PRECISION
        Eigen::ColPivHouseholderQR<Eigen::MatrixXf> decomp(A);
        Eigen::MatrixXf X = decomp.solve(b);
#else
        Eigen::ColPivHouseholderQR<Eigen::MatrixXd> decomp(A);
        Eigen::MatrixXd X = decomp.solve(b);
#endif

        // Write out the coefficients
        if (bSaveIntermediates) {
            FILE *fp = fopen("coeffs.txt", "w");
            for (unsigned j = 0; j < pchan * nPoly; j++)
                for (unsigned ch = 0; ch < outChan; ch++)
                    fprintf(fp, " %lf%s", X(j, ch), (ch >= outChan - 1) ? "\n" : ", ");
            fclose(fp);
        }

#if GRADCALC_DEBUG
        // POISON
        memset(poly, 0xff, nPoly * sizeof(Phusion_Polynomial));
#endif

        for (unsigned j = 0; j < pchan * nPoly; j++) {
            for (unsigned ch = 0; ch < outChan; ch++) {
                if (bVerbose)
                    printf(" %lf%s", X(j, ch), (ch >= outChan - 1) ? "\n" : ", ");

                poly[j / pchan].coeffs[ch][j % pchan] = X(j, ch);
            }
            poly[j / pchan].nPlanes = outChan;
            poly[j / pchan].nCoeffs = pchan;
        }
    }

    profile_stop_timing(&profile[PROF_APP_SOLVELINSYS]);

}
