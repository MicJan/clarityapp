/******************************************************************************/
/* Project:     Eyeteq                                                        */
/* File:        Bitmap.hpp                                                    */
/* Description: Bitmap image handling                                         */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __Bitmap_hpp
#define __Bitmap_hpp
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstdio>

#ifndef ABS
#define ABS(x) (((x)<0)?-(x):(x))
#endif

// #define BITMAP_SUPPORT_JPEG

// File formats for saved bitmap images
typedef enum
{
    FileFormat_Auto = 0,
    FileFormat_BMP,
    FileFormat_PGM,
    FileFormat_PPM,
    FileFormat_PNG,
    FileFormat_TIFF,
    FileFormat_JPEG
} BitmapFileFormat;

// In-memory bitmap image
//
// NOTE: In order to support both top-down and bottom-up BMP files,
//       we adopt the DIB convention of using -ve height for top-down
//       (raster scan) encoding
class Bitmap
{
public:
    //
    Bitmap() : mWidth(0), mHeight(0), mBPP(0), mData(NULL) {}
    Bitmap(uint32_t width, int32_t height, uint8_t bpp);
    Bitmap(uint32_t width, int32_t height, uint8_t bpp, uint32_t fillColour, bool bAlpha = false);
    ~Bitmap() { Release(); }

    // ------ Creation with optional filling ------
    bool Create(uint32_t width, int32_t height, uint8_t bpp,
                bool bFill = false, uint32_t fillColour = 0U,  // Optional fill to specified colour
                bool bAlpha = false);                          // Alpha-channel support (32-bit bitmaps)
    void Release();

    // ------ File manipulation ------
    bool Load(const char *filename);
    bool Save(const char *filename, BitmapFileFormat fileFmt = FileFormat_Auto) const;

    // ------ File format identification from the filename ------ */
    static BitmapFileFormat FormatFromName(const char *filename);

    static bool SaveRaw(const char *filename,
                        uint32_t width, int32_t height, uint8_t bpp,
                        uint8_t *data);

    // ------ Retrieve image properties ------
    uint32_t GetWidth()  const { return mWidth;  }
    uint32_t GetHeight() const { return (mHeight < 0) ? -mHeight : mHeight; }
    uint8_t  GetBPP()    const { return mBPP;    }
    uint32_t GetStride() const { return mStride; }

    // ------ Image manipulation ------

    // Return a direct pointer to a given scanline within the bitmap image for pixel reading/writing
    //  - rows are numbered from 0 in raster scan order, ie. topmost line at lowest address within buffer
    void    *GetRow(uint32_t row) const
    {
        assert(mData);
        assert(row < (uint32_t)ABS(mHeight));
        return (mData && row < (uint32_t)ABS(mHeight)) ? &mData[row * mStride] : NULL;
    }

    void     SwapRB();

    // Convert the image to a different depth
    bool     ConvertDepth(uint8_t newBPP);

    // ------ Rendering operations ------

    // Rectangular solid colour fill
    void     FillRectangle(uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1, uint32_t colour);

protected:
    // ------ Image properties ------
    uint32_t mWidth;
    int32_t  mHeight;  // Signed for top-down and bottom-up DIB support
    uint8_t  mBPP;
    bool     mbAlpha;
    uint32_t mPalette[0x100];

    // ------ Buffer properties -----
    uint8_t *mData;
    uint32_t mStride;

private:
    // Check whether the given header suggests a valid PGM/PPM file
    size_t  ValidPxM(const uint8_t *hdr, size_t hdr_bytes);

    // Check whether the given header suggests a valid JFIF/ExIF file
    size_t  ValidJPEG(const uint8_t *hdr, size_t hdr_bytes);

    // Check whether the given header suggests a valid PNG file
    size_t  ValidPNG(const uint8_t *hdr, size_t hdr_bytes);

    // Load JPEG from file
    bool    LoadJPEG(FILE *fp);

    // Load PNG from file
    bool    LoadPNG(FILE *fp);

    // Load PGM/PPM image from file
    bool    LoadPxM(FILE *fp, long hdr_len);

    // Skip whitespace and read decimal number from PGM/PPM header
    bool    ReadDecimal(const uint8_t *hdr, size_t hdr_bytes, size_t *pidx, uint32_t *pnum);

    // Skip whitespace, returning updated index
    bool    SkipWhitespace(const uint8_t *hdr, size_t hdr_bytes, size_t *pidx);

    // Saving functions for specific file formats
    bool    SaveBMP(FILE *fp, const char *filename) const;
    bool    SavePxM(FILE *fp, const char *filename, BitmapFileFormat fileFmt) const;
    bool    SaveJPEG(FILE *fp, const char *filename) const;

    // Set up default palette for 8bpp BMPs
    void    SetupPalette();
};

#endif
