/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        Types.hpp                                                     */
/* Description: Internal type definitions                                     */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __Toolkit_Types_hpp
#define __Toolkit_Types_hpp

#define NEW_CONVOLUTION 1

// TODO - ugliness until pertinent code, particularly Convolution has migrated
//        and foreground code may call formal APIs

#if NEW_CONVOLUTION
#include "phusion_cpu.hpp"
#include "phusion_sampler.hpp"
#else
// We use the Phusion Library internal types in this code in anticipation of later migration
#if PHUSION_SINGLE_PRECISION
#define PHPOLY_ZERO  0.0F
#define PHPOLY_ONE   1.0F
typedef float phpoly_t;
#define PHFLOAT_ZERO 0.0F
#define PHFLOAT_ONE  1.0F
typedef float phfloat_t;
#else
#define PHPOLY_ZERO  0.0
#define PHPOLY_ONE   1.0
typedef double phpoly_t;
#define PHFLOAT_ZERO 0.0
#define PHFLOAT_ONE  1.0
typedef double phfloat_t;
#endif
#endif

#endif

