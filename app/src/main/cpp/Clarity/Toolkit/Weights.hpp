/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        Weights.hpp                                                   */
/* Description: Generation of spatially-varying weight functions              */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __Toolkit_Weights_hpp
#define __Toolkit_Weights_hpp
#ifndef __Toolkit_Types_hpp
#include "Types.hpp"
#endif

#define PLANAR_WEIGHTS 1

#if 0 //PHUSION_INT_WEIGHTS
typedef uint16_t weight_t;
#else
typedef phpoly_t weight_t;
#endif

// Generate 'hw' (nw/2) interleaved weights for each pixel within the given bitmap
//   (since these weights are used in the polynomial application), then
//   their inverses to obtain 'nw', iterate until convergence and then
//   scale to unity as the final stage

void MakeWeights(weight_t *pWfn, weight_t *pWws, const Bitmap &thu, unsigned kernWidth, unsigned kernHeight, unsigned hw);

#endif
