/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        Convolution.hpp                                               */
/* Description: Image 2D Convolution Filtering                                */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#ifndef __Toolkit_Convolution_hpp
#define __Toolkit_Convolution_hpp
#include <cstdint>
#ifndef __Bitmap_hpp
#include "Bitmap.hpp"
#endif
#ifndef __Toolkit_Weights_hpp
#include "Weights.hpp"
#endif

#define LOG_SPACE 1

#define MAX_KERNEL_DIM 220  // Maximum dimension of kernel on one side of centre

// Convolution of 8-bit input image data with a large uniformly-weighted kernel
// to produce smoothly-varying output FP image data (= initial weights)
//
// NOTE: kernWidth and kernHeight are the total dimensions of the input kernel

void ConvolveAverageU(weight_t *pDest, uint8_t *pSrc, int32_t srcStride,  // source stride, in bytes
                      uint32_t numLines, uint32_t pixPerLine,
                      unsigned kernWidth, unsigned kernHeight,
                      phpoly_t &max, phpoly_t &min);

// Convolution of FP input weights with a large uniformly-weighted kernel
// to produce smoothly-varying output FP weights
//
// NOTE: kernWidth and kernHeight are the total dimensions of the input kernel

void ConvolveAverage(weight_t *pDest, size_t destSkip,  // destination skip, in elements
                     void *pSrc, int32_t srcStride,     // source stride, in bytes
                     uint32_t numLines, uint32_t pixPerLine,
                     unsigned kernWidth, unsigned kernHeight);

#if NEW_CONVOLUTION
void ConvolveImage(Bitmap &out, const Bitmap &in, const phfloat_t linKern[],
#else
void ConvolveImage(Bitmap &out, const Bitmap &in, const int32_t linKern[],
#endif
                   unsigned kernWidth, unsigned kernHeight,
                   int32_t &max, int32_t &min, int32_t cWeight, bool bLogSpace = false);

#endif
