/******************************************************************************/
/* Project:     Eyeteq                                                        */
/* File:        bitmap.cpp                                                    */
/* Description: Bitmap image handling                                         */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#include <cassert>
#include <cctype>
#include <cstdio>
#include <cstring>

#include "Bitmap.hpp"

#ifdef BITMAP_SUPPORT_JPEG
extern "C" {
#include "jpeglib.h"
};
#endif

#ifndef LOG
#if 0 //ndef NDEBUG
#define LOG printf
#else
#define LOG(...) (void)(1?0:do_nothing(__VA_ARGS__))
static inline int do_nothing(const char *fmt, ...) { return 0; }
#endif
#endif

#ifdef _MSC_VER
#pragma warning(disable:4996)
#endif

#ifndef BI_RGB
#define BI_RGB 0L
#endif
#ifndef BI_BITFIELDS
#define BI_BITFIELDS 3L
#endif

#ifndef LCS_WINDOWS_COLOR_SPACE
#define LCS_WINDOWS_COLOR_SPACE 0x57696E20U
#endif

#ifndef ABS
#define ABS(x) (((x)<0)?-(x):(x))
#endif

// Windows and OS/2 BMP info header, defined here for portability
typedef struct
{
    uint32_t biSize;
    uint32_t biWidth;
    int32_t  biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} BitmapInfoHeader;

// Extended version supports Alpha channel
//   (as well as colour space and tone response curves)
typedef struct
{
    // BitmapInfoHeader and BITFIELDS words
    uint32_t bV4CSType;
    uint32_t bV4ciexyzRed[3];
    uint32_t bV4ciexyzGreen[3];
    uint32_t bV4ciexyzBlue[3];
    uint32_t bV4GammaRed;
    uint32_t bV4GammaGreen;
    uint32_t bV4GammaBlue;
} BitmapV4InfoExtra;

// List of file extensions and corresponding file formats
static const struct
{
    char             ext[5];
    size_t           len;
    BitmapFileFormat fmt;
} fileExts[] =
{
    { "bmp",  3, FileFormat_BMP  },
    { "pgm",  3, FileFormat_PGM  },
    { "ppm",  3, FileFormat_PPM  },
    { "png",  3, FileFormat_PNG  },
    { "jpg",  3, FileFormat_JPEG },
    { "jpeg", 4, FileFormat_JPEG },
    { "tif",  3, FileFormat_TIFF },
    { "tiff", 4, FileFormat_TIFF },
};

// Constructor for a blank bitmap, to be initialised later
Bitmap::Bitmap(uint32_t width, int32_t height, uint8_t bpp)
{
    mData = NULL;
    (void)Create(width, height, bpp, false, 0, false);
}


// Constructor for a filled bitmap
Bitmap::Bitmap(uint32_t width, int32_t height, uint8_t bpp, uint32_t fillColour, bool bAlpha)
{
    mData = NULL;
    (void)Create(width, height, bpp, true, fillColour, bAlpha);
}

// Creation with optional filling
bool Bitmap::Create(uint32_t width, int32_t height, uint8_t bpp,
                    bool bFill, uint32_t fillColour,  // Optional background to specified colour
                    bool bAlpha)                      // Alpha channel support (32-bit bitmaps)
{
    LOG("Bitmap::Create %ux%u,%ubpp\n", width, ABS(height), bpp);

    if (mData)
        Release();

    switch (bpp)
    {
        case 8:
            SetupPalette();
            break;
        case 16: break;
        case 24: break;
        case 32: break;
        case 48: break;
        case 64: break;
        default:
            LOG("ERROR: Invalid BPP of %d requested when creating bitmap\n", bpp);
            return false;
    }

    // Calculate stride, with padding
    uint32_t stride = (((width * bpp) + 31) & ~31) >> 3;
    mData = new uint8_t[ABS(height) * stride];
    LOG("  allocated 0x%x bytes at %p\n", ABS(height) * stride, mData);
    if (!mData)
    {
        LOG("WARNING: Bitmap creation failed, out of memory\n");
        mWidth = mHeight = mBPP = 0;
        mStride = 0;
        return false;
    }

    mWidth  = width;
    mHeight = height;
    mBPP    = bpp;
    mStride = stride;
    mbAlpha = bAlpha;

    if (bFill)
        FillRectangle(0, 0, width, (uint32_t)ABS(height), fillColour);

    return true;
}

// Rectangular solid colour fill
void Bitmap::FillRectangle(uint32_t x0, uint32_t y0, uint32_t x1, uint32_t y1, uint32_t colour)
{
    uint8_t *dRow = mData;
    uint32_t h = y1 - y0;

    assert(x0 <= x1 && x1 <= mWidth);
    assert(y0 <= y1 && y1 <= (uint32_t)ABS(mHeight));

    LOG("Bitmap::FillRectangle %d,%d,%d,%d with %x\n", x0, y0, x1, y1, colour);

    switch (mBPP)
    {
        case 8:
        {
            dRow += (y0 * mStride) + x0;
            colour &= 0xff;
            uint32_t w = x1 - x0;
            if (mStride > w)
            {
                // Cannot use a single memset call
                while (h-- > 0)
                {
                    memset(dRow, colour, w);
                    dRow += mStride;
                }
            }
            else
                memset(dRow, colour, h * mStride);
        }
        break;

        case 16:
            colour = colour & 0xffff;
            colour |= (colour << 16);
            dRow += (y0 * mStride) + (x0 << 1);
            while (h-- > 0)
            {
                uint16_t *dp = (uint16_t*)dRow;
                unsigned w = x1 - x0;

                while (w > 7)
                {
                    ((uint32_t*)dp)[0] = colour;
                    ((uint32_t*)dp)[1] = colour;
                    ((uint32_t*)dp)[2] = colour;
                    ((uint32_t*)dp)[3] = colour;
                    dp += 8;
                    w -= 8;
                }
                while (w-- > 0)
                    *dp++ = (uint16_t)colour;

                dRow += mStride;
            }
            break;

        case 24:
            dRow += (y0 * mStride) + (x0 * 3);
            while (h-- > 0)
            {
                uint8_t *dp = (uint8_t*)dRow;
                uint32_t w = x1 - x0;
#if 1
//uint32_t d0 = (uint32_t)(colour << 24) | colour;
//uint32_t d1 = (uint32_t)(colour

                while (w > 3)
                {
                    ((uint8_t*)dp)[0]  = (uint8_t)colour;
                    ((uint8_t*)dp)[1]  = (uint8_t)(colour >> 8);
                    ((uint8_t*)dp)[2]  = (uint8_t)(colour >> 16);
                    ((uint8_t*)dp)[3]  = (uint8_t)colour;
                    ((uint8_t*)dp)[4]  = (uint8_t)(colour >> 8);
                    ((uint8_t*)dp)[5]  = (uint8_t)(colour >> 16);
                    ((uint8_t*)dp)[6]  = (uint8_t)colour;
                    ((uint8_t*)dp)[7]  = (uint8_t)(colour >> 8);
                    ((uint8_t*)dp)[8]  = (uint8_t)(colour >> 16);
                    ((uint8_t*)dp)[9]  = (uint8_t)colour;
                    ((uint8_t*)dp)[10] = (uint8_t)(colour >> 8);
                    ((uint8_t*)dp)[11] = (uint8_t)(colour >> 16);
                    dp += 12;
                    w -= 4;
                }
                while (w-- > 0)
                {
                    ((uint8_t*)dp)[0]  = (uint8_t)colour;
                    ((uint8_t*)dp)[1]  = (uint8_t)(colour >> 8);
                    ((uint8_t*)dp)[2]  = (uint8_t)(colour >> 16);
                    dp += 3;
                    w--;
                }
#endif
                dRow += mStride;
            }
            break;

        case 32:
            dRow += (y0 * mStride) + (x0 << 2);
            while (h-- > 0)
            {
                uint32_t *dp = (uint32_t*)dRow;
                unsigned w = x1 - x0;

                while (w > 7)
                {
                    dp[3] = dp[2] = dp[1] = dp[0] = colour;
                    dp[7] = dp[6] = dp[5] = dp[4] = colour;
                    dp += 8;
                }
                while (w-- > 0)
                    *dp++ = colour;

                dRow += mStride;
            }
            break;

        default:
            assert(!"FillRectangle not implemented for this BPP");
            break;
    }
}

// Release any resources associated with this bitmap, deleting the image
void Bitmap::Release()
{
    if (mData)
    {
        delete [] mData;
        mData = NULL;
    }
    mWidth = mHeight = mBPP = 0;
}

// Set up default palette for 8bpp BMPs
void Bitmap::SetupPalette()
{
    for (uint32_t c = 0; c <= 0xffffffU; c += 0x010101U)
        mPalette[c >> 16] = c;
}

// File format identification from the filename
BitmapFileFormat Bitmap::FormatFromName(const char *filename)
{
    const char *pExt = strrchr(filename, '.');
    if (pExt)
    {
        size_t len = strlen(++pExt);
        for (unsigned idx = 0; sizeof(fileExts)/sizeof(fileExts[0]); idx++)
        {
            if (len == fileExts[idx].len)
            {
                unsigned i = 0;
                while (toupper(pExt[i]) == toupper(fileExts[idx].ext[i]))
                    if (++i > len)
                        return fileExts[idx].fmt;
            }
        }
    }

    return FileFormat_BMP;
}

// Skip leading whitespace and read decimal number from PPM header
bool Bitmap::ReadDecimal(const uint8_t *hdr, size_t hdr_bytes, size_t *pidx, uint32_t *pnum)
{
    size_t idx = *pidx;

    if (!SkipWhitespace(hdr, hdr_bytes, &idx))
        return false;

    if (isdigit(hdr[idx]))
    {
        unsigned n = 0;
        do
            n = (n * 10) + (hdr[idx++] - '0');
        while (isdigit(hdr[idx]));
        *pidx = idx;
        *pnum = n;

        return true;
    }

    return false;
}

// Skip whitespace, returning updated index
bool Bitmap::SkipWhitespace(const uint8_t *hdr, size_t hdr_bytes, size_t *pidx)
{
    size_t idx = *pidx;

    if (!isspace(hdr[idx]))
        return false;

    do idx++;
        while (isspace(hdr[idx]));

    *pidx = idx;
    return true;
}

// Check whether the given header suggests a valid JFIF/ExIF (JPEG) file
size_t Bitmap::ValidJPEG(const uint8_t *hdr, size_t hdr_bytes)
{
    if (hdr_bytes >= 8 && hdr[0] == 0xFF && hdr[1] == 0xD8
                       && hdr[2] == 0xFF && (hdr[3] & 0xFE) == 0xE0)
    {
        // Found SOI and APP0 (JPFIF) or APP1 (ExIF)
        return 1;
    }

    return 0;
}

// Load JFIF/ExIF image from file
bool Bitmap::LoadJPEG(FILE *fp)
{
#ifdef BITMAP_SUPPORT_JPEG
    struct jpeg_decompress_struct cinfo;
    struct jpeg_error_mgr jerr;

    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_decompress(&cinfo);

    jpeg_stdio_src(&cinfo, fp);

    /* Read file header, set default decompression parameters */
    (void)jpeg_read_header(&cinfo, TRUE);
    (void)jpeg_start_decompress(&cinfo);

    // TODO - other bit depths
    if (!Create(cinfo.output_width, cinfo.output_height, 24))
        return false;

    JSAMPROW rows[3];
    while (cinfo.output_scanline < cinfo.output_height)
    {
// TODO - presumably need only one at present
        rows[0] = (JSAMPLE*)GetRow(cinfo.output_scanline);
        rows[1] = (JSAMPLE*)GetRow(cinfo.output_scanline);
        rows[2] = (JSAMPLE*)GetRow(cinfo.output_scanline);

//printf("Line %u of %u\n", cinfo.output_scanline, cinfo.output_height);
        //unsigned num_scanlines =
        (void)jpeg_read_scanlines(&cinfo, rows, 1);
        //dest_mgr->buffer,
        //dest_mgr->buffer_height);
    }

    (void)jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);

    SwapRB();

    return true;
#else
    LOG("ERROR: JPEG support not included in Bitmap implementation");
    return false;
#endif
}

// Check whether the given header suggests a valid PGM/PPM file
size_t Bitmap::ValidPxM(const uint8_t *hdr, size_t hdr_bytes)
{
    bool bGrayscale = (hdr_bytes >= 2 && hdr[1] == '5');
    uint32_t maxval = 0;
    uint32_t height = 0;
    uint32_t width = 0;
    size_t idx = 2;

    if (hdr_bytes >= 9 &&
        hdr[0] == 'P' && (hdr[1] == '5' || hdr[1] == '6') &&
        ReadDecimal(hdr, hdr_bytes, &idx, &width) &&
        ReadDecimal(hdr, hdr_bytes, &idx, &height) &&
        ReadDecimal(hdr, hdr_bytes, &idx, &maxval) &&
        idx < hdr_bytes && isspace(hdr[idx]))
    {
        // Skip the SINGLE whitespace character; any further 'whitespace' is actually pixel data
        idx++;

        // Validate image parameters, decide upon the pixel format and try to create the pixel buffer
        uint8_t bpp = bGrayscale ? 8 : 24;
        switch (maxval)
        {
            case 0xffff:
                bpp = bGrayscale ? 16 : 48;
                break;

            case 0xff:  // 8/24bpp
            default:
                break;
        }
        if (!width || !height || maxval < 0xff || !Create(width, -(int32_t)height, bpp, false, 0, false))
            return 0;

        // Return the header length
        return idx;
    }
    return 0;
}

// Load PGM/PPM image from file
bool Bitmap::LoadPxM(FILE *fp, long hdr_len)
{
    const unsigned BufferSize = 0x4000;
    static uint8_t dataBuffer[BufferSize];
    size_t validBytes = 0;
    size_t idx = 0;

    // Seek to the end of the valid PPM header
    if (fseek(fp, hdr_len, SEEK_SET))
        return false;

    uint8_t *pDestRow = (uint8_t*)mData;
    unsigned pixBytes = mBPP / 8;

    if (pixBytes == 1)
        SetupPalette();

    // Read each pixel in turn
    uint32_t iy = ABS(mHeight);
    while (iy > 0)
    {
        uint8_t *pDest = pDestRow;
        uint32_t x = 0;
        while (x < mWidth)
        {
            // Re-load additional data if not enough for the remainder of this scanline
            if (validBytes < (mWidth - x) * pixBytes)
            {
                // Image bytes remaining
                size_t toRead = (iy * mWidth - x) * pixBytes - validBytes;
                size_t bytesRead;

                // Shuffle existing data to the start of the buffer
                memmove(dataBuffer, &dataBuffer[idx], validBytes);
                idx = 0;

                if (toRead > BufferSize - validBytes)
                    toRead = BufferSize - validBytes;

                bytesRead = fread(&dataBuffer[validBytes], 1, toRead, fp);
                if (bytesRead < pixBytes)
                {
                    LOG("ERROR: Failed reading pixel data from PPM file\n");
                    return false;
                }
                validBytes += bytesRead;
            }

            switch (pixBytes)
            {
                // RGB PGM, 16bpc
                case 6:  // MSByte first
                    pDest[0] = dataBuffer[idx+1];
                    pDest[1] = dataBuffer[idx];
                    pDest[2] = dataBuffer[idx+3];
                    pDest[3] = dataBuffer[idx+2];
                    pDest[4] = dataBuffer[idx+5];
                    pDest[5] = dataBuffer[idx+4];
                    validBytes -= 6;
                    pDest += 6;
                    idx += 6;
                    break;

                // RGB PPM, 8bpc
                case 3:
                    pDest[0] = dataBuffer[idx++];
                    pDest[1] = dataBuffer[idx++];
                    pDest[2] = dataBuffer[idx++];
                    validBytes -= 3;
                    pDest += 3;
                    break;

                // Grayscale PGM, 16bpc
                case 2:
                    pDest[0] = dataBuffer[idx+1];
                    pDest[1] = dataBuffer[idx];
                    validBytes -= 2;
                    pDest += 2;
                    idx += 2;
                    break;

                // Grayscale PGM, 8bpc
                default:
                    *pDest++ = dataBuffer[idx++];
                    validBytes--;
                    break;
            }
            x++;
        }
        pDestRow += mStride;
        iy--;
    }

    return true;
}

// Load image from file
bool Bitmap::Load(const char *filename)
{
    BitmapInfoHeader info;
    bool bAlpha(false);
    size_t hdr_bytes;
    uint32_t masks[4];
    uint8_t hdr[20];
    long len;
    FILE *fp;

    fp = fopen(filename, "rb");
    if (!fp)
    {
        LOG("ERROR: Failed to load file '%s'\n", filename);
        return false;
    }

    // Read and validate file header, limiting by the file size
    fseek(fp, 0, SEEK_END);
    len = ftell(fp);
    rewind(fp);
    hdr_bytes = sizeof(hdr);
    if (len >= 0 && len < (long)hdr_bytes)
        hdr_bytes = len;
    if (hdr_bytes && hdr_bytes != fread(&hdr, 1, hdr_bytes, fp))
    {
        LOG("ERROR: Failed to read bitmap file header from '%s'\n", filename);
        fclose(fp);
        return false;
    }
    if (hdr_bytes < 14 || hdr[0] != 'B' || hdr[1] != 'M' ||
        (hdr[6] | hdr[7] | hdr[8] | hdr[9]))
    {
        // See whether it looks like a valid JFIF/ExIF file
        size_t hdr_len = ValidJPEG(hdr, hdr_bytes);
        if (hdr_len > 0)
        {
            rewind(fp);
            bool bValid = LoadJPEG(fp);
            if (!bValid)
                Release();
            fclose(fp);
            return bValid;
        }

        // See whether it looks like a valid PGM/PPM file
        hdr_len = ValidPxM(hdr, hdr_bytes);
        if (hdr_len > 0)
        {
            // Should be a valid PPM, attempt to load it
            bool bValid = LoadPxM(fp, hdr_len);
            if (!bValid)
                Release();
            fclose(fp);
            return bValid;
        }
        LOG("ERROR: Invalid BMP file '%s'\n", filename);
        fclose(fp);
        return false;
    }

    // Read bitmap info header
    fseek(fp, 14, SEEK_SET);
    if (sizeof(BitmapInfoHeader) != fread(&info, 1, sizeof(BitmapInfoHeader), fp))
    {
        LOG("ERROR: Failed to read bitmap info header from '%s'\n", filename);
        fclose(fp);
        return false;
    }

    // Read optional BITFIELDs for 16bpp and 32bpp-with-Alpha
    if (info.biCompression == BI_BITFIELDS && (info.biBitCount == 16 || info.biBitCount == 32))
    {
        size_t maskBytes = 4 * (3 + (info.biBitCount == 32));
        if (maskBytes != fread(masks, 1, maskBytes, fp))
        {
            LOG("ERROR: Failed to read BITFIELDs words from '%s'\n", filename);
            fclose(fp);
            return false;
        }

        // We only support fixed-format images; compatible with what we produce, of course
        bool bValid(false);
        switch (info.biBitCount)
        {
            case 16:
                bValid = (masks[0] == 0x001FU && masks[1] == 0x07E0 && masks[2] == 0xF800U);
                break;
            case 32:
                bValid = (masks[0] == 0x00FF0000U && masks[1] == 0x0000FF00U &&
                          masks[2] == 0x000000FFU && masks[3] == 0xFF000000U);
                bAlpha = bValid;
                break;
        }
        if (!bValid)
        {
            LOG("ERROR: BITFIELDs specify an unsupported/invalid pixel format in file '%s'\n", filename);
            fclose(fp);
            return false;
        }
    }
    // Read optional bmiColors into the palette
    else if (info.biBitCount == 8 && info.biCompression == BI_RGB)
    {
        size_t colBytes = 0x400;
        if (info.biClrUsed && info.biClrUsed < 0x100)
        {
            colBytes = info.biClrUsed * 4;
            SetupPalette();  // Ensure defined contents
        }
        if (colBytes != fread(mPalette, 1, colBytes, fp))
        {
            LOG("ERROR: Failed to read color palette within file '%s'\n", filename);
            fclose(fp);
            return false;
        }
    }

    // Align to the first scanline within the file
    long pos = hdr[10] | (hdr[11] << 8) | (hdr[12] << 16) | (hdr[13] << 24);
    if (fseek(fp, pos, SEEK_SET))
    {
        LOG("ERROR: Failed to set position to %lu within file '%s'\n", pos, filename);
        fclose(fp);
        return false;
    }

    // Read and validate image properties
    int32_t  h = info.biHeight;
    uint32_t w = info.biWidth;
    uint32_t bpp = info.biBitCount;
    uint32_t stride = (((w * bpp) + 31) & ~31) >> 3;

    // biSizeImage is allowed to be zero for BI_RGB format
    if (!h || !w || ((info.biSizeImage || info.biCompression != BI_RGB) && info.biSizeImage < ABS(h) * stride))
    {
        LOG("ERROR: Unable to read BMP file '%s'"
            " (Invalid parameters %ux%d,%ubpp, size 0x%x bytes)\n",
            filename, w, h, bpp, info.biSizeImage);
        fclose(fp);
        return false;
    }

    switch (bpp)
    {
        case 8:  break;
        case 16: break;
        case 24: break;
        case 32: break;
        default:
            LOG("ERROR: Unsupported bitmap depth (%d) in file '%s'\n", bpp, filename);
            fclose(fp);
            return false;
    }

    // Create in-memory buffer to receive pixel data
    if (!Create(w, h, bpp, false, 0, bAlpha))
    {
        fclose(fp);
        return false;
    }
    // Image properties have now been set

    // Read the pixel data into memory
    uint32_t sizeImage = info.biSizeImage;
    if (!sizeImage || sizeImage > ABS(h) * mStride)  // Zero size permissible for BI_RGB images
        sizeImage = ABS(h) * mStride;

    if (mHeight > 0)
    {
        // Bottom-up encoding, but we want the pixel data in raster scan order
        uint8_t *dp = mData + mHeight * mStride;
        for(uint32_t y = 0; y < (uint32_t)mHeight; y++)
        {
            dp -= mStride;
            if (mStride != fread(dp, 1, mStride, fp))
            {
                LOG("ERROR: Failed to read pixel scanline from '%s'\n", filename);
                fclose(fp);
                return false;
            }
        }
    }
    else if (sizeImage != fread(mData, 1, sizeImage, fp))
    {
        LOG("ERROR: Failed to read pixel data from '%s'\n", filename);
        fclose(fp);
        return false;
    }

    fclose(fp);
    return true;
}

// Save image to file
bool Bitmap::Save(const char *filename, BitmapFileFormat fileFmt) const
{
    if (fileFmt == FileFormat_Auto)
        fileFmt = FormatFromName(filename);

    FILE *fp = fopen(filename, "wb");
    if (!fp)
    {
        LOG("ERROR: Failed to save bitmap file '%s'\n", filename);
        return false;
    }

    bool bOK(false);
    switch (fileFmt)
    {
        case FileFormat_BMP:
            bOK = SaveBMP(fp, filename);
            break;

        case FileFormat_PGM:
        case FileFormat_PPM:
            bOK = SavePxM(fp, filename, fileFmt);
            break;

        case FileFormat_JPEG:
            bOK = SaveJPEG(fp, filename);
            break;

        default:
            assert(!"Unimplemented/Invalid file format specified to Save function");
            break;
    }

    fclose(fp);
    return bOK;
}

// Save functions for BMP files
bool Bitmap::SaveBMP(FILE *fp, const char *filename) const
{
    static const uint32_t masksa32[4] = { 0x00FF0000U, 0x0000FF00U, 0x000000FFU, 0xFF000000U };
    static const uint32_t  masks16[3] = { 0x1F, 0x7E0, 0xF800 };
    static const uint8_t    zeroes[3] = { 0, 0, 0 };
    const uint32_t *masks = masks16;
    BitmapV4InfoExtra extra;
    BitmapInfoHeader info;
    bool bUseV4(false);
    uint8_t hdr[14];

    // Include BITFIELD masks for 16bpp bitmaps or 32bpp alpha-channel bitmaps
    size_t maskBytes;
    switch (mBPP)
    {
        case 16:
            maskBytes = sizeof(masks16);
            // use of BITFIELD compression type implies presence of BITFIELD words;
            //    we do not need V4 header just for this
            break;
        case 32:
            if (mbAlpha)
            {
                maskBytes = sizeof(masksa32);
                masks = masksa32;
                bUseV4 = true;
                break;
            }
            // no break
        default:
            // stick with the basic info for greatest compatibility
            maskBytes = 0;
            break;
    }

    // Include color space and tone response information (BITMAPV4INFOHEADER)
    size_t extraBytes = 0;
    if (mBPP == 32 && mbAlpha)
    {
        extraBytes = sizeof(extra);
        memset(&extra, 0, sizeof(extra));
        extra.bV4CSType = LCS_WINDOWS_COLOR_SPACE;
    }

    // Include RGBQUADs describing color palette
    size_t colorBytes = 0;
    if (mBPP == 8)
        colorBytes = 0x400;

    // Size of the bitmap image data itself
    uint32_t szData = ABS(mHeight) * mStride;
    // Byte offset of the bitmap image data within the file
    uint32_t bfOffBits = 14 + sizeof(info) + maskBytes + extraBytes + colorBytes;
    // Include padding bytes to DWORD-align the first scanline
    //    (NOTE: It seems to be more common not to do this; either is valid,
    //           but this assists binary comparison of files)
    uint32_t bfPadBytes = 0; //bfOffBits & 3;
    if (bfPadBytes)
    {
        bfPadBytes = 4 - bfPadBytes;
        bfOffBits += bfPadBytes;
    }
    uint32_t bfSize = bfOffBits + szData;

    // Complete bitmap header
    hdr[0] = 'B';
    hdr[1] = 'M';
    hdr[2] = (uint8_t) bfSize;
    hdr[3] = (uint8_t)(bfSize >> 8);
    hdr[4] = (uint8_t)(bfSize >> 16);
    hdr[5] = (uint8_t)(bfSize >> 24);
    hdr[6] = 0;
    hdr[7] = 0;
    hdr[8] = 0;
    hdr[9] = 0;
    hdr[10] = (uint8_t) bfOffBits;
    hdr[11] = (uint8_t)(bfOffBits >> 8);
    hdr[12] = (uint8_t)(bfOffBits >> 16);
    hdr[13] = (uint8_t)(bfOffBits >> 24);

    // Complete bitmap info header
    info.biSize = sizeof(info) + (bUseV4 ? (maskBytes + extraBytes) : 0);
    info.biWidth = mWidth;
    info.biHeight = mHeight;
    info.biPlanes = 1;
    info.biBitCount = mBPP;
    info.biCompression = (mBPP == 16 || (mBPP == 32 && mbAlpha)) ? BI_BITFIELDS : BI_RGB;
    info.biSizeImage = szData;
    info.biXPelsPerMeter = 2835;
    info.biYPelsPerMeter = 2835;
    info.biClrUsed = 0;
    info.biClrImportant = 0;

    if (14 != fwrite(hdr, 1, 14, fp) ||
        sizeof(info) != fwrite(&info, 1, sizeof(info), fp) ||
        (maskBytes  && maskBytes  != fwrite(masks,    1, maskBytes,  fp)) ||
        (extraBytes && extraBytes != fwrite(&extra,   1, extraBytes, fp)) ||
        (colorBytes && colorBytes != fwrite(mPalette, 1, colorBytes, fp)) ||
        (bfPadBytes && bfPadBytes != fwrite(zeroes,   1, bfPadBytes, fp)))
    {
        return false;
    }

    if (mHeight >= 0)
    {
        // Bottom-up encoding, but we've stored our data in raster scan order
        uint32_t h = ABS(mHeight);
        uint8_t *sp = mData + h * mStride;
        for(uint32_t y = 0; y < h; y++)
        {
            sp -= mStride;
            if (mStride != fwrite(sp, 1, mStride, fp))
            {
                LOG("ERROR: Failed to write pixel scanline to '%s'\n", filename);
                return false;
            }
        }
    }
    else if (szData != fwrite(mData, 1, szData, fp))
    {
        LOG("ERROR: Failure writing to file '%s'\n", filename);
        return false;
    }

    return true;
}

// Save function for JFIF files
bool Bitmap::SaveJPEG(FILE *fp, const char *filename) const
{
#ifdef BITMAP_SUPPORT_JPEG
    struct jpeg_compress_struct cinfo;
    struct jpeg_error_mgr jerr;
    uint32_t h = ABS(mHeight);
    uint32_t w = mWidth;

    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_compress(&cinfo);

    cinfo.in_color_space = JCS_RGB;
    cinfo.input_components = 3;
    cinfo.data_precision = 8;
    cinfo.image_width = mWidth;
    cinfo.image_height = h;

    cinfo.X_density = 0;
    cinfo.Y_density = 0;
    cinfo.density_unit = 0;

    jpeg_set_defaults(&cinfo);

    jpeg_default_colorspace(&cinfo);

    jpeg_stdio_dest(&cinfo, fp);

    jpeg_start_compress(&cinfo, TRUE);

    // TODO - fix me!
    static uint8_t lineBuffer[0x8000];

    JSAMPLE *rows[3];
    while (cinfo.next_scanline < h)
    {
        const uint8_t *pSrc = (uint8_t*)GetRow(cinfo.next_scanline);
        for (unsigned x = 0; x < w; x++)
        {
            lineBuffer[x*3+0] = pSrc[x*3+2];
            lineBuffer[x*3+1] = pSrc[x*3+1];
            lineBuffer[x*3+2] = pSrc[x*3+0];
        }
        rows[0] = (JSAMPLE*)lineBuffer;
        (void)jpeg_write_scanlines(&cinfo, rows, 1);
    }

    jpeg_finish_compress(&cinfo);
    jpeg_destroy_compress(&cinfo);

    return true;
#else
    LOG("ERROR: JPEG support not included in Bitmap implementation\n");
    return false;
#endif
}

// Save function for PGM/PPM files
bool Bitmap::SavePxM(FILE *fp, const char *filename, BitmapFileFormat fileFmt) const
{
    bool bGrayscale = (fileFmt == FileFormat_PGM);
    unsigned maxVal = (1U << (bGrayscale ? mBPP : (mBPP/3))) - 1;
    uint32_t h = ABS(mHeight);
    const uint8_t *sp = mData;

    // Description of file format and image properties
    (void)fprintf(fp, "P%c\n%u %u\n%u\n", bGrayscale ? '5' : '6', mWidth, h, maxVal);

    uint32_t widthBytes = (mWidth * mBPP + 7) >> 3;
    while (h-- > 0)
    {
        if (maxVal == 0xff && bGrayscale)
        {
            // TODO - gamma correction still required here strictly speaking
            if (widthBytes != fwrite(sp, 1, widthBytes, fp))
            {
                LOG("ERROR: Failure writing to file '%s'\n", filename);
                return false;
            }
        }
        else
        {
            uint8_t buf[0x2000];
            uint32_t off = 0;

            // TODO - sample values are declared as non-linear in the specification,
            //        although linear is a common variant; sadly no way to indicate which?
            if (maxVal == 0xff)
            {
                const uint8_t *pp = (uint8_t*)sp;
                while (off < widthBytes)
                {
                    unsigned chunkBytes = (unsigned)(widthBytes - off);
                    unsigned idx = 0;

                    if (chunkBytes > sizeof(buf))
                        chunkBytes = sizeof(buf);

                    if (bGrayscale)
                    {
                        while (idx < chunkBytes)
                            buf[idx++] = (uint8_t)*pp++;
                    }
                    else
                    {
                        chunkBytes -= chunkBytes % 3;
                        while (idx < chunkBytes)
                        {
                            buf[idx++] = (uint8_t)pp[2];  // R
                            buf[idx++] = (uint8_t)pp[1];  // G
                            buf[idx++] = (uint8_t)pp[0];  // B
                            pp += 3;
                        }
                    }
                    if (chunkBytes != fwrite(buf, 1, chunkBytes, fp))
                    {
                        LOG("ERROR: Failure writing to file '%s'\n", filename);
                        return false;
                    }
                    off += chunkBytes;
                }
            }
            else
            {
                const uint16_t *pp = (uint16_t*)sp;
                while (off < widthBytes)
                {
                    unsigned chunkBytes = (unsigned)(widthBytes - off);
                    unsigned idx = 0;

                    if (chunkBytes > sizeof(buf))
                        chunkBytes = sizeof(buf);

                    if (bGrayscale)
                    {
                        while (idx < chunkBytes)
                        {
                            buf[idx++] = (*pp >> 8);
                            buf[idx++] = (uint8_t)(*pp++);
                        }
                    }
                    else
                    {
                        chunkBytes -= chunkBytes % 6;
                        while (idx < chunkBytes)
                        {
                            buf[idx++] = (pp[2] >> 8);    // R
                            buf[idx++] = (uint8_t)pp[2];
                            buf[idx++] = (pp[1] >> 8);    // G
                            buf[idx++] = (uint8_t)pp[1];
                            buf[idx++] = (pp[0] >> 8);    // B
                            buf[idx++] = (uint8_t)pp[0];
                            pp += 3;
                        }
                    }
                    if (chunkBytes != fwrite(buf, 1, chunkBytes, fp))
                    {
                        LOG("ERROR: Failure writing to file '%s'\n", filename);
                        return false;
                    }
                    off += chunkBytes;
                }
            }
        }
        sp += mStride;
    }

    return true;
}

// Save raw memory contents to a file in a standard, viewable format
bool Bitmap::SaveRaw(const char *filename, uint32_t width, int32_t height, uint8_t bpp, uint8_t *data)
{
    assert(!"Generation of BMPs from raw memory contents not yet implemented");
    return false;
}

// Exchange the R and B components for each pixel within the bitmap;
//    this, alas, is required for operation with OpenGL-ES on the ST box, it would appear
void Bitmap::SwapRB()
{
    uint8_t  *pRow = mData;
    uint32_t  h = ABS(mHeight);
    while (h-- > 0)
    {
        uint32_t w = mWidth;
        uint8_t *p = pRow;
        switch (mBPP)
        {
            case 24:
                while (w-- > 0)
                {
                    uint8_t tmp = p[0];
                    p[0] = p[2];
                    p[2] = tmp;
                    p += 3;
                }
                break;

            case 32:
                while (w-- > 0)
                {
                    uint8_t tmp = p[0];
                    p[0] = p[2];
                    p[2] = tmp;
                    p += 4;
                }
                break;
        }

        pRow += mStride;
    }
}

// Convert the image to a different depth
bool Bitmap::ConvertDepth(uint8_t newBPP)
{
    uint32_t newStride = (((mWidth * newBPP) + 31) & ~31) >> 3;
    uint32_t newSize = mHeight * newStride;
    const uint8_t *srcPtr = mData;
    uint32_t h = mHeight;
    uint8_t *destPtr;
    uint8_t *newData;
    unsigned destPad;
    unsigned srcPad;

    // Anything to be done?
    if (newBPP == mBPP)
        return true;

    // Allocate new image buffer
    newData = new uint8_t[newSize];
    if (!newData)
        return false;

    destPtr = newData;
    destPad = newStride - ((mWidth * newBPP) >> 3);
    srcPad = ((mWidth * mBPP) >> 3) & 3;
    if (srcPad)
        srcPad = 4 - srcPad;

    // Set up palette if converting to 8bpp
    if (newBPP == 8)
        SetupPalette();

    while (h-- > 0)
    {
        uint32_t w = mWidth;
        while (w-- > 0)
        {
            uint8_t b, g, r;

            // Read source components and scale to 8bpc
            switch (mBPP)
            {
                case 8:
                {
                    uint32_t palColor = mPalette[*srcPtr++];
                    r = (uint8_t)(palColor >> 16);
                    g = (uint8_t)(palColor >>  8);
                    b = (uint8_t) palColor;
                }
                break;

                case 16:
                    r = srcPtr[0] & 0x1f;
                    g = (srcPtr[0] >> 5) | ((srcPtr[1] & 0x7) << 3);
                    b = srcPtr[1] >> 3;
                    // Scale to fill dynamic range
                    r = (r << 3) | (r >> 2);
                    g = (g << 2) | (g >> 4);
                    b = (b << 3) | (b >> 2);
                    srcPtr += 2;
                    break;

                case 24:
                    b = srcPtr[0];
                    g = srcPtr[1];
                    r = srcPtr[2];
                    srcPtr += 3;
                    break;

                case 32:
                    b = srcPtr[0];
                    g = srcPtr[1];
                    r = srcPtr[2];
                    srcPtr += 4;
                    break;

                default:
                    assert(!"Unsupported source bit depth in ConvertDepth");
                    break;
            }

            // Convert from 8bpc and write to destination format
            switch (newBPP)
            {
                case 16:
                    r >>= 3;
                    b >>= 3;
                    g >>= 3;
                    g = (g << 1) | (g >> 4);
                    *(uint16_t*)destPtr = (b << 11) | (g << 5) | r;
                    destPtr += 2;
                    break;

                case 24:
                    destPtr[0] = b;
                    destPtr[1] = g;
                    destPtr[2] = r;
                    destPtr += 3;
                    break;

                case 32:
                    destPtr[0] = b;
                    destPtr[1] = g;
                    destPtr[2] = r;
                    destPtr[3] = 0xff;
                    destPtr += 4;
                    break;

                default:
                    assert(!"Unsupported destination bit depth in ConvertDepth");
                    // no break
                case 8:
                {
                    int32_t y = (4207 * r + 8262 * g + 1604 * b + 0x1000) >> 14;
                    *destPtr++ = (y >= 0x100) ? 0xff : (uint8_t)y;
                }
                break;
            }
        }

        uint32_t p = destPad;
        while (p-- > 0)
            *destPtr++ = 0;  // Not required to be zero, but desirable for comparisons
        srcPtr += srcPad;
    }

    // Discard old image data
    delete [] mData;

    // Update image properties
    mData   = newData;
    mStride = newStride;
    mBPP    = newBPP;

    return true;
}

