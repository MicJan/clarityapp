/******************************************************************************/
/* Project:     Phusion                                                       */
/* File:        Conversion.cpp                                                */
/* Description: Sample format/colour space/transfer function operations       */
/*                                                                            */
/* Copyright (C) Spectral Edge Ltd, 2016. All Rights Reserved.                */
/******************************************************************************/

#include <cmath>

#include "Conversion.hpp"
#include "Utils.hpp"

// Create luminance image from input RGB image
uint16_t RGB2Luminance(Bitmap &lum, const Bitmap &rgb)
{
    const unsigned N = 32;
    uint16_t partialMin[N];
    uint16_t minL = 0x100;

    profile_start_timing(&profile[PROF_APP_CREATELUM]);
    for (unsigned pix = 0; pix < N; pix++)
        partialMin[pix] = 0x100;

    for (uint32_t y = 0; y < lum.GetHeight(); y++)
    {
        const uint8_t *pSrc = (uint8_t*)rgb.GetRow(y);
        uint8_t *pLum = (uint8_t*)lum.GetRow(y);
        const unsigned N = 32;

        uint32_t pixLeft = lum.GetWidth();
        while (pixLeft >= N)
        {
            // TODO - eliminate the divisions in this code?
            for (unsigned pix = 0; pix < N; pix++)
            {
                uint8_t b = pSrc[pix*3 + 0];
                uint8_t g = pSrc[pix*3 + 1];
                uint8_t r = pSrc[pix*3 + 2];

                uint8_t muL  = (r + g + b) / 3;
                uint8_t maxL = max(b, g);
                maxL = max(maxL, r);

                // TODO - check whether we have a problem with range here; at least adjust constants
                uint16_t lum = (maxL * maxL + (255 - maxL) * muL) / 255;
                if (lum > 0xff)
                    lum = 0xff;

                // Track the minimum non-zero 'maxL' value across all pixels
                if (maxL && maxL < partialMin[pix])
                    partialMin[pix] = maxL;

                // luminance value
                pLum[pix] = (uint8_t)lum;
            }

            pSrc += N * 3;
            pLum += N;

            pixLeft -= N;
        }

        while (pixLeft-- > 0)
        {
            uint8_t b = pSrc[0];
            uint8_t g = pSrc[1];
            uint8_t r = pSrc[2];
            pSrc += 3;

            uint8_t muL  = (r + g + b) / 3;
            uint8_t maxL = max(b, g);
            maxL = max(maxL, r);

            // TODO - check whether we have a problem with range here; at least adjust constants
            uint16_t lum = (maxL * maxL + (255 - maxL) * muL) / 255;
            if (lum > 0xff)
                lum = 0xff;

            // Track the minimum non-zero 'maxL' value across all pixels
            if (maxL && maxL < partialMin[0])
                partialMin[0] = maxL;

            // luminance value
            *pLum++ = (uint8_t)lum;
        }
    }

    // Form the global minimum from the partials
    for (unsigned pix = 0; pix < N; pix++)
        if (partialMin[pix] < minL)
            minL = partialMin[pix];

    return minL;
}

// Convert RGB thumbnail to grayscale
void RGB2Gray(Bitmap &dest, const Bitmap &src)
{
    for (uint32_t y = 0; y < dest.GetHeight(); y++)
    {
        const uint8_t *pSrc = (uint8_t*)src.GetRow(y);
        uint8_t *pDest = (uint8_t*)dest.GetRow(y);
        uint32_t w = dest.GetWidth();
        const unsigned N = 8;

        while (w >= N)
        {
            for (unsigned pix = 0; pix < N; pix++)
            {
                uint8_t b = pSrc[pix * 3 + 0];
                uint8_t g = pSrc[pix * 3 + 1];
                uint8_t r = pSrc[pix * 3 + 2];

                // TODO - check that we're using BT.601-7
//                int32_t y = (4207 * r + 8262 * g + 1604 * b + 0x1000) >> 14;
                int32_t y = (int32_t)(r * 0.299 + 0.587 * g + 0.114 * b);
                y = (y < 0) ? 0 : ((y >= 0x100) ? 0xff : (uint8_t)y);

                pDest[pix] = y;
            }

            pSrc  += N * 3;
            pDest += N;

            w -= N;
        }

        while (w-- > 0)
        {
            uint8_t b = pSrc[0];
            uint8_t g = pSrc[1];
            uint8_t r = pSrc[2];
            pSrc += 3;

            // TODO - check that we're using BT.601-7
//            int32_t y = (4207 * r + 8262 * g + 1604 * b + 0x1000) >> 14;
            int32_t y = (int32_t)(r * 0.299 + 0.587 * g + 0.114 * b);
            y = (y < 0) ? 0 : ((y >= 0x100) ? 0xff : (uint8_t)y);

            *pDest++ = y;
        }
    }
}

// Tone mapping transfer function
void ToneMapping(Bitmap &fthu, const Bitmap &lthu, unsigned max, unsigned min, const double strength)
{
    const double normFact = 1 / 255.0;
    static double mmap[0x100];

    profile_start_timing(&profile[PROF_APP_TONE_MAPPING]);

    // Construct tone mapping curve...
    //   NOTE: since we know the min/max inputs, we don't need it to be complete
    assert(min >= 0 && min <= max && max <= 0xff);
    double scale = (2 * strength) / (max - min);
    for (unsigned in = min; in <= max; in++)
    {
        double r = (1.0 - strength) + ((in - min) * scale);  // TODO - obviously wasteful
        mmap[in] = r;
    }

    assert(fthu.GetBPP() == 8);
    assert(lthu.GetBPP() == 8);
    assert(fthu.GetWidth()  == lthu.GetWidth());
    assert(fthu.GetHeight() == lthu.GetHeight());
printf("min and max are %u %u\n", min, max);

    // Then each sample in fthu is run through is remapped to give the exponent
    //    appropriate for that
    for (uint32_t y = 0; y < fthu.GetHeight(); y++)
    {
        uint8_t *pDest = (uint8_t*)fthu.GetRow(y);
        uint8_t *pSrc  = (uint8_t*)lthu.GetRow(y);

        for (uint32_t x = 0; x < fthu.GetWidth(); x++)
        {
if (pDest[x] < min || pDest[x] > max) printf(" FAIL FAIL %u %u %u\n", pDest[x], x, y);
            assert(pDest[x] >= min && pDest[x] <= max);
            // TODO - yum, this is cheap!
            int out = (int)(0.5 + 255.0 * pow(pSrc[x] * normFact, mmap[pDest[x]]));
            assert(out >= 0);
            pDest[x] = (uint8_t)((out >= 0x100) ? 0xFFU : (uint8_t)out);
        }
    }

    profile_stop_timing(&profile[PROF_APP_TONE_MAPPING]);
}

// Apply luminance information to RGB input
void ApplyLuminance(Bitmap &out,
                    const Bitmap &rgb, const Bitmap &lum,  // Input RGB and derived Luminance images
                    const Bitmap &outLum,  // Luminance output from Phusion processing
                    const uint16_t outLUT[0x100],
                    const double alphaLUT[0x400],
                    uint16_t minL)
{
    uint32_t numLines = out.GetHeight();
    for (uint32_t y = 0; y < numLines; y++)
    {
        const uint8_t *pOutLum = (uint8_t*)outLum.GetRow(y);
        const uint8_t *pRGB = (uint8_t*)rgb.GetRow(y);
        const uint8_t *pLum = (uint8_t*)lum.GetRow(y);
        uint8_t *pDest = (uint8_t*)out.GetRow(y);
        const double muFact = 1.0 / (3.0 * 255.0);
        const double fact255 = 1.0 / 255.0;
        const unsigned N = 32;

        uint32_t pixLeft = out.GetWidth();
        while (pixLeft >= N)
        {
            for (unsigned pix = 0; pix < N; pix++)
            {
                unsigned out  = outLUT[pOutLum[pix]];
                double alpha  = alphaLUT[out<<2];
                double oppOut = (alpha * pLum[pix] + (1.0 - alpha) * out) * fact255;

                int b = pRGB[pix * 3 + 0];
                int g = pRGB[pix * 3 + 1];
                int r = pRGB[pix * 3 + 2];

                int maxBG = (b > g) ? b : g;

                double maxL = (minL + (maxBG > r ? maxBG : r)) * fact255;
                double muL  = (r + g + b) * muFact;

                const double THRESH = 1e-12;
                double tmp = 2.0*maxL*(maxL-muL);
                double mult = (tmp > THRESH) ? (-muL + sqrt(muL*muL + 2*tmp*oppOut)) / tmp : 1.0;
                b = (int)(b * mult);
                g = (int)(g * mult);
                r = (int)(r * mult);

                pDest[pix * 3 + 0] = (uint8_t)((b >= 0x100) ? 0xff : ((b < 0) ? 0 : b));
                pDest[pix * 3 + 1] = (uint8_t)((g >= 0x100) ? 0xff : ((g < 0) ? 0 : g));
                pDest[pix * 3 + 2] = (uint8_t)((r >= 0x100) ? 0xff : ((r < 0) ? 0 : r));
            }

            pRGB  += N * 3;
            pDest += N * 3;

            pixLeft -= N;
            pOutLum += N;
            pLum += N;
        }

        while (pixLeft-- > 0)
        {
            unsigned out  = outLUT[pOutLum[0]];
            double alpha  = alphaLUT[out];
            double oppOut = (alpha * pLum[0] + (1.0 - alpha) * out) * fact255;
            pOutLum++;
            pLum++;

            int b = pRGB[0];
            int g = pRGB[1];
            int r = pRGB[2];
            pRGB += 3;

            int maxBG = (b > g) ? b : g;

            double maxL = (minL + (maxBG > r ? maxBG : r)) * fact255;
            double muL  = (r + g + b) * muFact;

            const double THRESH = 1e-12;
            double tmp = 2.0*maxL*(maxL-muL);
            double mult = (tmp > THRESH) ? (-muL + sqrt(muL*muL + 2*tmp*oppOut)) / tmp : 1.0;
            b = (int)(b * mult);
            g = (int)(g * mult);
            r = (int)(r * mult);

            pDest[0] = (uint8_t)((b >= 0x100) ? 0xff : ((b < 0) ? 0 : b));
            pDest[1] = (uint8_t)((g >= 0x100) ? 0xff : ((g < 0) ? 0 : g));
            pDest[2] = (uint8_t)((r >= 0x100) ? 0xff : ((r < 0) ? 0 : r));
            pDest += 3;
        }
    }
}

